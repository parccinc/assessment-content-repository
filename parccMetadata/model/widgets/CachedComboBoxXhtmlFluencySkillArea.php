<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlFluencySkillArea extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetFluencySkillArea';
    
    protected function getUID()
    {
        return 'parcc-fluencyskillarea';
    }
}
