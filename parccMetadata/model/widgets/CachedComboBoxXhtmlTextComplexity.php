<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlTextComplexity extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetTextComplexity';
    
    protected function getUID()
    {
        return 'parcc-textcomplexity';
    }
}
