<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlItemGrade extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetItemGrade';
    
    protected function getUID()
    {
        return 'parcc-itemgrade';
    }
}
