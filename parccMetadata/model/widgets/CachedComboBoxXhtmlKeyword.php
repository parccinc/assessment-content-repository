<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlKeyword extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetKeyword';
    
    protected function getUID()
    {
        return 'parcc-keyword';
    }
}
