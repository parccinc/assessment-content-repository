<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlPassageDesignation extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetPassageDesignation';
    
    protected function getUID()
    {
        return 'parcc-passagedesignation';
    }
}
