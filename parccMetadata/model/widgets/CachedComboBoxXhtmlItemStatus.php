<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlItemStatus extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetItemStatus';
    
    protected function getUID()
    {
        return 'parcc-itemstatus';
    }
}
