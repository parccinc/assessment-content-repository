<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlTestElementType extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetTestElementType';
    
    protected function getUID()
    {
        return 'parcc-testelementtype';
    }
}
