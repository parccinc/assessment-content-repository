<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlGenre extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetGenre';
    
    protected function getUID()
    {
        return 'parcc-genre';
    }
}
