<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlELAContentStandard extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetELAContentStandard';
    
    protected function getUID()
    {
        return 'parcc-elacontentstandard';
    }
}
