<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlItemStrand extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetItemStrand';
    
    protected function getUID()
    {
        return 'parcc-itemstrand';
    }
}
