<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlPassageSource extends CachedComboBoxXhtml
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetPassageSource';
    
    protected function getUID()
    {
        return 'parcc-passagesource';
    }
}
