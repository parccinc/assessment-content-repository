<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlDomain extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetDomain';
    
    protected function getUID()
    {
        return 'parcc-domain';
    }
}
