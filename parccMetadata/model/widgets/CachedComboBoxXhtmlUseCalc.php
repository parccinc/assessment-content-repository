<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlUseCalc extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetUseCalc';
    
    protected function getUID()
    {
        return 'parcc-usecalc';
    }
}
