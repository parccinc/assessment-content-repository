<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlCluster extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetCluster';
    
    protected function getUID()
    {
        return 'parcc-cluster';
    }
}
