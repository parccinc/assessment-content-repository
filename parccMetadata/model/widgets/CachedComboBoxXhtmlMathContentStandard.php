<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlMathContentStandard extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetMathContentStandard';
    
    protected function getUID()
    {
        return 'parcc-mathcontentstandard';
    }
}
