<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlMathEvidenceStatement extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetMathEvidenceStatement';
    
    protected function getUID()
    {
        return 'parcc-mathevidencestatement';
    }
}
