<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlELAEvidenceStatements extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#ELAWidgetEvidenceStatements';
    
    protected function getUID()
    {
        return 'parcc-elaevidencestatements';
    }
}
