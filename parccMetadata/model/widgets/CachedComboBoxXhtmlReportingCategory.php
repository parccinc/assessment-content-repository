<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlReportingCategory extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetReportingCategory';
    
    protected function getUID()
    {
        return 'parcc-reportingcategory';
    }
}
