<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlCalculator extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetCalculator';
    
    protected function getUID()
    {
        return 'parcc-calculator';
    }
}
