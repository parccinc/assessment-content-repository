<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlCognitiveComplexity extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetCognitiveComplexity';
    
    protected function getUID()
    {
        return 'parcc-cognitivecomplexity';
    }
}
