<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlGrade extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetGrade';
    
    protected function getUID()
    {
        return 'parcc-grade';
    }
}
