<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlSubclaim extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetSubclaim';
    
    protected function getUID()
    {
        return 'parcc-subclaim';
    }
}
