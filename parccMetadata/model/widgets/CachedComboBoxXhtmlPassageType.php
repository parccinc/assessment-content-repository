<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlPassageType extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetPassageType';
    
    protected function getUID()
    {
        return 'parcc-passagetype';
    }
}
