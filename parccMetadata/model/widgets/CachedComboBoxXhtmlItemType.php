<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlItemType extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetItemType';
    
    protected function getUID()
    {
        return 'parcc-itemtype';
    }
}
