<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlSubskill extends CachedComboBoxXhtml 
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetSubskill';
    
    protected function getUID()
    {
        return 'parcc-subskill';
    }
}
