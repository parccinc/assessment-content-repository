<?php

namespace oat\parccMetadata\model\widgets;

class CachedComboBoxXhtmlSubject extends CachedComboBoxXhtml
{    
    protected $widget = 'http://www.parcconline.org/parcc-assessment/metadata#WidgetSubject';
    
    protected function getUID()
    {
        return 'parcc-subject';
    }
}
