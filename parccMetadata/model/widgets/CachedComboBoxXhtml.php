<?php

namespace oat\parccMetadata\model\widgets;

abstract class CachedComboBoxXhtml extends \tao_helpers_form_elements_MultipleElement
{
    public function render()
    {
        $returnValue = '';
        $returnValue .= "<label class='form_desc' for='{$this->name}'>"._dh($this->getDescription())."</label>";
        $returnValue .= "<select name='{$this->name}' id='{$this->name}' ";
        $returnValue .= $this->renderAttributes();
        $returnValue .= ">";
        
        $this->options = array_merge(
            array(' ' => ''),
            $this->options
        );
        
        $checkedValue = null;
        
        try {
            $options = \common_cache_FileCache::singleton()->get('parcccomboboxcache-' . $this->getUID());
        } catch (\common_cache_Exception $e) {
            $options = '<taoCachedComboBox>';
            
            foreach($this->options as $optionId => $optionLabel){
                $options .= "<option value=\"{$optionId}\" ";
                $options .= ">"._dh($optionLabel)."</option>";
            }
            
            $options .= '</taoCachedComboBox>';
            \common_cache_FileCache::singleton()->put($options, 'parcccomboboxcache-' . $this->getUID());
        }

        $doc = new \DOMDocument('1.0', 'UTF-8');
        $doc->loadXML($options);
        $docXpath = new \DOMXPath($doc);
        $options = $docXpath->query("//option[@value='" . $this->value . "']");
        
        if ($options->length === 1) {
            $options->item(0)->setAttribute('selected', 'selected');
        }
        
        foreach ($doc->documentElement->childNodes as $optionElt) {
            $returnValue .= $doc->saveXML($optionElt);
        }

        $returnValue .= "</select>";
        
        return str_replace(array('<taoCachedComboBox>', '</taoCachedComboBox>'), '', $returnValue);
    }
    
    abstract protected function getUID();
}
