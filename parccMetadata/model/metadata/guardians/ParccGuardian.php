<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\parccMetadata\model\metadata\guardians;

use oat\taoQtiItem\model\qti\metadata\MetadataGuardian;

/**
 * A PARCC Guardian based on the UIN property.
 * 
 * @author Jérôme Bogaerts <jerome@taotesting.com>
 *
 */
class ParccGuardian implements MetadataGuardian {
    
    public function guard(array $metadataValues) {
        // Load constants.
        \common_ext_ExtensionsManager::singleton()->getExtensionById('parccMetadata')->load();

        $guard = false;
        
        foreach ($metadataValues as $metadataValue) {
            
            $path = $metadataValue->getPath();
            $expectedPath = array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_UIN,
            );
            
            if ($path === $expectedPath) {
                // Check for such a value in database...
                $prop = new \core_kernel_classes_Property(PROPERTY_PARCC_UIN);
                $class = new \core_kernel_classes_Class(TAO_ITEM_CLASS);
                $instances = $class->searchInstances(array($prop->getUri() => $metadataValue->getValue()), array('like' => false, 'recursive' => true));
                
                if (count($instances) > 0) {
                    $guard = reset($instances);
                    break;
                }
            }
        }
        
        return $guard;
    }
}
