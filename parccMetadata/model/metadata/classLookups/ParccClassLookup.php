<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\parccMetadata\model\metadata\classLookups;

use oat\taoQtiItem\model\qti\metadata\MetadataClassLookup;

class ParccClassLookup implements MetadataClassLookup {
    
    public function lookup(array $metadataValues) {
        // Load constants.
        \common_ext_ExtensionsManager::singleton()->getExtensionById('parccMetadata')->load();

        $lookup = false;
        $expectedPath = array(
            PARCC_METADATA_ROOT,
            PROPERTY_PARCC_NATURE
        );
        
        foreach ($metadataValues as $metadataValue) {
            
            $path = $metadataValue->getPath();
            
            if ($path === $expectedPath) {
                $nature = $metadataValue->getValue();
                
                switch ($nature) {
                    case 'ELA_Decoding':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_ELADECODINGBUCKET);
                        break;

                    case 'ELA_Reading_Comp':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_ELAREADINGCOMPBUCKET);
                        break;

                    case 'ELA_Vocab':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_ELAVOCABBUCKET);
                        break;

                    case 'ELA_Fluency':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_ELAFLUENCYBUCKET);
                        break;

                    case 'ELA_Reader_Motivation':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_ELAREADERMOTIVATIONBUCKET);
                        break;

                    case 'Math_Comp':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_MATHCOMPBUCKET);
                        break;

                    case 'Math_Fluency':
                        $lookup = new \core_kernel_classes_Class(CLASS_PARCC_MATHFLUENCYBUCKET);
                        break;
                        
                    case 'RI2015_ELA':
                    case 'RI2015_Math':
                        $expectedPath = array(
                            PARCC_METADATA_ROOT,
                            PROPERTY_PARCC_GRADE
                        );
                        
                        foreach ($metadataValues as $metadataValue) {
                            $path = $metadataValue->getPath();
                            
                            if ($path === $expectedPath) {
                                $grade = $metadataValue->getValue();
                                $lookup = new \core_kernel_classes_Class(constant('CLASS_PARCC_' . strtoupper(str_replace('_', '', $nature)) . $grade . 'BUCKET'));
                                break;
                            }
                        }
                        
                        break;
                }
                
                if ($lookup !== false) {
                    break;
                }
            }
        }
        
        return $lookup;
    }
}
