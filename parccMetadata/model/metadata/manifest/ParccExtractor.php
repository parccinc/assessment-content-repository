<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\parccMetadata\model\metadata\manifest;

use oat\taoQtiItem\model\qti\metadata\imsManifest\ImsManifestMetadataExtractor;

/**
 * A PARCC dedicated metadata extractor implementation.
 *
 * @author Jérôme Bogaerts <jerome@taotesting.com>
 */
class ParccExtractor extends ImsManifestMetadataExtractor
{

    /**
     * @see ImsManifestDataExtractor::extract()
     */
    public function extract($manifest)
    {
        // Load constants.
        \common_ext_ExtensionsManager::singleton()->getExtensionById('parccMetadata')->load();
        
        $values = parent::extract($manifest);
        $newValues = array();
        
        
        
        foreach ($values as $resourceIdentifier => $metadataValueCollection) {
            $i = 0;
            
            foreach ($metadataValueCollection as $metadataValue) {
                $path = $metadataValue->getPath();
        
                if (isset($path[3]) && $path[3] === 'http://ltsc.ieee.org/xsd/apipv1p0/LOM/resource#source') {
                    $newMetadataValue = clone $metadataValue;
                    // The new metadata value is in the next occurence.
                    
                    if (isset($metadataValueCollection[$i + 1])) {

                        $entryPath = $metadataValueCollection[$i + 1]->getPath();
                        if (isset($entryPath[3]) && $entryPath[3] === 'http://ltsc.ieee.org/xsd/apipv1p0/LOM/resource#taxon') {
                            $actualValue = $metadataValueCollection[$i + 1]->getValue();
                            if ($actualValue !== '') {
                                $newMetadataValue->setValue(trim($actualValue));
                                $newMetadataValue->setPath(
                                    array(
                                        PARCC_METADATA_ROOT,
                                        PARCC_METADATA_ONTOLOGY . '#' . $metadataValue->getValue()
                                    )
                                );
                                
                                if (!isset($newValues[$resourceIdentifier])) {
                                    $newValues[$resourceIdentifier] = array();
                                }
                                
                                $newValues[$resourceIdentifier][] = $newMetadataValue;
                            }
                        }
                    }
                }
                
                $i++;
            }
        }

        return $newValues;
    }
}
