<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *
 */

namespace oat\parccMetadata\model\metadata\ontology;

use oat\oatbox\event\Event;
use oat\parccStudentTools\model\CreatorRegistry as CreatorRegistry;
use oat\qtiItemPic\model\CreatorRegistry as StudentToolbarRegistry;
use oat\tao\helpers\Template;
use oat\taoQtiItem\model\qti\Parser;
use oat\tao\model\event\MetadataModified;
use oat\taoQtiItem\model\qti\metadata\ontology\OntologyMetadataInjector;
use \core_kernel_classes_Property;
use oat\taoQtiItem\model\qti\Item;
use oat\taoQtiItem\model\qti\Stylesheet;

class MetadataInjector extends OntologyMetadataInjector
{
    public function __construct()
    {
        parent::__construct();
        
        \common_ext_ExtensionsManager::singleton()->getExtensionById('parccMetadata')->load();
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_ELADECODING,
            INSTANCE_PARCC_ELADECODING
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_ELAFLUENCY,
            INSTANCE_PARCC_ELAFLUENCY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_ELAREADINGCOMP,
            INSTANCE_PARCC_ELAREADINGCOMP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_ELAVOCAB,
            INSTANCE_PARCC_ELAVOCAB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_ELAREADERMOTIVATION,
            INSTANCE_PARCC_ELAREADERMOTIVATION
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_MATHFLUENCY,
            INSTANCE_PARCC_MATHFLUENCY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_NATURE
            ),
            PROPERTY_PARCC_NATURE,
            CODE_PARCC_MATHCOMP,
            INSTANCE_PARCC_MATHCOMP
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_UIN
            ),
            PROPERTY_PARCC_UIN
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_KINDERGARTEN,
            INSTANCE_PARCC_GRADE_KINDERGARTEN
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_FIRSTGRADE,
            INSTANCE_PARCC_GRADE_FIRSTGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_SECONDGRADE,
            INSTANCE_PARCC_GRADE_SECONDGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_THIRDGRADE,
            INSTANCE_PARCC_GRADE_THIRDGRADE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_FOURTHGRADE,
            INSTANCE_PARCC_GRADE_FOURTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_FIFTHGRADE,
            INSTANCE_PARCC_GRADE_FIFTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_SIXTHGRADE,
            INSTANCE_PARCC_GRADE_SIXTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_SEVENTHGRADE,
            INSTANCE_PARCC_GRADE_SEVENTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_EIGHTHGRADE,
            INSTANCE_PARCC_GRADE_EIGHTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_NINTHGRADE,
            INSTANCE_PARCC_GRADE_NINTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_TENTHGRADE,
            INSTANCE_PARCC_GRADE_TENTHGRADE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_ELEVENTHGRADE,
            INSTANCE_PARCC_GRADE_ELEVENTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_GRADE
            ),
            PROPERTY_PARCC_GRADE,
            CODE_PARCC_TWELFTHGRADE,
            INSTANCE_PARCC_GRADE_TWELFTHGRADE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_QUESTIONNO
            ),
            PROPERTY_PARCC_QUESTIONNO
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FORMID
            ),
            PROPERTY_PARCC_FORMID
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FORMNAME
            ),
            PROPERTY_PARCC_FORMNAME
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_EBSR,
            INSTANCE_PARCC_EBSR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_MC,
            INSTANCE_PARCC_MC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_MS,
            INSTANCE_PARCC_MS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_VOICERECORDER,
            INSTANCE_PARCC_VOICERECORDER
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_MATHTYPEI,
            INSTANCE_PARCC_MATHTYPEI
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_MATHTYPEII,
            INSTANCE_PARCC_MATHTYPEII
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_MATHTYPEIII,
            INSTANCE_PARCC_MATHTYPEIII
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_READINGEBSR,
            INSTANCE_PARCC_READINGEBSR
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_READINGTECR,
            INSTANCE_PARCC_READINGTECR
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMTYPE
            ),
            PROPERTY_PARCC_ITEMTYPE,
            CODE_PARCC_ELAPCR,
            INSTANCE_PARCC_ELAPCR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_TESTELEMENTTYPE
            ),
            PROPERTY_PARCC_TESTELEMENTTYPE,
            CODE_PARCC_SCORABLE,
            INSTANCE_PARCC_SCORABLE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTATUS
            ),
            PROPERTY_PARCC_ITEMSTATUS,
            CODE_PARCC_FTREPORT,
            INSTANCE_PARCC_FTREPORT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTATUS
            ),
            PROPERTY_PARCC_ITEMSTATUS,
            CODE_PARCC_SAMPLE,
            INSTANCE_PARCC_SAMPLE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_CVCWORDS,
            INSTANCE_PARCC_CVCWORDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_BLENDSANDDIGRAPHS,
            INSTANCE_PARCC_BLENDSANDDIGRAPHS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_COMPLEXVOWELS,
            INSTANCE_PARCC_COMPLEXVOWELS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_COMPLEXCONSONANTS,
            INSTANCE_PARCC_COMPLEXCONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_WORDRECOGNITIONINFLECTIONALENDINGS,
            INSTANCE_PARCC_WORDRECOGNITIONINFLECTIONALENDINGS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_REPORTINGCATEGORY
            ),
            PROPERTY_PARCC_REPORTINGCATEGORY,
            CODE_PARCC_AFFIXESPREFIXESSUFFIXESSYLLABICATION,
            INSTANCE_PARCC_AFFIXESPREFIXESSUFFIXESSYLLABICATION
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SHORTVOWELS,
            INSTANCE_PARCC_SHORTVOWELS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_INITIALCONSONANTS,
            INSTANCE_PARCC_INITIALCONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_FINALCONSONANTS,
            INSTANCE_PARCC_FINALCONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CVCWITHSHORTA,
            INSTANCE_PARCC_CVCWITHSHORTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CVCWITHSHORTE,
            INSTANCE_PARCC_CVCWITHSHORTE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CVCWITHSHORTI,
            INSTANCE_PARCC_CVCWITHSHORTI
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CVCWITHSHORTO,
            INSTANCE_PARCC_CVCWITHSHORTO
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CVCWITHSHORTU,
            INSTANCE_PARCC_CVCWITHSHORTU
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTDIGRAPHS,
            INSTANCE_PARCC_CONSONANTDIGRAPHS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTSBLENDS,
            INSTANCE_PARCC_CONSONANTSBLENDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTLBLENDS,
            INSTANCE_PARCC_CONSONANTLBLENDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTRBLENDS,
            INSTANCE_PARCC_CONSONANTRBLENDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_FINALCONSONANTDIGRAPHS,
            INSTANCE_PARCC_FINALCONSONANTDIGRAPHS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_FINALCONSONANTBLENDS,
            INSTANCE_PARCC_FINALCONSONANTBLENDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_FINALCONSONANTBLENDSANDCOMBINATIONS,
            INSTANCE_PARCC_FINALCONSONANTBLENDSANDCOMBINATIONS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_FINALCONSONANTCOMBINATIONS,
            INSTANCE_PARCC_FINALCONSONANTCOMBINATIONS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGVOWELSOUNDS,
            INSTANCE_PARCC_LONGVOWELSOUNDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_VOWELRWORDS,
            INSTANCE_PARCC_VOWELRWORDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_VOWELSOUNDSFOROO,
            INSTANCE_PARCC_VOWELSOUNDSFOROO
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_VOWELDIPHTHONGSOUOWANDOIOY,
            INSTANCE_PARCC_VOWELDIPHTHONGSOUOWANDOIOY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_VOWELSOUNDSFORYLONGIANDESOUNDS,
            INSTANCE_PARCC_VOWELSOUNDSFORYLONGIANDESOUNDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_VOWELSOUNDINBALL,
            INSTANCE_PARCC_VOWELSOUNDINBALL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SCHWASOUND,
            INSTANCE_PARCC_SCHWASOUND
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGAVOWELPATTERNS,
            INSTANCE_PARCC_LONGAVOWELPATTERNS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGEVOWELPATTERNS,
            INSTANCE_PARCC_LONGEVOWELPATTERNS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGIVOWELPATTERNS,
            INSTANCE_PARCC_LONGIVOWELPATTERNS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGOVOWELPATTERNS,
            INSTANCE_PARCC_LONGOVOWELPATTERNS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_LONGUVOWELPATTERNS,
            INSTANCE_PARCC_LONGUVOWELPATTERNS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_THREELETTERCOMBINATIONSWITHBLENDS,
            INSTANCE_PARCC_THREELETTERCOMBINATIONSWITHBLENDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_THREELETTERCOMBINATIONSWITHDIGRAPHS,
            INSTANCE_PARCC_THREELETTERCOMBINATIONSWITHDIGRAPHS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTCOMBINATIONSKNWR,
            INSTANCE_PARCC_CONSONANTCOMBINATIONSKNWR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTCOMBINATIONSGHPH,
            INSTANCE_PARCC_CONSONANTCOMBINATIONSGHPH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONSONANTCOMBINATIONSGNMB,
            INSTANCE_PARCC_CONSONANTCOMBINATIONSGNMB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_HARDANDSOFTCSOUND,
            INSTANCE_PARCC_HARDANDSOFTCSOUND
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_HARDANDSOFTGSOUND,
            INSTANCE_PARCC_HARDANDSOFTGSOUND
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_REVIEWCOMPLEXCONSONANTS,
            INSTANCE_PARCC_REVIEWCOMPLEXCONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PLURALSWITHS,
            INSTANCE_PARCC_PLURALSWITHS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PLURALSWITHES,
            INSTANCE_PARCC_PLURALSWITHES
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PLURALSTHATENDWITHOF,
            INSTANCE_PARCC_PLURALSTHATENDWITHOF
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PLURALSFORWORDSENDINGINY,
            INSTANCE_PARCC_PLURALSFORWORDSENDINGINY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_ENDINGSSESANDED,
            INSTANCE_PARCC_ENDINGSSESANDED
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_ENDINGSEDANDING,
            INSTANCE_PARCC_ENDINGSEDANDING
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_ENDINGSWITHY,
            INSTANCE_PARCC_ENDINGSWITHY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_ENDINGSWITHDOUBLEDCONSONANTS,
            INSTANCE_PARCC_ENDINGSWITHDOUBLEDCONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PHONICSIRREGULARVERBS,
            INSTANCE_PARCC_PHONICSIRREGULARVERBS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_CONTRACTIONS,
            INSTANCE_PARCC_CONTRACTIONS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_COMPOUNDWORDS,
            INSTANCE_PARCC_COMPOUNDWORDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_ROOTWORDS,
            INSTANCE_PARCC_ROOTWORDS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PREFIXESUNREPRE,
            INSTANCE_PARCC_PREFIXESUNREPRE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PREFIXESINIMILSUBMIS,
            INSTANCE_PARCC_PREFIXESINIMILSUBMIS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_PREFIXES,
            INSTANCE_PARCC_PREFIXES
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SUFFIXESEREST,
            INSTANCE_PARCC_SUFFIXESEREST
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SUFFIXESFULLESS,
            INSTANCE_PARCC_SUFFIXESFULLESS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SUFFIXESNESSMENT,
            INSTANCE_PARCC_SUFFIXESNESSMENT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_NUMBEROFSYLLABLES,
            INSTANCE_PARCC_NUMBEROFSYLLABLES
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SYLLABICATIONVCCVDIVIDEWORDSWITHLIKEANDUNLIKEDOUBLECONSONANTS,
            INSTANCE_PARCC_SYLLABICATIONVCCVDIVIDEWORDSWITHLIKEANDUNLIKEDOUBLECONSONANTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SYLLABICATIONVCVDIVIDEWORDSWITHSINGLECONSONANTSHORTVOWELSOUND,
            INSTANCE_PARCC_SYLLABICATIONVCVDIVIDEWORDSWITHSINGLECONSONANTSHORTVOWELSOUND
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SYLLABICATIONVCVDIVIDEWORDSWITHSINGLECONSONANTLONGVOWELSOUND,
            INSTANCE_PARCC_SYLLABICATIONVCVDIVIDEWORDSWITHSINGLECONSONANTLONGVOWELSOUND
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBSKILL
            ),
            PROPERTY_PARCC_SUBSKILL,
            CODE_PARCC_SYLLABICATIONACCENTEDSYLLABLES,
            INSTANCE_PARCC_SYLLABICATIONACCENTEDSYLLABLES
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYGENRE
            ),
            PROPERTY_PARCC_ELAFLUENCYGENRE,
            CODE_PARCC_INFORMATIONAL,
            INSTANCE_PARCC_INFORMATIONAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYGENRE
            ),
            PROPERTY_PARCC_ELAFLUENCYGENRE,
            CODE_PARCC_LITERARY,
            INSTANCE_PARCC_LITERARY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPGENRE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPGENRE,
            CODE_PARCC_INFORMATIONAL,
            INSTANCE_PARCC_INFORMATIONAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPGENRE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPGENRE,
            CODE_PARCC_LITERARY,
            INSTANCE_PARCC_LITERARY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABGENRE
            ),
            PROPERTY_PARCC_ELAVOCABGENRE,
            CODE_PARCC_INFORMATIONAL,
            INSTANCE_PARCC_INFORMATIONAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABGENRE
            ),
            PROPERTY_PARCC_ELAVOCABGENRE,
            CODE_PARCC_LITERARY,
            INSTANCE_PARCC_LITERARY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYLEXILE
            ),
            PROPERTY_PARCC_ELAFLUENCYLEXILE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPLEXILE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPLEXILE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABLEXILE
            ),
            PROPERTY_PARCC_ELAVOCABLEXILE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYRMM
            ),
            PROPERTY_PARCC_ELAFLUENCYRMM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPRMM
            ),
            PROPERTY_PARCC_ELAREADINGCOMPRMM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABRMM
            ),
            PROPERTY_PARCC_ELAVOCABRMM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYPASSAGETITLE
            ),
            PROPERTY_PARCC_ELAFLUENCYPASSAGETITLE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGETITLE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGETITLE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGETITLE
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGETITLE
        );


        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYWORDCOUNT
            ),
            PROPERTY_PARCC_ELAFLUENCYWORDCOUNT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPWORDCOUNT
            ),
            PROPERTY_PARCC_ELAREADINGCOMPWORDCOUNT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABWORDCOUNT
            ),
            PROPERTY_PARCC_ELAVOCABWORDCOUNT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY,
            CODE_PARCC_HIGH,
            INSTANCE_PARCC_HIGH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY,
            CODE_PARCC_LOW,
            INSTANCE_PARCC_LOW
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPCOGNITIVECOMPLEXITY,
            CODE_PARCC_MEDIUM,
            INSTANCE_PARCC_MEDIUM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY,
            CODE_PARCC_HIGH,
            INSTANCE_PARCC_HIGH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY,
            CODE_PARCC_LOW,
            INSTANCE_PARCC_LOW
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABCOGNITIVECOMPLEXITY,
            CODE_PARCC_MEDIUM,
            INSTANCE_PARCC_MEDIUM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGENUMBER
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGENUMBER
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGENUMBER
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGENUMBER
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAFLUENCYPASSAGENUMBER
            ),
            PROPERTY_PARCC_ELAFLUENCYPASSAGENUMBER
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L241,
            INSTANCE_PARCC_L241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L242,
            INSTANCE_PARCC_L242
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L243,
            INSTANCE_PARCC_L243
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L244,
            INSTANCE_PARCC_L244
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L251,
            INSTANCE_PARCC_L251
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L252,
            INSTANCE_PARCC_L252
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L261,
            INSTANCE_PARCC_L261
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L341,
            INSTANCE_PARCC_L341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L342,
            INSTANCE_PARCC_L342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L343,
            INSTANCE_PARCC_L343
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L351,
            INSTANCE_PARCC_L351
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L352,
            INSTANCE_PARCC_L352
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L353,
            INSTANCE_PARCC_L353
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L361,
            INSTANCE_PARCC_L361
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L441,
            INSTANCE_PARCC_L441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L442,
            INSTANCE_PARCC_L442
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L451,
            INSTANCE_PARCC_L451
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L452,
            INSTANCE_PARCC_L452
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L453,
            INSTANCE_PARCC_L453
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L461,
            INSTANCE_PARCC_L461
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L541,
            INSTANCE_PARCC_L541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L542,
            INSTANCE_PARCC_L542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L551,
            INSTANCE_PARCC_L551
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L552,
            INSTANCE_PARCC_L552
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L553,
            INSTANCE_PARCC_L553
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L561,
            INSTANCE_PARCC_L561
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L641,
            INSTANCE_PARCC_L641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L642,
            INSTANCE_PARCC_L642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L651,
            INSTANCE_PARCC_L651
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L652,
            INSTANCE_PARCC_L652
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L653,
            INSTANCE_PARCC_L653
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L654,
            INSTANCE_PARCC_L654
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L661,
            INSTANCE_PARCC_L661
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L741,
            INSTANCE_PARCC_L741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L742,
            INSTANCE_PARCC_L742
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L751,
            INSTANCE_PARCC_L751
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L752,
            INSTANCE_PARCC_L752
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L753,
            INSTANCE_PARCC_L753
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L761,
            INSTANCE_PARCC_L761
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L841,
            INSTANCE_PARCC_L841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L842,
            INSTANCE_PARCC_L842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L851,
            INSTANCE_PARCC_L851
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L852,
            INSTANCE_PARCC_L852
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L853,
            INSTANCE_PARCC_L853
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_L861,
            INSTANCE_PARCC_L861
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RH743,
            INSTANCE_PARCC_RH743
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RH843,
            INSTANCE_PARCC_RH843
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RH935,
            INSTANCE_PARCC_RH935
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI111,
            INSTANCE_PARCC_RI111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI121,
            INSTANCE_PARCC_RI121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI122,
            INSTANCE_PARCC_RI122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI131,
            INSTANCE_PARCC_RI131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI132,
            INSTANCE_PARCC_RI132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI133,
            INSTANCE_PARCC_RI133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI211,
            INSTANCE_PARCC_RI211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI221,
            INSTANCE_PARCC_RI221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI222,
            INSTANCE_PARCC_RI222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI232,
            INSTANCE_PARCC_RI232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI233,
            INSTANCE_PARCC_RI233
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI241,
            INSTANCE_PARCC_RI241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI311,
            INSTANCE_PARCC_RI311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI312,
            INSTANCE_PARCC_RI312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI321,
            INSTANCE_PARCC_RI321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI322,
            INSTANCE_PARCC_RI322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI323,
            INSTANCE_PARCC_RI323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI331,
            INSTANCE_PARCC_RI331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI332,
            INSTANCE_PARCC_RI332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI333,
            INSTANCE_PARCC_RI333
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI341,
            INSTANCE_PARCC_RI341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI342,
            INSTANCE_PARCC_RI342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI411,
            INSTANCE_PARCC_RI411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI412,
            INSTANCE_PARCC_RI412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI421,
            INSTANCE_PARCC_RI421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI422,
            INSTANCE_PARCC_RI422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI423,
            INSTANCE_PARCC_RI423
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI431,
            INSTANCE_PARCC_RI431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI432,
            INSTANCE_PARCC_RI432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI433,
            INSTANCE_PARCC_RI433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI441,
            INSTANCE_PARCC_RI441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI511,
            INSTANCE_PARCC_RI511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI512,
            INSTANCE_PARCC_RI512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI521,
            INSTANCE_PARCC_RI521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI522,
            INSTANCE_PARCC_RI522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI523,
            INSTANCE_PARCC_RI523
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI531,
            INSTANCE_PARCC_RI531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI532,
            INSTANCE_PARCC_RI532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI533,
            INSTANCE_PARCC_RI533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI541,
            INSTANCE_PARCC_RI541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI542,
            INSTANCE_PARCC_RI542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI611,
            INSTANCE_PARCC_RI611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI612,
            INSTANCE_PARCC_RI612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI621,
            INSTANCE_PARCC_RI621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI622,
            INSTANCE_PARCC_RI622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI623,
            INSTANCE_PARCC_RI623
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI631,
            INSTANCE_PARCC_RI631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI633,
            INSTANCE_PARCC_RI633
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI641,
            INSTANCE_PARCC_RI641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI711,
            INSTANCE_PARCC_RI711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI712,
            INSTANCE_PARCC_RI712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI721,
            INSTANCE_PARCC_RI721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI722,
            INSTANCE_PARCC_RI722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI731,
            INSTANCE_PARCC_RI731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI741,
            INSTANCE_PARCC_RI741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI811,
            INSTANCE_PARCC_RI811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI812,
            INSTANCE_PARCC_RI812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI821,
            INSTANCE_PARCC_RI821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI822,
            INSTANCE_PARCC_RI822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI823,
            INSTANCE_PARCC_RI823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI831,
            INSTANCE_PARCC_RI831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI832,
            INSTANCE_PARCC_RI832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI833,
            INSTANCE_PARCC_RI833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI841,
            INSTANCE_PARCC_RI841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI911,
            INSTANCE_PARCC_RI911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI912,
            INSTANCE_PARCC_RI912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI921,
            INSTANCE_PARCC_RI921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI922,
            INSTANCE_PARCC_RI922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI931,
            INSTANCE_PARCC_RI931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI932,
            INSTANCE_PARCC_RI932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RI933,
            INSTANCE_PARCC_RI933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL111,
            INSTANCE_PARCC_RL111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL121,
            INSTANCE_PARCC_RL121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL122,
            INSTANCE_PARCC_RL122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL131,
            INSTANCE_PARCC_RL131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL132,
            INSTANCE_PARCC_RL132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL133,
            INSTANCE_PARCC_RL133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL211,
            INSTANCE_PARCC_RL211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL221,
            INSTANCE_PARCC_RL221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL222,
            INSTANCE_PARCC_RL222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL231,
            INSTANCE_PARCC_RL231
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL232,
            INSTANCE_PARCC_RL232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL241,
            INSTANCE_PARCC_RL241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL311,
            INSTANCE_PARCC_RL311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL312,
            INSTANCE_PARCC_RL312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL321,
            INSTANCE_PARCC_RL321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL322,
            INSTANCE_PARCC_RL322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL323,
            INSTANCE_PARCC_RL323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL331,
            INSTANCE_PARCC_RL331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL332,
            INSTANCE_PARCC_RL332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL341,
            INSTANCE_PARCC_RL341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL342,
            INSTANCE_PARCC_RL342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL411,
            INSTANCE_PARCC_RL411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL412,
            INSTANCE_PARCC_RL412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL421,
            INSTANCE_PARCC_RL421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL422,
            INSTANCE_PARCC_RL422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL431,
            INSTANCE_PARCC_RL431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL432,
            INSTANCE_PARCC_RL432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL433,
            INSTANCE_PARCC_RL433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL441,
            INSTANCE_PARCC_RL441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL511,
            INSTANCE_PARCC_RL511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL512,
            INSTANCE_PARCC_RL512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL521,
            INSTANCE_PARCC_RL521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL522,
            INSTANCE_PARCC_RL522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL531,
            INSTANCE_PARCC_RL531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL532,
            INSTANCE_PARCC_RL532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL533,
            INSTANCE_PARCC_RL533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL541,
            INSTANCE_PARCC_RL541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL611,
            INSTANCE_PARCC_RL611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL612,
            INSTANCE_PARCC_RL612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL621,
            INSTANCE_PARCC_RL621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL622,
            INSTANCE_PARCC_RL622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL631,
            INSTANCE_PARCC_RL631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL632,
            INSTANCE_PARCC_RL632
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL641,
            INSTANCE_PARCC_RL641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL711,
            INSTANCE_PARCC_RL711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL712,
            INSTANCE_PARCC_RL712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL721,
            INSTANCE_PARCC_RL721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL722,
            INSTANCE_PARCC_RL722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL723,
            INSTANCE_PARCC_RL723
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL731,
            INSTANCE_PARCC_RL731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL741,
            INSTANCE_PARCC_RL741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL811,
            INSTANCE_PARCC_RL811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL812,
            INSTANCE_PARCC_RL812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL821,
            INSTANCE_PARCC_RL821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL822,
            INSTANCE_PARCC_RL822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL823,
            INSTANCE_PARCC_RL823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL831,
            INSTANCE_PARCC_RL831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL832,
            INSTANCE_PARCC_RL832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL833,
            INSTANCE_PARCC_RL833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL841,
            INSTANCE_PARCC_RL841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL911,
            INSTANCE_PARCC_RL911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL912,
            INSTANCE_PARCC_RL912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL921,
            INSTANCE_PARCC_RL921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL922,
            INSTANCE_PARCC_RL922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL931,
            INSTANCE_PARCC_RL931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL932,
            INSTANCE_PARCC_RL932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RL933,
            INSTANCE_PARCC_RL933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST642,
            INSTANCE_PARCC_RST642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST644,
            INSTANCE_PARCC_RST644
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST713,
            INSTANCE_PARCC_RST713
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST744,
            INSTANCE_PARCC_RST744
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST813,
            INSTANCE_PARCC_RST813
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST842,
            INSTANCE_PARCC_RST842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST844,
            INSTANCE_PARCC_RST844
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST913,
            INSTANCE_PARCC_RST913
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST921,
            INSTANCE_PARCC_RST921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST922,
            INSTANCE_PARCC_RST922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT1,
            CODE_PARCC_RST925,
            INSTANCE_PARCC_RST925
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L241,
            INSTANCE_PARCC_L241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L242,
            INSTANCE_PARCC_L242
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L243,
            INSTANCE_PARCC_L243
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L244,
            INSTANCE_PARCC_L244
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L251,
            INSTANCE_PARCC_L251
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L252,
            INSTANCE_PARCC_L252
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L261,
            INSTANCE_PARCC_L261
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L341,
            INSTANCE_PARCC_L341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L342,
            INSTANCE_PARCC_L342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L343,
            INSTANCE_PARCC_L343
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L351,
            INSTANCE_PARCC_L351
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L352,
            INSTANCE_PARCC_L352
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L353,
            INSTANCE_PARCC_L353
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L361,
            INSTANCE_PARCC_L361
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L441,
            INSTANCE_PARCC_L441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L442,
            INSTANCE_PARCC_L442
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L451,
            INSTANCE_PARCC_L451
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L452,
            INSTANCE_PARCC_L452
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L453,
            INSTANCE_PARCC_L453
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L461,
            INSTANCE_PARCC_L461
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L541,
            INSTANCE_PARCC_L541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L542,
            INSTANCE_PARCC_L542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L551,
            INSTANCE_PARCC_L551
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L552,
            INSTANCE_PARCC_L552
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L553,
            INSTANCE_PARCC_L553
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L561,
            INSTANCE_PARCC_L561
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L641,
            INSTANCE_PARCC_L641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L642,
            INSTANCE_PARCC_L642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L651,
            INSTANCE_PARCC_L651
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L652,
            INSTANCE_PARCC_L652
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L653,
            INSTANCE_PARCC_L653
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L654,
            INSTANCE_PARCC_L654
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L661,
            INSTANCE_PARCC_L661
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L741,
            INSTANCE_PARCC_L741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L742,
            INSTANCE_PARCC_L742
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L751,
            INSTANCE_PARCC_L751
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L752,
            INSTANCE_PARCC_L752
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L753,
            INSTANCE_PARCC_L753
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L761,
            INSTANCE_PARCC_L761
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L841,
            INSTANCE_PARCC_L841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L842,
            INSTANCE_PARCC_L842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L851,
            INSTANCE_PARCC_L851
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L852,
            INSTANCE_PARCC_L852
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L853,
            INSTANCE_PARCC_L853
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_L861,
            INSTANCE_PARCC_L861
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RH743,
            INSTANCE_PARCC_RH743
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RH843,
            INSTANCE_PARCC_RH843
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RH935,
            INSTANCE_PARCC_RH935
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI111,
            INSTANCE_PARCC_RI111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI121,
            INSTANCE_PARCC_RI121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI122,
            INSTANCE_PARCC_RI122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI131,
            INSTANCE_PARCC_RI131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI132,
            INSTANCE_PARCC_RI132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI133,
            INSTANCE_PARCC_RI133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI211,
            INSTANCE_PARCC_RI211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI221,
            INSTANCE_PARCC_RI221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI222,
            INSTANCE_PARCC_RI222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI232,
            INSTANCE_PARCC_RI232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI233,
            INSTANCE_PARCC_RI233
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI241,
            INSTANCE_PARCC_RI241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI311,
            INSTANCE_PARCC_RI311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI312,
            INSTANCE_PARCC_RI312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI321,
            INSTANCE_PARCC_RI321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI322,
            INSTANCE_PARCC_RI322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI323,
            INSTANCE_PARCC_RI323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI331,
            INSTANCE_PARCC_RI331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI332,
            INSTANCE_PARCC_RI332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI333,
            INSTANCE_PARCC_RI333
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI341,
            INSTANCE_PARCC_RI341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI342,
            INSTANCE_PARCC_RI342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI411,
            INSTANCE_PARCC_RI411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI412,
            INSTANCE_PARCC_RI412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI421,
            INSTANCE_PARCC_RI421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI422,
            INSTANCE_PARCC_RI422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI423,
            INSTANCE_PARCC_RI423
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI431,
            INSTANCE_PARCC_RI431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI432,
            INSTANCE_PARCC_RI432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI433,
            INSTANCE_PARCC_RI433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI441,
            INSTANCE_PARCC_RI441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI511,
            INSTANCE_PARCC_RI511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI512,
            INSTANCE_PARCC_RI512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI521,
            INSTANCE_PARCC_RI521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI522,
            INSTANCE_PARCC_RI522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI523,
            INSTANCE_PARCC_RI523
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI531,
            INSTANCE_PARCC_RI531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI532,
            INSTANCE_PARCC_RI532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI533,
            INSTANCE_PARCC_RI533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI541,
            INSTANCE_PARCC_RI541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI542,
            INSTANCE_PARCC_RI542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI611,
            INSTANCE_PARCC_RI611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI612,
            INSTANCE_PARCC_RI612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI621,
            INSTANCE_PARCC_RI621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI622,
            INSTANCE_PARCC_RI622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI623,
            INSTANCE_PARCC_RI623
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI631,
            INSTANCE_PARCC_RI631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI633,
            INSTANCE_PARCC_RI633
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI641,
            INSTANCE_PARCC_RI641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI711,
            INSTANCE_PARCC_RI711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI712,
            INSTANCE_PARCC_RI712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI721,
            INSTANCE_PARCC_RI721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI722,
            INSTANCE_PARCC_RI722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI731,
            INSTANCE_PARCC_RI731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI741,
            INSTANCE_PARCC_RI741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI811,
            INSTANCE_PARCC_RI811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI812,
            INSTANCE_PARCC_RI812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI821,
            INSTANCE_PARCC_RI821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI822,
            INSTANCE_PARCC_RI822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI823,
            INSTANCE_PARCC_RI823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI831,
            INSTANCE_PARCC_RI831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI832,
            INSTANCE_PARCC_RI832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI833,
            INSTANCE_PARCC_RI833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI841,
            INSTANCE_PARCC_RI841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI911,
            INSTANCE_PARCC_RI911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI912,
            INSTANCE_PARCC_RI912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI921,
            INSTANCE_PARCC_RI921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI922,
            INSTANCE_PARCC_RI922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI931,
            INSTANCE_PARCC_RI931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI932,
            INSTANCE_PARCC_RI932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RI933,
            INSTANCE_PARCC_RI933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL111,
            INSTANCE_PARCC_RL111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL121,
            INSTANCE_PARCC_RL121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL122,
            INSTANCE_PARCC_RL122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL131,
            INSTANCE_PARCC_RL131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL132,
            INSTANCE_PARCC_RL132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL133,
            INSTANCE_PARCC_RL133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL211,
            INSTANCE_PARCC_RL211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL221,
            INSTANCE_PARCC_RL221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL222,
            INSTANCE_PARCC_RL222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL231,
            INSTANCE_PARCC_RL231
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL232,
            INSTANCE_PARCC_RL232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL241,
            INSTANCE_PARCC_RL241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL311,
            INSTANCE_PARCC_RL311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL312,
            INSTANCE_PARCC_RL312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL321,
            INSTANCE_PARCC_RL321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL322,
            INSTANCE_PARCC_RL322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL323,
            INSTANCE_PARCC_RL323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL331,
            INSTANCE_PARCC_RL331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL332,
            INSTANCE_PARCC_RL332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL341,
            INSTANCE_PARCC_RL341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL342,
            INSTANCE_PARCC_RL342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL411,
            INSTANCE_PARCC_RL411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL412,
            INSTANCE_PARCC_RL412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL421,
            INSTANCE_PARCC_RL421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL422,
            INSTANCE_PARCC_RL422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL431,
            INSTANCE_PARCC_RL431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL432,
            INSTANCE_PARCC_RL432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL433,
            INSTANCE_PARCC_RL433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL441,
            INSTANCE_PARCC_RL441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL511,
            INSTANCE_PARCC_RL511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL512,
            INSTANCE_PARCC_RL512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL521,
            INSTANCE_PARCC_RL521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL522,
            INSTANCE_PARCC_RL522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL531,
            INSTANCE_PARCC_RL531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL532,
            INSTANCE_PARCC_RL532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL533,
            INSTANCE_PARCC_RL533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL541,
            INSTANCE_PARCC_RL541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL611,
            INSTANCE_PARCC_RL611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL612,
            INSTANCE_PARCC_RL612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL621,
            INSTANCE_PARCC_RL621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL622,
            INSTANCE_PARCC_RL622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL631,
            INSTANCE_PARCC_RL631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL632,
            INSTANCE_PARCC_RL632
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL641,
            INSTANCE_PARCC_RL641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL711,
            INSTANCE_PARCC_RL711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL712,
            INSTANCE_PARCC_RL712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL721,
            INSTANCE_PARCC_RL721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL722,
            INSTANCE_PARCC_RL722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL723,
            INSTANCE_PARCC_RL723
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL731,
            INSTANCE_PARCC_RL731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL741,
            INSTANCE_PARCC_RL741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL811,
            INSTANCE_PARCC_RL811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL812,
            INSTANCE_PARCC_RL812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL821,
            INSTANCE_PARCC_RL821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL822,
            INSTANCE_PARCC_RL822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL823,
            INSTANCE_PARCC_RL823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL831,
            INSTANCE_PARCC_RL831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL832,
            INSTANCE_PARCC_RL832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL833,
            INSTANCE_PARCC_RL833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL841,
            INSTANCE_PARCC_RL841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL911,
            INSTANCE_PARCC_RL911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL912,
            INSTANCE_PARCC_RL912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL921,
            INSTANCE_PARCC_RL921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL922,
            INSTANCE_PARCC_RL922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL931,
            INSTANCE_PARCC_RL931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL932,
            INSTANCE_PARCC_RL932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RL933,
            INSTANCE_PARCC_RL933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST642,
            INSTANCE_PARCC_RST642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST644,
            INSTANCE_PARCC_RST644
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST713,
            INSTANCE_PARCC_RST713
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST744,
            INSTANCE_PARCC_RST744
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST813,
            INSTANCE_PARCC_RST813
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST842,
            INSTANCE_PARCC_RST842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST844,
            INSTANCE_PARCC_RST844
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST913,
            INSTANCE_PARCC_RST913
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST921,
            INSTANCE_PARCC_RST921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST922,
            INSTANCE_PARCC_RST922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT1,
            CODE_PARCC_RST925,
            INSTANCE_PARCC_RST925
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L241,
            INSTANCE_PARCC_L241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L242,
            INSTANCE_PARCC_L242
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L243,
            INSTANCE_PARCC_L243
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L244,
            INSTANCE_PARCC_L244
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L251,
            INSTANCE_PARCC_L251
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L252,
            INSTANCE_PARCC_L252
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L261,
            INSTANCE_PARCC_L261
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L341,
            INSTANCE_PARCC_L341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L342,
            INSTANCE_PARCC_L342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L343,
            INSTANCE_PARCC_L343
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L351,
            INSTANCE_PARCC_L351
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L352,
            INSTANCE_PARCC_L352
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L353,
            INSTANCE_PARCC_L353
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L361,
            INSTANCE_PARCC_L361
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L441,
            INSTANCE_PARCC_L441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L442,
            INSTANCE_PARCC_L442
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L451,
            INSTANCE_PARCC_L451
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L452,
            INSTANCE_PARCC_L452
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L453,
            INSTANCE_PARCC_L453
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L461,
            INSTANCE_PARCC_L461
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L541,
            INSTANCE_PARCC_L541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L542,
            INSTANCE_PARCC_L542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L551,
            INSTANCE_PARCC_L551
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L552,
            INSTANCE_PARCC_L552
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L553,
            INSTANCE_PARCC_L553
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L561,
            INSTANCE_PARCC_L561
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L641,
            INSTANCE_PARCC_L641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L642,
            INSTANCE_PARCC_L642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L651,
            INSTANCE_PARCC_L651
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L652,
            INSTANCE_PARCC_L652
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L653,
            INSTANCE_PARCC_L653
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L654,
            INSTANCE_PARCC_L654
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L661,
            INSTANCE_PARCC_L661
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L741,
            INSTANCE_PARCC_L741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L742,
            INSTANCE_PARCC_L742
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L751,
            INSTANCE_PARCC_L751
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L752,
            INSTANCE_PARCC_L752
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L753,
            INSTANCE_PARCC_L753
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L761,
            INSTANCE_PARCC_L761
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L841,
            INSTANCE_PARCC_L841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L842,
            INSTANCE_PARCC_L842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L851,
            INSTANCE_PARCC_L851
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L852,
            INSTANCE_PARCC_L852
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L853,
            INSTANCE_PARCC_L853
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_L861,
            INSTANCE_PARCC_L861
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RH743,
            INSTANCE_PARCC_RH743
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RH843,
            INSTANCE_PARCC_RH843
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RH935,
            INSTANCE_PARCC_RH935
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI111,
            INSTANCE_PARCC_RI111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI121,
            INSTANCE_PARCC_RI121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI122,
            INSTANCE_PARCC_RI122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI131,
            INSTANCE_PARCC_RI131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI132,
            INSTANCE_PARCC_RI132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI133,
            INSTANCE_PARCC_RI133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI211,
            INSTANCE_PARCC_RI211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI221,
            INSTANCE_PARCC_RI221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI222,
            INSTANCE_PARCC_RI222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI232,
            INSTANCE_PARCC_RI232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI233,
            INSTANCE_PARCC_RI233
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI241,
            INSTANCE_PARCC_RI241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI311,
            INSTANCE_PARCC_RI311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI312,
            INSTANCE_PARCC_RI312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI321,
            INSTANCE_PARCC_RI321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI322,
            INSTANCE_PARCC_RI322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI323,
            INSTANCE_PARCC_RI323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI331,
            INSTANCE_PARCC_RI331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI332,
            INSTANCE_PARCC_RI332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI333,
            INSTANCE_PARCC_RI333
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI341,
            INSTANCE_PARCC_RI341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI342,
            INSTANCE_PARCC_RI342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI411,
            INSTANCE_PARCC_RI411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI412,
            INSTANCE_PARCC_RI412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI421,
            INSTANCE_PARCC_RI421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI422,
            INSTANCE_PARCC_RI422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI423,
            INSTANCE_PARCC_RI423
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI431,
            INSTANCE_PARCC_RI431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI432,
            INSTANCE_PARCC_RI432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI433,
            INSTANCE_PARCC_RI433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI441,
            INSTANCE_PARCC_RI441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI511,
            INSTANCE_PARCC_RI511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI512,
            INSTANCE_PARCC_RI512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI521,
            INSTANCE_PARCC_RI521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI522,
            INSTANCE_PARCC_RI522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI523,
            INSTANCE_PARCC_RI523
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI531,
            INSTANCE_PARCC_RI531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI532,
            INSTANCE_PARCC_RI532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI533,
            INSTANCE_PARCC_RI533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI541,
            INSTANCE_PARCC_RI541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI542,
            INSTANCE_PARCC_RI542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI611,
            INSTANCE_PARCC_RI611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI612,
            INSTANCE_PARCC_RI612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI621,
            INSTANCE_PARCC_RI621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI622,
            INSTANCE_PARCC_RI622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI623,
            INSTANCE_PARCC_RI623
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI631,
            INSTANCE_PARCC_RI631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI633,
            INSTANCE_PARCC_RI633
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI641,
            INSTANCE_PARCC_RI641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI711,
            INSTANCE_PARCC_RI711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI712,
            INSTANCE_PARCC_RI712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI721,
            INSTANCE_PARCC_RI721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI722,
            INSTANCE_PARCC_RI722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI731,
            INSTANCE_PARCC_RI731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI741,
            INSTANCE_PARCC_RI741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI811,
            INSTANCE_PARCC_RI811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI812,
            INSTANCE_PARCC_RI812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI821,
            INSTANCE_PARCC_RI821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI822,
            INSTANCE_PARCC_RI822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI823,
            INSTANCE_PARCC_RI823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI831,
            INSTANCE_PARCC_RI831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI832,
            INSTANCE_PARCC_RI832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI833,
            INSTANCE_PARCC_RI833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI841,
            INSTANCE_PARCC_RI841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI911,
            INSTANCE_PARCC_RI911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI912,
            INSTANCE_PARCC_RI912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI921,
            INSTANCE_PARCC_RI921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI922,
            INSTANCE_PARCC_RI922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI931,
            INSTANCE_PARCC_RI931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI932,
            INSTANCE_PARCC_RI932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RI933,
            INSTANCE_PARCC_RI933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL111,
            INSTANCE_PARCC_RL111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL121,
            INSTANCE_PARCC_RL121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL122,
            INSTANCE_PARCC_RL122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL131,
            INSTANCE_PARCC_RL131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL132,
            INSTANCE_PARCC_RL132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL133,
            INSTANCE_PARCC_RL133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL211,
            INSTANCE_PARCC_RL211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL221,
            INSTANCE_PARCC_RL221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL222,
            INSTANCE_PARCC_RL222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL231,
            INSTANCE_PARCC_RL231
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL232,
            INSTANCE_PARCC_RL232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL241,
            INSTANCE_PARCC_RL241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL311,
            INSTANCE_PARCC_RL311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL312,
            INSTANCE_PARCC_RL312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL321,
            INSTANCE_PARCC_RL321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL322,
            INSTANCE_PARCC_RL322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL323,
            INSTANCE_PARCC_RL323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL331,
            INSTANCE_PARCC_RL331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL332,
            INSTANCE_PARCC_RL332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL341,
            INSTANCE_PARCC_RL341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL342,
            INSTANCE_PARCC_RL342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL411,
            INSTANCE_PARCC_RL411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL412,
            INSTANCE_PARCC_RL412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL421,
            INSTANCE_PARCC_RL421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL422,
            INSTANCE_PARCC_RL422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL431,
            INSTANCE_PARCC_RL431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL432,
            INSTANCE_PARCC_RL432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL433,
            INSTANCE_PARCC_RL433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL441,
            INSTANCE_PARCC_RL441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL511,
            INSTANCE_PARCC_RL511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL512,
            INSTANCE_PARCC_RL512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL521,
            INSTANCE_PARCC_RL521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL522,
            INSTANCE_PARCC_RL522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL531,
            INSTANCE_PARCC_RL531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL532,
            INSTANCE_PARCC_RL532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL533,
            INSTANCE_PARCC_RL533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL541,
            INSTANCE_PARCC_RL541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL611,
            INSTANCE_PARCC_RL611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL612,
            INSTANCE_PARCC_RL612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL621,
            INSTANCE_PARCC_RL621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL622,
            INSTANCE_PARCC_RL622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL631,
            INSTANCE_PARCC_RL631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL632,
            INSTANCE_PARCC_RL632
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL641,
            INSTANCE_PARCC_RL641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL711,
            INSTANCE_PARCC_RL711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL712,
            INSTANCE_PARCC_RL712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL721,
            INSTANCE_PARCC_RL721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL722,
            INSTANCE_PARCC_RL722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL723,
            INSTANCE_PARCC_RL723
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL731,
            INSTANCE_PARCC_RL731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL741,
            INSTANCE_PARCC_RL741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL811,
            INSTANCE_PARCC_RL811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL812,
            INSTANCE_PARCC_RL812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL821,
            INSTANCE_PARCC_RL821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL822,
            INSTANCE_PARCC_RL822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL823,
            INSTANCE_PARCC_RL823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL831,
            INSTANCE_PARCC_RL831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL832,
            INSTANCE_PARCC_RL832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL833,
            INSTANCE_PARCC_RL833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL841,
            INSTANCE_PARCC_RL841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL911,
            INSTANCE_PARCC_RL911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL912,
            INSTANCE_PARCC_RL912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL921,
            INSTANCE_PARCC_RL921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL922,
            INSTANCE_PARCC_RL922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL931,
            INSTANCE_PARCC_RL931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL932,
            INSTANCE_PARCC_RL932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RL933,
            INSTANCE_PARCC_RL933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST642,
            INSTANCE_PARCC_RST642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST644,
            INSTANCE_PARCC_RST644
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST713,
            INSTANCE_PARCC_RST713
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST744,
            INSTANCE_PARCC_RST744
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST813,
            INSTANCE_PARCC_RST813
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST842,
            INSTANCE_PARCC_RST842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST844,
            INSTANCE_PARCC_RST844
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST913,
            INSTANCE_PARCC_RST913
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST921,
            INSTANCE_PARCC_RST921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST922,
            INSTANCE_PARCC_RST922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAREADINGCOMPEVIDENCESTATEMENT2,
            CODE_PARCC_RST925,
            INSTANCE_PARCC_RST925
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L241,
            INSTANCE_PARCC_L241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L242,
            INSTANCE_PARCC_L242
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L243,
            INSTANCE_PARCC_L243
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L244,
            INSTANCE_PARCC_L244
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L251,
            INSTANCE_PARCC_L251
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L252,
            INSTANCE_PARCC_L252
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L261,
            INSTANCE_PARCC_L261
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L341,
            INSTANCE_PARCC_L341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L342,
            INSTANCE_PARCC_L342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L343,
            INSTANCE_PARCC_L343
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L351,
            INSTANCE_PARCC_L351
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L352,
            INSTANCE_PARCC_L352
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L353,
            INSTANCE_PARCC_L353
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L361,
            INSTANCE_PARCC_L361
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L441,
            INSTANCE_PARCC_L441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L442,
            INSTANCE_PARCC_L442
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L451,
            INSTANCE_PARCC_L451
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L452,
            INSTANCE_PARCC_L452
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L453,
            INSTANCE_PARCC_L453
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L461,
            INSTANCE_PARCC_L461
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L541,
            INSTANCE_PARCC_L541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L542,
            INSTANCE_PARCC_L542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L551,
            INSTANCE_PARCC_L551
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L552,
            INSTANCE_PARCC_L552
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L553,
            INSTANCE_PARCC_L553
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L561,
            INSTANCE_PARCC_L561
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L641,
            INSTANCE_PARCC_L641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L642,
            INSTANCE_PARCC_L642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L651,
            INSTANCE_PARCC_L651
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L652,
            INSTANCE_PARCC_L652
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L653,
            INSTANCE_PARCC_L653
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L654,
            INSTANCE_PARCC_L654
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L661,
            INSTANCE_PARCC_L661
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L741,
            INSTANCE_PARCC_L741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L742,
            INSTANCE_PARCC_L742
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L751,
            INSTANCE_PARCC_L751
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L752,
            INSTANCE_PARCC_L752
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L753,
            INSTANCE_PARCC_L753
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L761,
            INSTANCE_PARCC_L761
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L841,
            INSTANCE_PARCC_L841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L842,
            INSTANCE_PARCC_L842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L851,
            INSTANCE_PARCC_L851
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L852,
            INSTANCE_PARCC_L852
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L853,
            INSTANCE_PARCC_L853
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_L861,
            INSTANCE_PARCC_L861
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RH743,
            INSTANCE_PARCC_RH743
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RH843,
            INSTANCE_PARCC_RH843
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RH935,
            INSTANCE_PARCC_RH935
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI111,
            INSTANCE_PARCC_RI111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI121,
            INSTANCE_PARCC_RI121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI122,
            INSTANCE_PARCC_RI122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI131,
            INSTANCE_PARCC_RI131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI132,
            INSTANCE_PARCC_RI132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI133,
            INSTANCE_PARCC_RI133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI211,
            INSTANCE_PARCC_RI211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI221,
            INSTANCE_PARCC_RI221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI222,
            INSTANCE_PARCC_RI222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI232,
            INSTANCE_PARCC_RI232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI233,
            INSTANCE_PARCC_RI233
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI241,
            INSTANCE_PARCC_RI241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI311,
            INSTANCE_PARCC_RI311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI312,
            INSTANCE_PARCC_RI312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI321,
            INSTANCE_PARCC_RI321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI322,
            INSTANCE_PARCC_RI322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI323,
            INSTANCE_PARCC_RI323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI331,
            INSTANCE_PARCC_RI331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI332,
            INSTANCE_PARCC_RI332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI333,
            INSTANCE_PARCC_RI333
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI341,
            INSTANCE_PARCC_RI341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI342,
            INSTANCE_PARCC_RI342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI411,
            INSTANCE_PARCC_RI411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI412,
            INSTANCE_PARCC_RI412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI421,
            INSTANCE_PARCC_RI421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI422,
            INSTANCE_PARCC_RI422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI423,
            INSTANCE_PARCC_RI423
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI431,
            INSTANCE_PARCC_RI431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI432,
            INSTANCE_PARCC_RI432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI433,
            INSTANCE_PARCC_RI433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI441,
            INSTANCE_PARCC_RI441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI511,
            INSTANCE_PARCC_RI511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI512,
            INSTANCE_PARCC_RI512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI521,
            INSTANCE_PARCC_RI521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI522,
            INSTANCE_PARCC_RI522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI523,
            INSTANCE_PARCC_RI523
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI531,
            INSTANCE_PARCC_RI531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI532,
            INSTANCE_PARCC_RI532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI533,
            INSTANCE_PARCC_RI533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI541,
            INSTANCE_PARCC_RI541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI542,
            INSTANCE_PARCC_RI542
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI611,
            INSTANCE_PARCC_RI611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI612,
            INSTANCE_PARCC_RI612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI621,
            INSTANCE_PARCC_RI621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI622,
            INSTANCE_PARCC_RI622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI623,
            INSTANCE_PARCC_RI623
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI631,
            INSTANCE_PARCC_RI631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI633,
            INSTANCE_PARCC_RI633
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI641,
            INSTANCE_PARCC_RI641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI711,
            INSTANCE_PARCC_RI711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI712,
            INSTANCE_PARCC_RI712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI721,
            INSTANCE_PARCC_RI721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI722,
            INSTANCE_PARCC_RI722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI731,
            INSTANCE_PARCC_RI731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI741,
            INSTANCE_PARCC_RI741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI811,
            INSTANCE_PARCC_RI811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI812,
            INSTANCE_PARCC_RI812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI821,
            INSTANCE_PARCC_RI821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI822,
            INSTANCE_PARCC_RI822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI823,
            INSTANCE_PARCC_RI823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI831,
            INSTANCE_PARCC_RI831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI832,
            INSTANCE_PARCC_RI832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI833,
            INSTANCE_PARCC_RI833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI841,
            INSTANCE_PARCC_RI841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI911,
            INSTANCE_PARCC_RI911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI912,
            INSTANCE_PARCC_RI912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI921,
            INSTANCE_PARCC_RI921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI922,
            INSTANCE_PARCC_RI922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI931,
            INSTANCE_PARCC_RI931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI932,
            INSTANCE_PARCC_RI932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RI933,
            INSTANCE_PARCC_RI933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL111,
            INSTANCE_PARCC_RL111
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL121,
            INSTANCE_PARCC_RL121
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL122,
            INSTANCE_PARCC_RL122
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL131,
            INSTANCE_PARCC_RL131
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL132,
            INSTANCE_PARCC_RL132
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL133,
            INSTANCE_PARCC_RL133
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL211,
            INSTANCE_PARCC_RL211
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL221,
            INSTANCE_PARCC_RL221
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL222,
            INSTANCE_PARCC_RL222
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL231,
            INSTANCE_PARCC_RL231
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL232,
            INSTANCE_PARCC_RL232
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL241,
            INSTANCE_PARCC_RL241
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL311,
            INSTANCE_PARCC_RL311
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL312,
            INSTANCE_PARCC_RL312
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL321,
            INSTANCE_PARCC_RL321
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL322,
            INSTANCE_PARCC_RL322
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL323,
            INSTANCE_PARCC_RL323
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL331,
            INSTANCE_PARCC_RL331
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL332,
            INSTANCE_PARCC_RL332
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL341,
            INSTANCE_PARCC_RL341
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL342,
            INSTANCE_PARCC_RL342
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL411,
            INSTANCE_PARCC_RL411
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL412,
            INSTANCE_PARCC_RL412
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL421,
            INSTANCE_PARCC_RL421
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL422,
            INSTANCE_PARCC_RL422
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL431,
            INSTANCE_PARCC_RL431
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL432,
            INSTANCE_PARCC_RL432
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL433,
            INSTANCE_PARCC_RL433
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL441,
            INSTANCE_PARCC_RL441
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL511,
            INSTANCE_PARCC_RL511
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL512,
            INSTANCE_PARCC_RL512
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL521,
            INSTANCE_PARCC_RL521
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL522,
            INSTANCE_PARCC_RL522
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL531,
            INSTANCE_PARCC_RL531
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL532,
            INSTANCE_PARCC_RL532
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL533,
            INSTANCE_PARCC_RL533
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL541,
            INSTANCE_PARCC_RL541
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL611,
            INSTANCE_PARCC_RL611
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL612,
            INSTANCE_PARCC_RL612
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL621,
            INSTANCE_PARCC_RL621
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL622,
            INSTANCE_PARCC_RL622
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL631,
            INSTANCE_PARCC_RL631
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL632,
            INSTANCE_PARCC_RL632
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL641,
            INSTANCE_PARCC_RL641
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL711,
            INSTANCE_PARCC_RL711
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL712,
            INSTANCE_PARCC_RL712
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL721,
            INSTANCE_PARCC_RL721
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL722,
            INSTANCE_PARCC_RL722
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL723,
            INSTANCE_PARCC_RL723
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL731,
            INSTANCE_PARCC_RL731
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL741,
            INSTANCE_PARCC_RL741
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL811,
            INSTANCE_PARCC_RL811
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL812,
            INSTANCE_PARCC_RL812
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL821,
            INSTANCE_PARCC_RL821
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL822,
            INSTANCE_PARCC_RL822
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL823,
            INSTANCE_PARCC_RL823
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL831,
            INSTANCE_PARCC_RL831
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL832,
            INSTANCE_PARCC_RL832
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL833,
            INSTANCE_PARCC_RL833
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL841,
            INSTANCE_PARCC_RL841
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL911,
            INSTANCE_PARCC_RL911
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL912,
            INSTANCE_PARCC_RL912
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL921,
            INSTANCE_PARCC_RL921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL922,
            INSTANCE_PARCC_RL922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL931,
            INSTANCE_PARCC_RL931
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL932,
            INSTANCE_PARCC_RL932
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RL933,
            INSTANCE_PARCC_RL933
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST642,
            INSTANCE_PARCC_RST642
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST644,
            INSTANCE_PARCC_RST644
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST713,
            INSTANCE_PARCC_RST713
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST744,
            INSTANCE_PARCC_RST744
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST813,
            INSTANCE_PARCC_RST813
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST842,
            INSTANCE_PARCC_RST842
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST844,
            INSTANCE_PARCC_RST844
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST913,
            INSTANCE_PARCC_RST913
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST921,
            INSTANCE_PARCC_RST921
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST922,
            INSTANCE_PARCC_RST922
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_ELAVOCABEVIDENCESTATEMENT2,
            CODE_PARCC_RST925,
            INSTANCE_PARCC_RST925
        );
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L24,
            INSTANCE_PARCC_L24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L25,
            INSTANCE_PARCC_L25
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L26,
            INSTANCE_PARCC_L26
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L34,
            INSTANCE_PARCC_L34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L34A,
            INSTANCE_PARCC_L34A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L35,
            INSTANCE_PARCC_L35
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L35C,
            INSTANCE_PARCC_L35C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L36,
            INSTANCE_PARCC_L36
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L44,
            INSTANCE_PARCC_L44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L45,
            INSTANCE_PARCC_L45
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L46,
            INSTANCE_PARCC_L46
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L54,
            INSTANCE_PARCC_L54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L54A,
            INSTANCE_PARCC_L54A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L55,
            INSTANCE_PARCC_L55
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L56,
            INSTANCE_PARCC_L56
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L64,
            INSTANCE_PARCC_L64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L64A,
            INSTANCE_PARCC_L64A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L65,
            INSTANCE_PARCC_L65
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L66,
            INSTANCE_PARCC_L66
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L74,
            INSTANCE_PARCC_L74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L75,
            INSTANCE_PARCC_L75
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L75C,
            INSTANCE_PARCC_L75C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L76,
            INSTANCE_PARCC_L76
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L84,
            INSTANCE_PARCC_L84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L84A,
            INSTANCE_PARCC_L84A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L84B,
            INSTANCE_PARCC_L84B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L85,
            INSTANCE_PARCC_L85
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L85C,
            INSTANCE_PARCC_L85C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_L86,
            INSTANCE_PARCC_L86
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF13,
            INSTANCE_PARCC_RF13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF13A,
            INSTANCE_PARCC_RF13A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF13F,
            INSTANCE_PARCC_RF13F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF23,
            INSTANCE_PARCC_RF23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF23C,
            INSTANCE_PARCC_RF23C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF23D,
            INSTANCE_PARCC_RF23D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF23E,
            INSTANCE_PARCC_RF23E
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF23F,
            INSTANCE_PARCC_RF23F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF33,
            INSTANCE_PARCC_RF33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF33A,
            INSTANCE_PARCC_RF33A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RF33C,
            INSTANCE_PARCC_RF33C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RH74,
            INSTANCE_PARCC_RH74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RH84,
            INSTANCE_PARCC_RH84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RH93,
            INSTANCE_PARCC_RH93
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI11,
            INSTANCE_PARCC_RI11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI12,
            INSTANCE_PARCC_RI12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI13,
            INSTANCE_PARCC_RI13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI21,
            INSTANCE_PARCC_RI21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI22,
            INSTANCE_PARCC_RI22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI23,
            INSTANCE_PARCC_RI23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI24,
            INSTANCE_PARCC_RI24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI31,
            INSTANCE_PARCC_RI31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI32,
            INSTANCE_PARCC_RI32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI33,
            INSTANCE_PARCC_RI33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI34,
            INSTANCE_PARCC_RI34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI41,
            INSTANCE_PARCC_RI41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI42,
            INSTANCE_PARCC_RI42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI43,
            INSTANCE_PARCC_RI43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI44,
            INSTANCE_PARCC_RI44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI51,
            INSTANCE_PARCC_RI51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI52,
            INSTANCE_PARCC_RI52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI53,
            INSTANCE_PARCC_RI53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI54,
            INSTANCE_PARCC_RI54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI61,
            INSTANCE_PARCC_RI61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI62,
            INSTANCE_PARCC_RI62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI63,
            INSTANCE_PARCC_RI63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI64,
            INSTANCE_PARCC_RI64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI71,
            INSTANCE_PARCC_RI71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI72,
            INSTANCE_PARCC_RI72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI73,
            INSTANCE_PARCC_RI73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI74,
            INSTANCE_PARCC_RI74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI81,
            INSTANCE_PARCC_RI81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI82,
            INSTANCE_PARCC_RI82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI83,
            INSTANCE_PARCC_RI83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI84,
            INSTANCE_PARCC_RI84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI9101,
            INSTANCE_PARCC_RI9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI9102,
            INSTANCE_PARCC_RI9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RI9103,
            INSTANCE_PARCC_RI9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL11,
            INSTANCE_PARCC_RL11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL12,
            INSTANCE_PARCC_RL12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL13,
            INSTANCE_PARCC_RL13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL21,
            INSTANCE_PARCC_RL21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL22,
            INSTANCE_PARCC_RL22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL23,
            INSTANCE_PARCC_RL23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL24,
            INSTANCE_PARCC_RL24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL31,
            INSTANCE_PARCC_RL31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL32,
            INSTANCE_PARCC_RL32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL33,
            INSTANCE_PARCC_RL33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL34,
            INSTANCE_PARCC_RL34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL41,
            INSTANCE_PARCC_RL41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL42,
            INSTANCE_PARCC_RL42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL43,
            INSTANCE_PARCC_RL43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL44,
            INSTANCE_PARCC_RL44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL51,
            INSTANCE_PARCC_RL51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL52,
            INSTANCE_PARCC_RL52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL53,
            INSTANCE_PARCC_RL53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL54,
            INSTANCE_PARCC_RL54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL61,
            INSTANCE_PARCC_RL61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL62,
            INSTANCE_PARCC_RL62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL63,
            INSTANCE_PARCC_RL63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL64,
            INSTANCE_PARCC_RL64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL71,
            INSTANCE_PARCC_RL71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL72,
            INSTANCE_PARCC_RL72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL73,
            INSTANCE_PARCC_RL73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL74,
            INSTANCE_PARCC_RL74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL81,
            INSTANCE_PARCC_RL81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL82,
            INSTANCE_PARCC_RL82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL83,
            INSTANCE_PARCC_RL83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL84,
            INSTANCE_PARCC_RL84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL9101,
            INSTANCE_PARCC_RL9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL9102,
            INSTANCE_PARCC_RL9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RL9103,
            INSTANCE_PARCC_RL9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST64,
            INSTANCE_PARCC_RST64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST74,
            INSTANCE_PARCC_RST74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST81,
            INSTANCE_PARCC_RST81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST84,
            INSTANCE_PARCC_RST84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST91,
            INSTANCE_PARCC_RST91
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELACONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELACONTENTSTANDARD,
            CODE_PARCC_RST92,
            INSTANCE_PARCC_RST92
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L24,
            INSTANCE_PARCC_L24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L25,
            INSTANCE_PARCC_L25
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L26,
            INSTANCE_PARCC_L26
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L34,
            INSTANCE_PARCC_L34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L34A,
            INSTANCE_PARCC_L34A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L35,
            INSTANCE_PARCC_L35
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L35C,
            INSTANCE_PARCC_L35C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L36,
            INSTANCE_PARCC_L36
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L44,
            INSTANCE_PARCC_L44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L45,
            INSTANCE_PARCC_L45
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L46,
            INSTANCE_PARCC_L46
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L54,
            INSTANCE_PARCC_L54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L54A,
            INSTANCE_PARCC_L54A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L55,
            INSTANCE_PARCC_L55
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L56,
            INSTANCE_PARCC_L56
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L64,
            INSTANCE_PARCC_L64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L64A,
            INSTANCE_PARCC_L64A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L65,
            INSTANCE_PARCC_L65
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L66,
            INSTANCE_PARCC_L66
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L74,
            INSTANCE_PARCC_L74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L75,
            INSTANCE_PARCC_L75
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L75C,
            INSTANCE_PARCC_L75C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L76,
            INSTANCE_PARCC_L76
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84,
            INSTANCE_PARCC_L84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84A,
            INSTANCE_PARCC_L84A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84B,
            INSTANCE_PARCC_L84B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L85,
            INSTANCE_PARCC_L85
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L85C,
            INSTANCE_PARCC_L85C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L86,
            INSTANCE_PARCC_L86
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13,
            INSTANCE_PARCC_RF13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13A,
            INSTANCE_PARCC_RF13A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13F,
            INSTANCE_PARCC_RF13F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23,
            INSTANCE_PARCC_RF23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23C,
            INSTANCE_PARCC_RF23C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23D,
            INSTANCE_PARCC_RF23D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23E,
            INSTANCE_PARCC_RF23E
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23F,
            INSTANCE_PARCC_RF23F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33,
            INSTANCE_PARCC_RF33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33A,
            INSTANCE_PARCC_RF33A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33C,
            INSTANCE_PARCC_RF33C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH74,
            INSTANCE_PARCC_RH74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH84,
            INSTANCE_PARCC_RH84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH93,
            INSTANCE_PARCC_RH93
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI11,
            INSTANCE_PARCC_RI11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI12,
            INSTANCE_PARCC_RI12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI13,
            INSTANCE_PARCC_RI13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI21,
            INSTANCE_PARCC_RI21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI22,
            INSTANCE_PARCC_RI22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI23,
            INSTANCE_PARCC_RI23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI24,
            INSTANCE_PARCC_RI24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI31,
            INSTANCE_PARCC_RI31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI32,
            INSTANCE_PARCC_RI32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI33,
            INSTANCE_PARCC_RI33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI34,
            INSTANCE_PARCC_RI34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI41,
            INSTANCE_PARCC_RI41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI42,
            INSTANCE_PARCC_RI42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI43,
            INSTANCE_PARCC_RI43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI44,
            INSTANCE_PARCC_RI44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI51,
            INSTANCE_PARCC_RI51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI52,
            INSTANCE_PARCC_RI52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI53,
            INSTANCE_PARCC_RI53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI54,
            INSTANCE_PARCC_RI54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI61,
            INSTANCE_PARCC_RI61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI62,
            INSTANCE_PARCC_RI62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI63,
            INSTANCE_PARCC_RI63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI64,
            INSTANCE_PARCC_RI64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI71,
            INSTANCE_PARCC_RI71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI72,
            INSTANCE_PARCC_RI72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI73,
            INSTANCE_PARCC_RI73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI74,
            INSTANCE_PARCC_RI74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI81,
            INSTANCE_PARCC_RI81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI82,
            INSTANCE_PARCC_RI82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI83,
            INSTANCE_PARCC_RI83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI84,
            INSTANCE_PARCC_RI84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9101,
            INSTANCE_PARCC_RI9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9102,
            INSTANCE_PARCC_RI9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9103,
            INSTANCE_PARCC_RI9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL11,
            INSTANCE_PARCC_RL11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL12,
            INSTANCE_PARCC_RL12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL13,
            INSTANCE_PARCC_RL13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL21,
            INSTANCE_PARCC_RL21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL22,
            INSTANCE_PARCC_RL22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL23,
            INSTANCE_PARCC_RL23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL24,
            INSTANCE_PARCC_RL24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL31,
            INSTANCE_PARCC_RL31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL32,
            INSTANCE_PARCC_RL32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL33,
            INSTANCE_PARCC_RL33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL34,
            INSTANCE_PARCC_RL34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL41,
            INSTANCE_PARCC_RL41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL42,
            INSTANCE_PARCC_RL42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL43,
            INSTANCE_PARCC_RL43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL44,
            INSTANCE_PARCC_RL44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL51,
            INSTANCE_PARCC_RL51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL52,
            INSTANCE_PARCC_RL52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL53,
            INSTANCE_PARCC_RL53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL54,
            INSTANCE_PARCC_RL54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL61,
            INSTANCE_PARCC_RL61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL62,
            INSTANCE_PARCC_RL62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL63,
            INSTANCE_PARCC_RL63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL64,
            INSTANCE_PARCC_RL64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL71,
            INSTANCE_PARCC_RL71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL72,
            INSTANCE_PARCC_RL72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL73,
            INSTANCE_PARCC_RL73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL74,
            INSTANCE_PARCC_RL74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL81,
            INSTANCE_PARCC_RL81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL82,
            INSTANCE_PARCC_RL82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL83,
            INSTANCE_PARCC_RL83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL84,
            INSTANCE_PARCC_RL84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9101,
            INSTANCE_PARCC_RL9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9102,
            INSTANCE_PARCC_RL9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9103,
            INSTANCE_PARCC_RL9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST64,
            INSTANCE_PARCC_RST64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST74,
            INSTANCE_PARCC_RST74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST81,
            INSTANCE_PARCC_RST81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST84,
            INSTANCE_PARCC_RST84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST91,
            INSTANCE_PARCC_RST91
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAREADINGSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST92,
            INSTANCE_PARCC_RST92
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L24,
            INSTANCE_PARCC_L24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L25,
            INSTANCE_PARCC_L25
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L26,
            INSTANCE_PARCC_L26
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L34,
            INSTANCE_PARCC_L34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L34A,
            INSTANCE_PARCC_L34A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L35,
            INSTANCE_PARCC_L35
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L35C,
            INSTANCE_PARCC_L35C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L36,
            INSTANCE_PARCC_L36
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L44,
            INSTANCE_PARCC_L44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L45,
            INSTANCE_PARCC_L45
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L46,
            INSTANCE_PARCC_L46
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L54,
            INSTANCE_PARCC_L54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L54A,
            INSTANCE_PARCC_L54A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L55,
            INSTANCE_PARCC_L55
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L56,
            INSTANCE_PARCC_L56
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L64,
            INSTANCE_PARCC_L64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L64A,
            INSTANCE_PARCC_L64A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L65,
            INSTANCE_PARCC_L65
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L66,
            INSTANCE_PARCC_L66
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L74,
            INSTANCE_PARCC_L74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L75,
            INSTANCE_PARCC_L75
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L75C,
            INSTANCE_PARCC_L75C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L76,
            INSTANCE_PARCC_L76
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84,
            INSTANCE_PARCC_L84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84A,
            INSTANCE_PARCC_L84A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L84B,
            INSTANCE_PARCC_L84B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L85,
            INSTANCE_PARCC_L85
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L85C,
            INSTANCE_PARCC_L85C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_L86,
            INSTANCE_PARCC_L86
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13,
            INSTANCE_PARCC_RF13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13A,
            INSTANCE_PARCC_RF13A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF13F,
            INSTANCE_PARCC_RF13F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23,
            INSTANCE_PARCC_RF23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23C,
            INSTANCE_PARCC_RF23C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23D,
            INSTANCE_PARCC_RF23D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23E,
            INSTANCE_PARCC_RF23E
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF23F,
            INSTANCE_PARCC_RF23F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33,
            INSTANCE_PARCC_RF33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33A,
            INSTANCE_PARCC_RF33A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RF33C,
            INSTANCE_PARCC_RF33C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH74,
            INSTANCE_PARCC_RH74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH84,
            INSTANCE_PARCC_RH84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RH93,
            INSTANCE_PARCC_RH93
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI11,
            INSTANCE_PARCC_RI11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI12,
            INSTANCE_PARCC_RI12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI13,
            INSTANCE_PARCC_RI13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI21,
            INSTANCE_PARCC_RI21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI22,
            INSTANCE_PARCC_RI22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI23,
            INSTANCE_PARCC_RI23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI24,
            INSTANCE_PARCC_RI24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI31,
            INSTANCE_PARCC_RI31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI32,
            INSTANCE_PARCC_RI32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI33,
            INSTANCE_PARCC_RI33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI34,
            INSTANCE_PARCC_RI34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI41,
            INSTANCE_PARCC_RI41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI42,
            INSTANCE_PARCC_RI42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI43,
            INSTANCE_PARCC_RI43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI44,
            INSTANCE_PARCC_RI44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI51,
            INSTANCE_PARCC_RI51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI52,
            INSTANCE_PARCC_RI52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI53,
            INSTANCE_PARCC_RI53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI54,
            INSTANCE_PARCC_RI54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI61,
            INSTANCE_PARCC_RI61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI62,
            INSTANCE_PARCC_RI62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI63,
            INSTANCE_PARCC_RI63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI64,
            INSTANCE_PARCC_RI64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI71,
            INSTANCE_PARCC_RI71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI72,
            INSTANCE_PARCC_RI72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI73,
            INSTANCE_PARCC_RI73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI74,
            INSTANCE_PARCC_RI74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI81,
            INSTANCE_PARCC_RI81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI82,
            INSTANCE_PARCC_RI82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI83,
            INSTANCE_PARCC_RI83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI84,
            INSTANCE_PARCC_RI84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9101,
            INSTANCE_PARCC_RI9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9102,
            INSTANCE_PARCC_RI9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RI9103,
            INSTANCE_PARCC_RI9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL11,
            INSTANCE_PARCC_RL11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL12,
            INSTANCE_PARCC_RL12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL13,
            INSTANCE_PARCC_RL13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL21,
            INSTANCE_PARCC_RL21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL22,
            INSTANCE_PARCC_RL22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL23,
            INSTANCE_PARCC_RL23
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL24,
            INSTANCE_PARCC_RL24
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL31,
            INSTANCE_PARCC_RL31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL32,
            INSTANCE_PARCC_RL32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL33,
            INSTANCE_PARCC_RL33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL34,
            INSTANCE_PARCC_RL34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL41,
            INSTANCE_PARCC_RL41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL42,
            INSTANCE_PARCC_RL42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL43,
            INSTANCE_PARCC_RL43
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL44,
            INSTANCE_PARCC_RL44
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL51,
            INSTANCE_PARCC_RL51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL52,
            INSTANCE_PARCC_RL52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL53,
            INSTANCE_PARCC_RL53
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL54,
            INSTANCE_PARCC_RL54
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL61,
            INSTANCE_PARCC_RL61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL62,
            INSTANCE_PARCC_RL62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL63,
            INSTANCE_PARCC_RL63
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL64,
            INSTANCE_PARCC_RL64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL71,
            INSTANCE_PARCC_RL71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL72,
            INSTANCE_PARCC_RL72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL73,
            INSTANCE_PARCC_RL73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL74,
            INSTANCE_PARCC_RL74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL81,
            INSTANCE_PARCC_RL81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL82,
            INSTANCE_PARCC_RL82
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL83,
            INSTANCE_PARCC_RL83
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL84,
            INSTANCE_PARCC_RL84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9101,
            INSTANCE_PARCC_RL9101
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9102,
            INSTANCE_PARCC_RL9102
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RL9103,
            INSTANCE_PARCC_RL9103
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST64,
            INSTANCE_PARCC_RST64
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST74,
            INSTANCE_PARCC_RST74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST81,
            INSTANCE_PARCC_RST81
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST84,
            INSTANCE_PARCC_RST84
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST91,
            INSTANCE_PARCC_RST91
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD
            ),
            PROPERTY_PARCC_ELAVOCABSECONDARYCONTENTSTANDARD,
            CODE_PARCC_RST92,
            INSTANCE_PARCC_RST92
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION,
            CODE_PARCC_DUALUSE,
            INSTANCE_PARCC_DUALUSE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION,
            CODE_PARCC_LONG,
            INSTANCE_PARCC_LONG
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION,
            CODE_PARCC_SHORT,
            INSTANCE_PARCC_SHORT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGEDESIGNATION,
            CODE_PARCC_SINGLEUSE,
            INSTANCE_PARCC_SINGLEUSE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION,
            CODE_PARCC_DUALUSE,
            INSTANCE_PARCC_DUALUSE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION,
            CODE_PARCC_LONG,
            INSTANCE_PARCC_LONG
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION,
            CODE_PARCC_SHORT,
            INSTANCE_PARCC_SHORT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGEDESIGNATION,
            CODE_PARCC_SINGLEUSE,
            INSTANCE_PARCC_SINGLEUSE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGETYPE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGETYPE,
            CODE_PARCC_PASSAGETYPEINFORMATIONAL,
            INSTANCE_PARCC_PASSAGETYPEINFORMATIONAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPPASSAGETYPE
            ),
            PROPERTY_PARCC_ELAREADINGCOMPPASSAGETYPE,
            CODE_PARCC_PASSAGETYPELITERARY,
            INSTANCE_PARCC_PASSAGETYPELITERARY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGETYPE
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGETYPE,
            CODE_PARCC_PASSAGETYPEINFORMATIONAL,
            INSTANCE_PARCC_PASSAGETYPEINFORMATIONAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABPASSAGETYPE
            ),
            PROPERTY_PARCC_ELAVOCABPASSAGETYPE,
            CODE_PARCC_PASSAGETYPELITERARY,
            INSTANCE_PARCC_PASSAGETYPELITERARY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM
            ),
            PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM,
            CODE_PARCC_ACCURACY,
            INSTANCE_PARCC_ACCURACY
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM
            ),
            PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM,
            CODE_PARCC_EVIDENCE,
            INSTANCE_PARCC_EVIDENCE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM
            ),
            PROPERTY_PARCC_ELAREADINGCOMPSUBCLAIM,
            CODE_PARCC_ACCURACYEVIDENCE,
            INSTANCE_PARCC_ACCURACYEVIDENCE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSUBCLAIM
            ),
            PROPERTY_PARCC_ELAVOCABSUBCLAIM,
            CODE_PARCC_ACCURACY,
            INSTANCE_PARCC_ACCURACY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSUBCLAIM
            ),
            PROPERTY_PARCC_ELAVOCABSUBCLAIM,
            CODE_PARCC_EVIDENCE,
            INSTANCE_PARCC_EVIDENCE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABSUBCLAIM
            ),
            PROPERTY_PARCC_ELAVOCABSUBCLAIM,
            CODE_PARCC_ACCURACYEVIDENCE,
            INSTANCE_PARCC_ACCURACYEVIDENCE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYLOW,
            INSTANCE_PARCC_TEXTCOMPLEXITYLOW
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYMEDIUM,
            INSTANCE_PARCC_TEXTCOMPLEXITYMEDIUM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAREADINGCOMPTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYHIGH,
            INSTANCE_PARCC_TEXTCOMPLEXITYHIGH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYLOW,
            INSTANCE_PARCC_TEXTCOMPLEXITYLOW
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYMEDIUM,
            INSTANCE_PARCC_TEXTCOMPLEXITYMEDIUM
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY
            ),
            PROPERTY_PARCC_ELAVOCABTEXTCOMPLEXITY,
            CODE_PARCC_TEXTCOMPLEXITYHIGH,
            INSTANCE_PARCC_TEXTCOMPLEXITYHIGH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_CALCULATOR
            ),
            PROPERTY_PARCC_CALCULATOR,
            CODE_PARCC_5FUNCTIONCALCULATOR,
            INSTANCE_PARCC_5FUNCTIONCALCULATOR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_CALCULATOR
            ),
            PROPERTY_PARCC_CALCULATOR,
            CODE_PARCC_NOCALCULATOR,
            INSTANCE_PARCC_NOCALCULATOR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_CALCULATOR
            ),
            PROPERTY_PARCC_CALCULATOR,
            CODE_PARCC_SCIENTIFICCALCULATOR,
            INSTANCE_PARCC_SCIENTIFICCALCULATOR
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1NBTA,
            INSTANCE_PARCC_1NBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1NBTB,
            INSTANCE_PARCC_1NBTB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1NBTC,
            INSTANCE_PARCC_1NBTC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1OAA,
            INSTANCE_PARCC_1OAA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1OAB,
            INSTANCE_PARCC_1OAB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1OAC,
            INSTANCE_PARCC_1OAC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_1OAD,
            INSTANCE_PARCC_1OAD
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2MDA,
            INSTANCE_PARCC_2MDA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2MDB,
            INSTANCE_PARCC_2MDB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2NBTA,
            INSTANCE_PARCC_2NBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2NBTB,
            INSTANCE_PARCC_2NBTB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2OAA,
            INSTANCE_PARCC_2OAA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2OAC,
            INSTANCE_PARCC_2OAC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3MDA,
            INSTANCE_PARCC_3MDA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3MDC,
            INSTANCE_PARCC_3MDC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3NFA,
            INSTANCE_PARCC_3NFA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3OAA,
            INSTANCE_PARCC_3OAA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3OAB,
            INSTANCE_PARCC_3OAB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3OAD,
            INSTANCE_PARCC_3OAD
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4NBTA,
            INSTANCE_PARCC_4NBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4NBTB,
            INSTANCE_PARCC_4NBTB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4NFA,
            INSTANCE_PARCC_4NFA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4NFB,
            INSTANCE_PARCC_4NFB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4NFC,
            INSTANCE_PARCC_4NFC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_4OAA,
            INSTANCE_PARCC_4OAA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5GA,
            INSTANCE_PARCC_5GA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5MDC,
            INSTANCE_PARCC_5MDC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5NBTA,
            INSTANCE_PARCC_5NBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5NBTB,
            INSTANCE_PARCC_5NBTB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5NFA,
            INSTANCE_PARCC_5NFA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_5NFB,
            INSTANCE_PARCC_5NFB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6EEA,
            INSTANCE_PARCC_6EEA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6EEB,
            INSTANCE_PARCC_6EEB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6EEC,
            INSTANCE_PARCC_6EEC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6NSA,
            INSTANCE_PARCC_6NSA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6NSC,
            INSTANCE_PARCC_6NSC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6RPA,
            INSTANCE_PARCC_6RPA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_7EEA,
            INSTANCE_PARCC_7EEA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_7EEB,
            INSTANCE_PARCC_7EEB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_7NSA,
            INSTANCE_PARCC_7NSA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_7RPA,
            INSTANCE_PARCC_7RPA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_8EEA,
            INSTANCE_PARCC_8EEA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_8EEB,
            INSTANCE_PARCC_8EEB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_8EEC,
            INSTANCE_PARCC_8EEC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_8FA,
            INSTANCE_PARCC_8FA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_8FB,
            INSTANCE_PARCC_8FB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_AREIB,
            INSTANCE_PARCC_AREIB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_AREID,
            INSTANCE_PARCC_AREID
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_FIFB,
            INSTANCE_PARCC_FIFB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_KCCA,
            INSTANCE_PARCC_KCCA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_KCCB,
            INSTANCE_PARCC_KCCB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_KCCC,
            INSTANCE_PARCC_KCCC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_KNBTA,
            INSTANCE_PARCC_KNBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_KOAA,
            INSTANCE_PARCC_KOAA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_2OAB,
            INSTANCE_PARCC_2OAB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3OAC,
            INSTANCE_PARCC_3OAC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_3NBTA,
            INSTANCE_PARCC_3NBTA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCLUSTER
            ),
            PROPERTY_PARCC_MATHCLUSTER,
            CODE_PARCC_6NSB,
            INSTANCE_PARCC_6NSB
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1MDA1,
            INSTANCE_PARCC_1MDA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1MDA2,
            INSTANCE_PARCC_1MDA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTA1,
            INSTANCE_PARCC_1NBTA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTB2,
            INSTANCE_PARCC_1NBTB2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTB3,
            INSTANCE_PARCC_1NBTB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTC4,
            INSTANCE_PARCC_1NBTC4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTC5,
            INSTANCE_PARCC_1NBTC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1NBTC6,
            INSTANCE_PARCC_1NBTC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAA1,
            INSTANCE_PARCC_1OAA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAA2,
            INSTANCE_PARCC_1OAA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAB3,
            INSTANCE_PARCC_1OAB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAB4,
            INSTANCE_PARCC_1OAB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAC5,
            INSTANCE_PARCC_1OAC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAC6,
            INSTANCE_PARCC_1OAC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAD7,
            INSTANCE_PARCC_1OAD7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_1OAD8,
            INSTANCE_PARCC_1OAD8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2GA3,
            INSTANCE_PARCC_2GA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDA1,
            INSTANCE_PARCC_2MDA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDA2,
            INSTANCE_PARCC_2MDA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDA3,
            INSTANCE_PARCC_2MDA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDA4,
            INSTANCE_PARCC_2MDA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDB5,
            INSTANCE_PARCC_2MDB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2MDB6,
            INSTANCE_PARCC_2MDB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTA1,
            INSTANCE_PARCC_2NBTA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTA2,
            INSTANCE_PARCC_2NBTA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTA3,
            INSTANCE_PARCC_2NBTA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTA4,
            INSTANCE_PARCC_2NBTA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTB5,
            INSTANCE_PARCC_2NBTB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTB6,
            INSTANCE_PARCC_2NBTB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2NBTB7,
            INSTANCE_PARCC_2NBTB7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2OAA1,
            INSTANCE_PARCC_2OAA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2OAB2,
            INSTANCE_PARCC_2OAB2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2OAC3,
            INSTANCE_PARCC_2OAC3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_2OAC4,
            INSTANCE_PARCC_2OAC4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3MDA1,
            INSTANCE_PARCC_3MDA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3MDA2,
            INSTANCE_PARCC_3MDA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3MDC5,
            INSTANCE_PARCC_3MDC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3MDC6,
            INSTANCE_PARCC_3MDC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3MDC7,
            INSTANCE_PARCC_3MDC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3NBTA2,
            INSTANCE_PARCC_3NBTA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3NFA1,
            INSTANCE_PARCC_3NFA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3NFA2,
            INSTANCE_PARCC_3NFA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3NFA3,
            INSTANCE_PARCC_3NFA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAA1,
            INSTANCE_PARCC_3OAA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAA2,
            INSTANCE_PARCC_3OAA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAA3,
            INSTANCE_PARCC_3OAA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAA4,
            INSTANCE_PARCC_3OAA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAB5,
            INSTANCE_PARCC_3OAB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAB6,
            INSTANCE_PARCC_3OAB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAC7,
            INSTANCE_PARCC_3OAC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_3OAD8,
            INSTANCE_PARCC_3OAD8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4INT2,
            INSTANCE_PARCC_4INT2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4INT3,
            INSTANCE_PARCC_4INT3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4INT4,
            INSTANCE_PARCC_4INT4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4INT7,
            INSTANCE_PARCC_4INT7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTA1,
            INSTANCE_PARCC_4NBTA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTA2,
            INSTANCE_PARCC_4NBTA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTA3,
            INSTANCE_PARCC_4NBTA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTB4,
            INSTANCE_PARCC_4NBTB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTB5,
            INSTANCE_PARCC_4NBTB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NBTB6,
            INSTANCE_PARCC_4NBTB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFA1,
            INSTANCE_PARCC_4NFA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFA2,
            INSTANCE_PARCC_4NFA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFB3,
            INSTANCE_PARCC_4NFB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFB4,
            INSTANCE_PARCC_4NFB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFC5,
            INSTANCE_PARCC_4NFC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFC6,
            INSTANCE_PARCC_4NFC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4NFC7,
            INSTANCE_PARCC_4NFC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4OAA1,
            INSTANCE_PARCC_4OAA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4OAA2,
            INSTANCE_PARCC_4OAA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_4OAA3,
            INSTANCE_PARCC_4OAA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5GA1,
            INSTANCE_PARCC_5GA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5GA2,
            INSTANCE_PARCC_5GA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5INT1,
            INSTANCE_PARCC_5INT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5MDC3,
            INSTANCE_PARCC_5MDC3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5MDC4,
            INSTANCE_PARCC_5MDC4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5MDC5,
            INSTANCE_PARCC_5MDC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTA1,
            INSTANCE_PARCC_5NBTA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTA2,
            INSTANCE_PARCC_5NBTA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTA3,
            INSTANCE_PARCC_5NBTA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTA4,
            INSTANCE_PARCC_5NBTA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTB5,
            INSTANCE_PARCC_5NBTB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTB6,
            INSTANCE_PARCC_5NBTB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NBTB7,
            INSTANCE_PARCC_5NBTB7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFA1,
            INSTANCE_PARCC_5NFA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFA2,
            INSTANCE_PARCC_5NFA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFB3,
            INSTANCE_PARCC_5NFB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFB4,
            INSTANCE_PARCC_5NFB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFB5,
            INSTANCE_PARCC_5NFB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFB6,
            INSTANCE_PARCC_5NFB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_5NFB7,
            INSTANCE_PARCC_5NFB7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEA1,
            INSTANCE_PARCC_6EEA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEA2,
            INSTANCE_PARCC_6EEA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEA3,
            INSTANCE_PARCC_6EEA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEA4,
            INSTANCE_PARCC_6EEA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEB5,
            INSTANCE_PARCC_6EEB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEB6,
            INSTANCE_PARCC_6EEB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEB7,
            INSTANCE_PARCC_6EEB7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEB8,
            INSTANCE_PARCC_6EEB8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6EEC9,
            INSTANCE_PARCC_6EEC9
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSA1,
            INSTANCE_PARCC_6NSA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSB2,
            INSTANCE_PARCC_6NSB2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSB3,
            INSTANCE_PARCC_6NSB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSC5,
            INSTANCE_PARCC_6NSC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSC6,
            INSTANCE_PARCC_6NSC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSC7,
            INSTANCE_PARCC_6NSC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6NSC8,
            INSTANCE_PARCC_6NSC8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6RPA1,
            INSTANCE_PARCC_6RPA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6RPA2,
            INSTANCE_PARCC_6RPA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_6RPA3,
            INSTANCE_PARCC_6RPA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7EEA1,
            INSTANCE_PARCC_7EEA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7EEA2,
            INSTANCE_PARCC_7EEA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7EEB3,
            INSTANCE_PARCC_7EEB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7EEB4,
            INSTANCE_PARCC_7EEB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7NSA1,
            INSTANCE_PARCC_7NSA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7NSA2,
            INSTANCE_PARCC_7NSA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7NSA3,
            INSTANCE_PARCC_7NSA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7RPA1,
            INSTANCE_PARCC_7RPA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7RPA2,
            INSTANCE_PARCC_7RPA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_7RPA3,
            INSTANCE_PARCC_7RPA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEA1,
            INSTANCE_PARCC_8EEA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEA2,
            INSTANCE_PARCC_8EEA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEA3,
            INSTANCE_PARCC_8EEA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEA4,
            INSTANCE_PARCC_8EEA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEB5,
            INSTANCE_PARCC_8EEB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEB6,
            INSTANCE_PARCC_8EEB6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEC7,
            INSTANCE_PARCC_8EEC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8EEC8,
            INSTANCE_PARCC_8EEC8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8FA1,
            INSTANCE_PARCC_8FA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8FA2,
            INSTANCE_PARCC_8FA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8FA3,
            INSTANCE_PARCC_8FA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8FB4,
            INSTANCE_PARCC_8FB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_8FB5,
            INSTANCE_PARCC_8FB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_AREIB3,
            INSTANCE_PARCC_AREIB3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_AREID11,
            INSTANCE_PARCC_AREID11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_FIFB4,
            INSTANCE_PARCC_FIFB4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KCCA3,
            INSTANCE_PARCC_KCCA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KCCB5,
            INSTANCE_PARCC_KCCB5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KCCC6,
            INSTANCE_PARCC_KCCC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KCCC7,
            INSTANCE_PARCC_KCCC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KNBTA1,
            INSTANCE_PARCC_KNBTA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KOAA1,
            INSTANCE_PARCC_KOAA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KOAA2,
            INSTANCE_PARCC_KOAA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KOAA3,
            INSTANCE_PARCC_KOAA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCONTENTSTANDARD
            ),
            PROPERTY_PARCC_MATHCONTENTSTANDARD,
            CODE_PARCC_KOAA4,
            INSTANCE_PARCC_KOAA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_2NBT,
            INSTANCE_PARCC_2NBT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_2OA,
            INSTANCE_PARCC_2OA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_3NBT,
            INSTANCE_PARCC_3NBT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_3OA,
            INSTANCE_PARCC_3OA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_4NBT,
            INSTANCE_PARCC_4NBT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_5NBT,
            INSTANCE_PARCC_5NBT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_6NS,
            INSTANCE_PARCC_6NS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_CC,
            INSTANCE_PARCC_CC
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_EE,
            INSTANCE_PARCC_EE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_F,
            INSTANCE_PARCC_F
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_G,
            INSTANCE_PARCC_G
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_MD,
            INSTANCE_PARCC_MD
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_NBT,
            INSTANCE_PARCC_NBT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_NF,
            INSTANCE_PARCC_NF
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_NS,
            INSTANCE_PARCC_NS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_OA,
            INSTANCE_PARCC_OA
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DOMAIN
            ),
            PROPERTY_PARCC_DOMAIN,
            CODE_PARCC_RP,
            INSTANCE_PARCC_RP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1MD11,
            INSTANCE_PARCC_1MD11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1MD12,
            INSTANCE_PARCC_1MD12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1MD2,
            INSTANCE_PARCC_1MD2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT1,
            INSTANCE_PARCC_1NBT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT2,
            INSTANCE_PARCC_1NBT2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT3,
            INSTANCE_PARCC_1NBT3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT41,
            INSTANCE_PARCC_1NBT41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT42,
            INSTANCE_PARCC_1NBT42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT5,
            INSTANCE_PARCC_1NBT5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1NBT6,
            INSTANCE_PARCC_1NBT6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA1,
            INSTANCE_PARCC_1OA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA2,
            INSTANCE_PARCC_1OA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA3,
            INSTANCE_PARCC_1OA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA4,
            INSTANCE_PARCC_1OA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA5,
            INSTANCE_PARCC_1OA5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA6,
            INSTANCE_PARCC_1OA6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA7,
            INSTANCE_PARCC_1OA7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_1OA8,
            INSTANCE_PARCC_1OA8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2G3,
            INSTANCE_PARCC_2G3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD1,
            INSTANCE_PARCC_2MD1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD2,
            INSTANCE_PARCC_2MD2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD3,
            INSTANCE_PARCC_2MD3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD4,
            INSTANCE_PARCC_2MD4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD5,
            INSTANCE_PARCC_2MD5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD61,
            INSTANCE_PARCC_2MD61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2MD62,
            INSTANCE_PARCC_2MD62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT1A,
            INSTANCE_PARCC_2NBT1A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT1B,
            INSTANCE_PARCC_2NBT1B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT2,
            INSTANCE_PARCC_2NBT2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT3,
            INSTANCE_PARCC_2NBT3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT4,
            INSTANCE_PARCC_2NBT4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT6,
            INSTANCE_PARCC_2NBT6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2NBT7,
            INSTANCE_PARCC_2NBT7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2OA11,
            INSTANCE_PARCC_2OA11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2OA12,
            INSTANCE_PARCC_2OA12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2OA3,
            INSTANCE_PARCC_2OA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_2OA4,
            INSTANCE_PARCC_2OA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD11,
            INSTANCE_PARCC_3MD11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD12,
            INSTANCE_PARCC_3MD12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD21,
            INSTANCE_PARCC_3MD21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD22,
            INSTANCE_PARCC_3MD22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD5,
            INSTANCE_PARCC_3MD5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD6,
            INSTANCE_PARCC_3MD6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD7B1,
            INSTANCE_PARCC_3MD7B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3MD7D,
            INSTANCE_PARCC_3MD7D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF1,
            INSTANCE_PARCC_3NF1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF2,
            INSTANCE_PARCC_3NF2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF3A1,
            INSTANCE_PARCC_3NF3A1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF3A2,
            INSTANCE_PARCC_3NF3A2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF3B1,
            INSTANCE_PARCC_3NF3B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF3C,
            INSTANCE_PARCC_3NF3C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3NF3D,
            INSTANCE_PARCC_3NF3D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA1,
            INSTANCE_PARCC_3OA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA2,
            INSTANCE_PARCC_3OA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA31,
            INSTANCE_PARCC_3OA31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA32,
            INSTANCE_PARCC_3OA32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA33,
            INSTANCE_PARCC_3OA33
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA34,
            INSTANCE_PARCC_3OA34
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA4,
            INSTANCE_PARCC_3OA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA5,
            INSTANCE_PARCC_3OA5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA6,
            INSTANCE_PARCC_3OA6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_3OA8,
            INSTANCE_PARCC_3OA8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4INT2,
            INSTANCE_PARCC_4INT2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4INT3,
            INSTANCE_PARCC_4INT3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4INT4,
            INSTANCE_PARCC_4INT4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4INT7,
            INSTANCE_PARCC_4INT7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT1,
            INSTANCE_PARCC_4NBT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT2,
            INSTANCE_PARCC_4NBT2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT3,
            INSTANCE_PARCC_4NBT3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT51,
            INSTANCE_PARCC_4NBT51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT52,
            INSTANCE_PARCC_4NBT52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT61,
            INSTANCE_PARCC_4NBT61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NBT62,
            INSTANCE_PARCC_4NBT62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF1,
            INSTANCE_PARCC_4NF1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF2,
            INSTANCE_PARCC_4NF2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF3A,
            INSTANCE_PARCC_4NF3A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF3B1,
            INSTANCE_PARCC_4NF3B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF3C,
            INSTANCE_PARCC_4NF3C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF3D,
            INSTANCE_PARCC_4NF3D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF4A,
            INSTANCE_PARCC_4NF4A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF4B1,
            INSTANCE_PARCC_4NF4B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF4B2,
            INSTANCE_PARCC_4NF4B2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF4C,
            INSTANCE_PARCC_4NF4C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF5,
            INSTANCE_PARCC_4NF5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF6,
            INSTANCE_PARCC_4NF6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4NF7,
            INSTANCE_PARCC_4NF7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4OA11,
            INSTANCE_PARCC_4OA11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4OA12,
            INSTANCE_PARCC_4OA12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4OA2,
            INSTANCE_PARCC_4OA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4OA31,
            INSTANCE_PARCC_4OA31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_4OA32,
            INSTANCE_PARCC_4OA32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5G1,
            INSTANCE_PARCC_5G1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5G2,
            INSTANCE_PARCC_5G2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5INT1,
            INSTANCE_PARCC_5INT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5MD3,
            INSTANCE_PARCC_5MD3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5MD4,
            INSTANCE_PARCC_5MD4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5MD5B,
            INSTANCE_PARCC_5MD5B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5MD5C,
            INSTANCE_PARCC_5MD5C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT1,
            INSTANCE_PARCC_5NBT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT22,
            INSTANCE_PARCC_5NBT22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT3A,
            INSTANCE_PARCC_5NBT3A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT3B,
            INSTANCE_PARCC_5NBT3B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT4,
            INSTANCE_PARCC_5NBT4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT52,
            INSTANCE_PARCC_5NBT52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT6,
            INSTANCE_PARCC_5NBT6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT71,
            INSTANCE_PARCC_5NBT71
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT72,
            INSTANCE_PARCC_5NBT72
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT73,
            INSTANCE_PARCC_5NBT73
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NBT74,
            INSTANCE_PARCC_5NBT74
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF11,
            INSTANCE_PARCC_5NF11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF12,
            INSTANCE_PARCC_5NF12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF13,
            INSTANCE_PARCC_5NF13
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF14,
            INSTANCE_PARCC_5NF14
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF15,
            INSTANCE_PARCC_5NF15
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF21,
            INSTANCE_PARCC_5NF21
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF22,
            INSTANCE_PARCC_5NF22
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF31,
            INSTANCE_PARCC_5NF31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF32,
            INSTANCE_PARCC_5NF32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF4A1,
            INSTANCE_PARCC_5NF4A1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF4A2,
            INSTANCE_PARCC_5NF4A2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF4B1,
            INSTANCE_PARCC_5NF4B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF5A,
            INSTANCE_PARCC_5NF5A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF61,
            INSTANCE_PARCC_5NF61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF62,
            INSTANCE_PARCC_5NF62
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF7A,
            INSTANCE_PARCC_5NF7A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF7B,
            INSTANCE_PARCC_5NF7B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_5NF7C,
            INSTANCE_PARCC_5NF7C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE11,
            INSTANCE_PARCC_6EE11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE12,
            INSTANCE_PARCC_6EE12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE2C2,
            INSTANCE_PARCC_6EE2C2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE2A,
            INSTANCE_PARCC_6EE2A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE2B,
            INSTANCE_PARCC_6EE2B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE2C1,
            INSTANCE_PARCC_6EE2C1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE3,
            INSTANCE_PARCC_6EE3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE4,
            INSTANCE_PARCC_6EE4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE51,
            INSTANCE_PARCC_6EE51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE52,
            INSTANCE_PARCC_6EE52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE6,
            INSTANCE_PARCC_6EE6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE7,
            INSTANCE_PARCC_6EE7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE8,
            INSTANCE_PARCC_6EE8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6EE9,
            INSTANCE_PARCC_6EE9
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS12,
            INSTANCE_PARCC_6NS12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS5,
            INSTANCE_PARCC_6NS5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS6A,
            INSTANCE_PARCC_6NS6A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS6B1,
            INSTANCE_PARCC_6NS6B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS6B2,
            INSTANCE_PARCC_6NS6B2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS6C1,
            INSTANCE_PARCC_6NS6C1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS6C2,
            INSTANCE_PARCC_6NS6C2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS7A,
            INSTANCE_PARCC_6NS7A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS7B,
            INSTANCE_PARCC_6NS7B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS7C1,
            INSTANCE_PARCC_6NS7C1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS7C2,
            INSTANCE_PARCC_6NS7C2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS7D,
            INSTANCE_PARCC_6NS7D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6NS8,
            INSTANCE_PARCC_6NS8
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP1,
            INSTANCE_PARCC_6RP1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP2,
            INSTANCE_PARCC_6RP2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP3A,
            INSTANCE_PARCC_6RP3A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP3B,
            INSTANCE_PARCC_6RP3B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP3C1,
            INSTANCE_PARCC_6RP3C1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP3C2,
            INSTANCE_PARCC_6RP3C2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_6RP3D,
            INSTANCE_PARCC_6RP3D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7EE1,
            INSTANCE_PARCC_7EE1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7EE2,
            INSTANCE_PARCC_7EE2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7EE3,
            INSTANCE_PARCC_7EE3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7EE4A1,
            INSTANCE_PARCC_7EE4A1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7EE4A2,
            INSTANCE_PARCC_7EE4A2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS1A,
            INSTANCE_PARCC_7NS1A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS1B1,
            INSTANCE_PARCC_7NS1B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS1B2,
            INSTANCE_PARCC_7NS1B2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS1C1,
            INSTANCE_PARCC_7NS1C1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS1D,
            INSTANCE_PARCC_7NS1D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS2A1,
            INSTANCE_PARCC_7NS2A1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS2A2,
            INSTANCE_PARCC_7NS2A2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS2B1,
            INSTANCE_PARCC_7NS2B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS2B2,
            INSTANCE_PARCC_7NS2B2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS2C,
            INSTANCE_PARCC_7NS2C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7NS3,
            INSTANCE_PARCC_7NS3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP1,
            INSTANCE_PARCC_7RP1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP2A,
            INSTANCE_PARCC_7RP2A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP2B,
            INSTANCE_PARCC_7RP2B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP2C,
            INSTANCE_PARCC_7RP2C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP2D,
            INSTANCE_PARCC_7RP2D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP31,
            INSTANCE_PARCC_7RP31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_7RP32,
            INSTANCE_PARCC_7RP32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE1,
            INSTANCE_PARCC_8EE1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE2,
            INSTANCE_PARCC_8EE2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE3,
            INSTANCE_PARCC_8EE3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE41,
            INSTANCE_PARCC_8EE41
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE42,
            INSTANCE_PARCC_8EE42
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE51,
            INSTANCE_PARCC_8EE51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE52,
            INSTANCE_PARCC_8EE52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE61,
            INSTANCE_PARCC_8EE61
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE7B,
            INSTANCE_PARCC_8EE7B
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE8A,
            INSTANCE_PARCC_8EE8A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE8B1,
            INSTANCE_PARCC_8EE8B1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE8B2,
            INSTANCE_PARCC_8EE8B2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE8B3,
            INSTANCE_PARCC_8EE8B3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8EE8C,
            INSTANCE_PARCC_8EE8C
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F11,
            INSTANCE_PARCC_8F11
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F12,
            INSTANCE_PARCC_8F12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F2,
            INSTANCE_PARCC_8F2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F31,
            INSTANCE_PARCC_8F31
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F32,
            INSTANCE_PARCC_8F32
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F4,
            INSTANCE_PARCC_8F4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F51,
            INSTANCE_PARCC_8F51
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_8F52,
            INSTANCE_PARCC_8F52
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_AREI3,
            INSTANCE_PARCC_AREI3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_AREI111A,
            INSTANCE_PARCC_AREI111A
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_FIF4,
            INSTANCE_PARCC_FIF4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KCC3,
            INSTANCE_PARCC_KCC3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KCC5,
            INSTANCE_PARCC_KCC5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KCC6,
            INSTANCE_PARCC_KCC6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KCC7,
            INSTANCE_PARCC_KCC7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KNBT1,
            INSTANCE_PARCC_KNBT1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KOA1,
            INSTANCE_PARCC_KOA1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KOA2,
            INSTANCE_PARCC_KOA2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KOA3,
            INSTANCE_PARCC_KOA3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_MATHCOMPEVIDENCESTATEMENT,
            CODE_PARCC_KOA4,
            INSTANCE_PARCC_KOA4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2D1D,
            INSTANCE_PARCC_2NBT2D1D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2D2DNOREGROUP,
            INSTANCE_PARCC_2NBT2D2DNOREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2D2DREGROUP,
            INSTANCE_PARCC_2NBT2D2DREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2DMINUS1D,
            INSTANCE_PARCC_2NBT2DMINUS1D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2DMINUS2DNOREGROUP,
            INSTANCE_PARCC_2NBT2DMINUS2DNOREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2NBT2DMINUS2DREGROUP,
            INSTANCE_PARCC_2NBT2DMINUS2DREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2OA1DDIFFERENCES,
            INSTANCE_PARCC_2OA1DDIFFERENCES
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_2OA1DSUMS,
            INSTANCE_PARCC_2OA1DSUMS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT23DDIFFERENCES2REGROUPS,
            INSTANCE_PARCC_3NBT23DDIFFERENCES2REGROUPS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT23DSUMS2REGROUPS,
            INSTANCE_PARCC_3NBT23DSUMS2REGROUPS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT2DSUMSREGROUP,
            INSTANCE_PARCC_3NBT2DSUMSREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT3DDIFFERENCES1REGROUP,
            INSTANCE_PARCC_3NBT3DDIFFERENCES1REGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT3DDIFFERENCESNOREGROUP,
            INSTANCE_PARCC_3NBT3DDIFFERENCESNOREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3NBT3DSUMS1REGROUP,
            INSTANCE_PARCC_3NBT3DSUMS1REGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3OA1DX1D,
            INSTANCE_PARCC_3OA1DX1D
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_3OA2DDIVIDEDBY1DEVENLY,
            INSTANCE_PARCC_3OA2DDIVIDEDBY1DEVENLY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_424DDIFFERENCES1REGROUP,
            INSTANCE_PARCC_424DDIFFERENCES1REGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_424DSUMS2ORMOREREGROUP,
            INSTANCE_PARCC_424DSUMS2ORMOREREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_434DDIFFERENCES1REGROUP,
            INSTANCE_PARCC_434DDIFFERENCES1REGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_434DDIFFERENCESNOREGROUP,
            INSTANCE_PARCC_434DDIFFERENCESNOREGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_434DSUM1REGROUP,
            INSTANCE_PARCC_434DSUM1REGROUP
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_52DBY2DPRODUCT,
            INSTANCE_PARCC_52DBY2DPRODUCT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_53DBY2DPRODUCT,
            INSTANCE_PARCC_53DBY2DPRODUCT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_53DBY3DPRODUCT,
            INSTANCE_PARCC_53DBY3DPRODUCT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_54DBY1DPRODUCT,
            INSTANCE_PARCC_54DBY1DPRODUCT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_54DBY2DPRODUCT,
            INSTANCE_PARCC_54DBY2DPRODUCT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_6234DIGITBY2DQUOTIENTEVENLY,
            INSTANCE_PARCC_6234DIGITBY2DQUOTIENTEVENLY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_623DBY1DQUOTIENTEVENLY,
            INSTANCE_PARCC_623DBY1DQUOTIENTEVENLY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_625DBY2DQUOTIENTEVENLYORWITHREMAINDER,
            INSTANCE_PARCC_625DBY2DQUOTIENTEVENLYORWITHREMAINDER
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63DIFFERENCESCOMMONDECIMALS,
            INSTANCE_PARCC_63DIFFERENCESCOMMONDECIMALS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63DIFFERENCESMIXEDDECIMALS,
            INSTANCE_PARCC_63DIFFERENCESMIXEDDECIMALS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63PRODUCTDECIMALXDECIMAL,
            INSTANCE_PARCC_63PRODUCTDECIMALXDECIMAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63PRODUCTWHOLEXDECIMAL,
            INSTANCE_PARCC_63PRODUCTWHOLEXDECIMAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63QUOTIENTDECIMALBYWHOLE,
            INSTANCE_PARCC_63QUOTIENTDECIMALBYWHOLE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63QUOTIENTDECIMALDECIMAL,
            INSTANCE_PARCC_63QUOTIENTDECIMALDECIMAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63QUOTIENTWHOLEDECIMAL,
            INSTANCE_PARCC_63QUOTIENTWHOLEDECIMAL
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63SUMSCOMMONDECIMALS,
            INSTANCE_PARCC_63SUMSCOMMONDECIMALS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMSTRAND
            ),
            PROPERTY_PARCC_ITEMSTRAND,
            CODE_PARCC_63SUMSMIXEDDECIMALS,
            INSTANCE_PARCC_63SUMSMIXEDDECIMALS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_1,
            INSTANCE_PARCC_1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_2,
            INSTANCE_PARCC_2
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_3,
            INSTANCE_PARCC_3
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_4,
            INSTANCE_PARCC_4
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_5,
            INSTANCE_PARCC_5
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_6,
            INSTANCE_PARCC_6
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_7,
            INSTANCE_PARCC_7
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_8,
            INSTANCE_PARCC_8
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_9,
            INSTANCE_PARCC_9
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_10,
            INSTANCE_PARCC_10
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_11,
            INSTANCE_PARCC_11
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_12,
            INSTANCE_PARCC_12
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ITEMGRADE
            ),
            PROPERTY_PARCC_ITEMGRADE,
            CODE_PARCC_K,
            INSTANCE_PARCC_K
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYWORD
            ),
            PROPERTY_PARCC_KEYWORD,
            CODE_PARCC_ADD,
            INSTANCE_PARCC_ADD
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYWORD
            ),
            PROPERTY_PARCC_KEYWORD,
            CODE_PARCC_DIVIDE,
            INSTANCE_PARCC_DIVIDE
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYWORD
            ),
            PROPERTY_PARCC_KEYWORD,
            CODE_PARCC_MULTIPLY,
            INSTANCE_PARCC_MULTIPLY
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYWORD
            ),
            PROPERTY_PARCC_KEYWORD,
            CODE_PARCC_SUBTRACT,
            INSTANCE_PARCC_SUBTRACT
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_TESTLETGROUPID
            ),
            PROPERTY_PARCC_TESTLETGROUPID
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHIN1,
            INSTANCE_PARCC_ADDSUBTRACTWITHIN1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDWITHIN100,
            INSTANCE_PARCC_ADDWITHIN100
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHIN1,
            INSTANCE_PARCC_ADDSUBTRACTWITHIN1
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHMULTIDIGITDECIMALNUMBERS,
            INSTANCE_PARCC_ADDSUBTRACTWITHMULTIDIGITDECIMALNUMBERS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHIN20,
            INSTANCE_PARCC_ADDSUBTRACTWITHIN20
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHIN1000,
            INSTANCE_PARCC_ADDSUBTRACTWITHIN1000
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_ADDSUBTRACTWITHIN1000000,
            INSTANCE_PARCC_ADDSUBTRACTWITHIN1000000
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_DIVIDEWITHMULTIDIGITDECIMALNUMBERS,
            INSTANCE_PARCC_DIVIDEWITHMULTIDIGITDECIMALNUMBERS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_DIVISIONOFMULTIDIGITWHOLENUMBERS,
            INSTANCE_PARCC_DIVISIONOFMULTIDIGITWHOLENUMBERS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_MULTIPLYDIVIDEWITHIN100,
            INSTANCE_PARCC_MULTIPLYDIVIDEWITHIN100
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_MULTIPLYMULITDIGITWHOLENUMBERS,
            INSTANCE_PARCC_MULTIPLYMULITDIGITWHOLENUMBERS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_MULTIPLYWITHMULTIDIGITDECIMALNUMBERS,
            INSTANCE_PARCC_MULTIPLYWITHMULTIDIGITDECIMALNUMBERS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_FLUENCYSKILLAREA
            ),
            PROPERTY_PARCC_FLUENCYSKILLAREA,
            CODE_PARCC_SUBTRACTWITHIN100,
            INSTANCE_PARCC_SUBTRACTWITHIN100
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_MAXSCOREPOINTS
            ),
            PROPERTY_PARCC_MAXSCOREPOINTS
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_TEMPLATEID
            ),
            PROPERTY_PARCC_TEMPLATEID
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBJECT
            ),
            PROPERTY_PARCC_SUBJECT,
            CODE_PARCC_MATH,
            INSTANCE_PARCC_MATH
        );

        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SUBJECT
            ),
            PROPERTY_PARCC_SUBJECT,
            CODE_PARCC_ELA,
            INSTANCE_PARCC_ELA
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_USECALC
            ),
            PROPERTY_PARCC_USECALC,
            CODE_PARCC_USECALCYES,
            INSTANCE_PARCC_USECALCYES
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_USECALC
            ),
            PROPERTY_PARCC_USECALC,
            CODE_PARCC_USECALCNO,
            INSTANCE_PARCC_USECALCNO
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PVALUE
            ),
            PROPERTY_PARCC_PVALUE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PDISTRACTOR1
            ),
            PROPERTY_PARCC_PDISTRACTOR1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PDISTRACTOR2
            ),
            PROPERTY_PARCC_PDISTRACTOR2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PDISTRACTOR3
            ),
            PROPERTY_PARCC_PDISTRACTOR3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PDISTRACTOR4
            ),
            PROPERTY_PARCC_PDISTRACTOR4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PDISTRACTOR5
            ),
            PROPERTY_PARCC_PDISTRACTOR5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE0
            ),
            PROPERTY_PARCC_PSCORE0
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE1
            ),
            PROPERTY_PARCC_PSCORE1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE2
            ),
            PROPERTY_PARCC_PSCORE2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE3
            ),
            PROPERTY_PARCC_PSCORE3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE4
            ),
            PROPERTY_PARCC_PSCORE4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE5
            ),
            PROPERTY_PARCC_PSCORE5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_PSCORE6
            ),
            PROPERTY_PARCC_PSCORE6
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIALCORRECT
            ),
            PROPERTY_PARCC_POINTBISERIALCORRECT
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIAL1
            ),
            PROPERTY_PARCC_POINTBISERIAL1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIAL2
            ),
            PROPERTY_PARCC_POINTBISERIAL2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIAL3
            ),
            PROPERTY_PARCC_POINTBISERIAL3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIAL4
            ),
            PROPERTY_PARCC_POINTBISERIAL4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POINTBISERIAL5
            ),
            PROPERTY_PARCC_POINTBISERIAL5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFANY
            ),
            PROPERTY_PARCC_DIFANY
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFFEMALE
            ),
            PROPERTY_PARCC_DIFFEMALE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFAA
            ),
            PROPERTY_PARCC_DIFAA
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFHISPANIC
            ),
            PROPERTY_PARCC_DIFHISPANIC
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFASIAN
            ),
            PROPERTY_PARCC_DIFASIAN
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFNA
            ),
            PROPERTY_PARCC_DIFNA
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFSWD
            ),
            PROPERTY_PARCC_DIFSWD
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFELL
            ),
            PROPERTY_PARCC_DIFELL
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_DIFSES
            ),
            PROPERTY_PARCC_DIFSES
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_POMIT
            ),
            PROPERTY_PARCC_POMIT
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_APARAMETER
            ),
            PROPERTY_PARCC_APARAMETER
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_BPARAMETER
            ),
            PROPERTY_PARCC_BPARAMETER
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_CPARAMETER
            ),
            PROPERTY_PARCC_CPARAMETER
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP1
            ),
            PROPERTY_PARCC_STEP1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP2
            ),
            PROPERTY_PARCC_STEP2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP3
            ),
            PROPERTY_PARCC_STEP3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP4
            ),
            PROPERTY_PARCC_STEP4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP5
            ),
            PROPERTY_PARCC_STEP5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STEP6
            ),
            PROPERTY_PARCC_STEP6
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_N
            ),
            PROPERTY_PARCC_N
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_SETID
            ),
            PROPERTY_PARCC_SETID
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYRESPONSE1
            ),
            PROPERTY_PARCC_KEYRESPONSE1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYRESPONSE2
            ),
            PROPERTY_PARCC_KEYRESPONSE2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYRESPONSE3
            ),
            PROPERTY_PARCC_KEYRESPONSE3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_KEYRESPONSE4
            ),
            PROPERTY_PARCC_KEYRESPONSE4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_ADMINTYPE
            ),
            PROPERTY_PARCC_ADMINTYPE
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD1
            ),
            PROPERTY_PARCC_STANDARD1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD2
            ),
            PROPERTY_PARCC_STANDARD2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD3
            ),
            PROPERTY_PARCC_STANDARD3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD4
            ),
            PROPERTY_PARCC_STANDARD4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD5
            ),
            PROPERTY_PARCC_STANDARD5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_STANDARD6
            ),
            PROPERTY_PARCC_STANDARD6
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT1
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT1
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT2
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT2
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT3
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT3
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT4
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT4
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT5
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT5
        );
        
        $this->addInjectionRule(
            array(
                PARCC_METADATA_ROOT,
                PROPERTY_PARCC_EVIDENCESTATEMENT6
            ),
            PROPERTY_PARCC_EVIDENCESTATEMENT6
        );
    }

    public static function modifyItemByMetadata(Event $event){

        if($event instanceof MetadataModified){
            /** @var \oat\taoQtiItem\model\qti\metadata\imsManifest\ImsManifestMetadataValue $metadata */
            $metadataValue = $event->getMetadataValue();
            $metadataValueUri = $event->getMetadataUri();
            if($metadataValueUri === "http://www.parcconline.org/parcc-assessment/metadata#Calculator"){
                $item = $event->getResource();
                switch($metadataValue){
                    case "Scientific Calculator":
                    case "http://www.parcconline.org/parcc-assessment/metadata#SCIENTIFICCALCULATOR":
                        $calculatorIdentifier = "parccScientificCalculator";
                        $position = 10;
                        break;
                    case "5-Function Calculator":
                    case "http://www.parcconline.org/parcc-assessment/metadata#5FUNCTIONCALCULATOR":
                        $calculatorIdentifier = "parccFourFunctionCalculator";
                        $position = 9;
                        break;
                    case "No Calculator":
                    case "http://www.parcconline.org/parcc-assessment/metadata#NOCALCULATOR":
                        $calculatorIdentifier = "";
                        break;
                    default :
                        $calculatorIdentifier = "";
                        break;
                }

                if($calculatorIdentifier !== ""){
                    $registry = new CreatorRegistry();
                    //add the resource to the folder item
                    $registry->addRequiredResources($calculatorIdentifier,$item);

                    //add studenttoolbar resource
                    $studentRegistry = new StudentToolbarRegistry();
                    $studentRegistry->addRequiredResources('studentToolbar',$item);

                    try{
                        $itemContent = new \core_kernel_file_File($item->getUniquePropertyValue(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent')));
                        $parser = new Parser($itemContent->getAbsolutePath());
                        $itemQti = $parser->load();
                    }
                    catch(\core_kernel_classes_EmptyProperty $e){
                        \common_Logger::i('creating item content for item : '.$e->getResource()->getUri());
                        $itemQti = new Item();
                        //set item title
                        $itemQti->setAttribute('title', 'default title');
                        \taoItems_models_classes_ItemsService::singleton()->setItemContent($item, $itemQti->toXML());
                        $itemContent = new \core_kernel_file_File($item->getUniquePropertyValue(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent')));
                    }

                    $body = file_get_contents($itemContent->getAbsolutePath());

                    if(preg_match('/<infoControl/',$body) === 0){
                        //add namespace
                        $itemQti->addNamespace("m","http://www.w3.org/1998/Math/MathML");
                        $itemQti->addNamespace("pic","http://www.imsglobal.org/xsd/portableInfoControl");
                        $itemQti->addNamespace("html5","html5");
                        $body = $itemQti->toXML();
                    }

                    //if no student toolbar is present add it
                    if(preg_match('/id="studentToolbar"/', $body) === 0){

                        //add the css
                        $stylesheetCommon = new Stylesheet();
                        $attributes = array("type" => "text/css", "media" => "all");
                        $attributes['href'] = "studentToolbar/runtime/css/common.css";
                        $stylesheetCommon->setAttributes($attributes);
                        $itemQti->addStylesheet($stylesheetCommon);
                        $stylesheet = new Stylesheet();
                        $attributes['href'] = "studentToolbar/runtime/css/student-toolbar.css";
                        $stylesheet->setAttributes($attributes);
                        $itemQti->addStylesheet($stylesheet);
                        $body = $itemQti->toXML();


                        $studenttoolTpl = file_get_contents(__DIR__.'/../../../includes/tpl/studentToolbar.tpl');
                        $pattern = '/<itemBody>(\s+)/';
                        $replacement = "<itemBody>\n{$studenttoolTpl}\n";
                        $body = preg_replace($pattern, $replacement, $body);

                    }

                    //if the requested calculator is nor present add it
                    $pattern = '/id="'.$calculatorIdentifier.'"/';
                    if(preg_match($pattern,$body) === 0){
                        //get the calculator manifest and template
                        $manifestLink = __DIR__.'/../../../../parccStudentTools/views/js/picCreator/dev/'.$calculatorIdentifier.'/picCreator.json';

                        //file doesn't exists so don't modify the xml
                        if(!file_exists($manifestLink)){
                            return;
                        }
                        $manifest = json_decode(file_get_contents($manifestLink));
                        $calculatorTpl = file_get_contents(__DIR__.'/../../../includes/tpl/calculator.tpl');

                        //add the css
                        $parser = new Parser($body);
                        $itemQti = $parser->load();

                        $attributes = array("type" => "text/css", "media" => "all");
                        foreach($manifest->css as $css){
                            $stylesheet = new Stylesheet();
                            $attributes['href'] = $calculatorIdentifier.'/'.$css;
                            $stylesheet->setAttributes($attributes);
                            $itemQti->addStylesheet($stylesheet);
                        }
                        $body = $itemQti->toXML();


                        //feed the template with calculator info
                        $calculatorTpl = str_replace('{$title}', $manifest->label, $calculatorTpl);
                        $calculatorTpl = str_replace('{$id}', $calculatorIdentifier, $calculatorTpl);
                        $calculatorTpl = str_replace('{$entrypoint}', $manifest->entryPoint, $calculatorTpl);
                        $calculatorTpl = str_replace('{$position}', $position, $calculatorTpl);
                        $calculatorTpl = str_replace('{$description}', $manifest->description, $calculatorTpl);
                        $calculatorTpl = str_replace('{$img}', $manifest->icon, $calculatorTpl);
                        $calculatorTpl = str_replace('{$alt}', $manifest->short, $calculatorTpl);

                        //add the template to the item body
                        $pattern = '/<itemBody>(\s+)/';
                        $replacement = "<itemBody>\n{$calculatorTpl}\n";
                        $body = preg_replace($pattern, $replacement, $body);
                    }
                    //save the new xml
                    $itemContent->setContent($body);
                }
            }

        }

    }
}
