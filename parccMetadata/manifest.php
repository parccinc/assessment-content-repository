<?php
/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2014-2016 (original work) Open Assessment Technologies;
 *               
 */ 
$extpath = dirname(__FILE__).DIRECTORY_SEPARATOR;
$taopath = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'tao'.DIRECTORY_SEPARATOR;

return array(
	'name' => 'parccMetadata',
    'label' => 'PARCC Metadata',
	'description' => '',
    'license' => 'GPL-2.0',
    'version' => '1.7.0',
	'author' => 'Open Assessment Technologies',
	'requires' => array(
        'taoQtiItem' => '>=2.6'
    ),
	'models' => array(
		'http://www.tao.lu/Ontologies/TAOItem.rdf',
	    'http://www.parcconline.org/parcc-assessment/metadata'
	),
    'autoload' => array (
        'psr-4' => array(
            'oat\\parccMetadata\\' => dirname(__FILE__) . DIRECTORY_SEPARATOR
        )
    ),
    'install' => array(
        'rdf' => array(
            dirname(__FILE__) . '/model/ontology/itemmetadata_new.rdf',
            dirname(__FILE__) . '/model/ontology/widgets.rdf',
            dirname(__FILE__) . '/model/ontology/metadataIndex_new.rdf'
        ),
        'php' => array(
            dirname(__FILE__) . '/install/local/addMetadataExtractors.php',
            dirname(__FILE__) . '/install/local/addMetadataInjectors.php',
            dirname(__FILE__) . '/install/local/addMetadataGuardians.php',
            dirname(__FILE__) . '/install/local/addMetadataClassLookups.php',
            dirname(__FILE__) . '/install/local/addMetadataListeners.php'
        )
    ),
    'update' => 'oat\\parccMetadata\\scripts\\update\\Updater'
);
