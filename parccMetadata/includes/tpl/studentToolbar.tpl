<infoControl title="Student Toolbar" id="studentToolbar" class="sts-scope">
    <pic:portableInfoControl infoControlTypeIdentifier="studentToolbar" hook="studentToolbar/runtime/studentToolbar.js">
        <pic:resources location="http://imsglobal.org/pci/1.0.15/sharedLibraries.xml">
            <pic:libraries>
                <pic:lib id="IMSGlobal/jquery_2_1_1"/>
            </pic:libraries>
        </pic:resources>
        <pic:properties>
            <pic:entry key="position">100</pic:entry>
            <pic:entry key="toolbarId">sts-studentToolbar</pic:entry>
        </pic:properties>
        <pic:markup>
            <html5:div class="sts-scope">
                <html5:div id="sts-studentToolbar" class="sts-container sts-studentToolbar-container sts-movable-container">
                    <html5:div class="sts-title-bar">
                        <html5:div class="sts-title">Student Toolbar</html5:div>
                    </html5:div>
                    <html5:div class="sts-content">
                        <!-- Actual tools go here -->
                    </html5:div>
                </html5:div>
            </html5:div>
        </pic:markup>
    </pic:portableInfoControl>
</infoControl>