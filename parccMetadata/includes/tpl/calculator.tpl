<infoControl title="{$title}" id="{$id}" class="sts-scope sts-tmp-element">
    <pic:portableInfoControl infoControlTypeIdentifier="{$id}" hook="{$id}/{$entrypoint}">
        <pic:resources location="http://imsglobal.org/pci/1.0.15/sharedLibraries.xml">
            <pic:libraries>
                <pic:lib id="IMSGlobal/jquery_2_1_1"/>
            </pic:libraries>
        </pic:resources>
        <pic:properties>
            <pic:entry key="movable"/>
            <pic:entry key="theme">tao-light</pic:entry>
            <pic:entry key="position">{$position}</pic:entry>
            <pic:entry key="toolbarId">sts-studentToolbar</pic:entry>
            <pic:properties key="is">

            </pic:properties>
        </pic:properties>
        <pic:markup>
            <html5:div id="sts-{$id}" class="sts-toolcontainer" data-position="{$position}">
                <html5:span class="sts-button sts-launch-button" data-typeIdentifier="{$id}" title="{$description}">
                    <html5:img src="{$img}" alt="{$alt}"/>
                </html5:span>
                <html5:div class="sts-container sts-hidden-container sts-{$id}-container">
                    <!-- Tool content here -->
                </html5:div>
            </html5:div>
        </pic:markup>
    </pic:portableInfoControl>
</infoControl>