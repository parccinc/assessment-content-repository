<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 */

namespace oat\parccMetadata\test;


use oat\parccMetadata\model\metadata\ontology\MetadataInjector;
use oat\tao\test\TaoPhpUnitTestRunner;
use oat\taoQtiItem\model\qti\Parser;
use Prophecy\Argument;

//include_once dirname(__FILE__) . '/../includes/raw_start.php';

class MetadataInjectorTest extends TaoPhpUnitTestRunner
{
    private $fsPath;
    /**
     * @var \core_kernel_versioning_Repository
     */
    private $fileSource;

    public function setUp()
    {
        $this->fsPath = sys_get_temp_dir().DIRECTORY_SEPARATOR.'taoFileTestCase'.DIRECTORY_SEPARATOR;
        mkdir($this->fsPath);
        $this->fileSource = \core_kernel_fileSystem_FileSystemFactory::createFileSystem(
            new \core_kernel_classes_Resource(INSTANCE_GENERIS_VCS_TYPE_LOCAL), '', '', '', $this->fsPath, 'testFileSource', true
        );
    }

    public function tearDown()
    {
        if(!is_null($this->fileSource)){
            $this->fileSource->delete();
        }
        else {
            throw new \common_Exception('should not be null, something wrong happen during test');
        }
        \helpers_File::remove($this->fsPath);
    }

    public function testModifyItemByMetadataScientificCalculator() {

        $file = $this->fileSource->spawnFile(__DIR__.'/samples/qti.xml');

        $tmpDir = \tao_helpers_File::createTempDir();
        $property = new \core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent');
        $itemProphet = $this->prophesize('\core_kernel_classes_Resource');
        $itemProphet->getUri()->willReturn('http://myTest.case#ItemUri');
        $itemProphet->getUniquePropertyValue($property)->willReturn($file->getUri());


        $item = $itemProphet->reveal();
        $itemServiceProphet = $this->prophesize('\taoItems_models_classes_ItemsService');
        $itemServiceProphet->getItemFolder($item, '')->willReturn($tmpDir);

        $taoServiceProphet = $this->prophesize('\tao_models_classes_TaoService');
        $taoServiceProphet->getPlatformVersion()->willReturn('MyTAOVersion');

        $ref = new \ReflectionProperty('tao_models_classes_Service', 'instances');
        $ref->setAccessible(true);
        $instances = $ref->getValue(null);
        $ref->setValue(null, array(
            'taoItems_models_classes_ItemsService' => $itemServiceProphet->reveal(),
            'tao_models_classes_TaoService' => $taoServiceProphet->reveal()
        ));

        $eventProphet = $this->prophesize('oat\tao\model\event\MetadataModified');
        $eventProphet->getMetadataValue()->willReturn('Scientific Calculator');
        $eventProphet->getMetadataUri()->willReturn('http://www.parcconline.org/parcc-assessment/metadata#Calculator');
        $eventProphet->getResource()->willReturn($item);
        $event = $eventProphet->reveal();
        MetadataInjector::modifyItemByMetadata($event);

        //verify that the xml correspond to what we expect
        $this->assertEquals(str_replace(' ', '', file_get_contents(__DIR__.'/samples/qtiScientific.xml')), str_replace(' ','',$file->getFileContent()));
        //verify that the directory contains the two corresponding sub folders
        $this->assertContains('parccScientificCalculator', scandir($tmpDir));
        $this->assertContains('studentToolbar', scandir($tmpDir));
        $file->delete(true);
        \tao_helpers_File::remove($tmpDir,true);
        $ref->setValue(null, $instances);
    }

    public function testModifyItemByMetadataFourFunctionCalculator() {

        $file = $this->fileSource->spawnFile(__DIR__.'/samples/qti.xml');

        $tmpDir = \tao_helpers_File::createTempDir();
        $property = new \core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent');
        $itemProphet = $this->prophesize('\core_kernel_classes_Resource');
        $itemProphet->getUri()->willReturn('http://myTest.case#ItemUri');
        $itemProphet->getUniquePropertyValue($property)->willReturn($file->getUri());


        $item = $itemProphet->reveal();
        $itemServiceProphet = $this->prophesize('\taoItems_models_classes_ItemsService');
        $itemServiceProphet->getItemFolder($item, '')->willReturn($tmpDir);

        $taoServiceProphet = $this->prophesize('\tao_models_classes_TaoService');
        $taoServiceProphet->getPlatformVersion()->willReturn('MyTAOVersion');

        $ref = new \ReflectionProperty('tao_models_classes_Service', 'instances');
        $ref->setAccessible(true);
        $instances = $ref->getValue(null);
        $ref->setValue(null, array(
            'taoItems_models_classes_ItemsService' => $itemServiceProphet->reveal(),
            'tao_models_classes_TaoService' => $taoServiceProphet->reveal()
            ));

        $eventProphet = $this->prophesize('oat\tao\model\event\MetadataModified');
        $eventProphet->getMetadataValue()->willReturn('5-Function Calculator');
        $eventProphet->getMetadataUri()->willReturn('http://www.parcconline.org/parcc-assessment/metadata#Calculator');
        $eventProphet->getResource()->willReturn($item);
        $event = $eventProphet->reveal();
        MetadataInjector::modifyItemByMetadata($event);

        //verify that the xml correspond to what we expect
        $this->assertEquals(str_replace(' ','',file_get_contents(__DIR__.'/samples/qtiFourFunction.xml')), str_replace(' ','',$file->getFileContent()));
        //verify that the directory contains the two corresponding sub folders
        $this->assertContains('parccFourFunctionCalculator', scandir($tmpDir));
        $this->assertContains('studentToolbar', scandir($tmpDir));
        $file->delete(true);
        \tao_helpers_File::remove($tmpDir,true);
        $ref->setValue(null, $instances);
    }

    public function testModifyItemByMetadataNoCalculator() {

        $itemProphet = $this->prophesize('\core_kernel_classes_Resource');
        $itemProphet
            ->getPropertyValues()
            ->shouldNotBeCalled();

        $eventProphet = $this->prophesize('oat\tao\model\event\MetadataModified');
        $eventProphet->getMetadataValue()->willReturn('No Calculator');
        $eventProphet->getMetadataUri()->willReturn('http://www.parcconline.org/parcc-assessment/metadata#Calculator');
        $eventProphet->getResource()->willReturn($itemProphet->reveal());
        $event = $eventProphet->reveal();
        MetadataInjector::modifyItemByMetadata($event);
        $itemProphet
            ->getPropertyValues()
            ->shouldNotHaveBeenCalled();

    }

    public function testModifyItemByMetadataUnknow() {

        $eventProphet = $this->prophesize('oat\tao\model\event\MetadataModified');

        $eventProphet->getMetadataValue()->willReturn('test');
        $eventProphet->getMetadataUri()->willReturn('http://myTest.case#Unknowm_metadata_Uri');
        $eventProphet->getResource()->shouldNotBeCalled();
        $event = $eventProphet->reveal();

        MetadataInjector::modifyItemByMetadata($event);
        $eventProphet->getResource()->shouldNotHaveBeenCalled();
    }
}