<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *
 */
namespace oat\parccMetadata\scripts\update;

use common_cache_FileCache;
use common_Logger;
use core_kernel_classes_Property;
use core_kernel_classes_Resource;
use oat\generis\model\data\ModelManager;
use oat\generis\model\kernel\persistence\file\FileModel;
use oat\oatbox\event\EventManager;
use oat\tao\scripts\update\OntologyUpdater;
use oat\taoQtiItem\model\qti\Parser;
use oat\taoQtiItem\model\qti\Service;
use tao_helpers_data_GenerisAdapterRdf;

/**
 *
 * @author Joel Bout <joel@taotesting.com>
 */
class Updater extends \common_ext_ExtensionUpdater {

    /**
     * (non-PHPdoc)
     * @see common_ext_ExtensionUpdater::update()
     */
    public function update($initialVersion) {

        $currentVersion = $initialVersion;

        //migrate from 0.1 to 0.1.1
        if ($currentVersion == '0.1') {

            //import index
            $file = dirname(__FILE__).DIRECTORY_SEPARATOR.'metadataIndex_0_1_1.rdf';

            $adapter = new tao_helpers_data_GenerisAdapterRdf();
            if($adapter->import($file)){
                $currentVersion = '0.1.1';
            } else{
                common_Logger::w('Import failed for '.$file);
            }
        }

        //migrate from 0.1.1 to 0.1.2
        if ($currentVersion == '0.1.1') {

            // double check
            $index = new core_kernel_classes_Resource('http://www.parcconline.org/parcc-assessment/metadata#entityIDindex');
            $default = $index->getPropertyValues(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#IndexDefaultSearch'));

            if (count($default) == 0) {

                //no default search set, import
                $file = dirname(__FILE__).DIRECTORY_SEPARATOR.'metadataIndex_0_1_2.rdf';

                $adapter = new tao_helpers_data_GenerisAdapterRdf();
                if($adapter->import($file)){
                    $currentVersion = '0.1.2';
                } else{
                    common_Logger::w('Import failed for '.$file);
                }

            } else {
                common_Logger::w('Default Search already set');
                $currentVersion = '0.1.2';
            }
        }

        if ($currentVersion == '0.1.2') {
            OntologyUpdater::correctModelId(dirname(__FILE__).DIRECTORY_SEPARATOR.'metadataIndex_0_1_1.rdf');
            OntologyUpdater::correctModelId(dirname(__FILE__).DIRECTORY_SEPARATOR.'metadataIndex_0_1_2.rdf');
            $currentVersion = '0.1.3';
        }

        if ($currentVersion == '0.1.3') {
            // Revert old metadata model.
            $rdfToRevert = array(
                dirname(__FILE__) . '/../../model/ontology/itemmetadata.rdf',
                dirname(__FILE__) . '/../../model/ontology/metadataIndex.rdf'
            );

            foreach ($rdfToRevert as $rdf) {
                $modelFile = new FileModel(array('file' => $rdf));
                $modelRdf = ModelManager::getModel()->getRdfInterface();
                foreach ($modelFile->getRdfInterface() as $triple) {
                    $modelRdf->remove($triple);
                }
            }

            // Update metadata model.
            $rdfToUpdate = array(
                dirname(__FILE__) . '/../../model/ontology/itemmetadata_new.rdf',
                dirname(__FILE__) . '/../../model/ontology/metadataIndex_new.rdf',
                dirname(__FILE__) . '/../../model/ontology/widgets.rdf',
            );

            $adapter = new tao_helpers_data_GenerisAdapterRdf();
            foreach ($rdfToUpdate as $rdf) {
                $adapter->import($rdf);
            }

            // Deal with extractors, injectors, guards, ...
            $qtiService = Service::singleton();
            $metadataRegistry = $qtiService->getMetadataRegistry();
            $metadataRegistry->unregisterMetadataExtractor('oat\\taoQtiItem\\model\\qti\\metadata\\imsManifest\\ImsManifestMetadataExtractor');
            $metadataRegistry->registerMetadataExtractor('oat\\parccMetadata\\model\\metadata\\manifest\\ParccExtractor');
            $metadataRegistry->registerMetadataGuardian('oat\\parccMetadata\\model\\metadata\\guardians\\ParccGuardian');
            $metadataRegistry->registerMetadataClassLookup('oat\\parccMetadata\\model\\metadata\\classLookups\\ParccClassLookup');

            $currentVersion = '1.0.0';
        }

        if ($currentVersion === '1.0.0') {
            // Fixing missing search index for UIN metadata property.
            $adapter = new tao_helpers_data_GenerisAdapterRdf();
            $adapter->import(dirname(__FILE__) . '/metadataIndex_new_1_0_1.rdf');

            $currentVersion = '1.0.1';
        }

        if ($currentVersion === '1.0.1') {
            // Fixing missing properties.
            $adapter = new tao_helpers_data_GenerisAdapterRdf();

            $rdfs = array(
                dirname(__FILE__) . '/itemmetadata_new_1_1_0.rdf',
                dirname(__FILE__) . '/metadataIndex_1_1_0.rdf',
                dirname(__FILE__) . '/widgets_1_1_0.rdf',
            );

            foreach ($rdfs as $rdf) {
                $adapter->import($rdf);
            }

            $currentVersion = '1.1.0';
        }

        if ($currentVersion === '1.1.0') {
            $modelFile = new FileModel(array('file' => dirname(__FILE__) . '/itemmetadata_new_1_1_1.rdf'));
            $modelRdf = ModelManager::getModel()->getRdfInterface();
            foreach ($modelFile->getRdfInterface() as $triple) {
                $modelRdf->remove($triple);
            }

            $currentVersion = '1.1.1';
        }

        if ($currentVersion === '1.1.1') {
            // Depending on the installation procedure, injetors, extractors, ... were
            // sometimes not registered. Indeed, the installation scripts performing the
            // actual hook registrations were in the 'local' key of the manifest.php instead
            // of the 'install' key...

            $qtiService = Service::singleton();
            $metadataRegistry = $qtiService->getMetadataRegistry();
            $mapping = $metadataRegistry->getMapping();

            if (!in_array('oat\\parccMetadata\\model\\metadata\\classLookups\\ParccClassLookup', $mapping['classLookups'])) {
                $metadataRegistry->registerMetadataClassLookup('oat\\parccMetadata\\model\\metadata\\classLookups\\ParccClassLookup');
            }

            if (!in_array('oat\\parccMetadata\\model\\metadata\\manifest\\ParccExtractor', $mapping['extractors'])) {
                $metadataRegistry->registerMetadataExtractor('oat\\parccMetadata\\model\\metadata\\manifest\\ParccExtractor');
            }

            if (!in_array('oat\\parccMetadata\\model\\metadata\\guardians\\ParccGuardian', $mapping['guardians'])) {
                $metadataRegistry->registerMetadataGuardian('oat\\parccMetadata\\model\\metadata\\guardians\\ParccGuardian');
            }

            if (!in_array('oat\\parccMetadata\\model\\metadata\\ontology\\MetadataInjector', $mapping['injectors'])) {
                $metadataRegistry->registerMetadataInjector('oat\\parccMetadata\\model\\metadata\\ontology\\MetadataInjector');
            }

            $currentVersion = '1.1.2';
        }

        if ($currentVersion === '1.1.2') {
            $rdfDir = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'ontology';
            $files = array(
                $rdfDir.DIRECTORY_SEPARATOR.'itemmetadata_new.rdf',
                $rdfDir.DIRECTORY_SEPARATOR.'metadataIndex_new.rdf',
                $rdfDir.DIRECTORY_SEPARATOR.'widgets.rdf',
                __DIR__.DIRECTORY_SEPARATOR.'metadataIndex_new_1_0_1.rdf',
                __DIR__.DIRECTORY_SEPARATOR.'itemmetadata_new_1_1_0.rdf',
                __DIR__.DIRECTORY_SEPARATOR.'metadataIndex_1_1_0.rdf',
                __DIR__.DIRECTORY_SEPARATOR.'widgets_1_1_0.rdf'
            );
            $modelRdf = ModelManager::getModel()->getRdfInterface();
            foreach ($files as $file) {
                $modelFile = new FileModel(array('file' => $file));
                foreach ($modelFile->getRdfInterface() as $triple) {
                    $triple->modelid = 1;
                    $modelRdf->remove($triple);
                }
            }

            OntologyUpdater::syncModels();
            $currentVersion = '1.1.3';
        }

        if ($currentVersion === '1.1.3') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.4';
        }

        if ($currentVersion === '1.1.4') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.5';
        }

        if ($currentVersion === '1.1.5') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.6';
        }

        if ($currentVersion === '1.1.6') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.7';
        }

        if ($currentVersion === '1.1.7') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.8';
        }

        if ($currentVersion === '1.1.8') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.9';
        }

        if ($currentVersion === '1.1.9') {
            OntologyUpdater::syncModels();

            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-grade')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-grade');
            }
            
            $currentVersion = '1.1.10';
        }

        if ($currentVersion === '1.1.10') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.11';
        }

        if ($currentVersion === '1.1.11') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.12';
        }

        if ($currentVersion === '1.1.12') {
            OntologyUpdater::syncModels();

            $itemClass = new \core_kernel_classes_Class('http://www.parcconline.org/parcc-assessment/metadata#MathCompBucket');
            $items = $itemClass->getInstances(true);

            /** @var core_kernel_classes_Resource $item */
            foreach($items as $item){
                try{
                    $value = $item->getOnePropertyValue(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Math_Comp_Cluster'));

                    if(!is_null($value)){
                        $item->removePropertyValues(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Math_Comp_Cluster'));
                        $item->setPropertyValue(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Math_Cluster'), $value);
                    }

                } catch (\Exception $e) {
                    echo 'item : ' . $item->getUri() . ' ( ' . $item->getLabel() . ' ) has more than one property (http://www.parcconline.org/parcc-assessment/metadata#Math_Comp_Cluster)' . PHP_EOL;
                }
            }

            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-cluster')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-cluster');
            }
            $currentVersion = '1.1.13';
        }

        if ($currentVersion === '1.1.13') {
            OntologyUpdater::syncModels();

            $itemClass = new \core_kernel_classes_Class('http://www.tao.lu/Ontologies/TAOItem.rdf#Item');
            $items = $itemClass->getInstances(true);

            /** @var core_kernel_classes_Resource $item */
            foreach($items as $item){

                    // update item grade metadata
                    try{
                        $value = $item->getOnePropertyValue(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Item_Grade'));

                        if(!is_null($value)){
                            $item->removePropertyValues(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Item_Grade'));
                            $item->setPropertyValue(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Item_Grade'), $value);
                        }

                    } catch (\Exception $e) {
                        echo 'item : ' . $item->getUri() . ' ( ' . $item->getLabel() . ' ) has more than one property (http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Item_Grade)' . PHP_EOL;
                    }


                    // update max score points metadata
                    $values = $item->getPropertiesValues(
                        array(
                            new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#ELA_Decoding_Max_Score_Points'),
                            new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Max_Score_Points'),
                            new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#ELA_Vocab_Max_Score_Points'),
                            new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Math_Max_Score_Points')
                        )
                    );

                    foreach($values as $propertyUri => $valueArray){
                        $property = new core_kernel_classes_Property($propertyUri);
                        $item->removePropertyValues($property);
                        foreach($valueArray as $value){
                            $item->setPropertyValue(new core_kernel_classes_Property('http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'), $value);
                        }
                    }
            }

            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-itemtype')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-itemtype');
            }

            $currentVersion = '1.1.14';
        }

        if ($currentVersion === '1.1.14') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-passagetype')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-passagetype');
            }
            $currentVersion = '1.1.15';
        }

        if ($currentVersion === '1.1.15') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-reportingcategory')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-reportingcategory');
            }
            $currentVersion = '1.1.16';
        }

        if ($currentVersion === '1.1.16') {
            OntologyUpdater::syncModels();
            $currentVersion = '1.1.17';
        }
        
        if ($currentVersion === '1.1.17') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-fluencyskillarea')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-fluencyskillarea');
            }
            $currentVersion = '1.1.18';
        }
        
        if ($currentVersion === '1.1.18') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-subclaim')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-subclaim');
            }
            $currentVersion = '1.1.19';
        }
        
        if ($currentVersion === '1.1.19') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-grade')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-grade');
            }
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-itemgrade')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-itemgrade');
            }
            $currentVersion = '1.1.20';
        }

        if ($currentVersion === '1.1.20') {
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-grade')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-grade');
            }
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-itemgrade')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-itemgrade');
            }
            $currentVersion = '1.1.21';
        }

        if ($currentVersion === '1.1.21') {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-itemstrand')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-itemstrand');
            }
            $currentVersion = '1.1.22';
        }

        if ($currentVersion === '1.1.22') {

            $eventManager = $this->getServiceManager()->get(EventManager::CONFIG_ID);
            $eventManager->attach(
                'oat\\tao\\model\\event\\MetadataModified',
                 array('oat\\parccMetadata\\model\\metadata\\ontology\\MetadataInjector', 'modifyItemByMetadata')
            );
            $this->getServiceManager()->register(EventManager::CONFIG_ID, $eventManager);
            $currentVersion = '1.2.0';
        }

        if ($currentVersion === '1.2.0') {
            $itemClass = new \core_kernel_classes_Class('http://www.tao.lu/Ontologies/TAOItem.rdf#Item');
            //get all items that contains a calculator
            $options = array('recursive' => true);
            $scientificItems = $itemClass->searchInstances(array(
                'http://www.parcconline.org/parcc-assessment/metadata#Calculator' => 'http://www.parcconline.org/parcc-assessment/metadata#SCIENTIFICCALCULATOR'
            ),$options);

            $functionItems = $itemClass->searchInstances(array(
                'http://www.parcconline.org/parcc-assessment/metadata#Calculator' => 'http://www.parcconline.org/parcc-assessment/metadata#5FUNCTIONCALCULATOR'
            ), $options);

            foreach($scientificItems as $item){
                \common_Logger::i('item : '.$item->getUri());
                try{
                    $itemContent = new \core_kernel_file_File($item->getUniquePropertyValue(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent')));
                    $content = file_get_contents($itemContent->getAbsolutePath());
                } catch(\core_kernel_classes_EmptyProperty $e){
                    \common_Logger::i('No item content for item : '.$item->getUri());
                    continue;
                }
                //for each item see if there is a broken link "/.js"
                $content = preg_replace('/parccScientificCalculator\/\.js/', 'parccScientificCalculator/./runtime/scientificCalculator.js', $content);

                //save the new content
                $itemContent->setContent($content);
            }
            foreach($functionItems as $item){
                try{
                    $itemContent = new \core_kernel_file_File($item->getUniquePropertyValue(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent')));
                    $content = file_get_contents($itemContent->getAbsolutePath());
                } catch(\core_kernel_classes_EmptyProperty $e){
                    \common_Logger::i('No item content for item : '.$item->getUri());
                    continue;
                }
                //for each item see if there is a broken link "/.js"
                $content = preg_replace('/parccFourFunctionCalculator\/\.js/', 'parccFourFunctionCalculator/./runtime/fourFunctionCalculator.js', $content);

                //save the new content
                $itemContent->setContent($content);
            }

            $currentVersion = '1.2.1';
        }
        
        $this->setVersion($currentVersion);
        
        if ($this->isVersion('1.2.1')) {
            OntologyUpdater::syncModels();
            if (common_cache_FileCache::singleton()->has('parcccomboboxcache-parcc-itemtype')) {
                common_cache_FileCache::singleton()->remove('parcccomboboxcache-parcc-itemtype');
            }
            $this->setVersion('1.3.0');
        }
        
        if ($this->isVersion('1.3.0')) {
            OntologyUpdater::syncModels();
            $this->setVersion('1.4.0');
        }
        
        if ($this->isVersion('1.4.0')) {
            OntologyUpdater::syncModels();
            $this->setVersion('1.5.0');
        }
        
        if ($this->isVersion('1.5.0')) {
            OntologyUpdater::syncModels();
            $this->setVersion('1.6.0');
        }
        
        if ($this->isVersion('1.6.0')) {
            OntologyUpdater::syncModels();
            $this->setVersion('1.6.1');
        }
        
        if ($this->isVersion('1.6.1')) {
            OntologyUpdater::syncModels();
            $this->setVersion('1.7.0');
        }
    }
}
