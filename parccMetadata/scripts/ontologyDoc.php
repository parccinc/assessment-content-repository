<?php
// Helps you to create ontology documentation
//
// to run this file, place it under the extension parccMetadata, then run
// php pack.php path/to/output.html


//cli bootstrap
require_once __DIR__. '/../includes/raw_start.php';


$outputPath  = $argv[1];
if(empty($outputPath) || $outputPath == ''){
    echo "Please provide an output path in argument".PHP_EOL;
    exit();
}

/**
 * Method that gets information about
 * @param core_kernel_classes_Class $class
 * @param int $level default 1
 * @return string
 */
function getClassOutput($class, $level = 1){
    $level++;
    $classDoc = '<li><h' . $level . ' class="classLabel">' . $class->getLabel() . ' Class Description</h' . $level . '>';
    $classDoc .= '<div class="classUri"><strong>Class URI</strong><br/>' . prefixUri($class->getUri()) . '</div>';

    echo 'getting information of class : ' . $class->getLabel() . PHP_EOL;

    $classDoc .= getClassProperties($class);
    $classDoc .= '<ol>';
    foreach($class->getSubClasses() as $subClass){
        $classDoc .= getClassOutput($subClass, $level);
    }
    $classDoc .= '</ol>';
    $classDoc .= '</li>';
    return $classDoc;
}

/**
 * @param core_kernel_classes_Class $class
 * @return string
 * @throws common_exception_InvalidArgumentType
 * @throws core_kernel_persistence_Exception
 */
function getClassProperties($class){

    $tableBody = '';

    try{
        /** @var core_kernel_classes_Property $property */
        foreach($class->getProperties() as $property){
            echo 'getting information of property : '.$property->getLabel().PHP_EOL;
            /** @var core_kernel_classes_Resource $index */
            $index = $property->getOnePropertyValue(new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#PropertyIndex'));

            $tableBody .= '<tr>';
            $tableBody .= '<td>' . $property->getLabel() . '</td>';
            $tableBody .= '<td>' . prefixUri($property->getUri()) . '</td>';
            $tableBody .= '<td>' . prefixUri($property->getRange()->getUri()) . '</td>';

            if(!is_null($index)){
                $values = $index->getPropertiesValues(
                    array(
                        new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#IndexIdentifier'),
                        new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#IndexTokenizer'),
                        new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#IndexFuzzyMatching'),
                        new core_kernel_classes_Property('http://www.tao.lu/Ontologies/TAO.rdf#IndexDefaultSearch'),
                    )
                );
                $tableBody .= '<td>' . $index->getLabel() . '</td>';
                foreach($values as $uri => $value){
                    $value = current($value);
                    if($value instanceof core_kernel_classes_Resource){
                        $value = $value->getLabel();

                    } elseif($value instanceof core_kernel_classes_Literal){
                        $value = $value->literal;
                    }
                    $tableBody .= '<td>' . $value . '</td>';
                }
            }
            else{
                $tableBody .= '<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
            }
            $tableBody .= '</tr>';
        }
        if($tableBody !== ''){
            return "<table>
                        <thead>
                            <tr>
                                <th>Label</th><th>URI</th><th>Range</th><th>Index Name</th><th>Index Identifier</th><th>Index Tokenizer</th><th>Index default search</th><th>Index Fuzzy Matching</th>
                            </tr>
                        </thead>
                        <tbody>$tableBody</tbody>
                    </table>";
        }
        return '';
    } catch(Exception $e){
        echo $e->getMessage().PHP_EOL;
        return '';
    }
}

function prefixUri($uri)
{
    $prefixes = array(
        'generis' => 'http://www.tao.lu/Ontologies/generis.rdf',
        'rdfs' => 'http://www.w3.org/2000/01/rdf-schema',
        'taoItem' => 'http://www.tao.lu/Ontologies/TAOItem.rdf',
        'parcc' => 'http://www.parcconline.org/parcc-assessment/metadata'
    );
    
    $exploded = explode('#', $uri);
    if (count($exploded) === 2 && ($s = array_search($exploded[0], $prefixes)) !== false) {
        return str_replace($exploded[0] . '#', $s . ':', $uri);
    } else {
        return $uri;
    }
}

$ext = \common_ext_ExtensionsManager::singleton()->getExtensionById('parccMetadata');

$outputString = '<html><head><style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
}
tr:nth-child(even) {background: #ddd}
tr:nth-child(odd) {background: #fff}

ol {
    list-style-type: none;
    padding: 0;
    margin: 0;
}

table {
    width: 100%;
    font-size: 13px;
    margin-bottom: 40px;
}

table td {
    padding: 5px;
}

table th {
    padding: 7px;
    background-color: #ccc;
}

body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 13px;
    width: 90%;
    margin: 30px auto;
    text-shadow: 1px 1px 1px rgba(150, 150, 150, 0.5);
    color: #333;
}


.classUri {
    margin-bottom: 10px;
}

h1 {
    font-size: 24px;
}

h2, h3, h4, h5, h6 {
    font-size: 18px;
}

h1, h2, h3, h4, h5, h6 {
    border-bottom: 1px solid #000;
}

#disclaimer {
    margin-bottom: 50px;
}
</style></head><body>';
$outputString .= '<h1>PARCC Ontology Documentation Version ' . $ext->getVersion() . '</h1>';
$outputString .= '<div id="disclaimer">';
$outputString .= '<p>This document describes the Metadata Ontology developped for the PARCC project. It describes all the classes, properties and search indexes involved in the ontology.<br/>Please note that this document is generated automatically.</p>';
$outputString .= '<p>The following URI prefixes are used in this document for a clearer output:';
$outputString .= '<ul>';
$outputString .= '<li><strong>rdfs</strong>: http://www.w3.org/2000/01/rdf-schema#</li>';
$outputString .= '<li><strong>generis</strong>: http://www.tao.lu/Ontologies/generis.rdf#</li>';
$outputString .= '<li><strong>taoitem</strong>: http://www.tao.lu/Ontologies/TAOItem.rdf#</li>';
$outputString .= '<li><strong>parcc</strong>: http://www.parcconline.org/parcc-assessment/metadata#</li>';
$outputString .= '</ul>';
$outputString .= '</p>';
$outputString .= '</div>';
$outputString .= '<ol>' . getClassOutput(taoItems_models_classes_ItemsService::singleton()->getRootClass()) . '</ol>';
$outputString .= '</body></html>';


if(file_put_contents($outputPath, $outputString)){
    echo "File correctly written in " . $outputPath . PHP_EOL;
} else{
    echo "An error occurred when writing in " . $outputPath . PHP_EOL;
}
