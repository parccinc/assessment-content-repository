<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>
<script type="text/javascript">
requirejs.config({
   config: {
		'taoDataManager/controller/cleanup' : {}
		}
   }
);
</script>
<div class="form-content" style="max-height: 800px; width: 450px; margin-left: 150px;">
	<header class="section-header flex-container-full">
	    <h2>Cleanup Data</h2>
	    Select from the following list what you want to cleanup<br>
	    (This action is permanent and can't be reversed) <br><br>
	</header>
	<div class="main-container flex-container-main-form">
		<div>
			<div class="xhtml_form">
				<form method="post" id="cleanup" name="cleanup" action="/taoDataManager/Cleanup/index">
					<div class="bool-list">
						<input type="checkbox" value="ItemSection" name="ItemSection" id="ItemSection" <?=get_data("ItemSection")?> />&nbsp; 
						<label class="elt_desc">Items Revisions</label><br>
						<div style="margin-left: 50px;">
							<input type="checkbox" value="ItemSectionRevision" name="ItemSectionRevision" id="ItemSectionRevision" <?=get_data("ItemSectionRevision")?> />&nbsp;
							<label class="elt_desc">Leave Committed History</label><br>
						</div>
						<input type="checkbox" value="TestSection" name="TestSection" id="TestSection" <?=get_data("TestSection")?> />&nbsp;
						<label class="elt_desc">Tests Revisions</label><br>
						<div style="margin-left: 50px;">
							<input type="checkbox" value="TestSectionRevision" name="TestSectionRevision" id="TestSectionRevision" <?=get_data("TestSectionRevision")?> />&nbsp;
							<label class="elt_desc">Leave Committed History</label><br>
						</div>
						<input type="checkbox" value="TestBatteriesSection" name="TestBatteriesSection" id="TestBatteriesSection" <?=get_data("TestBatteriesSection")?> />&nbsp; 
						<label class="elt_desc">Test Batteries Revision History</label><br>
						<hr>
						<input type="checkbox" value="DeliveriesSection" name="DeliveriesSection" id="DeliveriesSection" <?=get_data("DeliveriesSection")?> />&nbsp;
						<label class="elt_desc">Deliveries</label><br>
						<div style="margin-left: 50px;">
							<input type="checkbox" value="ActiveDeliveriesSection" name="ActiveDeliveriesSection" id="ActiveDeliveriesSection" <?=get_data("ActiveDeliveriesSection")?> />&nbsp;
							<label class="elt_desc">Leave Most Recent Test Deliveries</label><br>
						</div>
						<input type="checkbox" value="ExecutionSection" name="ExecutionSection" id="ExecutionSection" <?=get_data("ExecutionSection")?> />&nbsp;
						<label class="elt_desc">Executions</label><br>
						<input type="checkbox" value="ResultSection" name="ResultSection" id="ResultSection" <?=get_data("ResultSection")?> />&nbsp;
						<label class="elt_desc">All Results</label><br>
						<hr>
						<input type="checkbox" value="TestTakerSection" name="TestTakerSection" id="TestTakerSection" <?=get_data("TestTakerSection")?> />&nbsp;
						<label class="elt_desc">All Test Takers</label><br>
						<input type="checkbox" value="GroupSection" name="GroupSection" id="GroupSection" <?=get_data("GroupSection")?> />&nbsp;
						<label class="elt_desc">All Groups</label><br>
						<input type="checkbox" value="UserSection" name="UserSection" id="UserSection" <?=get_data("UserSection")?> />&nbsp; 
						<label class="elt_desc">All Users</label><br>
						
						<div class="form-toolbar">
						<button type="submit" name="Save" id="Save" class="form-submitter btn-success small" value="Save">
							<span class="icon-bin"></span> Start Cleanup</button>
						</div>
					</div>
				</form>
	    	</div>
	    </div>
	</div>
</div>
<?php if(has_data("confirm")):?>
	<h2><font style="color:green;"><?=get_data('confirm')?></font></h2>
<?php endif;?>
<?php
	Template::inc('footer.tpl', 'tao');
?>
