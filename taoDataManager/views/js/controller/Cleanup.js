/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

define(['jquery', 'helpers', 'ui/modal'],
function ($, helpers) {
	'use strict';
	
	return {
        start : function(){
			$('#DeliveriesSection').bind("change", this.checkAllDeliveries);
			$('#ActiveDeliveriesSection').bind("change", this.checkAllDeliveries);
			$('#ExecutionSection').bind("change", this.checkAllDeliveries);
			$('#ResultSection').bind("change", this.checkAllDeliveries);
			$('#ItemSection').bind("change", this.checkAllDeliveries);
			$('#ItemSectionRevision').bind("change", this.checkAllDeliveries);
			$('#TestSection').bind("change", this.checkAllDeliveries);
			$('#TestSectionRevision').bind("change", this.checkAllDeliveries);
			
        },// start function
        
        checkAllDeliveries: function(event) {
        	var deliveriesSection        = $('#DeliveriesSection');
        	var activeDeliveriesSection  = $('#ActiveDeliveriesSection');
			var executionSection         = $('#ExecutionSection');
			var resultSection            = $('#ResultSection');
			var itemSection              = $('#ItemSection');
			var itemSectionRevision      = $('#ItemSectionRevision');
			var testSection              = $('#TestSection');
			var testSectionRevision      = $('#TestSectionRevision');
			
			switch (event.target.id) {
				case 'DeliveriesSection':
					if (deliveriesSection.is(':checked')){
						executionSection.prop('checked', true);
						resultSection.prop('checked', true);
					} else {
						activeDeliveriesSection.prop('checked', false);
					}
					break;
				case 'ActiveDeliveriesSection':
					if (activeDeliveriesSection.is(':checked')){
						deliveriesSection.prop('checked', true);
						executionSection.prop('checked', true);
						resultSection.prop('checked', true);
					}
					break;
				case 'ExecutionSection':
					if (executionSection.is(':checked')){
						resultSection.prop('checked', true);
					} else {
						deliveriesSection.prop('checked', false);
						activeDeliveriesSection.prop('checked', false);
					}
					break;
				case 'ResultSection':
					if (!resultSection.is(':checked')){
						deliveriesSection.prop('checked', false);
						activeDeliveriesSection.prop('checked', false);
						executionSection.prop('checked', false);
					}
					break;
				case 'ItemSection':
					if (!itemSection.is(':checked')){
						itemSectionRevision.prop('checked', false);
					}
					break;
				case 'ItemSectionRevision':
					if (itemSectionRevision.is(':checked')){
						itemSection.prop('checked', true);
					}
					break;
				case 'TestSection':
					if (!testSection.is(':checked')){
						testSectionRevision.prop('checked', false);
					}
					break;
				case 'TestSectionRevision':
					if (testSectionRevision.is(':checked')){
						testSection.prop('checked', true);
					}
					break;
					
			}
        }
	}
});
