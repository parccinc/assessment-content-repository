<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

$ext_path = dirname(__FILE__) . DIRECTORY_SEPARATOR;

return array(
	'name' => 'taoDataManager',
	'label' => 'Data Manager',
	'description' => 'Extension that manages tao data and perform data clean up',
	'license' => 'GPL-2.0',
	'version' => '1.0.0',
	'author' => 'Breakthrough Technologies, LLC',
	'requires' => array(
		'tao'         => '>=2.7.0',
		'taoRevision' => '>=1.0.3'
	),
	'models' => array(),
	'managementRole' => 'http://www.tao.lu/Ontologies/generis.rdf#taoDataManagerRole',
	'install' => array(
		'rdf' => array(),
		'php' => dirname(__FILE__) . '/scripts/install/install.php'
	),
	'uninstall' => dirname(__FILE__) . '/scripts/install/uninstall.php',
	'update' => 'taoDataManager_scripts_update_Updater',
	'constants' => array(
		# actions directory
		"DIR_ACTIONS"         => $ext_path . "actions" . DIRECTORY_SEPARATOR,

		# views directory
		"DIR_VIEWS"           => $ext_path . "views" . DIRECTORY_SEPARATOR,

		# default module name
		'DEFAULT_MODULE_NAME' => 'CleanUp',

		#default action name
		'DEFAULT_ACTION_NAME' => 'index',

		#BASE PATH: the root path in the file system (usually the document root)
		'BASE_PATH'           => $ext_path,

		#BASE URL (usually the domain root)
		'BASE_URL'            => ROOT_URL . 'taoDataManager/',

		#BASE WWW the web resources path
		'BASE_WWW'            => ROOT_URL . 'taoDataManager/views/'
	)
);
