<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Cleanup Controller
 *
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries
 *
 */
use qtism\data\content\xhtml\text\P;
class taoDataManager_actions_Cleanup extends tao_actions_SaSModule
{
	/**
	 * @var taoQtiTest_models_classes_QtiTestService
	 */
	private $qtiTestService = null;
	
	/**
	 * @var taoItems_models_classes_ItemsService
	 */
	private $itemService = null;
	
	/**
	 * @var tao_models_classes_service_FileStorage
	 */
	private $fileStorageService = null;
	
	/**
	 * @var common_persistence_SqlPersistence
	 */ 
	private $dbPersistence = null;
	
	/**
	 * logfile path
	 * @var string
	 */
	private $logFile = null; 

	/**
	 * constructor: initialize the service and the default data
	 */
	public function __construct()
	{
		parent::__construct();
		
		$this->dbPersistence = common_persistence_SqlPersistence::getPersistence('default');
		$this->logFile       = taoDataManager_helpers_Config::getLogFileSource() . "cleanup_" . date("Ymd_His") . ".log";

	}
	/**
	 * default cleanup action
	 * @return void
	 */
	public function index(){
		$checkedValue = " checked=\"checked\" ";
		if($this->hasRequestParameter("ItemSection")) {
			if($this->hasRequestParameter("ItemSectionRevision")) {
				$this->setData("ItemSectionRevision", $checkedValue);
				$revisionFlag = false;
			} else {
				$revisionFlag = true;
			}
			$this->cleanupItemSection($revisionFlag);
			$this->setData("ItemSection", $checkedValue);
		}
		if($this->hasRequestParameter("TestSection")) {
			if($this->hasRequestParameter("TestSectionRevision")) {
				$this->setData("TestSectionRevision", $checkedValue);
				$revisionFlag = false;
			} else {
				$revisionFlag = true;
			}
			$this->cleanupTestSection($revisionFlag);
			$this->setData("TestSection", $checkedValue);
		}
		if($this->hasRequestParameter("TestBatteriesSection")) {
			$this->cleanupTestBatteriesSection();
			$this->setData("TestBatteriesSection", $checkedValue);
		}
		if($this->hasRequestParameter("DeliveriesSection")) {
			if($this->hasRequestParameter("ActiveDeliveriesSection")) {
				$this->cleanupDeliverySection(true);
				$this->setData("ActiveDeliveriesSection", $checkedValue);
			} else {
				$this->cleanupDeliverySection(false);	
			}
			$this->setData("DeliveriesSection", $checkedValue);
			$this->setData("ExecutionSection", $checkedValue);
			$this->setData("ResultSection", $checkedValue);
		}
		if($this->hasRequestParameter("ExecutionSection") && !$this->hasRequestParameter("DeliveriesSection")) {
			$this->cleanupExecutionSection();
			$this->setData("ExecutionSection", $checkedValue);
		}
		if($this->hasRequestParameter("ResultSection") && !$this->hasRequestParameter("DeliveriesSection")) {
			$this->cleanupResultSection();
			$this->setData("ResultSection", $checkedValue);
		}
		if($this->hasRequestParameter("TestTakerSection")) {
			$this->cleanupTestTakerSection();
			$this->setData("TestTakerSection", $checkedValue);
		}
		if($this->hasRequestParameter("GroupSection")) {
			$this->cleanupGroupSection();
			$this->setData("GroupSection", $checkedValue);
		}
		if($this->hasRequestParameter("UserSection")) {
			$this->cleanupUserSection();
			$this->setData("UserSection", $checkedValue);
		}
		if($this->hasRequestParameter("ItemSection") || $this->hasRequestParameter("TestSection")
			 || $this->hasRequestParameter("TestBatteriesSection") || $this->hasRequestParameter("DeliveriesSection")
			 || $this->hasRequestParameter("ExecutionSection") || $this->hasRequestParameter("ResultSection")
			 || $this->hasRequestParameter("TestTakerSection") || $this->hasRequestParameter("GroupSection")
			 || $this->hasRequestParameter("UserSection")) {
			$this->setData("confirm", "Cleanup Script has finsihed running successfully");
		}
		$this->setView('index.tpl');
	}
	
	/**
	 * Cleanup user section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupUserSection() {
		// Debug vars
		$queries = array();
		
		$query = "	SELECT `subject`
					FROM `statements`
					WHERE `predicate` = '" . RDF_TYPE . "'
						AND `object` = '" . CLASS_TAO_USER . "'
						AND `subject` <> '" . LOCAL_NAMESPACE . DEFAULT_USER_URI_SUFFIX . "'";
		$result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	    
	    foreach ($result->fetchAll() as $row) {
	    	$userResource = new core_kernel_classes_Resource($row['subject']);
	    	$this->log("Deleting User: '" . $userResource->getLabel() . "' ID:'" . $row['subject'] . "'");
	    	$userResource->delete();
	    	$this->log("Deleting User: Completed");
	    }
	}
	
	/**
	 * Cleanup group section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupGroupSection() {
		// Debug vars
		$queries = array();
		
		$query = "	SELECT `subject`
					FROM `statements`
					WHERE `predicate` = '" . RDF_TYPE . "'
						AND `object` = '" . TAO_GROUP_CLASS . "'";
		$result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	    
	    foreach ($result->fetchAll() as $row) {
	    	$groupResource = new core_kernel_classes_Resource($row['subject']);
	    	$this->log("Deleting Group: '" . $groupResource->getLabel() . "' ID:'" . $row['subject'] . "'");
	    	$groupResource->delete();
	    	$this->log("Deleting Group: Completed");
	    }
	}
	
	/**
	 * Cleanup testtaker section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupTestTakerSection() {
		// Debug vars
		$queries = array();
		
		// Load taoTestTaker extension
		common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestTaker')->load();
		
		$query = "	SELECT `subject`
	    			FROM `statements`
					WHERE `predicate` = '" . RDF_TYPE . "'
						AND `object` = '" . TAO_CLASS_SUBJECT . "'";
		$result = $this->dbPersistence->query($query);
		    
		$queries[] = $query;
		
	    foreach ($result->fetchAll() as $row) {
	    	$testTakerResource = new core_kernel_classes_Resource($row['subject']);
	    	$this->log("Deleting TestTaker: '" . $testTakerResource->getLabel() . "' ID:'" . $row['subject'] . "'");
	    	$testTakerResource->delete();
	    	$this->log("Deleting TestTaker: Completed");
	    }
	}
	
	/**
	 * Cleanup result section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupResultSection() {
		// Debug vars
		$queries = array();
		
		$query = "DELETE FROM `results_kv_storage`";
		$result = $this->dbPersistence->exec($query);
		$this->log("Delete data from table 'results_kv_storage'");
		
		$queries[] = $query;
		
		$query = "DELETE FROM `results_storage`";
		$result = $this->dbPersistence->exec($query);
		$this->log("Delete data from table 'results_storage'");
		
		$queries[] = $query;
		
		$query = "DELETE FROM `variables_storage`";
		$result = $this->dbPersistence->exec($query);
		$this->log("Delete data from table 'variables_storage'");
		
		$queries[] = $query;
	}
	
	/**
	 * Cleanup execution section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupExecutionSection() {
		// Debug vars
		$queries = array();
		
		// Load taoDelivery extension
		common_ext_ExtensionsManager::singleton()->getExtensionById('taoDelivery')->load();
		
		$query = "	SELECT `subject`
	    			FROM `statements`
					WHERE `predicate` = '" . RDF_TYPE . "'
						AND `object` = '" . CLASS_DELVIERYEXECUTION . "'";
		$result = $this->dbPersistence->query($query);
		    
		$queries[] = $query;
		
	    foreach ($result->fetchAll() as $row) {
	    	$executionResource = new core_kernel_classes_Resource($row['subject']);
	    	$this->log("Deleting Execution: '" . $executionResource->getLabel() . "' ID:'" . $row['subject'] . "'");
	    	$executionResource->delete();
	    	$this->log("Deleting Execution: Completed");
	    }
	}
	
	/**
	 * Cleanp delivery section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param Boolean $leaveLastTest
	 */
	private function cleanupDeliverySection($leaveLastTest = false) {
		// Debug vars
		$queries = array();
		
		// Load taoDelivery extension
		common_ext_ExtensionsManager::singleton()->getExtensionById('taoDelivery')->load();
		
		// Load file storage service
		$this->fileStorageService = tao_models_classes_service_FileStorage::singleton();
		
		$deliveryArray = array();
		$testArray     = array();
		
		$query = "	SELECT `subject`
					FROM `statements`
					WHERE `predicate` = '" . RDF_TYPE . "'
						AND `object` = '" . CLASS_COMPILEDDELIVERY . "'";
		$result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	    
	    $param  = "";
	    foreach ($result->fetchAll() as $row) {
	    	$param .= " '". $row['subject'] . "',";
	    }
	    
	    if (strlen($param) > 0) {
	    	
		    $param = substr($param, 0,-1);
		    
		    
		    $query = "	SELECT `subject`, `predicate`, `object`
		    			FROM `statements`
		    			WHERE `subject` IN ($param)
		    			ORDER BY `subject`";
			$result = $this->dbPersistence->query($query);
		    
		    $queries[] = $query;
	    }
	    
	    $param  = "";
	    foreach ($result->fetchAll() as $row) {
	    	if (isset($deliveryArray[$row['subject']][$row['predicate']])) {
	    		if (is_array($deliveryArray[$row['subject']][$row['predicate']])) {
	    			if ($row['predicate'] == PROPERTY_COMPILEDDELIVERY_DIRECTORY) {
	    				$deliveryArray[$row['subject']][$row['predicate']][] = $this->fileStorageService->getDirectoryById($row['object'])->getPath();;
	    			} else {
	    				$deliveryArray[$row['subject']][$row['predicate']][] = $row['object'];
	    			}
	    		} else {
	    			$temp = $deliveryArray[$row['subject']][$row['predicate']];
	    			unset($deliveryArray[$row['subject']][$row['predicate']]);
	    			if ($row['predicate'] == PROPERTY_COMPILEDDELIVERY_DIRECTORY) {
		    			$deliveryArray[$row['subject']][$row['predicate']][] = $this->fileStorageService->getDirectoryById($temp)->getPath();
	    				$deliveryArray[$row['subject']][$row['predicate']][] = $this->fileStorageService->getDirectoryById($row['object'])->getPath();
	    			} else { 
		    			$deliveryArray[$row['subject']][$row['predicate']][] = $temp;
	    				$deliveryArray[$row['subject']][$row['predicate']][] = $row['object'];
	    			}
	    		}
	    	} else {
    			$deliveryArray[$row['subject']][$row['predicate']] = $row['object'];
	    	}
	    }
	    
	    foreach ($deliveryArray as $key => $delivery) {
	    	$testArray[$delivery['http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryOrigin']][$key] = $delivery[PROPERTY_COMPILEDDELIVERY_TIME];
	    	arsort($testArray[$delivery['http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDeliveryOrigin']]);
	    }
	    
	    // Keep last compiled delivery per test
	    if($leaveLastTest) {
	    	foreach($testArray as $key => $value) {
	    		$this->log("Keeping last compiled delivery: " . key($testArray[$key]));
	    		unset($testArray[$key][key($testArray[$key])]);
	    	}
	    }
	    
	    // Loop through test array and delete deliveries, executions and test
	    foreach($testArray as $testKey => $testValue) {
	    	foreach ($testValue as $deliveryKey => $deliveryValue) {
		    	$query = "	SELECT `subject`
		    				FROM `statements`
		    				WHERE `predicate` = '" . PROPERTY_DELVIERYEXECUTION_DELIVERY . "'
		    					AND `object` ='" . $deliveryKey . "'";
			    $result = $this->dbPersistence->query($query);
			    
			    $queries[] = $query;
			    
			    foreach ($result->fetchAll() as $row) {
			    	// Delete results
			    	$query = "	DELETE FROM `results_kv_storage` 
			    				WHERE `variables_variable_id` IN (	SELECT `variable_id`
			    													FROM `variables_storage`
			    													WHERE `results_result_id` = '" . $row['subject'] . "')";
			    	$result    = $this->dbPersistence->exec($query);
			    	$this->log("Deleting results in table `results_kv_storage` for: " . $row['subject']);
			    	$queries[] = $query;
			    	
	
			    	$query = "	DELETE FROM `results_storage`
			    				WHERE `result_id` = '" . $row['subject'] . "'";
			    	$result    = $this->dbPersistence->exec($query);
			    	$this->log("Deleting results in table `results_storage` for: " . $row['subject']);
			    	$queries[] = $query;
			    	
			    	$query = "	DELETE FROM `variables_storage`
			    				WHERE `results_result_id` = '" . $row['subject'] . "'";
			    	$result    = $this->dbPersistence->exec($query);
			    	$this->log("Deleting results in table `variables_storage` for: " . $row['subject']);
			    	$queries[] = $query;
			    	
			    	// Delete execution
			    	$executionResource = new core_kernel_classes_Resource($row['subject']);
	    			$this->log("Deleting Execution: '" . $executionResource->getLabel() . "' ID:'" . $row['subject'] . "'");
			    	$executionResource->delete();
			    	$this->log("Deleting Execution: Completed");
			    }
			    
			    // Delete delivery files
			    foreach($deliveryArray[$deliveryKey][PROPERTY_COMPILEDDELIVERY_DIRECTORY] as $path) {
			    	$this->log("Start Deleting path: $path");
			    	$this->deletePath($path);
			    	$this->log("Deleting path completed: $path");
			    }
			    
			    // Delete db object
		    	$deliveryResource = new core_kernel_classes_Resource($deliveryKey);
	    		$this->log("Deleting Delivery: '" . $deliveryResource->getLabel() . "' ID:'$deliveryKey'");
		    	$deliveryResource->delete();
	    		$this->log("Deleting Delivery: Completed");
	    	}
	    }
	}
	
	/**
	 * Cleanup test section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param Boolean $revisionFlag
	 */
	private function cleanupTestSection($revisionFlag) {
		// Load QTI test service
		$this->qtiTestService = taoQtiTest_models_classes_QtiTestService::singleton();
		
		// Get test data directory
		$qtiTestDirectory        = $this->qtiTestService->getQtiTestDirectory();
		$test["fileSystem"]      = new core_kernel_classes_Class($qtiTestDirectory->getFileSystem()->getUri());
		$test["qtiTestFolder"]   = current($qtiTestDirectory->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_FILE_FILEPATH)));
		$test["path"]            = $qtiTestDirectory->getAbsolutePath();
		$test["relativePath"]    = current($qtiTestDirectory->getRelativePath());
		$test["uri"]             = $qtiTestDirectory->getUri();
		$test["revisionEnabled"] = $test["fileSystem"]->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_GENERIS_VERSIONEDREPOSITORY_ENABLED))->getUri();
		
		// Check if revision is enabled and get path
		if ($test["revisionEnabled"] == GENERIS_TRUE) {
			$test["revisionPath"] = current($test["fileSystem"]->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_GENERIS_VERSIONEDREPOSITORY_PATH)));
			$this->log("Starting test section cleanup: revision flag: '$revisionFlag' path: '" . $test["revisionPath"] . "' content folder: '" . $test["relativePath"] . "'");
			$this->cleanupSection($test["revisionPath"], array("contentFolder" => $test["relativePath"], "revision" => $revisionFlag));
		} else {
			$this->log("Starting test section cleanup: revision flag: '$revisionFlag' path: '" . $test["path"] . "'");
			$this->cleanupSection($test["path"], array("revision" => $revisionFlag));
		}
		$this->log("Cleanup for test section: Completed");
	}
	
	/**
	 * Cleanup item section
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param Boolean $revisionFlag
	 */
	private function cleanupItemSection($revisionFlag) {
		// Help search items with in db
		$postfix = "/itemContent/en-US";
		// Load item services
		$this->itemService = taoItems_models_classes_ItemsService::singleton();
			
		// Get test data directory
		$ItemDefaultFileSource   = $this->itemService->getDefaultFileSource();
		$item["path"]            = current($ItemDefaultFileSource->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_GENERIS_VERSIONEDREPOSITORY_PATH)));
		$this->log("Starting item section cleanup: revision flag: '$revisionFlag' path: '" . $item["path"] . "' postfix: '$postfix'");
		$this->cleanupSection($item["path"], array("postfix" => $postfix, "revision" => $revisionFlag));
		$this->log("Cleanup for item section: Completed");
	}
	
	/**
	 * Cleanup test batteries
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	private function cleanupTestBatteriesSection() {
		// Debug vars
		$queries = array();
		
		// Load taoTestBatteries extension
		common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries')->load();
		
		$query = "	SELECT s.`subject`
					FROM `statements` s
					WHERE s.`predicate` = '" . RDF_TYPE . "'
					AND s.`object` = '" . CLASS_TEST_BATTERY . "'";
		$result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	    
	    foreach ($result->fetchAll() as $row) {
	    	$query = "	DELETE
	    				FROM `revision`
	    				WHERE `resource` = '". $row['subject'] . "'";
	    	$this->dbPersistence->exec($query);
			$this->log("Delete data from table 'revision' for TestBattery '". $row['subject'] . "'");
	    	$queries[] = $query;
	    	
	    	$query = "	DELETE
	    				FROM `revision_data`
	    				WHERE `subject` = '". $row['subject'] . "'";
	    	$this->dbPersistence->exec($query);
			$this->log("Delete data from table 'revision_data' for TestBattery '". $row['subject'] . "'");
	    	$queries[] = $query;
	    }
	}
	/**
	 * Cleanup filesystem and database by path
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param array $options[path, contentFolder]
	 */
	private function cleanupSection($path, array $options = array()) {
		// Settings configurations
		(isset($options["contentFolder"]))? $contentFolder = $options["contentFolder"]:     $contentFolder = null;
		(isset($options["postfix"]))?       $postfix       = $options["postfix"]:           $postfix       = null;
		(isset($options["deleteObject"]))?  $postfix       = $deleteObject["deleteObject"]: $deleteObject  = true;
		(isset($options["deletePath"]))?    $deletePath    = $options["deletePath"]:        $deletePath    = true;
		(isset($options["revision"]))?      $revision      = $options["revision"]:          $revision      = true;
		
		// Debug vars
		$queries = array();

		$sectionArray = array();
		
		// Loop through path folders to get the folders needed to delete
		$param = "";
	    if (is_dir($path) === true) {
	    	$files = new DirectoryIterator($path);

	    	foreach ($files as $fileInfo)
	    	{
	    		if (!$fileInfo->isDot()) {

	    			//$file = str_replace($source, "", $file);
	    			if ($fileInfo == $contentFolder) {
	    				continue;	
	    			} else {
	    				//echo $fileInfo->getFilename() . "<br>";
	    				$param .= " '" . $fileInfo->getFilename() . "', '" . $fileInfo->getFilename(). $postfix . "',";
	    				$sectionArray["path"][$fileInfo->getFilename()] = false;
	    			}
	    		}
	    	}
	    }
		
	    $param = substr($param, 0,-1);
	    
	    // Find the object related to folders from db
	    $query  = "SELECT  `subject`, `object` FROM `statements` WHERE `object` IN ($param)";
	    $result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	    
	    $param  = "";
	    foreach ($result->fetchAll() as $row) {
	    	$param .= " '" . $row["subject"] . "',";
	    	if (strripos($row["object"], $postfix)) {
	    		$row["object"] = substr($row["object"], 0, strlen($row["object"]) - strlen($postfix));
	    	}
	    	$sectionArray["path"][$row["object"]]  = $row["subject"]; 
	    	$sectionArray["fileObject"][$row["subject"]] = false;
	    }
		$param = substr($param, 0,-1);
		
		// Get the resources for objects found in db
		$query = "	SELECT s.`subject`, s.`object`
					FROM `statements` s
					WHERE s.`object` IN ($param)";
		$result = $this->dbPersistence->query($query);
		
		$queries[] = $query;
		
		foreach ($result->fetchAll() as $row) {
	    	$sectionArray["fileObject"][$row["object"]] = $row["subject"];
	    }
	    
		$query = "  SELECT r.`id`, r.`version`, rd.`subject`, rd.`object` 
					FROM `revision` r, `revision_data` rd WHERE `object` IN ($param)
						AND r.`id` = rd.`revision`";
	    $result = $this->dbPersistence->query($query);
	    
	    $queries[] = $query;
	
	    foreach ($result->fetchAll() as $row) {
	    	$param .= " '" . $row["subject"] . "',";
	    	$sectionArray["dbObject"][$row["subject"]]["revision"][$row["version"]] = array("path" => array_search($row["object"],$sectionArray["path"]), "object" =>$row["object"], "versionId" => $row["id"]);
	    	if (in_array($row["object"], $sectionArray["path"])) {
	    		unset($sectionArray["path"][array_search($row["object"],$sectionArray["path"])]);
	    	}
	    	if (isset($sectionArray["fileObject"][$row["object"]])) {
	    		unset($sectionArray["fileObject"][$row["object"]]);
	    	}
	    	if (in_array($row["subject"],$sectionArray["fileObject"])) {
	    		$sectionArray["dbObject"][$row["subject"]]["content"]["path"] = array_search(array_search($row["subject"],$sectionArray["fileObject"]),$sectionArray["path"]);
	    		$sectionArray["dbObject"][$row["subject"]]["content"]["object"] = array_search($row["subject"],$sectionArray["fileObject"]);
	    		unset ($sectionArray["fileObject"][$sectionArray["dbObject"][$row["subject"]]["content"]["object"]]);
	    		unset ($sectionArray["path"][$sectionArray["dbObject"][$row["subject"]]["content"]["path"]]);
	    	}
	    }
	    
	    foreach ($sectionArray["path"] as $key => $value) {
	    	if (isset($sectionArray["fileObject"][$value]) && $sectionArray["fileObject"][$value] !== false) {
	    		$sectionArray["dbObject"][$sectionArray["fileObject"][$value]]["content"]["path"] = $key;
	    		$sectionArray["dbObject"][$sectionArray["fileObject"][$value]]["content"]["object"] = $sectionArray["path"][$key];
	    		
	    		unset($sectionArray["path"][$key]);
	    		unset($sectionArray["fileObject"][$value]);
	    	}
	    }
	    
	    unset($sectionArray["fileObject"]);
	    
	    // Start deleting released (uncommitted) revisions
	    $this->log("Section cleanup: start deleting uncommitted revisions.");
	    foreach($sectionArray["path"] as $key => $value) {
	    	if ($value !== false && $deleteObject) {
	    		$this->deleteObject($value);
	    	}
	    	if ($deletePath) {
	    		$this->deletePath($path . $key);
	    	}
	    }
	    $this->log("Section cleanup: done deleting uncommitted revisions.");
	    
	    // Delete revision history from filesystem, revision, revision_data and objects from statments
	    if ($revision) {
	    	$this->log("Section cleanup: start deleting committed revisions.");
			foreach($sectionArray["dbObject"] as $sectionKey => $sectionValue) {
		    	foreach($sectionValue["revision"] as $revisionKey => $revisionValue) {
		    		if ($deleteObject) {
		    			$this->deleteObject($revisionValue["object"]);
		    		}
		    		if ($deletePath) {
		    			$this->deletePath($path . $revisionValue["path"]);
		    		}
		    		
		    		if ($deleteObject) {
			    		$query = "	DELETE
									FROM `revision` 
									WHERE `resource` = '$sectionKey'
									AND `version` = $revisionKey";
			    		$result = $this->dbPersistence->exec($query);
			    		$this->log("Section cleanup: delete data from table `revision` resourse: '$sectionKey' version: '$revisionKey'");
			    		
			    		$queries[] = $query;
			    		
			    		$query = "	DELETE
									FROM `revision_data`
									WHERE `subject` = '$sectionKey'
											AND `revision` = " . $revisionValue["versionId"];
			    		$result = $this->dbPersistence->exec($query);
			    		$this->log("Section cleanup: delete data from table `revision_data` subject: '$sectionKey' revision: '" . $revisionValue["versionId"] . "'");
			    		
			    		$queries[] = $query;
		    		}
		    	}
		    }
	    }
	}
	
	/**
	 * Delete path from filesystem
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param array $path
	 */
	private function deletePath($item) {
		$files = array_diff(scandir($item), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$item/$file")) ? $this->deletePath("$item/$file") : unlink("$item/$file");
			$this->log("Delete directory/file '$item/$file'");
		}
		$this->log("Delete directory '$item'");
		return rmdir($item);
	}
	
	/**
	 * Delete object from database
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string $uri
	 */
	private function deleteObject($uri) {
		$object = new core_kernel_classes_Resource($uri);
	    $this->log("Deleting Object: '" . $object->getLabel() . "' ID:'$uri'");
		$object->delete();
	    $this->log("Deleting Object: Completed");
	}
	
	/**
	 * Cleanup log function
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string $message
	 */
	private function log($message) {
		// Prepare log message
		$log  = "[" . date("Y-m-d H:s:i") . "] " . debug_backtrace()[1]['function'] . ": $message".PHP_EOL;
		
		//Save string to log, use FILE_APPEND to append.
		file_put_contents($this->logFile, $log, FILE_APPEND);
	}
}
