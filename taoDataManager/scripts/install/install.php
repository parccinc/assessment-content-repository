<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\accessControl\func\AclProxy;
use oat\tao\model\accessControl\func\AccessRule;

require_once dirname(__FILE__) . '/../../../tao/includes/raw_start.php';
// create extension abstract management role
$roleClass = new core_kernel_classes_Class('http://www.tao.lu/Ontologies/TAO.rdf#ManagementRole');
// management role properties: label, comment, URI
$resource = $roleClass->createInstance('Data Manager', 'The Data Manager Role', 'http://www.tao.lu/Ontologies/generis#taoDataManagerRole');
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAO.rdf#BackOfficeRole']);

// grant access permissions
$extensionRoles = [
	['grant', 'http://www.tao.lu/Ontologies/generis#taoDataManagerRole', ['ext' => 'taoDataManager']]
	];
foreach ($extensionRoles as $tableEntry) {
	$accessRule = new AccessRule($tableEntry[0], $tableEntry[1], $tableEntry[2]);
	AclProxy::applyRule($accessRule);
}


// grant access permissions to global manager
$managementRoleClass = new core_kernel_classes_Class(INSTANCE_ROLE_GLOBALMANAGER);
$managementRoleClass->setPropertiesValues(array('http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/generis#taoDataManagerRole'));

// add file repository for batteries under TAO's main data folder
$source = tao_models_classes_FileSourceService::singleton()->addLocalSource('logDirectory', FILES_PATH . 'taoDataManager' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR);
$extension = common_ext_ExtensionsManager::singleton()->getExtensionById('taoDataManager');
$extension->setConfig('logFileSource', $source->getUri());
