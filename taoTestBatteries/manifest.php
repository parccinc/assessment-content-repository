<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

$ext_path = dirname(__FILE__) . DIRECTORY_SEPARATOR;

return [
	'name' => 'taoTestBatteries',
	'label' => 'Test Batteries',
	'description' => 'Test Batteries manages test forms and their revisions as test deliveries',
	'license' => 'GPL-2.0',
	'version' => '1.28.13',
	'author' => 'Breakthrough Technologies, LLC',
	'requires' => [
		'tao' => '>=2.7.0'
	],
	'models' => [
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf',
		'http://www.tao.lu/Ontologies/taoTestBatteryForm.rdf',
		'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf'
	],
	'managementRole' => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryManagerRole',
	'install' => [
		'rdf' => [
				dirname(__FILE__) . '/models/ontology/taoTestBattery.rdf',
				dirname(__FILE__) . '/models/ontology/taoTestBatteryForm.rdf',
				dirname(__FILE__) . '/models/ontology/taoTestBatteryFormRevision.rdf'
		],
		'php' => dirname(__FILE__) . '/scripts/install/install.php'
	],
	'uninstall' => dirname(__FILE__) . '/scripts/install/uninstall.php',
	'update' => 'taoTestBatteries_scripts_update_Updater',
	'constants' => [
		# actions directory
		"DIR_ACTIONS"         => $ext_path . "actions" . DIRECTORY_SEPARATOR,

		# views directory
		"DIR_VIEWS"           => $ext_path . "views" . DIRECTORY_SEPARATOR,

		# default module name
		'DEFAULT_MODULE_NAME' => 'TestBatteries',

		#default action name
		'DEFAULT_ACTION_NAME' => 'editTestBatteriesClass',

		#BASE PATH: the root path in the file system (usually the document root)
		'BASE_PATH'           => $ext_path,

		#BASE URL (usually the domain root)
		'BASE_URL'            => ROOT_URL . 'taoTestBatteries/',

		#BASE WWW the web resources path
		'BASE_WWW'            => ROOT_URL . 'taoTestBatteries/views/'
	]
];
