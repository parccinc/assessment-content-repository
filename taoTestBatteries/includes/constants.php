<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

$todefine = [

	'CLASS_TEST_BATTERY'                                         => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestBattery',
	'PROPERTY_TEST_BATTERY_TESTINGPROGRAM'                       => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestingProgram',
	'PROPERTY_TEST_BATTERY_SUBJECT'                              => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Subject',
	'PROPERTY_TEST_BATTERY_GRADE'                                => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Grade',
	'PROPERTY_TEST_BATTERY_ITEMSELECTIONALGORITHM'               => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ItemSelectionAlgorithm',
	'PROPERTY_TEST_BATTERY_SECURITY'                             => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Security',
	'PROPERTY_TEST_BATTERY_SCOREREPORT'                          => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ScoreReport',
	'PROPERTY_TEST_BATTERY_MULTIMEDIA'                           => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Multimedia',
	'PROPERTY_TEST_BATTERY_TESTSCORING'                          => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestScoring',
	'PROPERTY_TEST_BATTERY_TESTPERMISSIONS'                      => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Permission',
	'PROPERTY_TEST_BATTERY_TESTDESCRIPTION'                      => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestDescription',
	'PROPERTY_TEST_BATTERY_TESTPRIVACY'                          => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestPrivacy',
	'PROPERTY_TEST_BATTERY_LOCK'                                 => 'http://www.tao.lu/Ontologies/TAO.rdf#Lock',
	'PROPERTY_TEST_BATTERY_ADP_BATTERY_ID'						 => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#AdpBatteryId',

	'CLASS_FORM'                                                 => 'http://www.tao.lu/Ontologies/taoTestBatteryForm.rdf#Form',
	'PROPERTY_FORM_TEST'                                         => 'http://www.tao.lu/Ontologies/taoTestBatteryForm.rdf#DeliveryContent',
	'PROPERTY_FORM_TESTBATTERY'                                  => 'http://www.tao.lu/Ontologies/taoTestBatteryForm.rdf#FormTestBattery',
	'PROPERTY_FORM_ADP_FORM_ID'									 => 'http://www.tao.lu/Ontologies/taoTestBatteryForm.rdf#AdpFormId',

	'PROPERTY_HAS_PUBLISHED_REVISION'                            => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#HasPublishedRevisions',
	'PROPERTY_HAS_COMPILED_REVISION'                             => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#HasCompiledRevisions',

	'CLASS_REVISION'                                             => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FormRevision',
	'PROPERTY_REVISION_NUMBER'                                   => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FormRevisionNumber',
	'PROPERTY_REVISION_COMPILATION_TIME'                         => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#CompilationTime',
	'PROPERTY_REVISION_PUBLISHING_TIME'                          => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishTime',
	'PROPERTY_REVISION_PUBLISHING_STATUS'                        => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishStatus',
	'PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED'            => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Unpublished',
	'PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING'   => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#ScheduledPublishing',
	'PROPERTY_REVISION_PUBLISHING_STATUS_PUBLISHED'              => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Published',
	'PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING'      => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FailedPublishing',
	'PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_UNPUBLISHING' => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#ScheduledUnpublishing',
	'PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING'    => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FailedUnpublishing',
	'PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION'      => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishStatusErrorDescription',
	'PROPERTY_REVISION_RUNTIME'                                  => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Runtime',
	'PROPERTY_REVISION_TEST'                                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#DeliveryContent',
	'PROPERTY_REVISION_COMPILED_BY'                              => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#CompiledBy',
	'PROPERTY_REVISION_FORM'                                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FormRevisionForm',
	'PROPERTY_REVISION_ADP_FORM_REVISION_ID'                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#AdpFormRevisionId',
	'PROPERTY_REVISION_ADP_PUBLISHER_JOB_ID'                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#AdpPublisherJobID',

	'PROPERTY_REVISION_PUBLISHED_BY'                             => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishedBy',
	'PROPERTY_REVISION_PUBLISHED_ACTIVATION'                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishActivation',
	'PROPERTY_REVISION_PUBLISHED_ACTIVE'                         => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Active',
	'PROPERTY_REVISION_PUBLISHED_NOT_ACTIVE'                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Inactive',
	'PROPERTY_REVISION_PUBLISHED_REVISION'                       => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishedRevision',
	'PROPERTY_REVISION_PUBLISH_STATE_UPDATE'                     => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishStateUpdateTime',
	'PROPERTY_REVISION_PUBLISH_STATE_UPDATED_BY'                 => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishStateUpdatedBy',
	'PROPERTY_REVISION_PUBLISH_STATE_UPDATED_TIME'               => 'http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#PublishStateUpdateTime',

	'PROPERTY_TEST_TESTMODEL'                                    => 'http://www.tao.lu/Ontologies/TAOTest.rdf#TestTestModel',
	'INSTANCE_TEST_MODEL_QTI'                                    => 'http://www.tao.lu/Ontologies/TAOTest.rdf#QtiTestModel',
	'TEST_TESTCONTENT_PROP'                                      => 'http://www.tao.lu/Ontologies/TAOTest.rdf#TestContent',
	'TAO_ITEM_CONTENT_PROPERTY'                                  => 'http://www.tao.lu/Ontologies/TAOItem.rdf#ItemContent',
	'TAO_ITEM_MODEL_PROPERTY'                                    => 'http://www.tao.lu/Ontologies/TAOItem.rdf#ItemModel',
	'PRACTICE_TEST'                                              =>	'http://www.tao.lu/Ontologies/taoTestBattery.rdf#PracticeTest',

	'PUBLISHING_USER_URI'                                        => 'http://parcc.edu/PARCC.rdf#publishingUser',
	'EXECUTION_TIME_LIMIT'                                       => 3600,
	'MEMORY_LIMIT'                                               => '256M',
	'UPDATEABLE_BATTERY_PROPERTIES'								=> serialize([
		'http://www.w3.org/2000/01/rdf-schema#label'								=> 'name',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Subject'					=> 'subject',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Grade'						=> 'grade',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Security'					=> 'security',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Multimedia'				=> 'multimedia',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestScoring'				=> 'scoring',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#Permission'				=> 'permissions',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestDescription'			=> 'description',
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#TestPrivacy'				=> 'privacy'
	]),

	'SCORE_REPORT_REQUIRED_PROPERTIES'							=> serialize([
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ELADecoding'				=> [
			'REQUIRED'	=> [
				'http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'					=> 'maxScorePoints',
				'http://www.parcconline.org/parcc-assessment/metadata#UIN'								=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#Reporting_Category'				=> 'reportingCategory',
			]
		],
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ELAReaderComprehension'	=> [
			'REQUIRED'	=> [
				'http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'					=> 'maxScorePoints',
				'http://www.parcconline.org/parcc-assessment/metadata#UIN'								=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#Item_Grade'						=> 'itemGrade',
				'http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Passage_Type'	=> 'passageType',
				'http://www.parcconline.org/parcc-assessment/metadata#ELA_Reading_Comp_Subclaim'		=> 'subclaim'
			]
		],
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ELAVocabulary'				=> [
			'REQUIRED'	=> [
				'http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'					=> 'maxScorePoints',
				'http://www.parcconline.org/parcc-assessment/metadata#UIN'								=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#Item_Grade'						=> 'itemGrade',
				'http://www.parcconline.org/parcc-assessment/metadata#ELA_Vocab_Passage_Type'			=> 'passageType',
				'http://www.parcconline.org/parcc-assessment/metadata#ELA_Vocab_Subclaim'				=> 'subclaim'
			]
		],
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#MathFluency'				=> [
			'REQUIRED'	=> [
				'http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'					=> 'maxScorePoints',
				'http://www.parcconline.org/parcc-assessment/metadata#UIN'								=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#Item_Strand'						=> 'itemStrand',
				'http://www.parcconline.org/parcc-assessment/metadata#Fluency_Skill_Area'				=> 'fluencySkillArea'
			]
		],
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#MathComprehension'			=> [
			'REQUIRED'	=> [
				'http://www.parcconline.org/parcc-assessment/metadata#Max_Score_Points'					=> 'maxScorePoints',
				'http://www.parcconline.org/parcc-assessment/metadata#UIN'								=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#Item_Grade'						=> 'itemGrade',
				'http://www.parcconline.org/parcc-assessment/metadata#Domain'							=> 'domain',
				'http://www.parcconline.org/parcc-assessment/metadata#Math_Cluster'						=> 'cluster',
				'http://www.parcconline.org/parcc-assessment/metadata#Math_Content_Standard'			=> 'contentStandard',
				'http://www.parcconline.org/parcc-assessment/metadata#Math_Comp_Evidence_Statement'		=> 'evidenceStatement',
				'http://www.parcconline.org/parcc-assessment/metadata#Calculator'						=> 'calculator'
			]
		],
		'http://www.tao.lu/Ontologies/taoTestBattery.rdf#ELAReaderMotivationSurvey'	=> [
			'REQUIRED'	=> [
			'http://www.parcconline.org/parcc-assessment/metadata#UIN'											=> 'uin',
				'http://www.parcconline.org/parcc-assessment/metadata#ELA_Reader_Motivation_Reporting_Category'	=> 'reportingCategory',
				'http://www.parcconline.org/parcc-assessment/metadata#Question_No'								=> 'questionNo'
			]
		]
	]),

	'SUPPORTED_MIME_TYPES'                                       =>
		serialize([
			// images
			'png' => 'image/png',
			'jpg' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'jpe' => 'image/jpeg',
			'gif' => 'image/gif',
			// audio/video
			'mp3' => 'audio/mpeg',
			'mp4' => 'video/mp4',
			'webm' => 'video/webm'
		]),

	'TEST_RULES' => serialize([
		'pub_schemaVersion' => 1,
		'timeLimits' => [
			'maxTime' => null
		],

		'testPart' => [
			'navigationMode' => 'nonlinear',
			'submissionMode' => 'simultaneous',
			'timeLimits' => [
				'maxTime' => null
			],
			'itemSessionControl' => [
				'maxAttempts' => 0,
				'showFeedback' => false,
				'allowComment' => false,
				'allowSkipping' => true
			],

			'assessmentSection' => [
				'visible' => true,
				'keepTogether' => true,
				'itemSessionControl' => [
					'maxAttempts' => 0,
					'showFeedback' => false,
					'allowComment' => false,
					'allowSkipping' => true
				],
				'selection' => [
					'select' => 'integer',
					'withReplacement' => false
				],
				'ordering' => [
					'shuffle' => false
				],
				'timeLimits' => [
					'maxTime' => null
				],
				'assessmentItem' => [
					'required' => true,
					'fixed' => true,
					'timeDependent' => false,
					'itemSessionControl' => [
						'maxAttempts' => 0,
						'showFeedback' => false,
						'allowComment' => false,
						'allowSkipping' => true
					],
					'assessmentItemContent' => [
						'attributes' => [
							'adaptive' => '*',
							'toolName' => '*',
							'toolVersion' => '*',
							'label' => '*',
							'timeDependent' => '*'
						],
						'responses' => [
							'feedbackRules' => '*'
						],
	//					'responseProcessing' => [
	//							'attributes' => '*'
	//					],
						'feedbacks' => '*',
						'assetTypes' => '*',
						'type' => '*',
						'assetEncoders' => '*',
						'nestedResourcesInclusion' => '*',
						'assets' => []
					],
					'timeLimits' => [
						'maxTime' => null
					]
				]
			]
		]
	]),
	'PRACTICE_TEST_RULES' => serialize([
				'testPart' => [
						'assessmentSection' => [
								'assessmentItem' => [
										'required' => '*',
										'fixed' => '*',
										'maxScorePoints' => '*',
										'uin' => '*',
										'itemGrade' => '*',
										'domain' => '*',
										'cluster' => '*',
										'contentStandard' => '*',
										'evidenceStatement' => '*',
										'calculator' => '*'
								]
						]
				]
		])
];
