<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Configurable variables for testBatteries
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 */

return [
	// Publishing web service config
	// Credentials ADP needs to use, to access ACR download and confirm web services.
	'ADP_USERNAME'                   => '',
	'ADP_PASSWORD'                   => '',
	'ADP_SECRET'                     => '',
	
	// Remote ADP Credentials, ACR user in the ADP user table.
	'REMOTE_ADP_USERNAME'            => '',
	'REMOTE_ADP_PASSWORD'            => '',
	'REMOTE_ADP_SECRET'              => '',

	// ADP host without a trailing space
	'ADP_HOST'                       => '',
	// PUBLISH web service part, it would be concatenated to the ADP_HOST to form the publishing web service URL.
	'ADP_PUBLISH_SERVICE'            => '/api/test',
	// Flag used to enable/disable VERYIFYPEER and VERYFYHOST on CURL calls.
	'VERIFY_SSL_CERTIFICATE'         => false,

	// TaoTestBatteries DEBUG configuration
	'TAOTESTBATTERIES_DEBUG'         => false,
	// Flag used to add some pauses to make testing easier, does not work if TAOTESTBATTERIES_DEBUG is set to false.
	'DEBUG_PAUSE_DURATION'           => 10,

	// Test Package
	'PUB_SCHEMA_VERSION'             => 1,
	// Number of attempts that CURL needs to do in case of failure. 
	'CURL_ATTEMPTS'                  => 3,

	// TAO QTI test filename	
	'TAOQTITEST_FILENAME'            => 'tao-qtitest-testdefinition.xml',
	// Compiled filenames
	'COMPILE_JSON_TEST_FILENAME'     => 'test.json',
	'COMPILE_JSON_METADATA_FILENAME' => 'metadata.json',

	// Media Manager paths
	'ASSETS_MEDIAMANAGER_PATH'       => 'taomedia://mediamanager/',
	'ASSETS_MEDIAMANAGER_DATAPATH'   => 'media',

	// if set to false, an optimized test will be generated, 
	// if set to true both optimized and non optimized tests will be generated.
	'DEBUG_COMPILE_NON_OPTIMIZATION' => false,
	// This prefix is used in non optimized title (in the test json file).
	'DEBUG_COMPILE_PREFIX'           => 'NOT OPTIMIZED-',
	// This will be used as a file name for the non optimized test.
	'DEBUG_COMPILE_FILENAME'         => 'test-notoptimized.json',
];
