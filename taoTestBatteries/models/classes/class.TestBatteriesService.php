<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

class taoTestBatteries_models_classes_TestBatteriesService extends tao_models_classes_ClassService
{

	const CONFIG_DEFAULT_FILESOURCE = 'defaultBatteryFileSource';

	protected $testBatteryClass = null;

	/**
	 * Short description of method __construct
	 *
	 * @access public
	 */
	protected function __construct()
	{
		parent::__construct();
		$this->testBatteryClass = new core_kernel_classes_Class(CLASS_TEST_BATTERY);
	}

	/**
	 * Sets the file source to use for new batteries
	 *
	 * @access public
	 * @param  Repository ($filesource)
	 * @return mixed
	 */
	public function setDefaultFilesource(core_kernel_versioning_Repository $filesource)
	{
		$ext = common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries');
		$ext->setConfig(self::CONFIG_DEFAULT_FILESOURCE, $filesource->getUri());
	}

	/**
	 * Delete test battery instance
	 *
	 * @access public
	 * @author Vlad
	 * @param  Resource ($testBattery)
	 * @return boolean
	 */
	 public function deleteTestBattery(core_kernel_classes_Resource $testBattery)
	 {
		$returnValue = (bool) false;

		if (!is_null($testBattery)) {
			// delete the associated process:
			$model = $this->getTestBatteryModel($testBattery);

			if (!is_null($model)) {
				$impl = $this->getTestBatteryModelImplementation($model);
				$impl->deleteContent($testBattery);
			}

			$returnValue = $testBattery->delete();
		}

		return (bool) $returnValue;
	 }

	/**
	 * Get test battery class
	 *
	 * @access public
	 * @author Vlad
	 * @return core_kernel_classes_Class
	 */
		public function getRootclass()
		{
			return $this->testBatteryClass;
		}

	/**
	 * Called whenever the label of the Test changes
	 *
	 * @access public
	 * @author Joel Bout, <joel.bout@tudor.lu>
	 * @param  Resource ($test)
	 * @return boolean
	 */
		public function onChangeTestBatteryLabel(core_kernel_classes_Resource $test = null)
		{
			$returnValue = (bool) false;

			$testModel = $this->getTestBatteryModel($test);

			if (!is_null($testModel)) {
				$impl = $this->getTestModelImplementation($testModel);
				$impl->onChangeTestBatteryLabel($test);

				$returnValue = true;
			}

			return (bool) $returnValue;
		}

	/**
	 * Returns the model of the current test battery
	 *
	 * @param core_kernel_classes_Resource $testBattery
	 * @return core_kernel_classes_Container
	 */
		public function getTestBatteryModel(core_kernel_classes_Resource $testBattery)
		{
			return $testBattery->getOnePropertyValue(new core_kernel_classes_Property(CLASS_TEST_BATTERY));
		}
}
