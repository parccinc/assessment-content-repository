<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\scripts\update\OntologyUpdater;

/**
 *
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 */
class taoTestBatteries_scripts_update_Updater extends \common_ext_ExtensionUpdater
{

	/**
	 * (non-PHPdoc)
	 * @see common_ext_ExtensionUpdater::update()
	 * @param $initialVersion
	 * @return string
	 */
	public function update($initialVersion)
	{

		$currentVersion = $initialVersion;

		// Update from 1.0 to 1.7.2
		if ($currentVersion == '1.15.1' || $currentVersion == '1.15.2') {
			OntologyUpdater::syncModels();
			$currentVersion = '1.15.3';
		}

		// Update from 1.17.x && 1.18.x to 1.19.1
 		if (in_array($currentVersion, ['1.17.2', '1.17.3', '1.18.1', '1.18.2', '1.18.3']) ) {
			OntologyUpdater::syncModels();
			$currentVersion = '1.19.1';
		}

		// Update from 1.19.1 to 1.22.3
		$version = explode('.', $currentVersion);
		if ($version[1]<22 || ($version[1] == 22 && $version[2]<3)) {
			OntologyUpdater::syncModels();
			$currentVersion = '1.22.3';
		}

		// Update from previous versions to 1.25.1
		$version = explode('.', $currentVersion);
		if ($version[1]<25 || ($version[1] == 25 && $version[2]<=1)) {
			OntologyUpdater::syncModels();
			$currentVersion = '1.25.2';
		}

		return $currentVersion;
	}

}
