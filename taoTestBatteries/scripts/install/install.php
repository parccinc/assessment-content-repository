<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\accessControl\func\AclProxy;
use oat\tao\model\accessControl\func\AccessRule;

require_once dirname(__FILE__) . '/../../../tao/includes/raw_start.php';
// create extension abstract management role
$roleClass = new core_kernel_classes_Class('http://www.tao.lu/Ontologies/TAO.rdf#ManagementRole');
// management role properties: label, comment, URI
$resource = $roleClass->createInstance('Test Batteries Manager', 'The Test Batteries Manager Role', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryManagerRole');
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAO.rdf#BackOfficeRole']);

// grant access permissions
$extensionRoles = [
	['grant', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryManagerRole', ['ext' => 'taoTestBatteries', 'mod' => 'TestBatteries']],
	['grant', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryManagerRole', ['ext' => 'taoTestBatteries', 'mod' => 'Forms']]
];
foreach ($extensionRoles as $tableEntry) {
	$accessRule = new AccessRule($tableEntry[0], $tableEntry[1], $tableEntry[2]);
	AclProxy::applyRule($accessRule);
}


// create extension abstract management role
$roleClass = new core_kernel_classes_Class('http://www.tao.lu/Ontologies/TAO.rdf#ManagementRole');
// management role properties: label, comment, URI
$resource = $roleClass->createInstance('Test Batteries Publishing Manager', 'The Test Batteries Publishing Manager Role', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryPublishingManagerRole');
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAO.rdf#BackOfficeRole']);

// grant access permissions
$extensionRoles = [
	['grant', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryPublishingManagerRole', ['ext' => 'taoTestBatteries', 'mod' => 'Publish']],
	['grant', 'http://www.tao.lu/Ontologies/generis.rdf#AnonymousRole', ['ext'=>'taoTestBatteries','mod' => 'Publish', 'act' => 'downloadPackage']],
	['grant', 'http://www.tao.lu/Ontologies/generis.rdf#AnonymousRole', ['ext'=>'taoTestBatteries','mod' => 'Publish', 'act' => 'confirm']]
];
foreach ($extensionRoles as $tableEntry) {
	$accessRule = new AccessRule($tableEntry[0], $tableEntry[1], $tableEntry[2]);
	AclProxy::applyRule($accessRule);
}
// grant access permissions to global manager
$managementRoleClass = new core_kernel_classes_Class(INSTANCE_ROLE_GLOBALMANAGER);
$managementRoleClass->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryPublishingManagerRole']);


// add file repository for batteries under TAO's main data folder
$source = tao_models_classes_FileSourceService::singleton()->addLocalSource('batteryDirectory', FILES_PATH . 'taoBatteries' . DIRECTORY_SEPARATOR);
$extension = common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries');
$extension->setConfig('defaultTestBatteriesFileSource', $source->getUri());

// copy default config file
$defaultsConfigPath = ROOT_PATH . "taoTestBatteries" . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . 'testBatteries.conf.php';
$defaultsArray = require($defaultsConfigPath);
$extension->setConfig('testBatteries', $defaultsArray);


// create Content Author user role
$roleClass = new core_kernel_classes_Class('http://www.tao.lu/Ontologies/generis.rdf#UserRole');
// user role properties: label, comment, URI
$resource = $roleClass->createInstance('Content Author', 'The Content Author Role', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryContentAuthorRole');
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAO.rdf#LockManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOItem.rdf#ItemsManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOTest.rdf#TestsManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOSubject.rdf#SubjectsManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOGroup.rdf#GroupsManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/generis.rdf#taoDeliveryRdfManager']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOResult.rdf#ResultsManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOMedia.rdf#MediaManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/generis.rdf#taoRevisionManager']);
// additional Item Author user roles
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAO.rdf#PropertyManagerRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/TAOItem.rdf#QTIManagerRole']);
// Test Batteries user role
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryManagerRole']);

// create Content Publisher user role
$roleClass = new core_kernel_classes_Class('http://www.tao.lu/Ontologies/generis.rdf#UserRole');
// user role properties: label, comment, URI
$resource = $roleClass->createInstance('Content Publisher', 'The Content Publisher Role', 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryContentPublisherRole');
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryContentAuthorRole']);
$resource->setPropertiesValues(['http://www.tao.lu/Ontologies/generis.rdf#includesRole' => 'http://www.tao.lu/Ontologies/taoTestBattery.rdf#taoTestBatteryPublishingManagerRole']);
