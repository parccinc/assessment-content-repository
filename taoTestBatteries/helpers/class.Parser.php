<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\taoQtiItem\helpers\Authoring;
use oat\taoQtiItem\model\qti\Parser as QtiParser;
use oat\taoItems\model\pack\Packer;

/**
 * QtiParser to provide helpers functions
 *
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries
 * @license GPLv2 http://www.opensource.org/licenses/gpl-2.0.php
 */
class taoTestBatteries_helpers_Parser
{

	private $xml = null;

	/**
	 * Constructor
	 * @param $itemXml
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	public function __construct($itemXml)
	{
		$this->xml = $itemXml;
	}

	/**
	 * Function to get item array
	 * 
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($itemUri)
	 * @return Array ($returnValue)
	 * @throws common_Exception
	 */
	public function getItemArray($itemUri)
	{
		$returnValue = null;
		
		try {
			// Validate the Qti xml
			Authoring::validateQtiXml($this->xml);
			$qtiParser = new QtiParser($this->xml);
			$item = $qtiParser->load();
			
			if (is_null($item)) {
				throw new common_Exception('Invalid QTI XML');
			}
		} catch (common_Exception $ex) {
			common_Logger::e('Invalid QTI XML for Item : ' . $itemUri);
			throw new common_Exception('Invalid QTI XML');
		}
		
		try {
			// Using the packer to get the item content
			$resourceItem = new core_kernel_classes_Resource($itemUri);
			$packer = new Packer($resourceItem);
			
			/**
			 * Example how to use encoders
			 * If you need to use the encoders you have to pass the array bellow as a first parameter to pack() function
			 *
			 * $encoders = array(
			 * 'js' => 'base64file',
			 * 'img' => 'base64file',
			 * 'css' => 'base64file',
			 * 'font' => 'base64file',
			 * 'audio' => 'base64file',
			 * 'video' => 'base64file'
			 * );
			 */
			if (! is_null($resourceItem)) {
				$returnValue = $packer->pack();
			} else {
				throw new common_Exception('Item cannot be packed. We cannot get data for Item Content : ' . $itemUri);
			}
		} catch (common_Exception $ex) {
			common_Logger::e('Item cannot be packed. We cannot get data for Item Content : ' . $itemUri);
			throw $ex;
		}
		return $returnValue;
	}
}
