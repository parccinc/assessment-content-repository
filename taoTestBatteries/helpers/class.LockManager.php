<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\lock\LockManager;
use oat\taoRevision\model\RevisionService as RevisionService;
use oat\oatbox\Configurable as Configurable;

/**
 * LockManager methods for taoTestBatteries extension.
 *
 * @author Kendall Parks <kendall.parks@breaktech.com>
 */
class taoTestBatteries_helpers_LockManager
{
	private $implementation;
	private $defaultRole;
	
	public function __construct($implementation = null, $defaultRole = PUBLISHING_USER_URI) {
		$this->implementation = (!is_null($implementation) && $implementation instanceof Configurable) ? 
			$implementation : 
			$this->getImplementation_LM();
		
		$this->defaultRole = $defaultRole;
		return $this;
	}
	
	/**
	 * Interface function to commit a battery.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param string $message
	 * @codeCoverageIgnore
	 */
	protected function commitResource_LM(core_kernel_classes_Resource $resource, $message) {
		return RevisionService::commit($resource, $message);
	}
	
	/**
	 * Interface function to get the current user.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @return core_kernel_classes_Resource
	 * @codeCoverageIgnore
	 */
	protected function getCurrentUser_LM() {
		return tao_models_classes_UserService::singleton()->getCurrentUser();
	}
	
	/**
	 * Interface function to get the lock implementation.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @codeCoverageIgnore
	 */
	protected function getImplementation_LM() {
		return LockManager::getImplementation();
	}
	
	/**
	 * Returns the current user if $userUri is invalid.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $userUri
	 * @return string
	 */
	protected function handleUserUri($userUri = null) {
		if(is_null($userUri) || !is_string($userUri)) {
			$user = $this->getCurrentUser_LM();
			if($user instanceof core_kernel_classes_Resource) {
				return $user->getUri();
			}

			return $this->defaultRole;
		}
		return $userUri;
	}
	
	/**
	 * Force-release a lock.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_Classes_Resource $resource
	 * @returns bool
	 */
	protected function forceReleaseLock($resource) {
		return $this->implementation->forceReleaseLock($resource);
	}
	
	/**
	 * Nicely release a lock.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $battery
	 * @param string $userUri
	 * @returns bool
	 */
	protected function releaseLock(core_kernel_classes_Resource $battery, $userUri) {
		return $this->implementation->releaseLock($battery, $userUri);
	}
	
	/**
	 * Set a lock.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param string $userUri
	 * @returns bool
	 */
	protected function setLock(core_kernel_classes_Resource $resource, $userUri)
	{
		$this->implementation->setLock($resource, $userUri);
	}


	/**
	 * Returns true if this lock implementation is a LockSystem.
	 *
	 * @return bool
	 */
	protected function isLockSystem() {
		return $this->implementationIsA('oat\taoWorkspace\model\lockStrategy\LockSystem');
	}

	/**
	 * Returns true if this lock implementation is an OntoLock.
	 *
	 * @return bool
	 */
	protected function isOntoLock() {
		return $this->implementationIsA('oat\tao\model\lock\implementation\OntoLock');
	}

	/**
	 * Returns true if this lock implementation is a NoLock.
	 *
	 * @return bool
	 */
	protected function isNoLock() {
		return $this->implementationIsA('oat\tao\model\lock\implementation\NoLock');
	}

	/**
	 * Returns true if this lock implementation's name matches a given string.
	 *
	 * @param string $class
	 * @return bool
	 * @throws common_Exception
	 */
	protected function implementationIsA($class) {
		if(is_string($class)) {
			return get_class($this->implementation) === $class;
		}

		throw new common_Exception('String expected. Received ' . gettype($class));
	}

	/**
	 * Returns TRUE if the given lock and system can be committed by the current user.
	 *
	 * @param core_kernel_classes_Resource $resource
	 * @return bool
	 * @throws common_Exception
	 */
	protected function resourceIsCommittable($resource) {
		if($this->isLocked($resource)) {
			$owner = $this->getOwner($resource);
			$currentUser = $this->getCurrentUser_LM();

			if ($owner instanceof core_kernel_classes_Resource
				&& $currentUser instanceof core_kernel_classes_Resource
			) {
				if ($this->isLockSystem() && ($owner->getUri() === $currentUser->getUri())) {
					return true;
				}

				return false;
			}
		}
		return false;
	}
	
	//// PUBLIC API \\\\
	
	/**
	 * Function to return information about a lock.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @returns oat\taoWorkspace\model\lockStrategy\Lock | NULL
	 */
	public function getLockData(core_kernel_classes_Resource $resource) {
		return $this->implementation->getLockData($resource);
	}
	
	/**
	 * Gets the owner of a lock.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $battery
	 * @returns core_kernel_classes_Resource | NULL
	 */
	public function getOwner(core_kernel_classes_Resource $battery)
	{
		$lock = $this->getLockData($battery);

		if($this->isLockSystem()) {
			return is_null($lock) ? NULL : new core_kernel_classes_Resource($lock->getOwnerId());
		}

		if($this->isOntoLock()) {
			return is_null($lock) ? NULL : $lock->getOwner();
		}

		return NULL;
	}
	
	/**
	 * Check if lock is currently locked.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @return bool
	 */
	public function isLocked(core_kernel_classes_Resource $resource)
	{
		return $this->implementation->isLocked($resource);
	}
	
	/**
	 * Try setting a lock.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param type $userUri
	 * @throws common_Exception
	 */
	public function smartLock(core_kernel_classes_Resource $resource, $userUri = null) {
		$userRole = $this->handleUserUri($userUri);
		
		try {
			$this->setLock($resource, $userRole);
		} catch (Exception $ex) {
			throw new common_Exception("Can't set lock.");
		}
	}
	
	/**
	 * Try releasing a lock. Will force-release it if it fails and $forceRelease is TRUE.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param string $userUri
	 * @param bool $forceRelease
	 * @param bool $allowCommitting
	 * @throws common_Exception
	 */
	public function smartRelease(core_kernel_classes_Resource $resource, $userUri = null, $forceRelease = false, $allowCommitting = true)
	{
		try {
			if($this->isLocked($resource)) {
				$userRole = $this->handleUserUri($userUri);

				// (LockSystem, except delete and compile) Commit the lock.
				if($allowCommitting && $this->resourceIsCommittable($resource)) {
					try {
						return $this->commitResource_LM($resource, 'Committing resource.');
					} catch(Exception $ex) {
						throw new Exception('Failed committing resource.');
					}
				}

				// (OntoLock & LockSystem delete and compile) Release the lock.
				if($this->releaseLock($resource, $userRole) !== true) {
					throw new Exception('Failed releasing lock.');
				}
				return true;
			}
			return false;
		} catch (Exception $ex) {
			if($forceRelease) {
				if($this->forceReleaseLock($resource) !== true) {
					throw new common_Exception('Can\'t force-release lock.');
				}
				return true;
			}

			throw new common_Exception("Can't release lock. {$ex->getMessage()}");
		}
	}
}
