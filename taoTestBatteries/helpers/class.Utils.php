<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\taoQtiItem\helpers\QtiFile;
use oat\taoQtiTest\test\QtiTestServiceTest;
use oat\taoQtiTest\test\oat\taoQtiTest\test;

/**
 * Miscellaneous utility methods for the taoTestBatteries extension.
 *
 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
 * @author Oussama Saddane <oussama.saddane@brektech.com>
 */
class taoTestBatteries_helpers_Utils
{

	/**
	 * getFormTest
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param  string $formUri
	 * @return core_kernel_classes_Resource test.
	 */
	public static function getFormTest($formUri)
	{
		$form = new core_kernel_classes_Resource($formUri);
		$test = current($form->getPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_TEST)));

		if (!empty($test)) {
			return new core_kernel_classes_Resource($test);
		} else {
			return null;
		}
	}

	/**
	 * getFormsByBatteryUri
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param  string $batteryUri
	 * @return array a set of forms that belongs to a given battery
	 */
	public static function getFormsByBatteryUri($batteryUri)
	{
		$formClass = new core_kernel_classes_Class(CLASS_FORM);
		$forms = $formClass->searchInstances(array(PROPERTY_FORM_TESTBATTERY => tao_helpers_Uri::decode($batteryUri)), array('like' => false));

		return $forms;
	}

	/**
	 * getBatteryByFormRevision
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $formRevision revision URI
	 * @return \core_kernel_classes_Resource|boolean
	 */
	public static function getBatteryByFormRevision($formRevision)
	{
		try {
			$revision = new core_kernel_classes_Resource($formRevision);
			$formUri = current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_FORM)));

			if (!empty($formUri)) {
				$form = new core_kernel_classes_Resource($formUri);
				$batteryUri = current($form->getPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_TESTBATTERY)));

				if (!empty($batteryUri)) {
					return new core_kernel_classes_Resource($batteryUri);
				}
			}
			return false;
		} catch (Exception $ex) {
			return false;
		}
	}

	/**
	 * getRevisionsByFormUri
	 *
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param  string $formUri
	 * @param  int/NULL $revisionNumber
	 * @return array a set of revisions that belongs to a given form
	 */
	public static function getRevisionsByFormUri($formUri, $revisionNumber = null)
	{
		$revisionClass = new core_kernel_classes_Class(CLASS_REVISION);
		$filter = array(PROPERTY_REVISION_FORM => $formUri);

		if (!is_null($revisionNumber)) {
			$filter[PROPERTY_REVISION_NUMBER] = $revisionNumber;
		}

		$instances = $revisionClass->searchInstances($filter, array('like' => false));

		return $instances;
	}

	/**
	 * Get empty data values formatted for display
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param  mixed $data
	 * @return mixed $formatted
	 */
	public static function formatEmptyValues($data = '') {
		$filler = "<span style='color:#999'>Not Found</span>";

		if (is_array($data)) {
			$formatted = [];

			foreach ($data as $key => $value) {
				if (is_array($value)) {
					if (count($value) == count($value, COUNT_RECURSIVE)) {
						$formatted[$key] = $value;

						foreach ($formatted[$key] as $subkey => $value) {
							$value = trim($value);
							$formatted[$key][$subkey] = empty($value) ? $filler : $value;
						}
					}
					else {
						$formatted[$key] = self::formatEmptyValues($value, $filler);
					}
				}
				else {
					$value = trim($value);
					$formatted[$key] = empty($value) ? $filler : $value;
				}
			}
		}
		else {
			$data = trim($data);
			$formatted = empty($data) ? $filler : $data;
		}

		return $formatted;
	}

	/**
	 * Get revision publish status
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param  string $revisionUri
	 * @return string $status = { status | empty }
	 */
	public static function getRevisionPublishStatus($revisionUri = '')
	{
		$status = '';

		if ($revisionUri) {
			$revision = new core_kernel_classes_Resource($revisionUri);
			$status = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS));
			$status = empty($status) ? '' : $status[0];
		}

		return $status;
	}

	/**
	 * Get resource's ADP ID for either Battery, Form, or Form Revision.
	 * 
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $resourceUri
	 * @param string $propertyUri
	 * @return string
	 */
	public static function getADPResourcePropertyId($resourceUri, $propertyUri) {
		if(is_string($resourceUri)) {
			$resource = new core_kernel_classes_Resource($resourceUri);
			
			if(is_object($resource) && get_class($resource) === 'core_kernel_classes_Resource') {
				$value = $resource->getOnePropertyValue(new core_kernel_classes_Property($propertyUri));
				return (is_object($value) && get_class($value) === 'core_kernel_classes_Literal') ? $value->literal : '';
			}
		}
		return '';
	}

	/**
	 * Get form revision publishing error description
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param  string $revisionUri
	 * @return string $errorDesc
	 */
	public static function getPublishingErrorDescription($revisionUri = '')
	{
		$errorDesc = '';

		if ($revisionUri) {
			$revision = new core_kernel_classes_Resource($revisionUri);
			$errorDesc = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION));
			$errorDesc = empty($errorDesc) ? '' : trim($errorDesc[0]);
		}

		return $errorDesc;
	}

	/**
	 * Get revision ADP Publisher Job Id
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param  string $revisionUri
	 * @return string $adpJobId = { id | empty }
	 */
	public static function getADPJobId($revisionUri = '')
	{
		$adpJobId = '';

		if ($revisionUri) {
			$revision = new core_kernel_classes_Resource($revisionUri);
			$adpJobId = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_PUBLISHER_JOB_ID));
			$adpJobId = empty($adpJobId) ? '' : $adpJobId[0];
		}

		return $adpJobId;
	}

	/**
	 *  Get activation status of form revision
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $revisionUri
	 * @return string { 'Active' | 'Not Active' | 'Unknown' }
	 */
	public static function getActivationStatus($revisionUri)
	{
		if($revisionUri) {
			$revision = new core_kernel_classes_Resource($revisionUri);
			$status = $revision->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_ACTIVATION));
			if (!is_null($status)) {
				switch($status->getURI()) {
					case PROPERTY_REVISION_PUBLISHED_ACTIVE:
						return 'Active';
					case PROPERTY_REVISION_PUBLISHED_NOT_ACTIVE:
						return 'Not Active';
					default:
						return 'Unknown';
				}
			} else {
				return 'Not Active';
			}
		} else {
			throw new common_Exception("Can't get activation status. No revision supplied.");
		}
	}

	/**
	 * isDeletableBattery
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param $resource (instance | class)
	 * @return string $message = { empty | fail description } on {success | fail}
	 */
	public static function isDeletableBattery($resource = null)
	{
		$message = '';

		if ($resource) {
			if ($resource->isClass()) {
				if (!empty($resource->getInstances(true)) || !empty($resource->getSubClasses(true))) {
					$message = 'This battery class cannot be deleted as it is not empty';
				} elseif ($resource->getUri() == CLASS_TEST_BATTERY) {
					$message = 'root';
				}
			} else {
				$battery = new core_kernel_classes_Resource($resource);
				$hasPublishedRevision = taoTestBatteries_helpers_Utils::hasPublishedRevision($resource);

				if ($hasPublishedRevision) {
					$message = "This battery cannot be deleted as it has at least one published form revision";
				}
			}
		} else {
			$message = 'No Class or Battery specified!';
		}

		return $message;
	}

	/**
	 * isDelitableForm
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $formUri
	 * @return string $message = { empty | fail description } on {success | fail}
	 */
	public static function isDeletableForm($formUri = '')
	{
		$message = '';

		if ($formUri) {
			$form = new core_kernel_classes_Resource($formUri);
			$hasPublishedRevision = taoTestBatteries_helpers_Utils::hasPublishedRevision($formUri);

			if ($hasPublishedRevision) {
				$message = 'This form cannot be deleted because it has at least one published form revision';
			}
		} else {
			$message = 'No Form specified!';
		}

		return $message;
	}

	/**
	 * isDelitableRevision
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param  string $revisionUri
	 * @return string $message = { empty | fail short description } on {success | fail}
	 */
	public static function isDeletableRevision($revisionUri = '')
	{
		$message = '';

		if ($revisionUri) {
			$revision = new core_kernel_classes_Resource($revisionUri);
			$lock = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS));

			if ($lock[0] != PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED) {
				$message = 'This form revision cannot be deleted because it has already been published';
			}
		} else {
			$message = 'No Revision specified!';
		}

		return $message;
	}

	/**
	 * hasPublishedRevision
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string URI of form or URI of Battery.
	 * @return boolean
	 */
	public static function hasPublishedRevision($uri)
	{
		$resource = new core_kernel_classes_Resource($uri);

		if (!empty(current($resource->getPropertyValues(new core_kernel_classes_Property(PROPERTY_HAS_PUBLISHED_REVISION))))) {
			return true;
		}

		return false;
	}

	/**
	 * hasPublishedRevision
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string URI of form or URI of Battery.
	 * @param boolean $flag
	 * @return boolean
	 */
	public static function setHasPublishedRevision($uri, $flag)
	{
		$resource = new core_kernel_classes_Resource($uri);
		return $resource->editPropertyValues(new core_kernel_classes_Property(PROPERTY_HAS_PUBLISHED_REVISION), $flag);
	}

	/**
	 * getNextRevisionNumber
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param type $formUri
	 * @return type
	 */
	public static function getNextRevisionNumber($formUri)
	{
		$nextRevisionNumber = 0;

		$revisions = self::getRevisionsByFormUri($formUri);

		foreach ($revisions as $revision) {
			$revisionNumber = intval(current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_NUMBER))));
			if ($revisionNumber > $nextRevisionNumber) {
				$nextRevisionNumber = $revisionNumber;
			}
		}
		return ++$nextRevisionNumber;
	}

	/**
	 * hasCompiledRevision
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $uri battery uri
	 * @return boolean $flag
	 */
	public static function hasCompiledRevision($uri)
	{
		$resource = new core_kernel_classes_Resource($uri);
		$flag = false;

		if (!empty(current($resource->getPropertyValues(new core_kernel_classes_Property(PROPERTY_HAS_COMPILED_REVISION))))) {
			$flag = true;
		}

		return $flag;
	}

	/**
	 * setHasPublishedRevision property
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $uri battery uri
	 * @param boolean $flag
	 * @return boolean
	 */
	public static function setHasCompiledRevision($uri, $flag)
	{
		$resource = new core_kernel_classes_Resource($uri);

		return $resource->editPropertyValues(new core_kernel_classes_Property(PROPERTY_HAS_COMPILED_REVISION), $flag);
	}

	/**
	 * @author Oussama Saddane <oussama.saddane@brektech.com>
	 * @param array $array
	 * @param String $subkey
	 * @return array //sorted
	 */
	public static function subvalSort($array, $subkey)
	{
		foreach ($array as $key => $value) {
			$tempArray[$key] = strtolower($value[$subkey]);
		}
		asort($tempArray);
		foreach ($tempArray as $key => $val) {
			$finalArray[] = $array[$key];
		}
		return $finalArray;
	}

	/**
	 * Delete test battery class
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @params string obj $class resource
	 * @return sting $message {empty | fail description} on {success | fail}
	 * @throws Exception
	 */
	public static function deleteTestBatteryClass($resource = null)
	{
		if ($resource) {
			$message = '';

			try {
				$message = self::isDeletableBattery($resource);

				if (!$message) {
					$classUri = $resource->getUri();
					$batteryClass = new core_kernel_classes_Class($classUri);
					$batteries = $batteryClass->getInstances();

					foreach ($batteries as $battery) {
						$batteryUri = $battery->getUri();

						if (self::deleteTestBattery($batteryUri)) {
							common_Logger::e('Error deleting Battery ' . $batteryUri . ' for Class ' . $classUri);
							$message = 'Not all batteries deleted';
							throw new common_exception_Error($message);
						}
					}

					$message = $batteryClass->deleteInstances(array($classUri));

					if ($message) {
						$message = '';
					} else {
						$message = 'Error during Battery Class deletion';
					}
				} else {
					throw new common_exception_Error($message);
				}
			} catch (common_Exception $ex) {
				common_Logger::w($ex->getMessage());

				if ($ex instanceof common_exception_Error) {
					throw new common_exception_Error($message);
				}
			}
		} else {
			$message = 'No Battery Class specified for deletion!';
			common_Logger::e($message);
		}

		return $message;
	}

	/**
	 * Delete test battery
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @params obj $battery resource
	 * @return sting $message {empty | fail description} on {success | fail}
	 * @throws Exception
	 */
	public static function deleteTestBattery($resource = null)
	{
		if ($resource) {
			$message = '';

			try {
				$message = self::isDeletableBattery($resource);

				if (!$message) {
					$batteryUri = $resource->getUri();
					$forms = self::getFormsByBatteryUri($batteryUri);

					foreach ($forms as $form) {
						$formUri = $form->getUri();

						if (self::deleteForm($formUri)) {
							common_Logger::e('Error deleting Form ' . $formUri . ' for Battery ' . $batteryUri);
							$message = 'Not all forms deleted';
							throw new common_exception_Error($message);
						}
					}

					$batteryClass = new core_kernel_classes_Class(CLASS_TEST_BATTERY);
					$message = $batteryClass->deleteInstances(array($batteryUri));

					if ($message) {
						$message = '';
					} else {
						$message = 'Error during Battery deletion';
					}
				} else {
					throw new common_exception_Error($message);
				}
			} catch (common_Exception $ex) {
				common_Logger::w($ex->getMessage());

				if ($ex instanceof common_exception_Error) {
					throw new common_exception_Error($ex->getMessage());
				}
			}
		} else {
			$message = 'No Battery specified for deletion!';
			common_Logger::e($message);
		}

		return $message;
	}

	/**
	 * Delete a form
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @params string string $formUri
	 * @return sting $message {empty | fail description} on {success | fail}
	 * @throws Exception
	 */
	public static function deleteForm($formUri = null)
	{
		if ($formUri) {
			$message = '';

			try {
				$message = self::isDeletableForm($formUri);

				if (!$message) {
					$revisions = self::getRevisionsByFormUri($formUri);

					foreach ($revisions as $revision) {
						$revisionUri = $revision->getUri();

						if (self::deleteFormRevision($revisionUri)) {
							common_Logger::e('Error deleting revision ' . $revisionUri . ' for form ' . $formUri);
							$message = 'Not all revisions deleted';
							throw new common_exception_Error($message);
						}
					}

					$formClass = new core_kernel_classes_Class(CLASS_FORM);
					$message = $formClass->deleteInstances(array($formUri));

					if (!$message) {
						$message = 'Deletion denied';
						throw new common_exception_Error($message);
					} else {
						$message = '';
					}
				} else {
					throw new common_exception_Error($message);
				}
			} catch (common_Exception $ex) {
				common_Logger::w($ex->getMessage());

				if ($ex instanceof common_exception_Error) {
					throw new common_exception_Error($ex->getMessage());
				}
			}
		} else {
			$message = 'No Form specified for deletion!';
			common_Logger::e($message);
		}

		return $message;
	}

	/**
	 * Delete form revision
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $revisionUri
	 * @return sting $message {empty | fail description} on {success | fail}
	 * @throws Exception
	 */
	public static function deleteFormRevision($revisionUri = null)
	{
		if ($revisionUri) {
			$message = '';

			try {
				$message = self::isDeletableRevision($revisionUri);

				// delete instance and zip package
				if (!$message) {
					$message = self::deleteRevisionPackage($revisionUri);

					if (!$message) {
						$message = 'Error deleting Revision Package';
						common_Logger::e($message . ' for ' . $revisionUri);
					} else {
						$revisionClass = new core_kernel_classes_Class(CLASS_REVISION);
						$message = $revisionClass->deleteInstances(array($revisionUri));

						if (!$message) {
							$message = 'Revision deletion denied';
							common_Logger::e($message . ' for ' . $revisionUri);
						} else {
							$message = '';
						}
					}
				}

				if ($message) {
					throw new common_exception_Error($message);
				}
			} catch (common_Exception $ex) {
				common_Logger::w($ex->getMessage());

				if ($ex instanceof common_exception_Error) {
					throw new common_exception_Error($message);
				}
			}
		} else {
			$message = 'No Form Revision specified for deletion!';
			common_Logger::e($message);
		}

		return $message;
	}

	/**
	 * Deletes associated with revision (revision package)
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $revisionUri
	 * @return boolean {true | false} on {success | troubles}
	 */
	public static function deleteRevisionPackage($revisionUri = null)
	{
		$result = false;
		clearstatcache();

		if ($revisionUri) {
			list (, $revisionId) = explode("#", tao_helpers_Uri::decode($revisionUri));
			$filePath = FILES_PATH . 'taoBatteries' . DIRECTORY_SEPARATOR . $revisionId . ".zip";

			if (file_exists($filePath)) {
				if (is_readable($filePath)) {
					if (!($result = @unlink($filePath))) {
						common_Logger::e('Error deleting Revision Compilation Package ' . $filePath);
					}
				} else {
					common_Logger::e('Revision Compilation Package ' . $filePath . ' not accessible');
				}
			} else {
				common_Logger::e('Revision Compilation Package ' . $filePath . ' not found');
				$result = true;
			}
		} else {
			common_Logger::e('No Revision specified to delete its Package!');
		}

		return $result;
	}

	/**
	 * curlApiCall helper used to make CURL calls
	 *
	 * @access public
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param type $url
	 * @param type $data
	 * @param type $method could be one of the following 'POST', 'GET', 'PUT', 'DELETE'
	 * @param type $returnType
	 * @param type $httpHeaders
	 * @return Object
	 * @throws Exception
	 */
	public static function curlApiCall($url, $data = null, $method = 'GET', $returnType = 'data', $httpHeaders = array())
	{

		// try to call web service
		try {
			$curlHandler = curl_init($url);

			switch ($method) {

				case 'POST':
					curl_setopt($curlHandler, CURLOPT_POST, 1);
					if ($data) {
						curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $data);
					}
					break;

				case 'GET':
					if ($data) {
						$url = sprintf("%s?%s", $url, http_build_query($data));
						curl_setopt($curlHandler, CURLOPT_URL, $url);
					}
					curl_setopt($curlHandler, CURLOPT_HTTPGET, true);
					break;

				case 'PUT':
					if ($data) {
						curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $data);
						curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, 'PUT');
					}

					break;

				case 'DELETE':
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, "DELETE");
					break;

				case 'PATCH':
					curl_setopt($curlHandler, CURLOPT_CUSTOMREQUEST, "PATCH");
					if ($data) {
						curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $data);
					}
					break;

				default:
					// in case of unsupported request method
					throw new Exception('Unsupported method: ' . $method);
					break;
			}

			curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, VERIFY_SSL_CERTIFICATE);

			// KP: I removed these because they weren't working with GET requests. Perhaps they need to be merged into a case statement above?
			// $headers = array_merge(array("Accept: application/json"), $httpHeaders);
			curl_setopt($curlHandler, CURLOPT_HTTPHEADER, $httpHeaders);
			// Try to retrieve resource up to CURL_ATTEMPTS times.
			// If resource retrieval fails each time, throw an error message.
			$info = $result = null;
			for ($attempt = 0; $attempt < CURL_ATTEMPTS; $attempt++) {
				$result = curl_exec($curlHandler);
				$info = curl_getinfo($curlHandler);

				// Check if CURL result failed.
				$failed = ($result === false || $info['http_code'] != 200) ? true : false;

				// Throw error if resource can't be retrieved after CURL_ATTEMPTS tries.
				if ($attempt == CURL_ATTEMPTS - 1 && $failed) {
					curl_close($curlHandler);
					throw new Exception("Unable to retrieve external resource: $url", $info['http_code']);
				}

				if (!$failed) {
					break;
				}
			}

			curl_close($curlHandler);
			return $result;
		} // in case of an error
		catch (Exception $ex) {
			common_Logger::e('cURL Error ' . $ex->getCode() . ': ' . $ex->getMessage());
			throw new common_Exception($ex->getMessage());
		}
	}

	/**
	 * Function to return user's display name from user's uri
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string ($userUri)
	 * @param int ($maxLength)
	 * @return string (user's display name)
	 */
	public static function getUserDisplayName($userUri, $maxLength = null)
	{
		$return_value = "";
		$userClass = new core_kernel_classes_Class($userUri);
		$firstName = current($userClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_FIRSTNAME)));
		$lastName = current($userClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_LASTNAME)));
		$email = current($userClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_MAIL)));
		$userName = current($userClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_LOGIN)));

		// Return firstname and lastname
		if (strlen($firstName) > 0 && strlen($lastName) > 0) {
			$return_value = $firstName . " " . $lastName;
		} // Return firstnam0e
		elseif (strlen($firstName) > 0) {
			$return_value = $firstName;
		} // Return lastname
		elseif (strlen($lastName) > 0) {
			$return_value = $lastName;
		} // Return email address
		elseif (strlen($email) > 0) {
			$return_value = $email;
		} // Return username
		elseif (strlen($userName) > 0) {
			$return_value = $userName;
		} // Return N/A Not found
		else {
			$return_value = "N/A (Not found)";
		}

		if ($maxLength != null && strlen($return_value) > $maxLength) {
			$return_value = substr($return_value, 0, $maxLength - 1) . "…";
		}
		return $return_value;
	}

	/**
	 * Returns formatted test display name
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string ($testName)
	 * @param int ($maxLength)
	 * @return string (formatted test name)
	 */
	public static function formatTestDisplayName($testName, $maxLength = 100)
	{
		return strlen($testName) > $maxLength ? substr($testName, 0, $maxLength - 1) . "…" : $testName;
	}

	/**
	 * function to format dates from UTC to TAO Time Zone
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param array $formRevisions
	 * @param int $revisionSelected
	 * @return array (localPublishDateTime , localCompileDateTime)
	 */
	public static function dateTimeFormat($formRevisions, $revisionSelected = '')
	{
		$dateFormated = array();

		foreach ($formRevisions as $formRevision) {
			if ($formRevision['revisionUri'] == $revisionSelected) {
				if ($formRevision['compileDateTime'] != 'False') {
					$localCompileDateTime = date('m/d/Y g:i:s A', $formRevision['compileDateTime']);
				} else {
					$localCompileDateTime = '';
				}

				if ($formRevision['publishSDateTime'] != 'False') {
					$localPublishDateTime = date('m/d/Y g:i:s A', $formRevision['publishSDateTime']);
				} else {
					$localPublishDateTime = '';
				}

				$dateFormated['localCompileDateTime'] = $localCompileDateTime;
				$dateFormated['localPublishDateTime'] = $localPublishDateTime;

				break;
			}
		}

		return $dateFormated;
	}

	/**
	 * Validates if test has all its properties and values
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function hasAllTestProperties($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$testClass = new core_kernel_classes_Class(TAO_TEST_CLASS);
				$properties = $testClass->getProperties(false);
				$test = new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri));

				foreach ($properties as $key => $property) {
					$propertyValue = $test->getOnePropertyValue($property);
					$result = true;

					if ($propertyValue instanceof core_kernel_classes_Resource) {
						$uri = $propertyValue->getUri();

						switch ($key)
						{
							case TEST_TESTCONTENT_PROP:
								if (!empty($uri)) {
									$result = true;
								} else {
									$result = false;
								}

								break;

							case PROPERTY_TEST_TESTMODEL:
								if ($uri == INSTANCE_TEST_MODEL_QTI) {
									$result = true;
								} else {
									$result = false;
								}

								break;

							default:
								$result = true;
						}
					}

					if (!$result) {
							break;
					}
				}
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage() . ": Error retrieving test property $key for test $testUri");
			}
		}

		return $result;
	}

	/**
	 * Validates test existance
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function testExists($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$test = new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri));
				$result = $test->exists();
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage());
			}
		}

		return $result;
	}

	/**
	 * Validates if test has QTI model
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function isQtiTest($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$test = new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri));
				$testModel = $test->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_TEST_TESTMODEL));

				if ($testModel->getUri() == INSTANCE_TEST_MODEL_QTI) {
					$result = true;
				}
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage());
			}
		}

		return $result;
	}

	/**
	 * Items Validation
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param testUri
	 */
	public static function isValidItems($testUri = null)
	{
		try {
			$hasTestItems = taoTestBatteries_helpers_Utils::hasTestItems($testUri);
			if ($hasTestItems) {
				$testItems = self::getTestItems($testUri);

				foreach ($testItems as $testItem) {
					$resourceItem = new core_kernel_classes_Resource($testItem->getUri());
					$itemId = parse_url($resourceItem->getUri(), PHP_URL_FRAGMENT);
					//Check if the item referenced by test exists
					if (!$resourceItem->exists()) {
						throw new common_Exception('One or more items referenced by the test cannot be found ' . $resourceItem->getLabel() .' '.$itemId);
					} else {
						//get qti xml path
						$qtiXmlFilePath = QtiFile::getQtiFilePath($resourceItem);
						if (!file_exists($qtiXmlFilePath)) {
							throw new common_Exception('The QTI file cannot be found for an item ' . $resourceItem->getLabel() .' '.$itemId);
						} else {
							//get item xml content
							$itemXml = file_get_contents($qtiXmlFilePath);
							//check if the qti file is parsed
							if ($itemXml) {
								$parser = xml_parser_create();
								$result = xml_parse($parser, $itemXml);
								xml_parser_free($parser);
								if (!$result) {
									throw new common_Exception('The QTI file cannot be parsed for an item ' . $resourceItem->getLabel() .' '.$itemId);
								} else {
									//check if the item is not a qti item
									$itemModel = $resourceItem->getOnePropertyValue(new core_kernel_classes_Property(TAO_ITEM_MODEL_PROPERTY));
									if (!isset($itemModel) || $itemModel->getUri() != TAO_ITEM_MODEL_QTI) {
										throw new common_Exception('The item is not a QTI item ' . $resourceItem->getLabel() .' '.$itemId);
									} else {
										//Check Item Properties
										$resourceItemPropertiesValues = $resourceItem->getPropertiesValues(array(
										new core_kernel_classes_Property(RDFS_LABEL),
										new core_kernel_classes_Property(TAO_ITEM_CONTENT_PROPERTY),
										new core_kernel_classes_Property(TAO_ITEM_MODEL_PROPERTY)
										));

										foreach ($resourceItemPropertiesValues as $resourceItemPropertyValue) {
											if (empty($resourceItemPropertyValue)) {
												throw new common_Exception('Cannot get all properties for an item ' . $resourceItem->getLabel() .' '.$itemId);
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				throw new common_Exception('Test has no items');
			}
		} catch (common_Exception $ex) {
			common_Logger::e($ex->getMessage() . $resourceItem->getUri() . ' ' . $itemId);
			throw $ex;
		}
	}

	/**
	 * Validates if test has any items in it
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function hasTestItems($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$testModel = new taoQtiTest_models_classes_TestModel();
				$items = $testModel->getItems(new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri)));

				if (!empty($items)) {
					$result = true;
				}
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage());
			}
		}

		return $result;
	}

	/**
	 * Validates if test has corresponding QTI file
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function hasQtiFile($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$test = new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri));
				// Get test file path
				$filePath = taoQtiTest_models_classes_QtiTestService::singleton()->getDocPath($test);

				if (file_exists($filePath) && is_readable($filePath)) {
					$result = true;
				}
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage());
			}
		}

		return $result;
	}

	/**
	 * Validates if test cannot be parsed because it is corruipted
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return boolean {true | false} on {success | fail}
	 */
	public static function isQtiFileValid($testUri = null)
	{
		$result = false;

		if ($testUri) {
			try {
				$test = new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri));
				// Get test file path
				$filePath = taoQtiTest_models_classes_QtiTestService::singleton()->getDocPath($test);

				$xml = file_get_contents($filePath);

				if ($xml !== false) {
					$parser = xml_parser_create();
					$result = xml_parse_into_struct($parser, $xml, $values);
					xml_parser_free($parser);
				}
			} catch (Exception $ex) {
				$result = false;
				common_Logger::e($ex->getMessage());
			}
		}

		return $result;
	}

	/**
	 * Determines if an asset is a dummy or not.
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $asset
	 * @return bool
	 */
	public static function isDummyAsset($asset)
	{
		$arr = explode("_", $asset);
		foreach ($arr as $fragment) {
			if (!(is_numeric($fragment) || empty($fragment))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Determines if an asset is a supported file type.
	 * If an optional $fileType parameter is passed, file type is stored in it by reference.
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $asset
	 * @param string $fileType (optional)
	 * @return bool
	 */
	public static function isSupportedFiletype($asset)
	{
		$fileType = self::getMimeType($asset, false);
		return $fileType != 'application/octet-stream';
	}

	/**
	 * Determines file type of resource at the given path.
	 * The optional $extension parameter determines if the file extension should be used to determine file type.
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $path
	 * @param bool $extension
	 * @return string
	 * @throws common_Exception
	 * @throws Exception
	 */
	public static function getMimeType($path, $extension = false)
	{
		$mimetype = '';
		$mime_types = self::getMimeTypes();

		if (self::isExternalUrl($path)) {
			// External resources
			$fMeta = [];

			try {
				$fResource = @fopen($path, 'r');

				if ($fResource == false) {
					throw new Exception("Failed");
				}

				$fMeta = stream_get_meta_data($fResource);
				fclose($fResource);
			} catch (Exception $ex) {
				$msg = "Failed opening file: $path.";
				common_Logger::e($msg);
				throw new common_Exception($msg);
			}

			if (array_key_exists('wrapper_data', $fMeta)) {
				$key = self::arraySearchSubstring($fMeta['wrapper_data'], "Content-Type");

				if ($key) {
					$matches = [];
					preg_match('/^Content-Type: (.*)/', $fMeta['wrapper_data'][$key], $matches);

					if (array_key_exists(1, $matches)) {
						$mimetype = $matches[1];
					}
				}
			}
		} else {
			// Local resources
			if (!$extension) {
				$extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));

				$mimetype = array_key_exists($extension, $mime_types) ? $mime_types[$extension] : '';

				if (!in_array($extension, array('css'))) {
					if (file_exists($path)) {
						if (function_exists('finfo_open')) {
							$finfo = finfo_open(FILEINFO_MIME);
							$mimetype = finfo_file($finfo, $path);
							finfo_close($finfo);
						} elseif (function_exists('mime_content_type')) {
							$mimetype = mime_content_type($path);
						}
						if (!empty($mimetype)) {
							if (preg_match("/; charset/", $mimetype)) {
								$mimetypeInfos = explode(';', $mimetype);
								$mimetype = $mimetypeInfos[0];
							}
						}
					}
				}
			} else {
				// find out the mime-type from the extension of the file.
				$extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
				if (array_key_exists($extension, $mime_types)) {
					$mimetype = $mime_types[$extension];
				}
			}
		}

		// If no mime-type found ...
		if (empty($mimetype)) {
			$mimetype =  'application/octet-stream';
		}

		return (string) $mimetype;
	}

	/**
	 * Searches given array's values for a partial match, and returns the key, if found.
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param array $array
	 * @param string $substring
	 * @return string (or false)
	 */
	public static function arraySearchSubstring($array, $substring)
	{
		foreach ($array as $key => $value) {
			if (strpos($value, $substring) !== false) {
				return $key;
			}
		}
		return false;
	}

	/**
	 * Builds an array of supported MIME types and returns it.
	 * This function was copied from TAO core because it was inaccessible from this scope.
	 * @return array
	 */
	public static function getMimeTypes()
	{
		return unserialize(SUPPORTED_MIME_TYPES);
	}

	/**
	 *
	 * @param string $testUri
	 * @return array
	 */
	public static function getTestItems($testUri = null)
	{
		$testModel = new taoQtiTest_models_classes_TestModel();
		return $testModel->getItems(new core_kernel_classes_Resource(tao_helpers_Uri::decode($testUri)));
	}

	/**
	 * Parse ADP response
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array json $adpResponse
	 * @return array $response
	 * @throws Exception
	 */
	public static function parseResponse($adpResponse)
	{
		$message = '';
		$response = [];

		if (!empty($adpResponse)) {
			$response = json_decode($adpResponse, true);

			if (!self::hasEnvelope($response)) {
				$response = self::makeEnvelope($response);
			}

			$message = self::validateResponse($response);

			if (empty($message)) {        // validation is ok
				$status = strtolower(trim($response['meta']['status']));
				$response = $response['result'];

				if ($status == 'error') { // response error
					$response['success'] = false;
					$response['requestId'] = $response['publishedTimestamp'] = '';
					$message = $response['errorDescription'];
				}
				else {                    // response success
					$response['success'] = true;
					$response['publishedTimestamp'] = strtotime($response['publishedTimestamp'] . " UTC");
					$response['errorDescription'] = '';
				}
			}
		}
		else {                            // network/connection fail
			$response['success'] = false;
			$response['errorDescription'] = 'Network/connection error';
			$message = "An error occurred making a request to publish this form revision. Please check the log file for error details.";
		}

		if (!empty($message)) {
			common_Logger::e("Parsing ADP response: $message");
			throw new common_Exception($message);
		}

		return $response;
	}

	/**
	 * Determines if an ADP response contains an envelope.
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param array $response
	 * @return bool
	 */
	public static function hasEnvelope($response) {
		if (!empty($response)) {
			if(is_object($response) && property_exists($response, 'meta') && property_exists($response, 'result')) {
				return true;
			}

			if(is_array($response) && array_key_exists('meta', $response) && array_key_exists('result', $response)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Wrap ADP response in envelope
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array $response
	 * @return array $responseEnvelope
	 */
	public static function makeEnvelope($response) {
		$responseEnvelope = [];

		if (!empty($response)) {
			$responseEnvelope = ['meta' => [], 'result' => $response];

			if (array_key_exists('errorCode', $response)) {
				$responseEnvelope['meta']['status'] = 'ERROR';
			}
			else {
				$responseEnvelope['meta']['status'] = 'SUCCESS';
			}

			$responseEnvelope['meta']['count'] = count($response);
		}

		return $responseEnvelope;
	}

	/**
	 * Validate ADP response envelope
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param array $response
	 * @return string $message  {empty | error description} on {valid | invalid}
	 */
	public static function validateResponse($response)
	{
		$message = '';

		// response sections/fields arrays - can be adjusted to contain only required ones
		$reqSections = ['meta', 'result'];
		$reqMetaFields = ['status', 'count'];
		$reqResultFields = ['success' => ['requestId', 'publishedTimestamp', 'status'],
							'error'   => ['statusCode', 'statusDescription', 'errorCode', 'errorDescription']];

		if (!empty($response)) {
			// check out sections
			foreach ($reqSections as $name) {
				if (!array_key_exists($name, $response)) {
					$message = "no required section '$name' in response";
					break;
				}
			}

			// check out [meta] section fields
			if (!$message) {
				foreach ($reqMetaFields as $name) {
					if (!array_key_exists($name, $response['meta'])) {
						$message = "no required field '$name' in response [meta] section";
						break;
					}
				}
			}

			// check out [result] section fields
			if (!$message) {
				$status = strtolower(trim($response['meta']['status']));

				if ($status == 'success' || $status == 'error') {
					foreach ($reqResultFields[$status] as $name) {
						if (!array_key_exists($name, $response['result'])) {
							$message = "no required field '$name' in response [result] section";
							break;
						}
					}
				}
				else {
					$message = "incorrect status value '$status' in response [meta] section";
				}
			}
		}
		else {
			$message = 'response JSON empty/corrupted/cannot be decoded';
		}

		if (!empty($message)) {
			common_Logger::e("ADP response validation: $message");
			$message = 'Invalid ADP response';
		}

		return $message;
	}

	/**
	 * Form Test validation
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $testUri
	 * @return string $result: {empty | error description} on {success | fail}
	 */
	public static function validateFormTest($testUri)
	{
		$validators = array(
			'hasAllTestProperties' => 'Cannot get all test properties for the test #test_name#',
			'testExists' => 'The test cannot be found.',
			'isQtiTest' => 'The test #test_name# is not a QTI test. Please choose a QTI test, then try again.',
			'hasQtiFile' => 'The QTI file for the test #test_name# cannot be found.',
			'isQtiFileValid' => 'The QTI file for the test #test_name# cannot be parsed.',
			'hasTestItems' => 'The test #test_name# does not have any QTI items. Please add QTI items to the test, then try again.'
		);

		$test = new core_kernel_classes_Resource($testUri);
		$testName = $test->getLabel();
		$result = '';

		foreach ($validators as $validator => $message) {
			$flag = self::$validator($testUri);

			if (!$flag) {
				$result = str_replace('#test_name#', $testName, $message);
				common_Logger::e("Test validation error for test $testUri");
				break;
			}
		}

		return $result;
	}

	/**
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $url
	 * @return bool
	 */
	public static function isDataUrl($url)
	{
		return (bool) preg_match("/^data:.*$/", $url);
	}


	/**
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $url
	 * @return bool
	 */
	public static function isExternalUrl($url)
	{
		return in_array(parse_url($url, PHP_URL_SCHEME), ["http","https"]);
	}

	/**
	 * Parses a string for a number and returns a result of the appropriate type.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $string
	 * @return int | float | string
	 */
	public static function parseForNumeric($string)
	{
		if(preg_match("/^[0-9]*$/", $string)) {
			return (int) $string;
		} elseif(preg_match("/^[0-9]+\.[0-9]+$/", $string)) {
			return (float) $string;
		} else {
			return $string;
		}
	}
	
	/**
	 * Generates an HMAC token.
	 * 
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $apiURI
	 * @return string
	 */
	public static function generateHmacSignature($apiURI) {
		$microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone.
		$nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range.
		$message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp.
		$hash = hash_hmac('sha256', $message, REMOTE_ADP_SECRET);
		
		common_Logger::d("HmacSignature Details:");
		common_Logger::d("Timestamp : $microtime");
		common_Logger::d("Secret : " . REMOTE_ADP_SECRET);
		common_Logger::d("Nonce : $nonce");
		common_Logger::d("Hash : $hash");
		
		return base64_encode($microtime . ':' . $nonce . ':' . base64_encode($hash));
	}
}
