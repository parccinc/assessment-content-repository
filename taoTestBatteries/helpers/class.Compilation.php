<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\taoItems\model\pack\Packer;
use oat\taoItems\model\media\ItemMediaResolver;
use oat\tao\model\media\sourceStrategy\HttpSource;

require dirname(__FILE__) . "/../jsv4-php/jsv4.php";
require dirname(__FILE__) . "/../jsv4-php/schema-store.php";


/**
 * QTI Compilation to provide helpers functions
 *
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries
 *
 * @license GPLv2  http://www.opensource.org/licenses/gpl-2.0.php
 */


class taoTestBatteries_helpers_Compilation
{

	/**
	 * Path to data folder
	 * @var string
	 */
	private $dataPath = null;

	/**
	 * @var taoQtiTest_models_classes_QtiTestService
	 */
//	private $qtiTestService = null;

	/**
	 * The supported assets types
	 * @var string[]
	 */
	private $assetTypes = [/* 'js', 'css', 'font', */ 'img', 'audio', 'video'];

	/**
	 * @var taoItems_models_classes_ItemsService
	 */
	private $itemService = null;

	/**
	 * Package resources with full path
	 * @var string[]
	 */
	private $resourceManifest = null;

	/**
	 * Set the maximum length for user display name
	 * @var int
	 */
	private $displayUserMaxLength = 100;
	public static $testUriParam = '';

	/**
	 * taoTestBatteries_helpers_Compilation constructor
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string $dataPath
	 * @param taoQtiTest_models_classes_QtiTestService $qtiTestService
	 */

	public function __construct($dataPath = null, $qtiTestService = null)
	{
		$this->dataPath = $dataPath ? : taoTestBatteries_helpers_Config::getDefaultFileSource();
//		$this->qtiTestService = $qtiTestService ? : taoQtiTest_models_classes_QtiTestService::singleton();
	}

	/**
	 * Function to create the revision zip package
	 *
	 * @param string (test Uri)
	 * @param string (revision Uri)
	 * @param Array ($revisionArrayNonOptimized)
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @throws common_Exception
	 */
	public function createRevisionPackage($testUri, $revisionUri, $optimizedRevisionArray, $nonOptimizedRevisionArray = null)
	{
		try {
			$cleanFiles = [];
			list($garbage, $revisionId) = explode("#", $revisionUri);

			try {
				// Generate metadata json
				$metadata = [];
				$metadata = $this->createMetadata($revisionUri, $testUri);
			} catch (common_Exception $ex) {
				common_logger::e("Cannot create Metadata for Test" . $testUri . "  Revision " . $revisionUri);
			}

			// Encode revisions into JSON
			try {
				$revisionJsonOptimized = json_encode($optimizedRevisionArray);

				if (!is_null($nonOptimizedRevisionArray)) {
					$revisionJsonNonOptimized = json_encode($nonOptimizedRevisionArray);
				}
			} catch (common_Exception $ex) {
				common_logger::e("Cannot encode Test Object for Test: " . $testUri . "  Revision: " . $revisionUri);
			}

			// Encode metadata into JSON
			try {
				$metadataJson = json_encode($metadata);
			} catch (common_Exception $ex) {
				common_logger::e("Cannot encode Metadata Object for Test" . $testUri . "  Revision " . $revisionUri);
			}
			$revisionDirectory = $this->dataPath . $revisionId . DIRECTORY_SEPARATOR;

			try {
				// Create folder for files before packaging
				mkdir($revisionDirectory);
			} catch (common_Exception $ex) {
				common_logger::e("Cannot create folder for the package");
			}

			try {
				// Write test.json file
				$revisionJsonFile = $revisionDirectory . COMPILE_JSON_TEST_FILENAME;

				$revisionPackage = fopen($revisionJsonFile, "w");
				if (! $revisionPackage) {
					throw new common_Exception("Unable to open file : $revisionJsonFile");
				}
				fwrite($revisionPackage, $revisionJsonOptimized);
				fclose($revisionPackage);
				$cleanFiles[] = $revisionJsonFile;

				// Write test-notoptimized.json file if debug flag is set to true
				if (!is_null($nonOptimizedRevisionArray)) {
					$revisionJsonFileNonOptimized = $revisionDirectory . DEBUG_COMPILE_FILENAME;

					$revisionPackageNonOptimized = fopen($revisionJsonFileNonOptimized, "w");
					if (! $revisionPackageNonOptimized) {
						throw new common_Exception("Unable to open file : $revisionJsonFile");
					}
					fwrite($revisionPackageNonOptimized, $revisionJsonNonOptimized);
					fclose($revisionPackageNonOptimized);
					$cleanFiles[] = $revisionJsonFileNonOptimized;
				}
			} catch (common_Exception $ex) {
				common_logger::e($ex->getMessage() . "Cannot write test.json file");
			}

			try {
				// Write metadata.json file
				$metadataJsonFile = $revisionDirectory . COMPILE_JSON_METADATA_FILENAME;

				$metadataPackage = fopen($metadataJsonFile, "w");
				if (! $metadataPackage) {
					throw new common_Exception("Unable to open file : $metadataJsonFile");
				}
				fwrite($metadataPackage, $metadataJson);
				fclose($metadataPackage);
				$cleanFiles[] = $metadataJsonFile;
			} catch (common_Exception $ex) {
				common_logger::e($ex->getMessage() . "Cannot write metadata.json file");
			}

			try {
				// Generate zip package
				$zipFile = new ZipArchive();
				$zipFile->open($this->dataPath . $revisionId . ".zip", ZIPARCHIVE::CREATE);
				$zipFile->addFile($revisionJsonFile, COMPILE_JSON_TEST_FILENAME);
				$zipFile->addFile($metadataJsonFile, COMPILE_JSON_METADATA_FILENAME);

				// Adding Non Optimized test file if debug flag is set to true
				if (!is_null($nonOptimizedRevisionArray)) {
					$zipFile->addFile($revisionJsonFileNonOptimized, DEBUG_COMPILE_FILENAME);
				}
			} catch (common_Exception $ex) {
				common_logger::e("Cannot aio package");
			}
			// Adding resources
			foreach ($this->assetTypes as $type) {
				if (@isset($this->resourceManifest[$type])) {
					$i = 1;
					foreach ($this->resourceManifest[$type] as $key => $asset) {
						$failed = false;
						$fileType = '';

						if (taoTestBatteries_helpers_Utils::isSupportedFiletype($asset['ASSET'], $fileType)) {
							if (taoTestBatteries_helpers_Utils::isExternalUrl($asset['ASSET'])) {
								$cResult = taoTestBatteries_helpers_Utils::curlApiCall($asset['ASSET'], null, 'GET');
								if (is_null($cResult)) {
									$failed = true;
								} else {
									$zipFile->addFromString($type . DIRECTORY_SEPARATOR . self::ADPizeURL($asset["ASSET"], $asset["COUNTER"], false), $cResult);
								}
							} else {
								if (file_exists($asset['ASSET'])) {
									if(ctype_upper(pathinfo(basename($asset['ASSET']), PATHINFO_EXTENSION))) {
										$zipFile->addFile($asset['ASSET'], $type . DIRECTORY_SEPARATOR . $asset['COUNTER'] . substr_replace (basename($asset['ASSET']) ,
											strtolower(pathinfo(basename($asset['ASSET']), PATHINFO_EXTENSION)),
											strrpos(basename($asset['ASSET']) , '.') +1));
									} else {
										$zipFile->addFile($asset['ASSET'], $type . DIRECTORY_SEPARATOR . $asset['COUNTER'] . basename($asset['ASSET']));
									}
								} else {
									$failed = true;
								}
							}

							if ($failed) {
								common_logger::e("The resource " . $asset['ASSET'] ." cannot be found.");
								throw new common_Exception("The resource " . $asset['ASSET'] ." cannot be found.");
							}
						} else {
							throw new common_Exception("The resource " . basename($asset['ASSET']) . " is not a supported file type.");
						}
						$i ++;
					}
				}
			}
			$zipFile->close();

			// Clean-up revision folder
			foreach ($cleanFiles as $file) {
				unlink($file);
			}

			// Remove directory
			rmdir($revisionDirectory);
		} catch (common_Exception $ex) {
			// Rollback
			$this->compilationRollback($revisionUri);

			common_logger::e($ex->getMessage());
			common_Logger::w("Compilation failed. Deleting form revision:" . $revisionUri);

			throw new common_Exception("An error occurred while compiling the test " . self::getIdFromUri($testUri) . ". Please check the log file for error details.");
		}

		// Check if the zip file exists on data folder
		if (!file_exists(substr($revisionDirectory, 0, -1) . '.zip')) {
			common_Logger::w("Compilation failed. Deleting form revision:" . $revisionUri);
			$this->compilationRollback($revisionUri);
			common_logger::e("File" . substr($revisionDirectory, 0, -1) . ".zip is not existing");
			throw new common_Exception("File " . substr($revisionDirectory, 0, -1) . ".zip is not existing");
		}
	}

	/**
	 * Function to generate metadata array
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($revisionUri)
	 * @param String ($testUri)
	 * @return array ($metadata)
	 */
	private function createMetadata($revisionUri, $testUri)
	{

		$metadata = [];

		// Getting revision info
		$revision = [];
		$revisionClass = new core_kernel_classes_Class($revisionUri);

		$revision["id"] = self::getIdFromUri($revisionUri);
		$revision["version"] = current($revisionClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_NUMBER)));
		$revision["compiledDateTime"] = current($revisionClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_COMPILATION_TIME)));
		$revision["compiledBy"] = taoTestBatteries_helpers_Utils::getUserDisplayName(current($revisionClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_COMPILED_BY))), $this->displayUserMaxLength);

		// Getting form info
		$form = [];
		$formClass = new core_kernel_classes_Class(current($revisionClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_FORM))));

		$form["id"] = self::getIdFromUri($formClass->getUri());
		$form["name"] = $formClass->getLabel();
		$testClass = new core_kernel_classes_Class($testUri);
		$form["testId"] = self::getIdFromUri($testClass->getUri());
		$form["testName"] = taoTestBatteries_helpers_Utils::formatTestDisplayName($testClass->getLabel(), 100);

		// Getting battery info
		$battery = [];
		$batteryClass = new core_kernel_classes_Class(current($formClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_TESTBATTERY))));

		$battery["id"] = self::getIdFromUri($batteryClass->getUri());
		$battery["name"] = $batteryClass->getLabel();
		$programClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTINGPROGRAM))));
		$battery["program"] = current($programClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$subjectClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SUBJECT))));
		$battery["subject"] = current($subjectClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$gradeClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_GRADE))));
		$battery["grade"] = current($gradeClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$itemSelectionAlgorithmClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_ITEMSELECTIONALGORITHM))));
		$battery["itemSelectionAlgorithm"] = current($itemSelectionAlgorithmClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$securityClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SECURITY))));
		$battery["security"] = current($securityClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$scoreReportClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SCOREREPORT))));
		$battery["scoreReport"] = current($scoreReportClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$multimediaClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_MULTIMEDIA))));
		$battery["multimedia"] = current($multimediaClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$scoringClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTSCORING))));
		$battery["scoring"] = current($scoringClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$permissionsClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPERMISSIONS))));
		$battery["permissions"] = current($permissionsClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$privacyClass = new core_kernel_classes_Class(current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPRIVACY))));
		$battery["privacy"] = current($privacyClass->getPropertyValues(new core_kernel_classes_Property(RDF_VALUE)));
		$battery["description"] = current($batteryClass->getPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTDESCRIPTION)));

		$metadata["battery"] = $battery;
		$metadata["form"] = $form;
		$metadata["revision"] = $revision;

		return $metadata;
	}

	/**
	 * Function to create the revision JSON Object
	 *
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param AssessmentTest (test Doc)
	 * @return Array (revisionJsonObject)
	 */
	public function createRevisionArray(qtism\data\AssessmentTest $testDoc, $batteryUri)
	{
		$this->itemService = taoItems_models_classes_ItemsService::singleton();
		$revisionJSONObject = [];

		$revisionJSONObject["identifier"] = $testDoc->getIdentifier();
		$revisionJSONObject["title"] = DEBUG_COMPILE_PREFIX . $testDoc->getTitle();
		$revisionJSONObject["pub_schemaVersion"] = PUB_SCHEMA_VERSION;

		if ($testDoc->hasTimeLimits()) {
			$revisionJSONObject["timeLimits"]["maxTime"] = ($testDoc->getTimeLimits()->hasMaxTime() ? $testDoc->getTimeLimits()->getMaxTime()->getSeconds(true) : null);
		}
		$testPartCounter = 0;
		// Get test parts
		foreach ($testDoc->getTestParts() as $part) {
			$partArray = [];

			$partArray["identifier"] = $part->getIdentifier();
			$partArray["navigationMode"] = ($part->getNavigationMode() ? "nonlinear" : "linear");
			$partArray["submissionMode"] = ($part->getSubmissionMode() ? "simultaneous" : "individual");

			if ($part->hasTimeLimits()) {
				$partArray["timeLimits"]["maxTime"] = ($part->getTimeLimits()->hasMaxTime() ? $part->getTimeLimits()->getMaxTime()->getSeconds(true) : null);
			}

			if ($part->getItemSessionControl() != null) {
				$partArray["itemSessionControl"]["maxAttempts"] = $part->getItemSessionControl()->getMaxAttempts();
				$partArray["itemSessionControl"]["showFeedback"] = $part->getItemSessionControl()->mustShowFeedback();
				$partArray["itemSessionControl"]["allowComment"] = $part->getItemSessionControl()->doesAllowComment();
				$partArray["itemSessionControl"]["allowSkipping"] = $part->getItemSessionControl()->doesAllowSkipping();
			}
			$sectionCounter = 0;
			// Get assessment sections
			foreach ($part->getAssessmentSections() as $assessmentSection) {
				$assessmentSectionArray = [];

				$assessmentSectionArray["identifier"] = $assessmentSection->getIdentifier();
				$assessmentSectionArray["title"] = $assessmentSection->getTitle();
				$assessmentSectionArray["visible"] = $assessmentSection->isVisible();
				$assessmentSectionArray["keepTogether"] = $assessmentSection->mustKeepTogether();

				if ($assessmentSection->hasSelection()) {
					$assessmentSectionArray["selection"]["select"] = $assessmentSection->getSelection()->getSelect();
					$assessmentSectionArray["selection"]["withReplacement"] = $assessmentSection->getSelection()->isWithReplacement();
				}
				if ($assessmentSection->hasOrdering()) {
					$assessmentSectionArray["ordering"]["shuffle"] = $assessmentSection->getOrdering()->getShuffle();
				}

				if ($assessmentSection->hasTimeLimits()) {
					$assessmentSectionArray["timeLimits"]["maxTime"] = (
					$assessmentSection->getTimeLimits()->hasMaxTime() ? $assessmentSection->getTimeLimits()->getMaxTime()->getSeconds(true) : null
					);
				}

				if ($assessmentSection->getItemSessionControl() != null) {
					$assessmentSectionArray["itemSessionControl"]["maxAttempts"] = $assessmentSection->getItemSessionControl()->getMaxAttempts();
					$assessmentSectionArray["itemSessionControl"]["showFeedback"] = $assessmentSection->getItemSessionControl()->mustShowFeedback();
					$assessmentSectionArray["itemSessionControl"]["allowComment"] = $assessmentSection->getItemSessionControl()->doesAllowComment();
					$assessmentSectionArray["itemSessionControl"]["allowSkipping"] = $assessmentSection->getItemSessionControl()->doesAllowSkipping();
				}

				try {
					$itemCounter = 0;
					// Get section items
					foreach ($assessmentSection->getSectionParts() as $itemRef) {
						$itemArray = $dataRefs = [];
						$idsArray = [$testPartCounter, $sectionCounter, $itemCounter];
						$itemParser = new taoTestBatteries_helpers_Parser($this->itemService->getItemContent(new core_kernel_classes_Resource($itemRef->getHref())));
						$itemPath = $this->itemService->getItemFolder(new core_kernel_classes_Resource($itemRef->getHref()));

						$itemClass = new core_kernel_classes_Resource($itemRef->getHref());
						$itemPacker = new Packer($itemClass);

						// Get all content for an item.
						$itemArray["assessmentItemContent"] = $itemParser->getItemArray($itemClass->getUri());

						foreach ($this->assetTypes as $type) {
							foreach ($itemPacker->pack()->getAssets($type) as $asset) {
								$counter = self::ADPizeURL('', $idsArray, false);

								// Check if resource is local or external or base64.
								if (taoTestBatteries_helpers_Utils::isExternalUrl($asset)) {
									$revisionJSONObject["pub_resourceManifest"][] = self::ADPizeURL($asset, $idsArray);
									$assetArray = ['ASSET' => $asset, 'COUNTER' => $counter];
									$this->resourceManifest[$type][] = $assetArray;
								} elseif(taoTestBatteries_helpers_Utils::isDataUrl($asset)) {
									$filepath = self::createFileFromBase64($asset, $itemPath);
									$newUrl = self::ADPizeURL($filepath, $idsArray);
									$revisionJSONObject["pub_resourceManifest"][] = $newUrl;
									$assetArray = ['ASSET' => $filepath, 'COUNTER' => $counter];
									$this->resourceManifest[$type][] = $assetArray;
									$dataRefs[$asset] = $newUrl;
								} elseif (file_exists($itemPath . $asset)) {
									$revisionJSONObject["pub_resourceManifest"][] = self::ADPizeURL($itemPath . $asset, $idsArray);
									$assetArray = ['ASSET' => $itemPath . $asset, 'COUNTER' => $counter];
									$this->resourceManifest[$type][] = $assetArray;
								} elseif (strpos($asset, ASSETS_MEDIAMANAGER_PATH) !== false) {
									$resolver = new ItemMediaResolver(new core_kernel_classes_Resource($itemRef->getHref()), 'en');
									$mediaAsset = $resolver->resolve($asset);
									$mediaSource = $mediaAsset->getMediaSource();
									$srcPath = $mediaSource->download($mediaAsset->getMediaIdentifier());

									if (file_exists($srcPath)) {
										$revisionJSONObject["pub_resourceManifest"][] = self::ADPizeURL($srcPath, $idsArray);
										$assetArray = ['ASSET' => $srcPath, 'COUNTER' => $counter];
										$this->resourceManifest[$type][] = $assetArray;
									}
								}
							}
						}

						if (! empty($itemRef->getCategories()->__toString())) {
							$itemArray["category"] = explode(",", $itemRef->getCategories()->__toString());
						}
						$itemArray["itemId"] = $this->getIdFromUri($itemRef->getHref());
						// TODO: Add Pearson item id
						$itemArray["required"] = $itemRef->isRequired();
						$itemArray["fixed"] = $itemRef->isFixed();

						if ($itemRef->hasTimeLimits()) {
							$itemArray["timeLimits"]["maxTime"] = ($itemRef->getTimeLimits()->hasMaxTime() ? $itemRef->getTimeLimits()->getMaxTime()->getSeconds(true) : null);
						}

						// Get assessmentItemContent data
						$itemContentData = $itemArray["assessmentItemContent"]->getData();

						// Skip generating required properties if item is a system item.
						$systemItemCategories = [
							'systemItem',
							'notScored'
						];
						if(!array_key_exists('category', $itemArray) || empty(array_intersect($systemItemCategories, $itemArray['category']))) {
							// Add score report-dependent properties.
							$scoreReportRequiredProperties = $this->getScoreReportRequiredPropertyList($batteryUri);

							if($scoreReportRequiredProperties === false) {
								throw new common_Exception("Couldn't find battery battery: $batteryUri");
							} else {
								foreach($scoreReportRequiredProperties as $uri => $propertyLabel) {
									$property = new core_kernel_classes_Property($uri);
									$propertyValue = $itemClass->getOnePropertyValue($property);
									$propertyValueClass = is_null($propertyValue) ? 'null' : get_class($propertyValue);

									switch($propertyValueClass) {
										case "core_kernel_classes_Literal":
											if(property_exists($propertyValue, 'literal') && strlen(trim($propertyValue->literal))) {
												$itemArray[$propertyLabel] = taoTestBatteries_helpers_Utils::parseForNumeric($propertyValue->literal);
												break;
											} else {
												throw new common_Exception("Required property '$propertyLabel' is missing");
											}

										case "core_kernel_classes_Resource":
											$label = $propertyValue->getLabel();
											if(strlen(trim($label))) {
												$itemArray[$propertyLabel] = $label;
												break;
											} else {
												throw new common_Exception("Required property '$propertyLabel' is missing");
											}

										default:
											throw new common_Exception("Required property '$propertyLabel' is missing");
									}
								}
							}
						}

						if (@isset($itemContentData["attributes"]["timeDependent"])) {
							$itemArray["timeDependent"] = $itemContentData['attributes']["timeDependent"];
						}

						// sanitize Item
						$this->sanitizeItem($itemArray, $idsArray, $dataRefs);

						// Add item to section
						$assessmentSectionArray["assessmentItem"][] = $itemArray; // k-v pair should be complete ADP URL. ([[ADP]]0_0_0_intro.png)
						$itemCounter ++;
					}
				} catch (common_Exception $ex) {
					$itemResource = new core_kernel_classes_Resource($itemRef->getHref());
					common_Logger::e($ex->getMessage() . " For Item: [URI] " . $itemResource->getUri() . " [LABEL] " . $itemResource->getLabel());
					throw new common_Exception($ex->getMessage() . " For Item : " . $itemResource->getLabel() . "  " . $itemResource->getUri());
				}
			// Add section to test part
				$partArray["assessmentSection"][] = $assessmentSectionArray;
				$sectionCounter++;
			}
		// Add test part to test
			$revisionJSONObject["testPart"][] = $partArray;
			$testPartCounter++;
		}

		// Remove (debug, stylesheets)
		$this->sanitizeRevision($revisionJSONObject);

		return $revisionJSONObject;
	}

	/**
	 * Function to return ID from URI
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param String ($uri)
	 * @return string ($id)
	 */
	public static function getIdFromUri($uri)
	{
		list($garbage, $id) = explode("#", $uri);
		return $id;
	}

	/**
	 * Function to rollback when compilation fail
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string ($revisionUri)
	 * @return boolean ($returnValue)
	 */
	private function compilationRollback($revisionUri)
	{
		common_Logger::i("Compilation Rollback started for $revisionUri");
		try {
			$returnValue = true;
			$revisionId = self::getIdFromUri($revisionUri);

			$compilationDirectory = $this->dataPath . $revisionId . DIRECTORY_SEPARATOR;

			// Delete compilation directory
			$this->compilationRollbackDeleteDir($compilationDirectory);

			// Delete revision from database
			$revisionClass = new core_kernel_classes_Resource($revisionUri);
			$revisionClass->delete();
		} catch (Exception $ex) {
			common_Logger::w("Compilation Rollback failed on " . $ex->getMessage());
			$returnValue = false;
		}

		common_Logger::i("Compilation Rollback ended for $revisionUri");

		return $returnValue;
	}

	/**
	 * Function to delete compilation directory
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string $path
	 */
	private function compilationRollbackDeleteDir($path)
	{
		// Delete directory with sub files
		if (is_dir($path)) {
			$files = glob($path . '*', GLOB_MARK);
			foreach ($files as $file) {
				if (is_dir($file)) {
					self::compilationRollbackDeleteDir($file);
				} else {
					unlink($file);
				}
			}
			rmdir($path);
		} else {
			common_Logger::i("compilation rollback directory is missing");
		}
	}

	/**
	 * Function to sanitize items by removing un-necessary elements and defaults values
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param array $itemArray
	 * @param array[int] $idsArray
	 * @param array[string] $dataRefs
	 * @throws common_Exception
	 * @internal param $array (&$itemArray) pass item by reference
	 */
	private function sanitizeItem(&$itemArray, array $idsArray, array $dataRefs = [])
	{
		$assets = [];

		// Get assetEncoders to use them on assetTypes
		foreach ($itemArray["assessmentItemContent"]->getAssetEncoders() as $key => $value) {
			$assetEncoders[] = $key;
		}
		// Get assets from Item Content
		foreach ($this->assetTypes as $type) {
			foreach ($itemArray["assessmentItemContent"]->getAssets($type) as $key => $asset) {
				if(taoTestBatteries_helpers_Utils::isDataUrl($key) && array_key_exists($key, $dataRefs)) {
					$key = $asset = $dataRefs[$key];
				}

				$assets[$type][$key] = $asset;
			}
		}

		// Build assessmentItemContentArray to access the elements of the packer
		$itemArray["assessmentItemContentArray"] = ["assetTypes" => $assetEncoders,
													"type" => $itemArray["assessmentItemContent"]->getType(),
													"data" => $itemArray["assessmentItemContent"]->getData(),
													"assets" => $assets,
													"assetEncoders" => $itemArray["assessmentItemContent"]->getAssetEncoders(),
													"nestedResourcesInclusion" => $itemArray["assessmentItemContent"]->isNestedResourcesInclusion()];

		// Associate  the built array to assessmentItemContent
		$itemArray["assessmentItemContent"] = $itemArray["assessmentItemContentArray"];

		// Delete the built array after association
		unset($itemArray["assessmentItemContentArray"]);

		// Get assessmentItemContent elements to change the prefix
		$elements = &$itemArray["assessmentItemContent"]["data"]["body"]["elements"];

		foreach ($elements as &$element) {
			if(@isset($element["attributes"]["src"])) {
				$element["attributes"]["src"] = $this->transformUrl($element["attributes"]["src"], $itemArray["itemId"], $idsArray);
			}

			if (@isset($element["object"]["attributes"]["data"])) {

				$element["object"]["attributes"]["data"] = $this->transformUrl($element["object"]["attributes"]["data"], $itemArray["itemId"], $idsArray);

				if (strpos($element["object"]["attributes"]["data"], '[[ADP]]') === false) {
					$element["object"]["attributes"]["data"] = self::ADPizeURL($element["object"]["attributes"]["data"], $idsArray);
				}

			}

			if (@isset($element["attributes"]["data"])) {
				$element["attributes"]["data"] = self::ADPizeURL($element["attributes"]["data"], $idsArray);
			}

			if (@isset($element["prompt"]["elements"]) && is_array($element["prompt"]["elements"])) {
				$promptElements = &$element["prompt"]["elements"];
				foreach ($promptElements as &$promptElement) {
					if (@isset($promptElement["attributes"]["src"])) {
						$promptElement["attributes"]["src"] = $this->transformUrl($promptElement["attributes"]["src"], $itemArray["itemId"], $idsArray);
					}
				}
			}

			if (@isset($element["choices"])) {
				$choices = &$element["choices"];
				foreach ($choices as &$choice) {
					if (@isset($choice["body"]["elements"])) {
						$choiceElements = &$choice["body"]["elements"];
						foreach ($choiceElements as &$choiceElement) {
							if (@isset($choiceElement["attributes"]["src"])) {
								$choiceElement["attributes"]["src"] = self::ADPizeURL($choiceElement["attributes"]["src"], $idsArray);
							}
						}
					}
				}

			}

			if (@isset($element["gapImgs"]) && is_array(($element["gapImgs"]))) {
				$gapImgsElements = &$element["gapImgs"];
				foreach ($gapImgsElements as &$gapImgsElement) {
					if (strpos($gapImgsElement["object"]["attributes"]["data"], '[[ADP]]') === false) {
						$gapImgsElement["object"]["attributes"]["data"] = self::ADPizeURL($gapImgsElement["object"]["attributes"]["data"], $idsArray);
					}
				}

			}

			if(@isset($element["body"]["elements"])) { // For data URLs in passages
				$element = &$element["body"]["elements"];
				foreach($element as &$resource) {
					if(@isset($resource["attributes"]["src"]) && array_key_exists($resource["attributes"]["src"], $dataRefs)) {
						$resource["attributes"]["src"] = $dataRefs[$resource["attributes"]["src"]];
						unset($dataRefs[$resource["attributes"]["src"]]);
					}
				}
			}
		}

		// Get assets to change the prefix
		$assets = &$itemArray["assessmentItemContent"]["assets"];

			foreach ($assets as $type => &$asset) {
				$i = 0;
				$counter = count($assets[$type]);

				if (is_array($asset)) {
					foreach ($asset as $key => &$value) {
						if ($i < $counter) {
							if(!in_array($key, $dataRefs)) { // Skip if resource was a data URL
								$asset[$key] = $this->transformUrl($value, $itemArray["itemId"], $idsArray);

								// Change the $key to be like 0_0_0_images.jpg
								$asset[$asset[$key]] = $asset[$key];
								unset($asset[$key]);
							}

							$i ++; // Should come out of this with k-v pair as ADP URL
						} else {
							break;
						}
					}
				}
			}
	}

	/**
	 * Function to sanitize revision by removing (debug, stylesheets) elements
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param Array (&$elementArray)
	 */
	private function sanitizeRevision(Array &$elementArray)
	{
		// Array of elements to be removed
		$sanitizeArray = ["debug", "stylesheets"];

		foreach ($elementArray as $key => &$element) {
			if (in_array($key, $sanitizeArray, true)) {
				unset($elementArray[$key]);
			} elseif (is_array($element)) {
				$this->sanitizeRevision($element);
			}
		}
	}

	/**
	 * Function to validate the battery
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($batteryUri)
	 * @param array $batteryPropertiesValues
	 * @throws common_Exception
	 */
	public function validateBattery($batteryUri, array $batteryPropertiesValues = null)
	{
		try {
			$batteryResource = new core_kernel_classes_Property(new core_kernel_classes_Resource($batteryUri));
			$batteryPropertiesValues = $batteryPropertiesValues ? : $batteryResource->getPropertiesValues([
						new core_kernel_classes_Property(RDFS_LABEL),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTINGPROGRAM),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SUBJECT),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_GRADE),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_ITEMSELECTIONALGORITHM),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SECURITY),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SCOREREPORT),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_MULTIMEDIA),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTSCORING),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPERMISSIONS),
						new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPRIVACY)
			]);

			foreach ($batteryPropertiesValues as $key => $batteryPropertyValue) {
				if (empty($batteryPropertyValue)) {
					throw new common_Exception('Cannot get all battery properties');
				} elseif (empty(current(current($batteryPropertyValue)))) {
					throw new common_Exception('One or more required battery properties are missing');
				}
			}
		} catch (Exception $ex) {
			common_Logger::e($ex->getMessage() .' For Battery: '. $batteryResource->getLabel() .' URI: '.$batteryUri. ' Property: '.$key);
			throw new common_Exception($ex->getMessage());
		}
	}

	/**
	 * Function to validate the form
	 * @author Oussama Saddane <oussama.saddane@breakth.com>
	 * @param String ($formUri)
	 * @throws common_Exception
	 */
	public function validateForm($formUri)
	{
		try {
			$formResource = new core_kernel_classes_Property(new core_kernel_classes_Resource($formUri));
			$formPropertiesValues = $formResource->getPropertiesValues([
						new core_kernel_classes_Property(RDFS_LABEL),
						new core_kernel_classes_Property(PROPERTY_FORM_TESTBATTERY)
			]);
			foreach ($formPropertiesValues as $key => $formPropertyValue) {
				if (empty($formPropertyValue)) {
					throw new common_Exception('Cannot get all form properties');
				} elseif (empty(current(current($formPropertyValue)))) {
					throw new common_Exception('One or more required form properties are missing');
				}
			}
		} catch (common_Exception $ex) {
			common_Logger::e($ex->getMessage() . ' For Form ' . $formResource->getLabel() . ' URI ' . $formResource->getUri(). ' Property: ' .$key);
			throw $ex;
		}
	}

	/**
	 * Function to Validate Form Revision
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($revisionUri)
	 * @throws common_Exception
	 */
	public function validateRevision($revisionUri)
	{
		try {
			$revisionResource = new core_kernel_classes_Property(new core_kernel_classes_Resource($revisionUri));
			$revisionPropertiesValues = $revisionResource->getPropertiesValues([
					new core_kernel_classes_Property(RDFS_LABEL),
					new core_kernel_classes_Property(PROPERTY_REVISION_FORM),
					new core_kernel_classes_Property(PROPERTY_REVISION_COMPILED_BY),
					new core_kernel_classes_Property(PROPERTY_REVISION_TEST),
					new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS),
					new core_kernel_classes_Property(PROPERTY_REVISION_RUNTIME),
					new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_TIME),
					new core_kernel_classes_Property(PROPERTY_REVISION_COMPILATION_TIME),
					new core_kernel_classes_Property(PROPERTY_REVISION_NUMBER)
			]);

			foreach ($revisionPropertiesValues as $key => $revisionPropertyValue) {
				if (empty($revisionPropertyValue)) {
					throw new common_Exception('Cannot get all form revision properties');
				} elseif (empty(current(current($revisionPropertyValue)))) {
					throw new common_Exception('One or more required form revision properties are missing');
				}
			}
		} catch (common_Exception $ex) {
			common_Logger::e($ex->getMessage() . ' For Form Revision' . $revisionUri . 'Property: ' . $key);
			throw $ex;
		}
	}

	/**
	 * Function to Validate Json Object against the Json Schema
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($testUri)
	 * @return Array ($revisionJson)
	 * @throws common_Exception
	 */
	public function validateJsonObject($revisionJson)
	{
		try {
			// Validate the JSON Object against the schema
			$store = new SchemaStore();
			$path = common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries')->getDir();
			$schema = file_get_contents($path . 'jsv4-php' . DIRECTORY_SEPARATOR . 'testJsonSchema.json');
			$store->add("schema_1", json_decode($schema));
			$isValidJson = Jsv4::validate(json_decode($revisionJson), $store->get("schema_1"));

			if (!$isValidJson->valid) {
				throw new common_Exception('Invalid Test Json');
			}
		} catch (common_Exception $ex) {
			$messages = [$ex->getMessage()];

			if (!empty($isValidJson->errors)) {
				foreach ($isValidJson->errors as $error) {
					$messages[] = $error->message . ' ==> Json Path ' . $error->dataPath . ' ==> Schema Path ' . $error->schemaPath;
				}
			}
			return $messages;
		}

		return true;
	}

	/**
	 * Transforms an assessmentItemContent's 'data' or 'src' attribute to an ADP-consumable string.
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param array &$element (pass-by-reference)
	 * @param int $testNum
	 * @param int $sectionNum
	 * @param int $itemNum
	 */
	private static function transformItemContentURL($element, $testNum, $sectionNum, $itemNum)
	{
		$key = (isset($element["attributes"]["src"]) ? "src" : (isset($element["object"]["attributes"]["data"]) ? "data" : null));
		$url = "";

		switch($key) {
			case "src":
				$url = self::ADPizeURL($element["attributes"]["src"], [$testNum, $sectionNum, $itemNum]);
				$element["attributes"]["src"] = $url;
				break;
			case "data":
				$url = self::ADPizeURL($element["object"]["attributes"]["data"], [$testNum, $sectionNum, $itemNum]);
				$element["object"]["attributes"]["data"] = $url;
				break;
			default:
				return false;
		}
		return $element;
	}

	/**
	 * Converts a data source URL into an ADP-consumable string.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $url
	 * @param array[3] or string $ids
	 * @param bool $prefix (optional)
	 * @param string (reference) $filepath (optional)
	 * @return string
	 * @throws common_Exception
	 */
	public static function ADPizeURL($url, $ids, $prefix = true, &$filepathRef = null)
	{
		$error = false;

		if (is_array($ids)) {
			if (count($ids) != 3) {
				$error = true;
			}
		} elseif (is_string($ids)) {
			$ids = explode('_', $ids);
		} else {
			$error = true;
		}

		list ($partNum, $sectionNum, $itemNum) = $ids;

		if (! (isset($url) && isset($partNum) && isset($sectionNum) && isset($itemNum))) {
			$error = true;
		}

		if ($error) {
			throw new common_Exception("Can't ADP-ize URL" . (isset($url) ? ": $url." : ""));
		}

		$supported = taoTestBatteries_helpers_Utils::isSupportedFiletype($url);

		if(ctype_upper(pathinfo($url, PATHINFO_EXTENSION))) {
			$url = substr_replace($url , strtolower(pathinfo($url, PATHINFO_EXTENSION)), strrpos($url , '.') +1);
		}

		$basename = basename($url);

		$prefix = $prefix ? "[[ADP]]" : "";

		return self::convertIllegalFilenameCharacters($prefix . $partNum . "_$sectionNum" . "_$itemNum" . "_$basename");
	}

	/**
	 * Replaces illegal filename characters with optional $replacement parameter.
	 * @param string $str
	 * @param string $replacement
	 * @return string
	 */
	public static function convertIllegalFilenameCharacters($str, $replacement = "_")
	{
		$illegalChars = [
		'?'
		];

		return str_replace($illegalChars, $replacement, $str);
	}

	/**
	 * Function to return media manager file path
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string ($uri)
	 * @return string (path)
	 * @throws common_Exception
	 */
	public static function getFilePathFromMediaManager($uri)
	{
		$mediaManagerConstants = common_ext_ExtensionsManager::singleton()->getExtensionById('taoMediaManager')->getManifest()->getConstants();
		require_once $mediaManagerConstants["BASE_PATH"] . "includes" . DIRECTORY_SEPARATOR . 'constants.php';

		$taoFileManagement = common_ext_ExtensionsManager::singleton()->getExtensionById('taoMediaManager')->getConfig("fileManager");

		$mediaFileObject = new core_kernel_classes_Resource($uri);

		try {
			$mediaFileName = $taoFileManagement->retrieveFile(current($mediaFileObject->getPropertyValues(new core_kernel_classes_Property(MEDIA_LINK))));
		} catch (Exception $ex) {
			common_Logger::e("[MediaManager] File does not exist for uri: $uri");
			throw new common_Exception("Can't find resource: $uri");
		}

		if (file_exists($mediaFileName)) {
			return $mediaFileName;
		}

		return false;
	}

	/**
	 * Function to sanitize the test
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @author Kendall parks <kendall.parks@breaktech.com>
	 * @param Array ($revisionArray)
	 * @param array ($level)
	 * @return Array ($sanitizedRevisionJson)
	 */
	public static function testOptimization($revisionArray, $testRules, array $level = [])
	{
		$levelRules = $testRules;
		$levelKey = $level;

		// Drill down into $levelRules to get the rules for the current level.
		if (count($level) > 0) {
			for ($i = 0; $i < count($level); $i++) {
				$levelRules = $levelRules[$level[$i]];
			}
		}

		foreach ($revisionArray as $key => &$element) {
			if (@isset($levelRules[$key])) {
				if (($revisionArray[$key] === $levelRules[$key]) || ($levelRules[$key] === '*') || gettype($revisionArray[$key]) === $levelRules[$key]) {
					unset($revisionArray[$key]);
				} elseif ($levelRules[$key] === 'select' && is_int($revisionArray[$key])) {
					unset($revisionArray[$key]);
				} // Step into lower level if the element is an array and rules exist for that element.
				elseif (is_array($element) && array_key_exists($key, $levelRules)) {
					$levelKey[] = $key;
					$element = self::testOptimization($element, $testRules, $levelKey);
					array_pop($levelKey);
				} elseif (count($element) != count($element, COUNT_RECURSIVE)) {
					foreach ($element as $elem) {
						$levelKey[] = $key;
						$element = self::testOptimization($elem, $testRules, $levelKey);
						array_pop($levelKey);
					}
				}
			} // To get here, $element has to be a numeric identifier for objects in an array. Step down into the element, but leave the level rules untouched.
			elseif (is_array($element)) {
				$element = self::testOptimization($element, $testRules, $levelKey);
			}
		}
		if (is_array($revisionArray) && count($revisionArray) == 0) {
			array_filter($revisionArray);
		}

		return $revisionArray;
	}

	/**
	 * Function to transform resources url's to relative url
	 * @author Oussama Saddane <oussama.saddane@breaktech.org>
	 * @param String $resourceUrl
	 * @param array[int] $idsArray
	 * @return String $resourceUrl
	 */
	private function transformUrl($resourceUrl, $itemId, array $idsArray)
	{
		$resolver = new ItemMediaResolver(new core_kernel_classes_Resource(LOCAL_NAMESPACE . '#' . $itemId), 'en-US');
		$mediaAsset = $resolver->resolve($resourceUrl);
		$mediaSource = $mediaAsset->getMediaSource();

		if ($mediaSource instanceof HttpSource) {
			$resourceUrl = self::ADPizeURL($mediaAsset->getMediaIdentifier(), $idsArray);
		} else {
			$srcPath = $mediaSource->download($mediaAsset->getMediaIdentifier());
			$resourceUrl = self::ADPizeURL($srcPath, $idsArray);
		}

		return $resourceUrl;
	}

	/**
	 * Creates a file from a given base64 data string.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $data
	 * @param string $location
	 * @return string
	 * @throws common_Exception
	 */
	private static function createFileFromBase64($data, $location) {
		list($type, $data) = explode(';', $data);
		list(, $type) = explode('/', $type);
		list($type, ) = explode('+', $type); // Get rid of +xml just in case it exists.
		list(, $data) = explode(',', $data);

		$mtime = strval((int)(fmod(microtime(true), 1.0) * 10000));
		$filepath = $location . "tmp$mtime.$type";
		$data = base64_decode($data);

		if(file_put_contents($filepath, $data) === false) {
			throw new common_Exception("Couldn't write file $filepath.");
		}
		return $filepath;
	}

	/**
	 * Gets the required properties for the item, dependent upon the battery's Score Report.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param string $batteryUri
	 * @return array (false if invalid Battery URI)
	 */
	private function getScoreReportRequiredPropertyList($batteryUri)
	{
		$battery = new core_kernel_classes_Resource($batteryUri);

		if(!$battery) {
			return false;
		}

		$scoreReportProperties = unserialize(SCORE_REPORT_REQUIRED_PROPERTIES);

		if($scoreReportProperties === false) {
			throw new common_Exception("Can't find item properties file.");
		}

		$scoreReport = $battery->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SCOREREPORT));

		if(array_key_exists($scoreReport->getUri(), $scoreReportProperties)) {
			$requiredProperties = $scoreReportProperties[$scoreReport->getUri()];
			return array_key_exists('REQUIRED', $requiredProperties) ? $requiredProperties['REQUIRED'] : [];
		} else {
			return [];
		}
	}
}
