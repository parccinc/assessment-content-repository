<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * QTI Compilation to provide helpers functions
 *
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries
 *
 * @license GPLv2  http://www.opensource.org/licenses/gpl-2.0.php
 */

class taoTestBatteries_helpers_Config
{
	/**
	 * Default config file
	 * @var string
	 */
	const DEFAULTCONFIG = 'testBatteries';
	
	/**
	 * Default file source config value
	 * @var String
	 */
	const DEFAULTFILESOURCE = 'defaultTestBatteriesFileSource';
	
	/**
	 * static function to define defaults config constants
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	public static function getDefaultsConfig()
	{
		
		$configDefine = \common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries')->getConfig(self::DEFAULTCONFIG);
		foreach ($configDefine as $key => $value) {
			if (!defined($key)) {
				define($key, $value);
			}
		}
	}
	
	/**
	 * static function to return default file source path
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @throws Exception when can't retrieve defaultFileSource path
	 */
	public static function getDefaultFileSource()
	{
		$return_value = false;
		$configClass = new core_kernel_classes_Resource(\common_ext_ExtensionsManager::singleton()->getExtensionById('taoTestBatteries')->getConfig(self::DEFAULTFILESOURCE));
		try {
			$prop = new core_kernel_classes_Property(PROPERTY_GENERIS_VERSIONEDREPOSITORY_PATH);
			$return_value = current($configClass->getPropertyValues($prop));
		} catch (Exception $ex) {
			common_Logger::e("Unable to retrieve defaultFileSource");
		}
		if ($return_value) {
			return $return_value;
		} else {
			common_Logger::e("Unable to retrieve defaultFileSource");
			throw new common_Exception("Unable to retrieve defaultFileSource");
		}
	}
}
