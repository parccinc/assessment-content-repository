<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\lock\LockManager;
use oat\tao\model\accessControl\AclProxy;

/**
 * Forms Controller provide actions performed from url resolution
 *
 * @author Oussama Saddane <oussama.saddane@breaktech.com>
 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries

 * @license GPLv2  http://www.opensource.org/licenses/gpl-2.0.php
 *
 */


class taoTestBatteries_actions_Forms extends tao_actions_SaSModule
{

	/**
	 * @var array $forms
	 */
	private $forms = [];

	/**
	 * Path to data folder
	 * @var string
	 */
	private $dataPath = null;

	/**
	 *
	 * @var array $revisionActions
	 */
	private $revisionActions = [];

	private $qtiTestService = null;

	protected $lockManager;

	/**
	 * constructor: initialize the service and the default data
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param String ($dataFolder)
	 */
	public function __construct($dataFolder = null, $qtiTestService = null)
	{
		parent::__construct();

		// Load defaults configs
		taoTestBatteries_helpers_Config::getDefaultsConfig();

		$this->dataPath = FILES_PATH . 'taoBatteries'. DIRECTORY_SEPARATOR . (is_null($dataFolder) ? '' : ($dataFolder . DIRECTORY_SEPARATOR));

		//the service is initialized by default
		$this->defaultData();

		// initialize revisions actions array
		$this->initRevisionActions();

		$this->qtiTestService = $qtiTestService ? : taoQtiTest_models_classes_QtiTestService::singleton();
		$this->lockManager = new taoTestBatteries_helpers_LockManager();
	}

	/**
	 * get the main class
	 * @return \core_kernel_classes_Class
	 */
	protected function getRootClass()
	{
		return new core_kernel_classes_Class(CLASS_FORM);
	}

	/**
	 * getFormRevisions
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param string $formUri
	 * @return array revision data.
	 */
	protected function getFormRevisions($formUri = '')
	{
		if (empty($this->forms)) {
			$this->getForms();
		}

		if (isset($this->forms[$formUri])) {
			return $this->forms[$formUri]['revisions'];
		} else {
			return [];
		}
	}

	/**
	 * Create New Form
	 *
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @throws \common_Exception
	 */
	public function newForm()
	{
		$batteryUri = $this->getRequestParameter('battery_uri');
		//Check if battery is in use
		$battery = new core_kernel_classes_Resource($batteryUri);
		if (!$battery->exists()) {
			throw new common_Exception('This battery no longer exists. Please refresh the page.');
		}

		if (!$this->isLocked($battery, 'battery_locked.tpl')) {
			$batteryLabel = $this->getRequestParameter('battery_label');

			try {
				$batteryPropertiesValues = $battery->getPropertiesValues([
					new core_kernel_classes_Property(RDFS_LABEL),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTINGPROGRAM),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SUBJECT),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_GRADE),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_SECURITY),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_MULTIMEDIA),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTSCORING),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPERMISSIONS),
					new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_TESTPRIVACY)
				]);

				foreach ($batteryPropertiesValues as $batteryPropertyValue) {
					if (empty($batteryPropertyValue)) {
						throw new common_Exception('You cannot create a form because one or more required battery properties are missing for ' . $batteryLabel . '. Please supply the required battery properties.');
					}
				}

				$this->setData('battery_uri', $batteryUri);
				$this->setData('battery_label', $batteryLabel);
				$this->setData('tests_array', $this->getQtiTests());
				$this->setView('new_form.tpl');
			} catch (common_Exception $ex) {
				throw new common_Exception($ex->getMessage());
			}
		}
	}

	/**
	 * getQtiTests
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @param String ($linkedTestUri)
	 * @return array Qti Tests
	 */
	private function getQtiTests($linkedTestUri = '')
	{
		common_ext_ExtensionsManager::singleton()->getExtensionById('taoQtiTest');
		$testClass = new core_kernel_classes_Class(TAO_TEST_CLASS);
		$tests = $testClass->searchInstances([PROPERTY_TEST_TESTMODEL => INSTANCE_TEST_MODEL_QTI], ['like' => false, 'recursive' => true]);
		$testsArray = [];
		foreach ($tests as $test) {
			$selected = ($linkedTestUri == $test->getUri() ? true : false);
			$testsArray[] = ['uri' => $test->getUri(),
				'label' => $test->getLabel(),
				'selected' => $selected
			];
		}

		if (count($testsArray) > 0) {
			$testsArray = taoTestBatteries_helpers_Utils::subvalSort($testsArray, 'label');
		}
		return $testsArray;
	}

	/**
	 * Get data for Forms/Form Revisions/Form Revision Properties/Published Revision Properties display
	 * @return mixed
	 * @throws common_Exception
	 * @throws common_exception_Error
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @todo this function needs more review
	 */
	public function renderFormsUI()
	{
		try {
			$battery = $this->hasRequestParameter('battery_uri') ? new core_kernel_classes_Resource($this->getRequestParameter('battery_uri')) : $this->getCurrentInstance();

			if (!$battery->exists()) {
				throw new Exception('This battery no longer exists. Please refresh the page.');
			}

			$batteryUri = $battery->getUri();
			$batteryLabel = $battery->getLabel();
			$batteryId = parse_url($batteryUri, PHP_URL_FRAGMENT);

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				$forms = $this->getForms($batteryUri);

				if (empty($forms)) {
					$formRevisions = [];
					$formSelected = $formId = '';
				} else {
					$formSelected = $this->hasRequestParameter('form_selected') ? $this->getRequestParameter('form_selected') : key($forms);
					$formRevisions = $this->getFormRevisions($formSelected);
					$formId = parse_url($formSelected, PHP_URL_FRAGMENT);
				}

				if (empty($formRevisions)) {
					$revisionSelected = '';
				} else {
					$revisionSelected = $this->hasRequestParameter('revision_selected') ? $this->getRequestParameter('revision_selected') : $formRevisions[key($formRevisions)]['revisionUri'];
				}

				$revisionSelectedNumber = $this->getRevisionSelectedNumber($formRevisions, $revisionSelected);

				if (!empty($forms) && !empty($formRevisions) && !array_key_exists($revisionSelectedNumber, $formRevisions)) {
					throw new Exception('The resource is unavailable.');
				}

				$activationStatus = 'Unknown';
				$revisionActions = [];
				$revisionId = $adpJobId = $adpFormRevisionId = $adpFormId = $adpBatteryId = $publishErrorDesc = '';

				if (!empty($formRevisions)) {
					$revisionUri = $revisionSelected;
					$revisionId = parse_url($revisionUri, PHP_URL_FRAGMENT);
					$publishStatus = taoTestBatteries_helpers_Utils::getRevisionPublishStatus($revisionUri);
					$adpFormRevisionId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($revisionUri, PROPERTY_REVISION_ADP_FORM_REVISION_ID);
					$adpFormId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($formSelected, PROPERTY_FORM_ADP_FORM_ID);
					$adpBatteryId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($batteryUri, PROPERTY_TEST_BATTERY_ADP_BATTERY_ID);
					$adpJobId = taoTestBatteries_helpers_Utils::getADPJobId($revisionUri);
					$publishErrorDesc = taoTestBatteries_helpers_Utils::getPublishingErrorDescription($revisionUri);

					if (($publishStatus == PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING ||
						 $publishStatus == PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING) &&
						 empty($publishErrorDesc)) {
						$publishErrorDesc = taoTestBatteries_helpers_Utils::formatEmptyValues($publishErrorDesc);
					}

					$this->initRevisionActions($revisionUri);
					$revisionActions = $this->getRevisionActions($publishStatus);
					$activationStatus = taoTestBatteries_helpers_Utils::getActivationStatus($revisionUri);
				}

				//Format dates from UTC to TAO local TIME_ZONE
				$dateFormatted = taoTestBatteries_helpers_Utils::dateTimeFormat($formRevisions, $revisionSelected);

				if (empty($dateFormatted)) {
					$dateFormatted['localCompileDateTime'] = $dateFormatted['localPublishDateTime'] = '';
				}

				// Create success message if battery properties were successfully updated.
				if($this->hasRequestParameter('updated')) {
					if($this->getRequestParameter('updated') === 'true') {
						$this->setData('message', 'Battery properties updated.');
					} else {
						throw new common_Exception("Failed updating battery properties.");
					}
				}

				$this->setData('battery_uri', taoTestBatteries_helpers_Utils::formatEmptyValues($batteryUri));
				$this->setData('battery_id', taoTestBatteries_helpers_Utils::formatEmptyValues($batteryId));
				$this->setData('battery_label', taoTestBatteries_helpers_Utils::formatEmptyValues($batteryLabel));
				$this->setData('forms', $forms);
				$this->setData('form_selected', taoTestBatteries_helpers_Utils::formatEmptyValues($formSelected));
				$this->setData('revision_selected_number', taoTestBatteries_helpers_Utils::formatEmptyValues($revisionSelectedNumber));
				$this->setData('form_id', taoTestBatteries_helpers_Utils::formatEmptyValues($formId));
				$this->setData('form_revisions', taoTestBatteries_helpers_Utils::formatEmptyValues($formRevisions));
				$this->setData('revision_selected', taoTestBatteries_helpers_Utils::formatEmptyValues($revisionSelected));
				$this->setData('revision_id', taoTestBatteries_helpers_Utils::formatEmptyValues($revisionId));
				$this->setData('revision_actions', $revisionActions);
				$this->setData('adp_job_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpJobId));
				$this->setData('adp_form_revision_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpFormRevisionId));
				$this->setData('adp_form_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpFormId));
				$this->setData('adp_battery_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpBatteryId));
				$this->setData('publish_error_desc', $publishErrorDesc);
				$this->setData('local_compileDateTime', taoTestBatteries_helpers_Utils::formatEmptyValues($dateFormatted['localCompileDateTime']));
				$this->setData('local_publishDateTime', taoTestBatteries_helpers_Utils::formatEmptyValues($dateFormatted['localPublishDateTime']));
				$this->setData('activation_status', $activationStatus);
				$this->setView('forms.tpl');
			}
		} catch (Exception $ex) {
			$message = $ex->getMessage();
			common_Logger::w($message);
			throw new common_exception_Error($message);
		}
	}

	/**
	 * Get forms data
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@brektech.com>
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param String $battery_uri
	 * @throws common_Exception
	 * @return array(<form_uri>, <form_label>, <form_revision_number>, <associated_test_uri>) for each form as an array
	 */
	protected function getForms($battery_uri = null)
	{
		$forms = [];

		if (is_null($battery_uri)) {
			if ($this->hasRequestParameter('battery_uri')) {
				$batteryUri = tao_helpers_Uri::decode($this->getRequestParameter('battery_uri'));
			} else {
				throw new common_Exception('Battery is required!');
			}
		} else {
			$batteryUri = tao_helpers_Uri::decode($battery_uri);
		}

		$formClass = new core_kernel_classes_Class(CLASS_FORM);
		$instances = $formClass->searchInstances([PROPERTY_FORM_TESTBATTERY => $batteryUri]);

		foreach ($instances as $instance) {
			$formLabel = $instance->getLabel();
			$formUri = $instance->getUri();
			$test = taoTestBatteries_helpers_Utils::getFormTest($formUri);

			if (is_null($test)) {
				$forms[$formUri] = ['label' => $formLabel, "revisions" => [], 'test' => null];
				continue;
			}

			$revisions = [];
			foreach (taoTestBatteries_helpers_Utils::getRevisionsByFormUri($formUri) as $revision) {
				$test = new core_kernel_classes_Class(current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_TEST))));
				$publishStatus = new core_kernel_classes_Class(current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS))));
				$compiledUser = new core_kernel_classes_Class(current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_COMPILED_BY))));

				$publishedBy = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_BY));
				if (!empty(current($publishedBy))) {
					$publishedUser = new core_kernel_classes_Class(current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_BY))));
					$publishedUser = taoTestBatteries_helpers_Utils::getUserDisplayName($publishedUser->getUri());
				} else {
					$publishedUser = '';
				}

				$revisions[current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_NUMBER)))] = [
					'revisionUri' => $revision->getUri(),
					'revisionNumber' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_NUMBER))),
					'compileDateTime' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_COMPILATION_TIME))),
					'publishStatus' => $publishStatus->getLabel(),
					'publishErrorDescription' =>  current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION))),
					'deliveryRuntime' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_RUNTIME))),
					'publishSDateTime' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_TIME))),
					'testUri' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_TEST))),
					'test' => $test->getLabel(),
					'compiledBy' => taoTestBatteries_helpers_Utils::getUserDisplayName($compiledUser->getUri()),
					'form' => current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_FORM))),
					'downloadURL' => _url("downloadRevision", "Forms", "taoTestBatteries", ["revisionUri" => tao_helpers_Uri::encode($revision->getUri())]),
					'publishedBy' => $publishedUser
				];
			}

			$forms[$formUri] = ['label' => $formLabel, "revisions" => $revisions, 'test' => $test->getUri()];
		}

		$this->forms = $forms;
		return $forms;
	}

	/**
	 * Compile form request screen
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @throws common_Exception
	 * @return void
	 */
	public function compileForm()
	{
		try {
			$batteryUri = $this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : false;
			$batteryLabel = $this->hasRequestParameter('battery_label') ? $this->getRequestParameter('battery_label') : false;
			$formSelected = $this->hasRequestParameter('form_selected') ? $this->getRequestParameter('form_selected') : false;

			$battery = new core_kernel_classes_Resource($batteryUri);
			if (!$battery->exists()) {
				throw new Exception('This battery no longer exists. Please refresh the page.');
			}

			$linkedTestUri = '';

			//Check if battery is in use
			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				$forms = $this->getForms($batteryUri);
				$test = taoTestBatteries_helpers_Utils::getFormTest($formSelected);
				if (!is_null($test)) {
					$linkedTestUri = $test->getUri();
				}

				// Cautiously retrieve form and label.
				$thisForm = array_key_exists($formSelected, $forms) ? $forms[$formSelected] : false;
				$formLabel = $thisForm ? $thisForm['label'] : false;
				if (!($thisForm && $formLabel)) {
					throw new Exception('This form no longer exists. Please refresh the page.');
				}

				$this->setData('battery_uri', $batteryUri);
				$this->setData('battery_label', $batteryLabel);
				$this->setData('form_label', $formLabel);
				$this->setData('tests_array', $this->getQtiTests($linkedTestUri));
				$this->setData('form_uri', $formSelected);
				$this->setView('form_compile.tpl');
			}
		} catch (Exception $ex) {
			$message = $ex->getMessage();
			common_Logger::e($message);
			throw new common_Exception($message);
		}
	}

	/**
	 * Function to compile the form and create a new revision
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 */
	public function compileFormNow()
	{
		try {

			// Setting the time limit to 1 hour.
			set_time_limit(EXECUTION_TIME_LIMIT);

			// Increase Memory Limit Consumption.
			ini_set('memory_limit', MEMORY_LIMIT);

			common_ext_ExtensionsManager::singleton()->getExtensionById('taoQtiTest');
			$testUri = $this->getRequestParameter('test_uri');
			$batteryLabel = $this->getRequestParameter('battery_label');
			$batteryUri = $this->getRequestParameter('battery_uri');
			$battery = new core_kernel_classes_Resource($batteryUri);
			if (!$battery->exists()) {
				throw new common_Exception('This battery no longer exists. Please refresh the page.');
			}

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				// Locking the battery
				$this->lockManager->smartLock($battery);

				if (TAOTESTBATTERIES_DEBUG) {
					common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
					sleep(DEBUG_PAUSE_DURATION);
					common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
				}

				$formUri = $this->getRequestParameter('form_uri');
				$formLabel = $this->getRequestParameter('form_label');
				$revisionNumber = taoTestBatteries_helpers_Utils::getNextRevisionNumber($formUri);

				// Saving Revision Properties
				$revisionClass = new core_kernel_classes_Class(CLASS_REVISION);
				$revision = $revisionClass->createInstanceWithProperties(
					[
							RDFS_LABEL => $revisionNumber,
							PROPERTY_REVISION_NUMBER => $revisionNumber,
							PROPERTY_REVISION_COMPILATION_TIME => time(),
							PROPERTY_REVISION_PUBLISHING_TIME => 'False', // To be added with actual compilation
							PROPERTY_REVISION_RUNTIME => 'False', // To be added with actual compilation
							PROPERTY_REVISION_PUBLISHING_STATUS => PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED,
							PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION => '',
							PROPERTY_REVISION_TEST => $testUri,
							PROPERTY_REVISION_COMPILED_BY => common_session_SessionManager::getSession()->getUser()->getIdentifier(),
							PROPERTY_REVISION_FORM => $formUri
					]
				);


				$compilationHelper = new taoTestBatteries_helpers_Compilation();
				// Validate Battery
				$compilationHelper->validateBattery($batteryUri);
				// Validate Form
				$compilationHelper->validateForm($formUri);
				//Validate Revision
				$compilationHelper->validateRevision($revision->getUri());
				// Validate Test
				try {
					// Validate Test
					$message = taoTestBatteries_helpers_Utils::validateFormTest($testUri);

					if ($message) {
						common_Logger::e("Test $testUri didn't pass validation: $message");
						throw new common_Exception($message);
					}
				} catch (common_Exception $ex) {
					common_Logger::e($ex->getMessage() . $message);
					throw $ex;
				}

				// Validate Items
				taoTestBatteries_helpers_Utils::isValidItems($testUri);

				$testDoc = $this->qtiTestService->getDoc(new core_kernel_classes_Resource($testUri))->getDocumentComponent();
				$revisionArray = $compilationHelper->createRevisionArray($testDoc, $batteryUri);

				// Test Rules.
				$testRules = unserialize(TEST_RULES);

				// Optimize Revision Array
				$optimizedRevisionArray = $compilationHelper->testOptimization($revisionArray, $testRules);

				// if a Practice Test, more optimization.
				$program = current($battery->getPropertyValues(new core_kernel_classes_Property
				(PROPERTY_TEST_BATTERY_TESTINGPROGRAM)));

				if ($program == PRACTICE_TEST) {
					common_Logger::i("Stripping more item metada for Practice Test (battery Uri: $batteryUri)");
					//Practice Test Rules.
					$practiceTestRules = unserialize(PRACTICE_TEST_RULES);
					// Optimize Revision Array
					$optimizedRevisionArray = $compilationHelper->testOptimization($optimizedRevisionArray, $practiceTestRules);
				}

				if (array_key_exists('title', $optimizedRevisionArray)) {
					$optimizedRevisionArray['title'] = preg_replace('/^'.DEBUG_COMPILE_PREFIX.'/', '', $optimizedRevisionArray['title']);
				}

				// Validate Json Object Against the Schema
				$isValid = $compilationHelper->validateJsonObject(json_encode($optimizedRevisionArray));

				if($isValid !== true) {
					foreach($isValid as $message) {
						common_Logger::e($message);
					}

					throw new common_Exception("Invalid test JSON.");
				}

				// Generating revision package
				if(DEBUG_COMPILE_NON_OPTIMIZATION) {
					$compilationHelper->createRevisionPackage($testUri, $revision->getUri(), $optimizedRevisionArray, $revisionArray);
				} else {
					$compilationHelper->createRevisionPackage($testUri, $revision->getUri(), $optimizedRevisionArray);
				}
				common_Logger::i('New version created! Uri:' . $revision->getUri() . ' Label:' . $revision->getLabel());

				$downloadUrl = _url("downloadRevision", "Forms", "taoTestBatteries", ["revisionUri" => tao_helpers_Uri::encode($revision->getUri())]);

				//Edit the test related with Form
				$form = new core_kernel_classes_Resource($formUri);
				$form->editPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_TEST), $testUri);

				$test = new core_kernel_classes_Resource($testUri);

				common_Logger::i('New version created! Uri:' . $revision->getUri() . ' Label:' . $revision->getLabel());

				// Set hasCompiledRevision battery property
				taoTestBatteries_helpers_Utils::setHasCompiledRevision($batteryUri, true);

				$this->setData('battery_label', $batteryLabel);
				$this->setData('battery_uri', $batteryUri);
				$this->setData('form_label', $formLabel);
				$this->setData('form_uri', $formUri);
				$this->setData('test_label', $test->getLabel());
				$this->setData('revision_number', $revisionNumber);
				$this->setData('revision_selected', $revision->getUri());
				$this->setData('download_url', $downloadUrl);
				$this->setData('test_uri', $testUri);
				$this->setView('form_compile_result.tpl');

				// unlocking the battery
				$this->lockManager->smartRelease($battery);
			}
		} catch (common_Exception $ex) {
			common_Logger::w($ex->getMessage());

			// unlocking the battery
			if(isset($battery) && ($battery instanceof core_kernel_classes_Resource)) {
				$this->lockManager->smartRelease($battery, null, true);
			}

			// revision creation operation failed, we should clean the revision
			if (isset($revision) && ($revision instanceof core_kernel_classes_Resource) && $revision->exists()) {
				common_Logger::w("Cleaning up revision (" . $revision->getUri() . ") ");
				$revision->delete();
			}
			throw $ex;
		}
	}

	/**
	 * Function to download revision package
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 * @param string $revisionUri
	 * @throws common_Exception
	 */
	public function downloadRevision($revisionUri = null)
	{
		if ($revisionUri === null) {
			$revisionUri = $this->getRequestParameter("revisionUri");
		}

		list($namespace, $revisionId) = explode("#", tao_helpers_Uri::decode($revisionUri));

		$filePath = $this->dataPath . $revisionId . ".zip";
		if (file_exists($filePath)) {
			// concatenate revision ID
			$fileName = "test_" . $revisionId . ".zip";

			header('Content-type: application/force-download; filename="' . $fileName . '"');
			// download document if exists
			if ($filePath) {
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="' . $fileName . '"');
				header('Content-Transfer-Encoding: binary');
				header('Pragma: public, no-cache');
				header('Pragma-Directive: no-cache');
				header('Cache-Directive: no-cache');
				header('Cache-Control: no-store, no-cache, must-revalidate');
				header('Cache-Control: post-check=0, pre-check=0');
				header('Expires: -1');
				header('Content-Length: ' . filesize($filePath));
				if (ob_get_length()) {
					ob_clean();
				}
				flush();
				readfile($filePath);
			}
		} else {
			common_Logger::e('File ' . $filePath . ' not found.');
			throw new tao_models_classes_FileNotFoundException($filePath);
		}

		session_write_close();
		exit;
	}

	/**
	 * editForm edit a Form and sets its label, the battery uri it belongs to and the test uri it is associated with.
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @throws common_Exception
	 */
	public function editForm()
	{
		if ($this->hasRequestParameter('formLabel')) {
			$formLabel = tao_helpers_Uri::decode($this->getRequestParameter('formLabel'));
		} else {
			throw new common_Exception('Form label is required!');
		}

		if (strlen($formLabel) > 20) {
			throw new common_Exception('The Form Name provided exceeds the maximum length of 20 characters.<br>Please adjust the Form Name to 20 characters or less.');
		}

		if (empty($formLabel)) {
			throw new common_Exception('Form label should not be empty');
		}

		if ($this->hasRequestParameter('battery_uri')) {
			$batteryUri = tao_helpers_Uri::decode($this->getRequestParameter('battery_uri'));
		} else {
			throw new common_Exception('Battery is required!');
		}
		//Check if battery is in use
		$battery = new core_kernel_classes_Resource($batteryUri);

		if (!$this->isLocked($battery, 'battery_locked.tpl')) {
			// 	Validation if a form name is unique
			$formClass = new core_kernel_classes_Class(CLASS_FORM);
			$forms = taoTestBatteries_helpers_Utils::getFormsByBatteryUri($batteryUri);
			foreach ($forms as $form) {
				if (strtolower($formLabel) == strtolower($form->getLabel()) && $form->getUri() != tao_helpers_Uri::decode($this->getRequestParameter('form_uri'))) {
					throw new common_Exception('Form name already exists. Please choose a different Form name.');
				}
			}

			try {
				// Locking the battery
				$this->lockManager->smartLock($battery);

				if (TAOTESTBATTERIES_DEBUG) {
					common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
					sleep(DEBUG_PAUSE_DURATION);
					common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
				}

				if ($this->hasRequestParameter('form_uri')) {
					$selectedForm = new core_kernel_classes_Resource($this->getRequestParameter('form_uri'));
					$selectedForm->editPropertyValues(new core_kernel_classes_Property(RDFS_LABEL), $formLabel);
					$selectedForm->editPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_TEST), tao_helpers_Uri::decode($this->getRequestParameter('test')));
				} else {
					$selectedForm = $formClass->createInstanceWithProperties(
						[
								RDFS_LABEL => $formLabel,
								PROPERTY_FORM_TEST => tao_helpers_Uri::decode($this->getRequestParameter('test')),
								PROPERTY_FORM_TESTBATTERY => tao_helpers_Uri::decode($batteryUri)
						]
					);

					common_Logger::i('New form created! Uri:' . $selectedForm->getUri() . ' Label:' . $selectedForm->getLabel());
				}
				// unlocking the battery
				$this->lockManager->smartRelease($battery);

				// Redirecting to the main page.
				$this->redirect(_url('renderFormsUI', 'Forms', 'taoTestBatteries', [
					'battery_uri' => $batteryUri
				]));
			} catch (common_Exception $ex) {
				common_Logger::w($ex->getMessage());
				// unlocking the battery
				$this->lockManager->smartRelease($battery, null, true);
			}
		}
	}

	/**
	 * Validate if user has publishing permissions
	 *
	 * @author Bojan Vulevic <bojan.vulevic@breaktech.com>
	 * @return Boolean
	 */
	private function hasPublishPermission()
	{
		$user = \common_session_SessionManager::getSession()->getUser();
		// Check if user has permission to do publishing.
		if (AclProxy::hasAccess($user, 'taoTestBatteries_actions_Publish', 'publisher', [])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Publish (republish) a form revision
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @author Bojan Vulevic <bojan.vulevic@breaktech.com>
	 * @throws common_Exception
	 */
	public function publishFormRevision()
	{
		$battery = $republishUrlFragment = '';
		$method = 'POST';

		// Check first if user has permission to do publishing.
		if (!$this->hasPublishPermission()) {
			throw new common_exception_Unauthorized();
		}

		try {
			$batteryUri = $this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : null;
			$batteryId = parse_url($batteryUri, PHP_URL_FRAGMENT);

			//Check if battery is in use
			$battery = new core_kernel_classes_Resource($batteryUri);

			if (!$battery->exists()) {
				throw new common_Exception('This battery no longer exists. Please refresh the page.');
			}

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				// Make sure nothing interrupts publishing process once it initiates!
				ignore_user_abort(true);
				set_time_limit(0);

				// Locking the battery
				$this->lockManager->smartLock($battery, PUBLISHING_USER_URI);

				if (TAOTESTBATTERIES_DEBUG) {
					common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
					sleep(DEBUG_PAUSE_DURATION);
					common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
				}

				if ($this->hasRequestParameter('form_selected')) {
					$formSelected = $this->getRequestParameter('form_selected');
					$formId = parse_url($formSelected, PHP_URL_FRAGMENT);
				} else {
					throw new common_Exception('No Form selected for publishing');
				}

				$form = new core_kernel_classes_Resource($formSelected);
				$formUri = $form->getUri();

				if ($this->hasRequestParameter('revision_selected')) {
					$revisionSelected = $this->getRequestParameter('revision_selected');
					$revisionId = parse_url($revisionSelected, PHP_URL_FRAGMENT);
				} else {
					throw new common_Exception('No Revision selected for publishing');
				}

				$revision = new core_kernel_classes_Resource($revisionSelected);

				if (!$revision->exists()) {
					throw new common_Exception('Revision does not exist, or multiple revisions have the same revision number for form uri: ' . $formSelected);
				}

				$publishedBy = common_session_SessionManager::getSession()->getUserUri();
				$namePublishedBy = taoTestBatteries_helpers_Utils::getUserDisplayName($publishedBy);
				$data = ['testFormRevisionId' => $revisionId, 'publishedBy' => $namePublishedBy];

				$adpId = $revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_FORM_REVISION_ID));

				// Check if republish
				if (!empty($adpId)) {
					$republishUrlFragment = "/{$adpId[0]}";
					$method = 'PUT';
				}

				$headers = [
					'Authorization: Basic ' . base64_encode(REMOTE_ADP_USERNAME . ":" . REMOTE_ADP_PASSWORD),
					'Content-Type:application/json; charset=UTF-8',
					'Authentication: ' . taoTestBatteries_helpers_Utils::generateHmacSignature(ADP_PUBLISH_SERVICE . $republishUrlFragment),
					'X-Requested-With:' . REMOTE_ADP_SECRET
				];

				// Call ADP_PUBLISH_SERVICE
				$adpResponse = taoTestBatteries_helpers_Utils::curlApiCall(ADP_HOST . ADP_PUBLISH_SERVICE . $republishUrlFragment,
																		   json_encode($data),
																		   $method,
																		   'data',
																		   $headers);

				common_Logger::i("[Publish Response] $adpResponse");

				$response = taoTestBatteries_helpers_Utils::parseResponse($adpResponse);
				$status = $response['success'];

				if ($status) {
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($batteryUri, true);
					common_Logger::i("setHasPublishedRevision to true for battery (" . $batteryUri . ")");
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($formUri, true);
					common_Logger::i("setHasPublishedRevision to true for form (" . $formUri . ")");
				}

				$publishedProperties['jobId'] = $response['requestId'];
				$publishedProperties['errorDescription'] = $response['errorDescription'];
				$publishedProperties['dateTime'] = $response['publishedTimestamp'];

				$publishStatus = $this->getNewPublishStatus($status, $revision);
				$this->initRevisionActions();
				$revisionActions = $this->getRevisionActions($publishStatus);
				$this->savePublishedRevisionProperties($publishStatus, $publishedProperties, $batteryUri, $formSelected, $revision);

				$adpFormRevisionId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($revisionSelected, PROPERTY_REVISION_ADP_FORM_REVISION_ID);
				$adpFormId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($formSelected, PROPERTY_FORM_ADP_FORM_ID);
				$adpTestBatteryId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($batteryUri, PROPERTY_TEST_BATTERY_ADP_BATTERY_ID);
				$publishErrorDesc = $publishedProperties['errorDescription'];

				if (($publishStatus == PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING ||
					 $publishStatus == PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING) &&
					 empty($publishErrorDesc)) {
					$publishErrorDesc = taoTestBatteries_helpers_Utils::formatEmptyValues($publishedProperties['errorDescription']);
				}

				$forms = $this->getForms();
				$formRevisions = $this->getFormRevisions($formSelected);
				$revisionSelectedNumber = 0;

				foreach ($formRevisions as $formRevision) {
					if ($formRevision['revisionUri'] == $revisionSelected) {
						$revisionSelectedNumber = $formRevision['revisionNumber'];
						break;
					}
				}

				//Format dates from UTC to TAO local TIME_ZONE
				$dateFormatted = taoTestBatteries_helpers_Utils::dateTimeFormat($formRevisions, $revisionSelected);

				$this->setData('battery_uri', taoTestBatteries_helpers_Utils::formatEmptyValues($batteryUri));
				$this->setData('battery_id', taoTestBatteries_helpers_Utils::formatEmptyValues($batteryId));
				$this->setData('battery_label', taoTestBatteries_helpers_Utils::formatEmptyValues($battery->getLabel()));
				$this->setData('forms', $forms);
				$this->setData('form_selected', taoTestBatteries_helpers_Utils::formatEmptyValues($formSelected));
				$this->setData('form_id', taoTestBatteries_helpers_Utils::formatEmptyValues($formId));
				$this->setData('form_revisions', taoTestBatteries_helpers_Utils::formatEmptyValues($formRevisions));
				$this->setData('revision_selected', taoTestBatteries_helpers_Utils::formatEmptyValues($revisionSelected));
				$this->setData('revision_id', taoTestBatteries_helpers_Utils::formatEmptyValues($revisionId));
				$this->setData('revision_selected_number', $revisionSelectedNumber);
				$this->setData('revision_actions', $revisionActions);
				$this->setData('adp_job_id', taoTestBatteries_helpers_Utils::formatEmptyValues($publishedProperties['jobId']));
				$this->setData('adp_form_revision_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpFormRevisionId));
				$this->setData('adp_form_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpFormId));
				$this->setData('adp_battery_id', taoTestBatteries_helpers_Utils::formatEmptyValues($adpTestBatteryId));
				$this->setData('publish_error_desc', $publishErrorDesc);
				$this->setData('local_compileDateTime', taoTestBatteries_helpers_Utils::formatEmptyValues($dateFormatted['localCompileDateTime']));
				$this->setData('local_publishDateTime', taoTestBatteries_helpers_Utils::formatEmptyValues($dateFormatted['localPublishDateTime']));
				$this->setData('activation_status', taoTestBatteries_helpers_Utils::getActivationStatus($revisionSelected));
				$this->setView('forms.tpl');

				// unlocking the battery
				$this->lockManager->smartRelease($battery, PUBLISHING_USER_URI);
			}
		} catch (common_Exception $ex) {
			$message = $ex->getMessage();
			common_Logger::w($message);
				// unlocking the battery
				$this->lockManager->smartRelease($battery, PUBLISHING_USER_URI, true);
				// Set hasPublishedRevision to false if the battery has no published revision
			if (!taoTestBatteries_helpers_Utils::hasPublishedRevision($battery->getUri())) {
				taoTestBatteries_helpers_Utils::setHasPublishedRevision($battery, false);
				common_Logger::i("Set hasPublishedRevision to false for battery: " . $battery->getUri());
			}

			if (isset($form)) {
				// Set hasPublishedRevision to false if the form has no published revision
				if (!taoTestBatteries_helpers_Utils::hasPublishedRevision($form->getUri())) {
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($form, false);
					common_Logger::i("Set hasPublishedRevision to false for form: " . $form->getUri());
				}
			}
			if ($message && isset($revisionId)) {
				common_Logger::e("ADP service: Revision $revisionId : $message");
			}
			if ($message) {
				throw new common_exception_Error($message);
			}
		}
	}

	/**
	 * unpublishFormRevision
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @throws common_Exception
	 */
	public function unpublishFormRevision()
	{
		$formSelected = null;
		try {
			$batteryUri = $this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : null;

			//Check if battery is in use
			$battery = new core_kernel_classes_Resource($batteryUri);
			if (!$battery->exists()) {
				throw new Exception('This battery no longer exists. Please refresh the page.');
			}

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				if ($this->hasRequestParameter('form_selected')) {
					$formSelected = $this->getRequestParameter('form_selected');
				}

				if ($this->hasRequestParameter('revision_selected')) {
					$revisionToPublish = $this->getRequestParameter('revision_selected');
				} else {
					$revisionToPublish = 0;
				}

				$revisionClass = new core_kernel_classes_Class(CLASS_REVISION);
				$revisions = $revisionClass->searchInstances([PROPERTY_REVISION_FORM => $formSelected, PROPERTY_REVISION_NUMBER => $revisionToPublish]);

				if (count($revisions) != 1) {
					throw new common_Exception('Revision does not exist, or many revisions having the same revision number for form uri: ' . $formSelected);
				}

				/**
				 * @todo Description
				 * WEB SERVICE CALL TO UNPUBLISH A REVISION
				 * Once Successful a big function should be run, in order to update those hasPublishedRevision flags going from the ground up till the battery.
				 *
				 */
				$forms = $this->getForms();
				$formResource = new core_kernel_classes_Resource($formSelected);
				$formRevisions = $this->getFormRevisions($formResource->getUri());

				$this->setData('battery_uri', $batteryUri);
				$this->setData('battery_label', $battery->getLabel());
				$this->setData('forms', $forms);
				$this->setData('form_selected', $formSelected);
				$this->setData('form_revisions', $formRevisions);
				$this->setData('revision_selected', $revisionToPublish);
				$this->setView('forms.tpl');
			}
		} catch (Exception $ex) {
			$message = $ex->getMessage();
			common_Logger::e($message);
			throw new common_Exception($message);
		}
	}

	/**
	 * Update an existing Form
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @throws common_Exception if form uri is missing.
	 */
	public function updateForm()
	{
		try {
			$batteryUri = $this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : null;
			$linkedTestUri = '';
			//Check if battery is in use
			$battery = new core_kernel_classes_Resource($batteryUri);

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				// Cautiously retrieve battery and form labels.
				if (!$battery->exists()) {
					throw new Exception('This battery no longer exists. Please refresh the page.');
				}
				$batteryLabel = $this->hasRequestParameter('battery_label') ? $this->getRequestParameter('battery_label') : false;
				$formSelected = $this->hasRequestParameter('form_selected') ? $this->getRequestParameter('form_selected') : false;
				if (!$formSelected) {
					throw new Exception('This form no longer exists. Please refresh the page.');
				}

				$forms = $this->getForms($batteryUri);

				// Cautiously retrieve form and label.
				$thisForm = array_key_exists($formSelected, $forms) ? $forms[$formSelected] : false;
				$formLabel = $thisForm ? $thisForm['label'] : false;
				if (!($formLabel && $thisForm)) {
					throw new Exception('This form no longer exists. Please refresh the page.');
				}

				$test = taoTestBatteries_helpers_Utils::getFormTest($formSelected);
				if (!is_null($test)) {
					$linkedTestUri = $test->getUri();
				}

				$this->setData('battery_uri', $batteryUri);
				$this->setData('battery_label', $batteryLabel);
				$this->setData('tests_array', $this->getQtiTests($linkedTestUri));
				$this->setData('form_label', $formLabel);
				$this->setData('form_uri', $formSelected);
				$this->setData('form_id', parse_url($formSelected, PHP_URL_FRAGMENT));
				$this->setView('edit_form.tpl');
			}
		} catch (Exception $ex) {
			$message = $ex->getMessage();
			common_Logger::e($message);
			throw new common_Exception($message);
		}
	}

	/**
	 * Delete a form
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @params string $batteryUri, string $formUri
	 * @throws Exception
	 */
	public function delete()
	{
		$battery = null;
		try {
			$batteryUri = $this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : null;
			$formUri = $this->hasRequestParameter('form_uri') ? $this->getRequestParameter('form_uri') : null;
			$battery = new core_kernel_classes_Resource($batteryUri);
			if (!$battery->exists()) {
				throw new Exception('This battery no longer exists. Please refresh the page.');
			}

			$message = 'Deletion denied';

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				// Locking the battery
				$this->lockManager->smartLock($battery);

				if (TAOTESTBATTERIES_DEBUG) {
					common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
					sleep(DEBUG_PAUSE_DURATION);
					common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
				}

				$message = taoTestBatteries_helpers_Utils::deleteForm($formUri);

				// unlocking the battery just in case something wrong happened before the battery got unlocked
				$this->lockManager->smartRelease($battery);

				if (!$message) {
					$this->redirect(_url('renderFormsUI', 'Forms', 'taoTestBatteries', [
						'battery_uri' => $batteryUri
					]));
				}
			} else {
				throw new common_exception_Error($message);
			}
		} catch (Exception $ex) {
			common_Logger::w($ex->getMessage());
			// unlocking the battery just in case something wrong happened before the battery got unlocked
			$this->lockManager->smartRelease($battery, null, true);
			throw $ex;
		}
	}

	/**
	 * Delete form revision
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @throws Exception
	 */
	public function deleteRevision()
	{
		$battery = null;
		$revisionNumber = 0;
		$formUri = null;
		try {
			$batteryUri = $this->getRequestParameter('battery_uri');
			$formUri = $this->hasRequestParameter('form_selected') ? $this->getRequestParameter('form_selected') : false;
			$revisionUri = $this->hasRequestParameter('revision_selected') ? $this->getRequestParameter('revision_selected') : false;
			$battery = new core_kernel_classes_Resource($batteryUri);

			if (!$battery->exists()) {
				throw new Exception('This battery no longer exists. Please refresh the page.');
			}

			$message = 'Deletion denied';

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				// Locking the battery
				$this->lockManager->smartLock($battery);

				if (TAOTESTBATTERIES_DEBUG) {
					common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
					sleep(DEBUG_PAUSE_DURATION);
					common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
				}

				$message = taoTestBatteries_helpers_Utils::deleteFormRevision($revisionUri);

				// unlocking the battery just in case something wrong happened before the battery got unlocked
				$this->lockManager->smartRelease($battery);

				if (!$message) {
					// Unset hasCompiledRevision battery property if no revisions left
					if (empty($this->getFormRevisions($formUri))) {
						taoTestBatteries_helpers_Utils::setHasCompiledRevision($batteryUri, false);
					}

					$this->redirect(_url('renderFormsUI', 'Forms', 'taoTestBatteries', [
						'battery_uri' => $batteryUri,
						'form_selected' => $formUri
					]));
				}
			} else {
				throw new common_exception_Error($message);
			}
		} catch (Exception $ex) {
			common_Logger::w($ex->getMessage());
			// unlocking the battery just in case something wrong happened before the battery got unlocked
			$this->lockManager->smartRelease($battery, null, true);
			throw $ex;
		}
	}

	/**
	 * If you want strictly to check if the resource is locked,
	 * you should use LockManager::getImplementation()->isLocked($resource)
	 * Controller level convenience method to check if @resource is being locked, prepare data and set view,
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param $view
	 *
	 * @return boolean
	 */
	protected function isLocked($resource, $view = null)
	{
		if ($this->lockManager->isLocked($resource)) {
			$params = [
				'id' => $resource->getUri(),
				'destination' => tao_helpers_Uri::url(null, null, null, $this->getRequestParameters())
			];
			if (!is_null($view)) {
				$params['view'] = $view;
			}
			$this->forward('locked', 'TestBatteries', 'taoTestBatteries', $params); // This cannot be called from the command line.
		}
		return false;
	}

	/**
	 * Calculate new publish status after ADP call
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param boolean $responseStatus - response status from ADP
	 * @param Object $revision
	 * @return string $newPublishStatus
	 */
	public function getNewPublishStatus($responseStatus = false, $revision = null)
	{
		$properties = $revision->getPropertiesValues([PROPERTY_REVISION_PUBLISHING_STATUS]);
		$properties = reset($properties);
		$newPublishStatus = $lastPublishStatus = $properties[0]->getUri();

		if ($responseStatus) { // success
			switch ($lastPublishStatus) {
				case PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_PUBLISHED;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_PUBLISHED:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_UNPUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_UNPUBLISHING;
					break;

				default:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED;
			}
		} else {   // fail
			switch ($lastPublishStatus) {
				case PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_PUBLISHED:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_UNPUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING;
					break;

				case PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING;
					break;

				default:
					$newPublishStatus = PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING;
			}
		}

		return $newPublishStatus;
	}

	/**
	 * Save published(unpublished) revision properties
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $publishStatus
	 * @param array $publishedProperties
	 * @param string $batteryUri
	 * @param string $formUri
	 * @param Object $revision
	 * @throws common_exception_Error
	 */
	public function savePublishedRevisionProperties($publishStatus = null, $publishedProperties = null, $batteryUri = null, $formUri = null, $revision = null)
	{
		if (!empty($publishStatus) && !empty($publishedProperties) && !empty($batteryUri) && !empty($formUri) && !empty($revision)) {
			try {
				// due to sync issues we will have to add a check on the datetime so that if the status was confirmed as published, a late response won't override confirmation details.
				$oldTimeStamp = current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_TIME)));
				common_Logger::w("Current Publishing Time (" . intval($oldTimeStamp) . ") - New Publishing Time (" . intval($publishedProperties['dateTime']) . ")");

				if (intval($oldTimeStamp) < intval($publishedProperties['dateTime'])) {
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_ACTIVATION), PROPERTY_REVISION_PUBLISHED_NOT_ACTIVE);
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_TIME), $publishedProperties['dateTime']);
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS), $publishStatus);
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION), $publishedProperties['errorDescription']);

					taoTestBatteries_helpers_Utils::setHasPublishedRevision($batteryUri, true);
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($formUri, true);

					common_Logger::i("Enabled hasPublishedRevision for battery uri: $batteryUri");
					common_Logger::i("Enabled hasPublishedRevision for form uri: $formUri");
				}
				else {
					common_Logger::w("Skipping Response Update as Existing Publishing Time ($oldTimeStamp) is more recent than Upcoming Publishing Time ({$publishedProperties['dateTime']}) ( batteryUri :$batteryUri, revision = $revision)");
				}

				$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_BY), common_session_SessionManager::getSession()->getUserUri());
				$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_PUBLISHER_JOB_ID), $publishedProperties['jobId']);
			}
			catch (Exception $ex) {
				common_Logger::e($ex->getMessage() . ": Error saving Revision properties: Battery: $batteryUri, Form: $formUri, Revision: " . $revision->getUri());
				throw new common_exception_Error("Error saving revision properties");
			}
		}
		else {
			common_Logger::i("Publish Status: ($publishStatus),  batteryUri: ($batteryUri), jobId: ({$publishedProperties['jobId']}), formUri: ($formUri), revision: ($revision), datetime: ({$publishedProperties['dateTime']})");
			common_Logger::e("No Publish Revision data specified to save!");
		}
	}

	/**
	 * Revision actions array init
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param void
	 * @return void
	 */
	private function initRevisionActions($revision = null)
	{
		if (empty($this->revisionActions) || !is_null($revision)) {
			if ($this->hasPublishPermission()) {
				$disablePublishing = false;
			} else {
				$disablePublishing = true;
			}

			if(is_null($revision)) {
				$revision = $this->hasRequestParameter('revision_selected') ? $this->getRequestParameter('revision_selected') : '';
			}

			$active = empty($revision) ? false : taoTestBatteries_helpers_Utils::getActivationStatus($revision) == "Active";
			$batteryUri = $this->hasRequestParameter('id') ? $this->getRequestParameter('id') : ($this->hasRequestParameter('battery_uri') ? $this->getRequestParameter('battery_uri') : false);

			$update = (bool) $batteryUri ? taoTestBatteries_helpers_Utils::hasPublishedRevision($batteryUri) : false;

			$this->revisionActions = [
				PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => false, 'disabled' => true],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => false, 'disabled' => true],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				],
				PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_PUBLISHING => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => false, 'disabled' => true],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => false, 'disabled' => true],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				],
				PROPERTY_REVISION_PUBLISHING_STATUS_PUBLISHED => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing], // consider visible: true for future
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => !$active, 'disabled' => $active],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => $active, 'disabled' => !$active],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				],
				PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_PUBLISHING => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => false, 'disabled' => true],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => false, 'disabled' => true],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				],
				PROPERTY_REVISION_PUBLISHING_STATUS_SCHEDULED_UNPUBLISHING => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => false, 'disabled' => true],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => false, 'disabled' => true],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				],
				PROPERTY_REVISION_PUBLISHING_STATUS_FAILED_UNPUBLISHING => [
					['nick' => 'delete', 'button' => 'Delete Form Revision', 'visible' => true, 'disabled' => false],
					['nick' => 'publish', 'button' => 'Publish Form Revision', 'visible' => true, 'disabled' => $disablePublishing],
					['nick' => 'republish', 'button' => 'Republish Form Revision', 'visible' => false, 'disabled' => true],
					['nick' => 'unpublish', 'button' => 'Unpublish Form Revision', 'visible' => false, 'disabled' => $disablePublishing], // consider visible: true for future
					['nick' => 'cancel_pub', 'button' => 'Cancel Publishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'cancel_unpub', 'button' => 'Cancel Unpublishing', 'visible' => false, 'disabled' => $disablePublishing],
					['nick' => 'activate', 'button' => 'Activate', 'visible' => false, 'disabled' => true],
					['nick' => 'deactivate', 'button' => 'Deactivate', 'visible' => false, 'disabled' => true],
					['nick' => 'republishBatteryProperties', 'button' => 'Republish Battery Properties', 'visible' => $update, 'disabled' => !$update]
				]
			];

			$this->revisionActions = array_change_key_case($this->revisionActions, CASE_LOWER);
		}
	}

	/**
	 * Get Revision actions description array
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $publishStatus
	 * @return array $actions
	 */
	public function getRevisionActions($publishStatus = '')
	{
		if (!empty($publishStatus)) {
			$publishStatus = strtolower($publishStatus);

			if (array_key_exists($publishStatus, $this->revisionActions)) {
				return $this->revisionActions[$publishStatus];
			}
		}

		return [];
	}

	/**
	 * Set Revision action description array for certain publish status
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @param string $publishStatus
	 * @param array $action properties ('nick' => value, 'button' => value, 'visible' => value, 'disabled' => value)
	 * @return void
	 */
	public function setRevisionActions($publishStatus = '', $action = [])
	{
		if (!empty($publishStatus) && !empty($action)) {
			$publishStatus = strtolower($publishStatus);

			$n = count($this->revisionActions[$publishStatus]);

			for ($i = 0; $i < $n; $i++) {
				if ($action['nick'] == $this->revisionActions[$publishStatus][$i]['nick']) {
					if (isset($action['button']) && !empty($action['button'])) {
						$this->revisionActions[$publishStatus][$i]['button'] = $action['button'];
					}

					if (isset($action['visible'])) {
						$this->revisionActions[$publishStatus][$i]['visible'] = $action['visible'];
					}

					if (isset($action['disabled'])) {
						$this->revisionActions[$publishStatus][$i]['disabled'] = $action['disabled'];
					}

					break;
				}
			}
		}
	}

	/**
	 * Sets the revision activation status to 'Active' or 'Not Active'.
	 * @author <kendall.parks@breaktech.com>
	 * @param string $revisionUri
	 * @param Boolean $activate
	 * @return Boolean
	 */
	private function setACRActivationStatus($revisionUri, $activate = true)
	{
		$resource = new core_kernel_classes_Resource($revisionUri);
		$newStatus = $activate ? PROPERTY_REVISION_PUBLISHED_ACTIVE : PROPERTY_REVISION_PUBLISHED_NOT_ACTIVE;
		return $resource->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_ACTIVATION), $newStatus);
	}

	/**
	 * Sends Activate/De-Activate Request to ADP
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param bool $activate_revision
	 * @throws common_Exception
	 */
	public function setADPActivationStatus()
	{
		try {
			$activate_revision = $this->getRequestParameter('activate_revision') === 'activate';

			$revisionUri = '';
			if ($this->hasRequestParameter('revision_selected')) {
				$revisionUri = $this->getRequestParameter('revision_selected');
			} else {
				throw new common_Exception('No Revision selected.');
			}

			$revision = new core_kernel_classes_Resource($revisionUri);

			$publishedBy = $revision->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_BY))->getUri();
			$adpTestFormRevisionId = $revision->getOnePropertyValue(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_FORM_REVISION_ID)); // ADP's Test Battery Form Revision ID that was provided when it was published the first time.

			common_Logger::d("[ChangeADPActivationStatus] Making " . ($activate_revision ? 'activate' : 'deactivate') . " request to " . ADP_HOST . ADP_PUBLISH_SERVICE . '/' . $adpTestFormRevisionId);

			$ch = $this->sendPublishingPatchRequest([
				'testFormRevisionId' => parse_url($revisionUri, PHP_URL_FRAGMENT),
				'publishedBy' => parse_url($publishedBy, PHP_URL_FRAGMENT),
				'active' => $activate_revision
			], $adpTestFormRevisionId);
		} catch (Exception $ex) {
			throw new common_Exception($ex->getMessage());
		}

		common_Logger::d("[ChangeADPActivationStatus] Result: " . json_encode($ch));
		$formUri = $this->getRequestParameter('form_uri');
		$batteryUri = $this->getRequestParameter('battery_uri');

		if (property_exists($ch, 'result')) {
			$result = $ch->result;
		} else {
			$result = $ch;
		}

		if(property_exists($result, 'status') && $result->status === 'Scheduled') {
			common_Logger::i("Activation status updated successfully on ADP. Updating ACR...");
			$this->setACRActivationStatus($revisionUri, $activate_revision);
		} else {
			common_Logger::e("[ChangeADPActivationStatus] Error: " . json_encode($result));
			throw new common_Exception("Failed updating activation status on ADP. Canceling " . ($activate_revision ? '' : 'de') . 'activation action.');
		}
		$this->redirect(_url('renderFormsUI', 'Forms', 'taoTestBatteries', [
			'battery_uri' => $batteryUri,
			'form_selected' => $formUri,
			'revision_selected' => $revisionUri
		]));
	}

	/**
	 * Generic function for sending PATCH request to the Publishing web service.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param array $data
	 * @param string $uri
	 * @return array - Response from ADP
	 */
	private function sendPublishingPatchRequest($data, $uri = null) {
		$apiURI = ADP_PUBLISH_SERVICE . (is_null($uri) ? '' : "/$uri");
		$headers = [
		   "Content-Type: application/json; charset=UTF-8",
		   "Authorization: Basic " . base64_encode(REMOTE_ADP_USERNAME . ":" . REMOTE_ADP_PASSWORD),
		   "Authentication: " . taoTestBatteries_helpers_Utils::generateHmacSignature($apiURI)
		];

		return json_decode(taoTestBatteries_helpers_Utils::curlApiCall(ADP_HOST . $apiURI, json_encode($data), 'PATCH', 'data', $headers));
	}

	/**
	 * Sends request to ADP to update some of the battery's properties.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @throws common_Exception
	 */
	public function updateTestProperties() {
		$batteryUri = $this->getRequestParameter('battery_uri');
		$battery = new core_kernel_classes_Resource($batteryUri);
		$adpBatteryId = taoTestBatteries_helpers_Utils::getADPResourcePropertyId($batteryUri, PROPERTY_TEST_BATTERY_ADP_BATTERY_ID);

		preg_match('/i[0-9]+$/', $batteryUri, $matches);
		if(is_array($matches) && array_key_exists(0, $matches)) {
			$batteryId = $matches[0];
		} else {
			throw new common_Exception("Couldn't match test '$batteryUri'");
		}

		$triples = $battery->getRdfTriples();
		if(property_exists($triples, 'sequence')) {
			$triples = $triples->sequence;
		} else {
			throw new common_Exception('Couldn\'t get battery properties.');
		}

		$data = ['batteryId' => $batteryId];
		$batteryClass = new core_kernel_classes_Class($batteryUri);
		$updateableProperties = unserialize(UPDATEABLE_BATTERY_PROPERTIES);
		// Build array of properties and property values to send in request. Some properties are excluded.
		foreach($triples as $triple) {
			$predicate = property_exists($triple, 'predicate') ? $triple->predicate : null;
			$object = property_exists($triple, 'object') ? $triple->object : null;

			if(!is_null($predicate) && !is_null($object) && array_key_exists($predicate, $updateableProperties)) {
				$key = $updateableProperties[$predicate];
				$prop = new core_kernel_classes_Property($predicate);
				$propVal = $batteryClass->getOnePropertyValue($prop);

				if(get_class($propVal) !== 'core_kernel_classes_Literal') {
					$propClass = new core_kernel_classes_Class($propVal);
					$value = $propClass->getOnePropertyValue(new core_kernel_classes_Property(RDF_VALUE));
				} else {
					$value = $propVal;
				}
				$data[$key] = $value->literal;
			}
		}

		$ch = $this->sendPublishingPatchRequest($data, "properties/$adpBatteryId");

		// Was battery update successful?
		$updated = $this->batteryPropertiesUpdateSuccessful($ch);

		$revisionUri = '';
		if ($this->hasRequestParameter('revision_selected')) {
			$revisionUri = $this->getRequestParameter('revision_selected');
		} else {
			throw new common_Exception('No Revision selected.');
		}

		$this->redirect(_url('renderFormsUI', 'Forms', 'taoTestBatteries', [
			'battery_uri' => $batteryUri,
			'form_selected' => $this->getRequestParameter('form_selected'),
			'revision_selected' => $revisionUri,
			'updated' => ($updated ? 'true' : 'false')
		]));
	}

	private function getRevisionSelectedNumber($formRevisions, $revisionSelected) {
		foreach ($formRevisions as $formRevision) {
			if ($formRevision['revisionUri'] == $revisionSelected) {
				return $formRevision['revisionNumber'];
			}
		}
		return 0;
	}

	/**
	 * Determines if a battery properties update request was successful.
	 *
	 * @author Kendall Parks <kendall.parks@breaktech.com>
	 * @param Object $response
	 * @return bool
	 */
	private function batteryPropertiesUpdateSuccessful(stdClass $response) {
		if(!empty($response)) {
			if (taoTestBatteries_helpers_Utils::hasEnvelope($response)) {
				return property_exists($response->meta, 'status') && property_exists($response->result, 'requestId') && $response->meta->status === 'SUCCESS';
			}
			return property_exists($response, 'status') && property_exists($response, 'requestId');
		}
		return false;
	}
}
