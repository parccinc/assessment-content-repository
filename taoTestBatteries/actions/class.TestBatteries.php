<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\lock\LockManager;
use oat\tao\helpers\UserHelper;

/**
 * TestBatteries Controller
 *
 * @author Oussama Saddane <oussama.saddane@breaktech.com>
 * @package taoTestBatteries
 *
 */
class taoTestBatteries_actions_TestBatteries extends tao_actions_SaSModule
{
	protected $lockManager;
	
	/**
	 * constructor: initialize the service and the default data
	 */
	public function __construct()
	{
		parent::__construct();

		// Load defaults configs
		taoTestBatteries_helpers_Config::getDefaultsConfig();

		//the service is initialized by default
		$this->service = taoTestBatteries_models_classes_TestBatteriesService::singleton();
		$this->lockManager = new taoTestBatteries_helpers_LockManager();
		$this->defaultData();
	}

	/**
	 * get the main class
	 * @return core_kernel_classes_Class
	 */
	protected function getRootClass()
	{
		return $this->service->getRootclass();
	}

	/**
	 *
	 */
	public function generateTestPackage()
	{
		$this->setView('download.tpl');
	}

	/**
	 * Edit a Battery with fields validation
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @return void
	 * @throws Exception
	 */
	public function editTestBatteries()
	{
		$selectedForm = null;
		$battery = null;
		try {
			$clazz = $this->getCurrentClass();
			$classUri = $clazz->getUri();
			$rootClass = CLASS_TEST_BATTERY;
			$battery = $this->getCurrentInstance();
			$batteryUri = $battery->getUri();
			$batteryId = parse_url($batteryUri, PHP_URL_FRAGMENT);
			$batteryLabel = $battery->getLabel();
			$hasCompiledRevision = taoTestBatteries_helpers_Utils::hasCompiledRevision($batteryUri);

			if (!$this->isLocked($battery, 'battery_locked.tpl')) {
				$formContainer = new tao_actions_form_Instance($clazz, $battery);
				$selectedForm = $formContainer->getForm();
			}
		} catch(Exception $ex) {
			throw $ex;
		}

		try {
			/**
			 * START Validate Elements (NOT EMPTY)
			 */
			// ID
			$idElt = new tao_helpers_form_elements_xhtml_Readonly('ID');
			$idElt->setValue($batteryId);
			if (!is_null($idElt)) {
				// Element array is already built. Insert new element at front of the array.
				$allElems = array_merge([$idElt], $selectedForm->getElements());
				$selectedForm->setElements($allElems);
			}

			// Label
			$labelElt = $selectedForm->getElement(tao_helpers_Uri::encode(RDFS_LABEL));
			if (!is_null($labelElt)) {
				if (taoTestBatteries_helpers_Utils::hasPublishedRevision($batteryUri)) {
					$labelElt->setAttribute('disabled', 'disabled');
				}
				$labelElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty'),
					tao_helpers_form_FormFactory::getValidator('Length', ['max' => 100])
				]);
				$labelElt->setAttribute('maxlength', 100);
				$selectedForm->addElement($labelElt);
			}

			// Testing Program
			$testingProgramElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_TESTINGPROGRAM));
			if (!is_null($testingProgramElt)) {
				$testingProgramElt->setDescription($testingProgramElt->getDescription() . ' *');
				$testingProgramElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				if ($hasCompiledRevision) {
					$testingProgramElt->setAttribute('disabled', 'disabled');
				}
				$selectedForm->addElement($testingProgramElt);
			}

			// Subject
			$subjectElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_SUBJECT));
			if (!is_null($subjectElt)) {
				$subjectElt->setDescription($subjectElt->getDescription() . ' *');
				$subjectElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($subjectElt);
			}

			// Grade
			$gradeElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_GRADE));
			if (!is_null($gradeElt)) {
				$gradeElt->setDescription($gradeElt->getDescription() . ' *');
				$gradeElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);

				// the below code is to fix the order issue in the grade drop-down list.
				$options = $gradeElt->getOptions();
				uasort($options, function ($a, $b) {
					if ($a == $b) {
						return 0;
					}
					return (intVal(preg_replace('/.*_(\d+$)/', '$1', $a)) < intVal(preg_replace('/.*_(\d+$)/', '$1', $b))) ? -1 : 1;
				});
				$gradeElt->setOptions($options);

				$selectedForm->addElement($gradeElt);
			}

			// Item Selection Algorithm
			$itemSelectionAlgorithmElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_ITEMSELECTIONALGORITHM));
			if (!is_null($itemSelectionAlgorithmElt)) {
				$itemSelectionAlgorithmElt->setDescription($itemSelectionAlgorithmElt->getDescription() . ' *');
				$itemSelectionAlgorithmElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($itemSelectionAlgorithmElt);
			}

			// Security
			$securityElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_SECURITY));
			if (!is_null($securityElt)) {
				$securityElt->setDescription($securityElt->getDescription() . ' *');
				$securityElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($securityElt);
			}

			// Score Report
			$scoreReportElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_SCOREREPORT));
			if (!is_null($scoreReportElt)) {
				$scoreReportElt->setDescription($scoreReportElt->getDescription() . ' *');
				$scoreReportElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				if ($hasCompiledRevision) {
					$scoreReportElt->setAttribute('disabled', 'disabled');
				}
				$selectedForm->addElement($scoreReportElt);
			}

			// Multimedia
			$multimediaElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_MULTIMEDIA));
			if (!is_null($multimediaElt)) {
				$multimediaElt->setDescription($multimediaElt->getDescription() . ' *');
				$multimediaElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($multimediaElt);
			}

			// Test Scoring
			$testScoringElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_TESTSCORING));
			if (!is_null($testScoringElt)) {
				$testScoringElt->setDescription($testScoringElt->getDescription() . ' *');
				$testScoringElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($testScoringElt);
			}

			// Test Permissions
			$testPermissionsElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_TESTPERMISSIONS));
			if (!is_null($testPermissionsElt)) {
				$testPermissionsElt->setDescription($testPermissionsElt->getDescription() . ' *');
				$testPermissionsElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);

				$selectedForm->addElement($testPermissionsElt);
			}

			// Test Privacy
			$testPrivacyElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_TESTPRIVACY));
			if (!is_null($testPrivacyElt)) {
				$testPrivacyElt->setDescription($testPrivacyElt->getDescription() . ' *');
				$testPrivacyElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('NotEmpty')
				]);
				$selectedForm->addElement($testPrivacyElt);
			}

			// Test Description
			$testDescriptionElt = $selectedForm->getElement(tao_helpers_Uri::encode(PROPERTY_TEST_BATTERY_TESTDESCRIPTION));
			if (!is_null($testDescriptionElt)) {
				$testDescriptionElt->setDescription($testDescriptionElt->getDescription());
				$testDescriptionElt->addValidators([
					tao_helpers_form_FormFactory::getValidator('Length', ['max' => 4096])
				]);
				$testDescriptionElt->setAttribute('maxlength', 4096);
				$selectedForm->addElement($testDescriptionElt);
			}

			$selectedForm->evaluate();

			/**
			 * END Validate Elements
			 */
			if ($selectedForm->isSubmited()) {
				if ($selectedForm->isValid()) {
					// Locking battery
					$this->lockManager->smartLock($battery);

					if (TAOTESTBATTERIES_DEBUG) {
						common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
						sleep(DEBUG_PAUSE_DURATION);
						common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
					}

					$propertyValues = $selectedForm->getValues();

					//check if a battery has the same Label/Grade/Subject
					$filter[RDFS_LABEL] = $propertyValues[RDFS_LABEL];
					$filter[PROPERTY_TEST_BATTERY_GRADE] = $propertyValues[PROPERTY_TEST_BATTERY_GRADE];
					$filter[PROPERTY_TEST_BATTERY_SUBJECT] = $propertyValues[PROPERTY_TEST_BATTERY_SUBJECT];
					$rootClass = new core_kernel_classes_Class(CLASS_TEST_BATTERY);
					$isExistingBattery = $rootClass->searchInstances($filter, ['like' => false, 'recursive' => true]);
					// exclude the current battery saved from the result
					unset($isExistingBattery[$batteryUri]);

					if (empty($isExistingBattery)) {
						//then save the property values as usual
						$binder = new tao_models_classes_dataBinding_GenerisFormDataBinder($battery);
						$battery = $binder->bind($propertyValues);

						// Unlock battery
						$this->lockManager->smartRelease($battery);

						//edit process label:
						$this->service->onChangeTestBatteryLabel($battery);
						$this->setData('message', 'Battery saved');
						$this->setData('reload', true);

					} else {
						throw new common_Exception("Battery already exists with the same Label/Grade/Subject.");
					}
				}
			}

			$this->setSessionAttribute("showNodeUri", tao_helpers_Uri::encode($battery->getUri()));
			$this->setData('battery_label', $batteryLabel);
			$this->setData('battery_uri', tao_helpers_Uri::encode($battery->getUri()));
			$this->setData('classUri', tao_helpers_Uri::encode($classUri));
			$this->setData('base', tao_helpers_Uri::encode(substr($rootClass, 0, strpos($rootClass, '#') + 1)));
			$this->setData('formTitle', 'Test Battery properties');
			$this->setData('selectedForm', $selectedForm->render());
			$this->setView('test_batteries.tpl');
		} catch (Exception $ex) {
			common_Logger::w($ex->getMessage());
			// unlocking the battery just in case something wrong happened before the battery got unlocked
			$this->lockManager->smartRelease($battery, null, true);
			throw $ex;
		}
	}

	/**
	 * Edit a Battery Class
	 * @author Oussama Saddane <oussama.saddane@breaktech.com>
	 * @return void
	 */
	public function editTestBatteriesClass()
	{
		$clazz = $this->getCurrentClass();
		if ($this->hasRequestParameter('property_mode')) {
			$this->setSessionAttribute('property_mode', $this->getRequestParameter('property_mode'));
		}
		$selectedForm = $this->getClassForm($clazz, $this->service->getRootClass());
		if ($selectedForm->isSubmited()) {
			if ($selectedForm->isValid()) {
				if ($clazz instanceof core_kernel_classes_Resource) {
					$this->setData("selectNode", tao_helpers_Uri::encode($clazz->getUri()));
				}
				$this->setData('message', 'Battery Class saved');
				$this->setData('reload', true);
			}
		}
		$this->setData('selectedForm', $selectedForm->render());
		$this->setView('form.tpl');
	}

	/**
	 * Delete a test battery or a test battery sub-class
	 *
	 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @return JSON
	 * @throws Exception
	 */
	public function delete()
	{
		if (!tao_helpers_Request::isAjax()) {
			throw new Exception("Wrong request mode");
		}

		$message = 'Deletion denied';

		if ($this->hasRequestParameter('uri')) {
			$resource = $this->getCurrentInstance();
			$func = 'deleteTestBattery';
		} else {
			$resource = $this->getCurrentClass();
			$func = 'deleteTestBatteryClass';
		}

		$resourceUri = $resource->getUri();

		if ($resourceUri == CLASS_TEST_BATTERY) {
			return $this->forward('deleteClass', 'TestBatteries', 'taoTestBatteries', (['id' => $this->getRequestParameter('id')]));
		} else {
			if (!$this->isLocked($resource, 'battery_locked.tpl')) {
				try {
					// Locking the battery
					$this->lockManager->smartLock($resource);

					if (TAOTESTBATTERIES_DEBUG) {
						common_Logger::i("Starting " . DEBUG_PAUSE_DURATION . " seconds pause.");
						sleep(DEBUG_PAUSE_DURATION);
						common_Logger::i("Ending " . DEBUG_PAUSE_DURATION . " seconds pause.");
					}

					$message = taoTestBatteries_helpers_Utils::$func($resource);

					// unlocking the battery just in case something wrong happened before the battery got unlocked
					$this->lockManager->smartRelease($resource, null, true, false);

					if (!$message) {
						$this->returnJson(['deleted' => true]);
					} else {
						throw new common_exception_Error($message);
					}
				} catch (Exception $ex) {
					common_Logger::w($ex->getMessage());
					// unlocking the battery just in case something wrong happened before the battery got unlocked
					$this->lockManager->smartRelease($resource);
					throw $ex;
				}
			} else {
				throw new common_exception_Error($message);
			}
		}
	}

	/**
	 * Controller level convenience method to check if @resource is locked, prepares data and sets view,
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @param core_kernel_classes_Resource $resource
	 * @param $view
	 *
	 * @return boolean
	 */
	protected function isLocked($resource, $view = null)
	{
		try {
			if ($this->lockManager->isLocked($resource)) {
				$params = [
				'id' => $resource->getUri(),
				'destination' => tao_helpers_Uri::url(null, null, null, $this->getRequestParameters())
				];
				if (!is_null($view)) {
					$params['view'] = $view;
				}
				$this->forward('locked', 'TestBatteries', 'taoTestBatteries', $params);
			}
			return false;
		} catch (InterruptedActionException $iae) {
			session_write_close();
			exit();
		} catch (Exception $ex) {
			session_write_close();
			throw new common_Exception($ex->getMessage());
		}
	}

	/**
	 * actions that get prevented by a lock are forwarded to this action
	 * parameter view is currently ignored
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 */
	public function locked()
	{
		$resource = new core_kernel_classes_Resource($this->getRequestParameter('id'));
		$lockData = $this->lockManager->getLockData($resource);

		$this->setData('id', $resource->getUri());
		$this->setData('batteryUri', $resource->getUri());
		$this->setData('label', $resource->getLabel());

		$this->setData('lockDate', $lockData->getCreationTime());
		$this->setData('ownerHtml', UserHelper::renderHtmlUser($this->lockManager->getOwner($resource)));

		$currentUserId = common_session_SessionManager::getSession()->getUser()->getIdentifier();
		$this->setData('isOwner', $this->lockManager->getOwner($resource)->getUri() == $currentUserId);

		$this->setData('destination', $this->getRequestParameter('destination'));
		$this->setView('battery_locked.tpl');
	}
}
