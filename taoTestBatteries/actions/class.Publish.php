<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\model\lock\LockManager;
use oat\tao\helpers\UserHelper;

/**
 * Publish controller provide action that will listen to ADP confirm publishing calls.
 *
 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
 * @package taoTestBatteries
 * @license GPLv2  http://www.opensource.org/licenses/gpl-2.0.php
 */
class taoTestBatteries_actions_Publish extends tao_actions_CommonModule
{

	const STATUS_SCHEDULED		= 'Scheduled';
	const STATUS_PUBLISHED		= 'Published';
	const STATUS_UNPUBLISHED	= 'Unpublished';
	const STATUS_FAILED			= 'Failed';
	const STATUS_CANCELED		= 'Canceled';
	
	protected $lockManager;

	/**
	 * Adding constructor to include defaults
	 * @author Mahmoud Kasdi <mahmoud.kasdi@breaktech.com>
	 */
	public function __construct()
	{
		parent::__construct();

		// Load defaults configs
		taoTestBatteries_helpers_Config::getDefaultsConfig();
		
		$this->lockManager = new taoTestBatteries_helpers_LockManager();
	}

	public function publisher()
	{
		// DO NOT DELETE !!!   This is used as a dummy placeholder for publishing permissions. //
	}

	/**
	 * basicAuthorization
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 */
	private function basicAuthorization()
	{
		$validated = false;
		$valid_passwords = [ADP_USERNAME => ADP_PASSWORD];
		$valid_users = array_keys($valid_passwords);
		$user = $pass = "undefined";

		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$user = $_SERVER['PHP_AUTH_USER'];
			$pass = $_SERVER['PHP_AUTH_PW'];

			$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
		}

		if (!$validated) {
//			common_Logger::e("[Publish/basicAuthorization] user: $user and password: $pass Not authorized");
			header('HTTP/1.0 401 Unauthorized');
			session_write_close();
			exit("Not authorized");
		}
	}

	/**
	 * hmacAuthentication
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @return void
	 */
	private function hmacAuthentication()
	{
		$headers = getallheaders();
		$headers = array_change_key_case($headers, CASE_UPPER);

		if (isset($headers['AUTHENTICATION'])) {
			// Validate HMAC Authentication
			$header = $headers['AUTHENTICATION'];
			common_Logger::i($header);

			if ($header) {
				$hmacKeys = explode(':', base64_decode($header));
				$timestamp = round($hmacKeys[0] / 1000);

				common_Logger::i("timestamp: " . $timestamp);

				$nonce = $hmacKeys[1];
				common_Logger::i("nonce: " . $nonce);
				$digest = $hmacKeys[2];
				common_Logger::i("digest: " . $digest);
				$secret = (string) ADP_SECRET;
				common_Logger::i("secret: " . $secret);

//				if ($_SERVER['REQUEST_METHOD'] === 'GET' || $_SERVER['REQUEST_METHOD'] === 'HEAD') {
//					/** @noinspection PhpUndefinedFieldInspection */
//				}

				$requestURI = strstr($_SERVER['REQUEST_URI'], "/taoTestBatteries"); //$app->request->getURI();
				common_Logger::i('request uri : ' . $requestURI);

				$token = base64_encode(hash_hmac('sha256', $requestURI . $timestamp . $nonce, $secret));

				common_Logger::i('token: ' . $token);

				if ($digest === $token /* && time() - $timestamp < 10 */) {
					return;
				}
			}
		}

		common_Logger::e("[Publish/hmacAuthentication] Not authorized");
		header('HTTP/1.0 401 Unauthorized');
		session_write_close();
		exit("[ACR] Not authorized");
	}

	/**
	 * downloadPackage
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @throws common_exception_BadRequest
	 * @throws common_Exception
	 */
	public function downloadPackage()
	{
		try {
			/** basic authorization verification * */
			$this->basicAuthorization();
			/** hmac authentication verification * */
			$this->hmacAuthentication();
			common_Logger::i('[Publish/getPackage] Authorized Request: [REMOTE_ADDR:' . $_SERVER['REMOTE_ADDR'] . (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? ', HTTP_X_FORWARDED_FOR:' . $_SERVER['HTTP_X_FORWARDED_FOR'] : '') . '] ');

			$body = @file_get_contents('php://input');
			$requestBody = json_decode($body, true);

			common_Logger::i("[Publish/getPackage] Incoming request: $body");

			if (!isset($requestBody['adpPublisherRequestId'])) {
				common_Logger::e('[Publish/getPackage] adpPublisherRequestId is missing.');
				throw new common_exception_BadRequest('adpPublisherRequestId is missing.');
			} else {
				$jobId = $requestBody['adpPublisherRequestId'];
			}

			if (!isset($requestBody['testFormRevisionId'])) {
				common_Logger::e('[Publish/getPackage] testFormRevisionId is missing.');
				throw new common_exception_BadRequest('testFormRevisionId is missing.');
			} else {
				$testFormRevisionId = $requestBody['testFormRevisionId'];
			}

			$revisionUri = LOCAL_NAMESPACE . '#' . $testFormRevisionId;
			$revision = new core_kernel_classes_Resource(tao_helpers_Uri::decode($revisionUri));

			if (!$revision->exists()) {
				throw new common_Exception("Revision ($revisionUri) does not exist!");
			}

			if (!$revision->isInstanceOf(new core_kernel_classes_Class(CLASS_REVISION))) {
				throw new common_Exception("($revisionUri) is not a revision!");
			}

//			// Make sure that the $revision has the correct jobId (adpPublisherRequestId) coming from the server.
//			$adpPublisherRequestId = current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_PUBLISHER_JOB_ID)));
//
//			if ($jobId != $adpPublisherRequestId) {
//				throw new common_Exception("($revisionUri) unable to find the revision with ADP_PUBLISH_JOB_ID($jobId)");
//			}

			$filePath = FILES_PATH . 'taoBatteries' . DIRECTORY_SEPARATOR . $testFormRevisionId . ".zip";

			if (!file_exists($filePath)) {
				throw new common_Exception("($revisionUri) package not found !");
			}

			if (!is_readable($filePath)) {
				throw new common_Exception("($revisionUri) package could not be read !");
			}

			$fileName = "$testFormRevisionId.zip";

			header('Content-type: application/force-download; filename="' . $fileName . '"');
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="' . $fileName . '"');
			header('Content-Transfer-Encoding: binary');
			header('Pragma: public, no-cache');
			header('Pragma-Directive: no-cache');
			header('Cache-Directive: no-cache');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0');
			header('Expires: -1');
			header('Content-Length: ' . filesize($filePath));
			if (ob_get_length()) {
				ob_clean();
			}
			flush();
			readfile($filePath);

			session_write_close();
			exit;
		} catch (Exception $ex) {
			common_Logger::e($ex->getMessage());
			$this->returnJson([
				'error' => $ex->getMessage(),
				'code' => $ex->getCode()
			]);
		}
	}

	/**
	 * confirm
	 *
	 * @author Mehdi Karamosly <mehdi.karamosly@breaktech.com>
	 * @throws Exception
	 * @throws common_exception_BadRequest
	 * @throws common_Exception
	 */
	public function confirm()
	{
		common_Logger::i('[Publish/confirm] Beginning confirmation process.');
		try {
			/**
			 * Parameters coming from ADP should be : adpPublisherRequestId, adpFormRevisionId, testFormRevisionId, publishingTimeStamp (unix time in UTC time zone)
			 *
			 * {
			 *		"testFormRevisionId":"i14308500342972188",
			 *		"adpPublisherRequestId":"54",
			 *		"adpFormRevisionId":"84",
			 *		"publishedTimestamp":1430857303,
			 *		"status":"Published",
			 *		"errorDescription" : "Error Description" // Optional
			 * }
			 */
			// RESTRICT THE METHOD TO POST
			if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
				throw new Exception("Method needs to be POST");
			}

			/** basic authorization verification * */
			$this->basicAuthorization();
			/** hmac authentication verification * */
			$this->hmacAuthentication();

			common_Logger::i('[Publish/confirm] Authorized Request: [REMOTE_ADDR:' . $_SERVER['REMOTE_ADDR'] . (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? ', HTTP_X_FORWARDED_FOR:' . $_SERVER['HTTP_X_FORWARDED_FOR'] : '') . '] ');

			// request needs to be JSON
			$body = @file_get_contents('php://input');
			$requestBody = json_decode($body, true);

			common_Logger::i("[Publish/confirm] Incoming request: $body");

			$requestKeys = [
				'testFormRevisionId', 
				'adpPublisherRequestId', 
				'status',
				'adpTestBatteryId',
				'adpFormId'
			];
			
			// Check if each item in $requestKeys array was provided in the request and store in a variable. Otherwise, throw an exception.
			foreach($requestKeys as $key) {
				if(!isset($requestBody[$key])) {
					common_Logger::e("[Publish/confirm] $key is missing.");
					throw new common_exception_BadRequest("$key is missing.");
				} else {
					$$key = $requestBody[$key];
				}
			}

			//// Special cases in the request body. ////
			if (!isset($requestBody['adpFormRevisionId'])) {
				$adpFormrevisionId = null;
				common_Logger::w('[Publish/confirm] adpFormRevisionId is missing.');
				if (!in_array($status, [self::STATUS_CANCELED, self::STATUS_FAILED])) {//testId in some cases could be missing!! in case of publishing failure/canceled before we created a test.
					throw new common_exception_BadRequest("adpFormRevisionId is missing when status is ($status)");
				}
			} else {
				$adpFormrevisionId = $requestBody['adpFormRevisionId'];
			}

			if (isset($requestBody['publishedTimestamp'])) {
				$publishTimeStamp = strtotime($requestBody['publishedTimestamp'] . " UTC");
			} else {
				$publishTimeStamp = time();
				common_Logger::w("Request is missing 'publishedTimestamp' $publishTimeStamp will be used instead.");
			}
			//// End special cases. ////

			// Processing below: Saving the Test Id to a specific revision id
			$revisionUri = LOCAL_NAMESPACE . '#' . $testFormRevisionId;
			$revision = new core_kernel_classes_Resource(tao_helpers_Uri::decode($revisionUri));

			// Make sure the revision exist.
			if (!$revision->exists()) {
				throw new common_Exception("Revision ($revisionUri) does not exist!");
			}

			if (!$revision->isInstanceOf(new core_kernel_classes_Class(CLASS_REVISION))) {
				throw new common_Exception("($revisionUri) is not a revision!");
			}

			// Lock the battery
			$battery = taoTestBatteries_helpers_Utils::getBatteryByFormRevision($revisionUri);

			if ($battery === false) {
				throw new common_Exception("Unable to find battery for revision ($revisionUri)");
			}

			$locked = false;
			if ($this->lockManager->isLocked($battery)) {
				$locked = true;
				$this->lockManager->smartRelease($battery, PUBLISHING_USER_URI);
			}
			
			if ($battery !== false) { // redundant, line 317
				$battery->editPropertyValues(new core_kernel_classes_Property(PROPERTY_TEST_BATTERY_ADP_BATTERY_ID), $adpTestBatteryId); // move inside lock closure
			}
			
			if($locked === true) {
				$this->lockManager->smartLock($battery, PUBLISHING_USER_URI);
			}

			// check that jobId coming from ADP matches the job id saved with the revision
			$revisionJobId = current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_PUBLISHER_JOB_ID)));

			if ($adpPublisherRequestId != $revisionJobId) {
//				throw new common_Exception("($revisionUri) unable to find the revision with JobId ($jobId) Actual JobId ($revisionJobId)!");
				common_Logger::w("($revisionUri) unable to find the revision with JobId ($adpPublisherRequestId) Actual JobId ($revisionJobId)!");
			}

			if (!is_null($adpFormrevisionId)) {
				$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_ADP_FORM_REVISION_ID), $adpFormrevisionId);
			}

			$statusMap[self::STATUS_PUBLISHED] = "http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#Published";
			$statusMap[self::STATUS_FAILED] = "http://www.tao.lu/Ontologies/taoTestBatteryFormRevision.rdf#FailedPublishing";


			// Setting status coming from ADP
			if (in_array($status, [self::STATUS_PUBLISHED, self::STATUS_FAILED])) {	//, self::STATUS_UNPUBLISHED, self::STATUS_CANCELED, self::STATUS_SCHEDULED))) {
				$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS), $statusMap[$status]);
				$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_TIME), $publishTimeStamp);
				if (isset($requestBody['errorDescription']) && $requestBody['errorDescription'] != '') {
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION), $requestBody['errorDescription']);
				} else if ($status == self::STATUS_PUBLISHED) {
					// if published we need to set hasPublishedRevision flag to true for battery and form.
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($battery, true);
					common_Logger::i("setHasPublishedRevision to true for battery (" . $battery->getUri() . ")");

					// Reset the error field to empty string and activation status to active.
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHING_STATUS_ERROR_DESCRIPTION), '');
					common_Logger::i("Activating published revision.");
					$revision->editPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_PUBLISHED_ACTIVATION), PROPERTY_REVISION_PUBLISHED_ACTIVE);

					// get the form from the revision
					$formUri = current($revision->getPropertyValues(new core_kernel_classes_Property(PROPERTY_REVISION_FORM)));
					taoTestBatteries_helpers_Utils::setHasPublishedRevision($formUri, true);
					common_Logger::i("setHasPublishedRevision to true for form (" . $formUri . ")");
					common_Logger::i("ADP FORM ID: $adpFormId");
					if (!is_null($adpFormId)) {
						$form = new core_kernel_classes_Resource($formUri);
						$form->editPropertyValues(new core_kernel_classes_Property(PROPERTY_FORM_ADP_FORM_ID), $adpFormId);
					}

					$this->lockManager->smartRelease($battery, PUBLISHING_USER_URI);
				}
			} else {
				throw new common_Exception("status ($status) is not an allowed status value.");
			}

			$this->returnJson([
				'success' => true,
				'adpFormRevisionId'		=> $adpFormrevisionId,
				'adpFormId'				=> $adpFormId,
				'adpBatteryId'			=> $adpTestBatteryId,
				'testFormRevisionId'	=> $testFormRevisionId,
				'adpPublisherRequestId' => $adpPublisherRequestId
			]);
		} catch (common_exception $ex) {
			common_Logger::e($ex->getMessage());
			
			// Before releasing lock
			if(isset($battery)) {
				$this->lockManager->smartRelease($battery, PUBLISHING_USER_URI, true);
			}
			
			$this->returnJson([
				'success' => false,
				'error' => $ex->getMessage(),
				'code' => $ex->getCode()
			]);
		}
	}
}
