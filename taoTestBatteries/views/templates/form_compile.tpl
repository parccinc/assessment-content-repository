<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>

<script type="text/javascript">
	requirejs.config ({
		config: {
			'taoTestBatteries/controller/batteries': {
				battery_label: <?= json_encode(get_data('battery_label')) ?>,
				battery_uri: <?= json_encode(get_data('battery_uri')) ?>,
				form_label: <?= json_encode(get_data('form_label')) ?>,
				tests_array: <?= json_encode(get_data('tests_array') ? get_data('tests_array') : array()) ?>
			}
		}
	});
</script>

<div class="xhtml_form form-content space">
	<h2>Compile Form</h2>
	<form action="<?= _url('compileFormNow', 'Forms', 'taoTestBatteries') ?>" method="post" name="form" style="width: auto;">
		<div>
			<label class="form_desc" style="width: 120px;">Battery</label>
			<span class="form-elt-info form-elt-container"><?= get_data('battery_label') ?></span>
		</div>
		<br/>
		<div>
			<label class="form_desc" style="width: 120px;">Form</label>
			<span class="form-elt-info form-elt-container"><?= get_data('form_label') ?></span>
		</div>
		<br/>
		<div>
			<label class="form_desc" style="width: 120px;">Test *</label>
			<span class="form-elt-info form-elt-container">
				<select id="test" name="test_uri">
					<option value="">(Pick a Test)</option>
					<?php foreach (get_data('tests_array') as $test): ?>
						<option value="<?= $test['uri'] ?>"<?= $test['selected'] ? ' selected="selected"' : '' ?>><?= $test['label'] ?></option>
					<?php endforeach ?>
				</select>
				<br/>
				<span style="float: left; margin-left: 127px;"><i>The Test you choose will be saved to<br>this form when you compile</i></span>
			</span>
		</div>
		<br/><br/>
		<div>
			<input type="hidden" name="battery_uri" value="<?= get_data('battery_uri') ?>">
			<input type="hidden" name="battery_label" value="<?= get_data('battery_label') ?>">
			<input type="hidden" name="form_label" value="<?= get_data('form_label') ?>">
			<input type="hidden" name="form_uri" value="<?= $form_uri ?>">
			<br/>
			<div style="float: right;">
                <button type="submit" class="form-submitter action btn-info small active" id="compileFormBtn">Compile Form Now</button>
			</div>
			<br/>
		</div>
	</form>
</div>
