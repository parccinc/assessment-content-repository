<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

$battery_uri = get_data('battery_uri');
$battery_id = get_data('battery_id');
$battery_label = get_data('battery_label');
$forms = get_data('forms');
$has_forms = count($forms);
$form_selected = get_data('form_selected');
$form_id = get_data('form_id');
$form_revisions = get_data('form_revisions');
$form_revision_selected = get_data('revision_selected');
$form_revision_selected_number = get_data('revision_selected_number');
$revision_id = get_data('revision_id');
$revision_actions = get_data('revision_actions');
$adp_job_id = get_data('adp_job_id');
$adp_form_revision_id = get_data('adp_form_revision_id');
$adp_form_id = get_data('adp_form_id');
$adp_test_battery_id = get_data('adp_battery_id');
$publish_error_desc = get_data('publish_error_desc');
$local_compileDateTime = get_data('local_compileDateTime');
$local_publishDateTime = get_data('local_publishDateTime');
$activation_status = get_data('activation_status');
?>

<script type="text/javascript">
requirejs.config({
   config: {
		'taoTestBatteries/controller/batteries' : {
			battery_uri: <?= json_encode(get_data('battery_uri')) ?>,
			battery_id: <?= json_encode(get_data('battery_id')) ?>,
			battery_label: <?= json_encode(get_data('battery_label')) ?>,
			forms: <?= json_encode(get_data('forms')) ?>,
			has_forms: <?= count(get_data('forms')) ?>,
			form_selected: <?= json_encode(get_data('form_selected')) ?>,
			form_id: <?= json_encode(get_data('form_id')) ?>,
			form_revisions: <?= json_encode(get_data('form_revisions')) ?>,
			form_revision_selected: <?= json_encode(get_data('revision_selected')) ?>,
			form_revision_selected_number: <?= json_encode(get_data('revision_selected_number')) ?>,
			revision_id: <?= json_encode(get_data('revision_id')) ?>,
			revision_actions: <?= json_encode(get_data('revision_actions')) ?>,
			adp_job_id: <?= json_encode(get_data('adp_job_id')) ?>,
			adp_form_revision_id: <?= json_encode(get_data('adp_form_revision_id')) ?>,
			adp_form_id: <?= json_encode(get_data('adp_form_id')) ?>,
			adp_test_battery_id: <?= json_encode(get_data('adp_battery_id')) ?>,
			publish_error_desc: <?= json_encode(get_data('publish_error_desc')) ?>,
			local_compileDateTime: <?= json_encode(get_data('local_compileDateTime')) ?>,
			local_publishDateTime: <?= json_encode(get_data('local_publishDateTime')) ?>}
		}
   }
);
</script>

<!-- FORMS SECTION -->
<div class="form-content xhtml_form space">
	<table>
		<tr>
			<td>
				<h2>Forms</h2>
			</td>
			<td>
				<label style="margin-top: 10px;" for="submit_new" class="action btn-info small active">New Form</label>
			</td>
		</tr>
	</table>

	<form name="f1" id="compile_form" method="post" action="<?= _url('newForm', 'Forms', 'taoTestBatteries') ?>" style="height: 239px; width: 300px;<?= $has_forms ? '' : ' display: none;' ?>">
		<?php if ($has_forms): ?>
		<select name="form_selected" id="form_selected" size="5">
			<?php foreach ($forms as $form_uri => $content_array): ?>
			<option value="<?= $form_uri ?>"<?= $form_uri == $form_selected ? ' selected="selected"' : '' ?>><?= $content_array['label'] ?></option>
			<?php endforeach ?>
		</select>
		<br/><br/>
		<button type="submit" class="form-submitteraction btn-info small active" onclick="f1.action = '<?= _url('compileForm', 'Forms', 'taoTestBatteries') ?>'; return true;">Compile Form</button>
		<button type="submit" class="form-submitter action btn-info small active" onclick="f1.action = '<?= _url('updateForm', 'Forms', 'taoTestBatteries') ?>'; return true;">Edit Form</button>
		<?php endif; ?>
        <button type="submit" class="form-submitter action btn-info small active" id="submit_new" style="display: none;"></button>
		<input type="hidden" name="battery_uri" value="<?= $battery_uri ?>">
		<input type="hidden" name="battery_label" value="<?= $battery_label ?>">
	</form>
</div>

<!-- REVISIONS SECTION -->
<?php if ($has_forms): ?>
    <div class="form-content xhtml_form space" style="padding-left: 10px;">
	<h2>Form Revision</h2>
	<form action="" method="post" name="form_revisions"  id="form_revisions" style="height:239px; width:300px">
		<select name="revision_selected" id="revision_selected" size="7">
			<?php foreach ($form_revisions as $revision): ?>
			<option value="<?= $revision['revisionUri'] ?>"<?= $form_revision_selected == $revision['revisionUri'] ? ' selected="selected"' : '' ?>><?= $revision['revisionNumber'] ?></option>
			<?php endforeach ?>
		</select>
		<input type="hidden" name="battery_uri" value="<?= $battery_uri ?>">
		<input type="hidden" name="form_selected" value="<?= $form_selected ?>">
	</form>
</div>
<br/>

<!-- COMPILATION DETAILS -->
<?php if ($forms[$form_selected]['revisions']): ?>
<br/>
<div id = "test-revision" style="display: table-cell; width:1200px;" >
    <div class="form-content xhtml_form space">
		<h2> Form Revision Properties  </h2>
		<form name="f2" id="formRevisionProperties" method="post" action="">
			<label style="cursor: auto"><strong>Form Revision:</strong></label><span id="form_revision_selected_number"><?= $form_revision_selected_number ?></span>
			<span style="float: right">
            <button type="submit" class="action btn-info small active" id="deleteRevisionBtn" style="display:none;" form="formRevisionProperties"></button>
            <button type="submit" class="action btn-info small active" id="publishRevisionBtn" style="display:none;" form="formRevisionProperties"></button>
			<button type="submit" class="action btn-info small active" id="republishRevisionBtn" style="display:none;" form="formRevisionProperties"></button>
            <button type="submit" class="action btn-info small active" id="unpublishRevisionBtn" style="display:none;" form="formRevisionProperties"></button>
            <button type="submit" class="action btn-info small active" id="cancelPublishBtn" style="display:none;" form="formRevisionProperties"></button>
            <button type="submit" class="action btn-info small active" id="cancelUnpublishBtn" style="display:none;" form="formRevisionProperties"></button>
			<button type="submit" class="action btn-info small active" id="updateBatteryBtn" style="display:none;" formaction="<?= BASE_URL ?>Forms/updateTestProperties"></button>
			</span>
			<br/><br/>
			<hr>
			<label style="cursor:auto"><strong> Compile Status </strong> </label>
			<br/><br/>
            <label style="cursor: auto">Battery Id:</label><span id="battery_id"><?= $battery_id ?></span>
			<br/>
            <label style="cursor: auto">Form Id:</label><span id="form_id"><?= $form_id ?></span>
			<br/>
            <label style="cursor: auto">Revision Id:</label><span id="revision_id"><?= $revision_id ?></span>
			<br/>
			<label style="cursor: auto">Test:</label><span id="test"><?= $form_revisions[$form_revision_selected_number]['test'] ?></span>
			<br/>
			<label style="cursor: auto">Compile Date/Time:</label><span id="compile_date_time"><?= $local_compileDateTime ?></span>
			<br/>
			<label style="cursor: auto">Compiled By:</label><span id="compiled_by"><?= $form_revisions[$form_revision_selected_number]['compiledBy'] ?></span>
			<br/><br/>
			<a id="revisionDownloadURL" href="<?= $form_revisions[$form_revision_selected_number]['downloadURL'] ?>">Download Test Package (*.zip)</a>
			<br/><br/>
			<input type="hidden" name="uri" value="<?= $battery_uri ?>" />
			<input type="hidden" name="form_selected" value="<?= $form_selected ?>" />
			<input type="hidden" id="revision_selected" name="revision_selected" value="<?= $form_revision_selected ?>" />
			<input type="hidden" id="update_battery" name="update_battery" value="false" />
			<input type="hidden" id="battery_uri_1" name="battery_uri" value="<?= $battery_uri ?>" />
		</form>
	</div>
</div>

<!-- PUBLISHING DETAILS -->
<?php if ($form_revisions[$form_revision_selected_number]['publishStatus'] != 'Unpublished'): ?>
<br/>
<div id = "pusblished-revision" style="display: table-cell; width:1200px;" >
    <div class="xhtml_form form-content space">
		<h2> Published Revision Properties  </h2>
		<form method="post" name="revision" id="publishedRevisionProperties" action="<?= BASE_URL ?>Forms/setADPActivationStatus">
			<span style="float: right">
            <button type="submit" class="form-submitter action btn-info small active" id="activateBtn" style="display:none;">Activate</button>
            <button type="submit" class="form-submitter action btn-info small active" id="deactivateBtn" style="display:none;">Deactivate</button>
			</span>
			<br/><br/>
			<hr>
			<label style="cursor:auto"><strong>Last Publish Status:</strong></label><span id="publish_status"><?= $form_revisions[$form_revision_selected_number]['publishStatus'] ?></span>
			<?php if (!empty($publish_error_desc)): ?>
				<br />
				<label style="cursor: auto">Error Description:</label><span id="publish_error_desc"><?= $publish_error_desc ?></span>
			<?php endif; ?>
			<br/><br/>
			<label style="cursor: auto">ADP Request Id:</label><span id="adpRequestId"><?= $adp_job_id ?></span>
			<br />
			<label style="cursor: auto">ADP Test Battery Id:</label><span id="adpTestBatteryId"><?= $adp_test_battery_id ?></span>
			<br />
			<label style="cursor: auto">ADP Form Id:</label><span id="adpFormId"><?= $adp_form_id ?></span>
			<br />
			<label style="cursor: auto">ADP Form Revision Id:</label><span id="adpFormRevisionId"><?= $adp_form_revision_id ?></span>
			<br />
			<label style="cursor: auto">Last Published Date/Time:</label><span id="publishDateTime"><?= $local_publishDateTime ?></span>
			<br/>
			<label style="cursor: auto">Last Published By:</label><span id="publishedBy"><?= $form_revisions[$form_revision_selected_number]['publishedBy'] ?></span>
			<br/><br/>
			<label style="cursor: auto"><strong>Activation Status:</strong></label><span id="activation_status"><?= $activation_status ?></span>
			<br/><br/>
			<input type="hidden" id="revision_selected" name="revision_selected" value="<?= $form_revision_selected ?>" />
			<input type="hidden" id="revision_id" name="revision_id" value="<?= $revision_id ?>" />
			<input type="hidden" id="activate_revision" name="activate_revision" />
			<input type="hidden" id="battery_uri" name="battery_uri" value="<?= $battery_uri ?>" />
			<input type="hidden" id="form_uri" name="form_uri" value="<?= $form_selected ?>" />
		</form>
	</div>
</div>
<?php endif; ?>

<?php endif; ?>

<?php endif; ?>
<?php
Template::inc('footer.tpl', 'tao');
?>
