<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>

<style type="text/css">
	.property-adder{
		display: none;
	}
</style>

<header class="flex-container-full">
	<h2>Edit Battery Class</h2>
</header>
<div class="main-container flex-container-main-form">
    <div class="xhtml_form form-content form-container">
		<?= get_data('selectedForm') ?>
	</div>
</div>
<div class="data-container-wrapper flex-container-remainer"></div>
<?php
	Template::inc('footer.tpl', 'tao');
?>
