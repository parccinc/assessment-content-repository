<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>
<header class="section-header flex-container-full">
    <h2><?=__('Battery Locked')?></h2>
</header>
<div class="flex-container-half">

    <div class="feedback feedback-warning">
		<h3><span class="icon-lock big"></span> <?=__('This battery is currently being edited and has been locked')?></h3> 
		<div class="grid-row">
		    <span class="col-2"><?=__('Battery Locked:')?></span>
            <span class="col-2"><?=get_data('label')?></span>
        </div>
		<div class="grid-row">
		    <span class="col-2"><?=__('Lock Owner:')?></span>
            <span class="col-2"><a href="mailto:<?=get_data('ownerMail')?>"><?=get_data('ownerHtml')?></a></span>
        </div>
		<div class="grid-row">
		    <span class="col-2"><?=__('Locking Date:')?></span>
            <span class="col-2"><?=tao_helpers_Date::displayeDate(get_data('lockDate'))?></span>
        </div>
		
        <?php if (get_data('isOwner')): ?>
		<p>
            <em><?=__('As the owner of this resource, you may release the lock')?></em>
            <button id="release" class="btn btn-warning"><?=__('ReleaseLock')?></button>
        </p>
		<?php else : ?>
		<p><em><?=__('Please contact the owner of the resource to unlock it')?></em></p>
		<?php endif;?>
	    
	</div>
    
</div>
<script type="text/javascript">
require(
['jquery', 'i18n', 'helpers', 'lock', 'layout/section', 'ui/feedback'], 
function($, __, helpers, Lock, sectionApi, feedback){

    var uri = <?=json_encode(get_data('batteryUri'))?>;
    var dest = helpers._url('editTestBatteries', 'TestBatteries', 'taoTestBatteries', {uri : uri});

    var successCallBack = function successCallBack() {
        sectionApi.current().loadContentBlock(dest);
    };
    var errorBack = function errorBack(){
        feedback().error(__('Unable to release the lock'));
    };
    $("#release").click(function(e) {
        e.preventDefault();
        new Lock(uri).release(successCallBack, errorBack);
    });
});
</script>
<?php Template::inc('footer.tpl', 'tao'); ?>
