<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>

    <script type="text/javascript">
	var base = "<?= get_data('base') ?>";

    $(document).ready(function() {
		var sourceId = '#' + base + 'TestingProgram';
		var targetId = '#' + base + 'ScoreReport';
        var selectedSource = $(sourceId).val();

		// adjust target menu on load
		resetMenus(selectedSource);

        $(sourceId).on('change', function() {
            selectedSource = $(sourceId).val();
            $(targetId).val('');
			resetMenus(selectedSource);

            return;
        });

		function resetMenus(selectedSource) {
            switch (selectedSource) {
                case base + 'DiagnosticAssessments':
                    $(targetId).children("option[value='']").show();
                    $(targetId).children("option[value='" + base + "None']").show();
                    $(targetId).children("option[value='" + base + "Generic']").hide();
                    $(targetId).children("option[value='" + base + "ELADecoding']").show();
                    $(targetId).children("option[value='" + base + "ELAReaderMotivationSurvey']").show();
                    $(targetId).children("option[value='" + base + "ELAReaderComprehension']").show();
                    $(targetId).children("option[value='" + base + "ELAVocabulary']").show();
                    $(targetId).children("option[value='" + base + "MathComprehension']").show();
                    $(targetId).children("option[value='" + base + "MathFluency']").show();
                    break;

                case base + 'PracticeTest':
                    $(targetId).children("option[value='']").show();
                    $(targetId).children("option[value='" + base + "None']").show();
                    $(targetId).children("option[value='" + base + "Generic']").show();
                    $(targetId).children("option[value='" + base + "ELADecoding']").hide();
                    $(targetId).children("option[value='" + base + "ELAReaderMotivationSurvey']").hide();
                    $(targetId).children("option[value='" + base + "ELAReaderComprehension']").hide();
                    $(targetId).children("option[value='" + base + "ELAVocabulary']").hide();
                    $(targetId).children("option[value='" + base + "MathComprehension']").hide();
                    $(targetId).children("option[value='" + base + "MathFluency']").hide();
                    break;

                default:
                    $(targetId).children("option[value='']").show();
                    $(targetId).children("option[value='" + base + "None']").show();
                    $(targetId).children("option[value='" + base + "Generic']").hide();
                    $(targetId).children("option[value='" + base + "ELADecoding']").hide();
                    $(targetId).children("option[value='" + base + "ELAReaderMotivationSurvey']").hide();
                    $(targetId).children("option[value='" + base + "ELAReaderComprehension']").hide();
                    $(targetId).children("option[value='" + base + "ELAVocabulary']").hide();
                    $(targetId).children("option[value='" + base + "MathComprehension']").hide();
                    $(targetId).children("option[value='" + base + "MathFluency']").hide();
            }

			return;
		}
    });

    </script>

<?php if (has_data('authoring')):?>
	<div id="test-left-container">
		<?= get_data('authoring') ?>
		<div class="breaker"></div>
	</div>
<?php endif;?>
<div style="display: table-cell; width: 1200px;">
    <div class="xhtml_form main-container">
		<h2>Edit Battery Properties</h2>
		<div class="form-content">
			<?= get_data('selectedForm') ?>
		</div>
	</div>
</div>
<?php
	Template::inc('footer.tpl', 'tao');
?>
