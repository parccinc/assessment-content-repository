<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>

<script type="text/javascript">
	requirejs.config ({
		config: {
			'taoTestBatteries/controller/batteries': {
				battery_label: <?= json_encode(get_data('battery_label')) ?>,
				battery_uri: <?= json_encode(get_data('battery_uri')) ?>,
				form_label: <?= json_encode(get_data('form_label')) ?>,
				test_label: <?= json_encode(get_data('test_label')) ?>,
				revision_number: <?= json_encode(get_data('revision_number')) ?>,
				revision_selected: <?= json_encode(get_data('revision_selected')) ?>,
				download_url: <?= json_encode(get_data('download_url')) ?>
			}
		}
	});
</script>

<!-- add .xhtml_form below -->
<div class="xhtml_form form-content space">
	<h2>Compilation successful</h2>
	<form action="<?= _url('renderFormsUI', 'Forms', 'taoTestBatteries') ?>" method="get" name="compile_result_form">
		<div>
			<label class="form_desc" style="width: 120px;">Battery</label>
			<span class="form-elt-info form-elt-container"><?= get_data('battery_label') ?></span>
		</div>
		<br/>
		<div>
			<label class="form_desc" style="width: 120px;">Form</label>
			<span class="form-elt-info form-elt-container"><?= get_data('form_label') ?></span>
		</div>
		<br/>
		<div>
			<label class="form_desc" style="width: 120px;">Test</label>
			<span class="form-elt-info form-elt-container"><?= get_data('test_label') ?></span>
		</div>
		<br/>
		<div>
			<label class="form_desc" style="width: 120px;">Form Revision</label>
			<span class="form-elt-info form-elt-container"><?= get_data('revision_number') ?></span>
		</div>
		<div>
			<br/><br/>
			<div><a href="<?= get_data('download_url') ?>">Download test package (*.zip)</a></div>
			<br/><br/>
			<input type="hidden" name="form_selected"  value="<?= get_data('form_uri') ?>">
			<input type="hidden" name="battery_uri" value="<?= get_data('battery_uri') ?>">
			<input type="hidden" name="revision_selected_number" value="<?= get_data('revision_number') ?>">
			<input type="hidden" name="revision_selected" value="<?= get_data('revision_selected') ?>">
            <button type="submit" class="form-submitter action btn-info small active" id="backToForms">Go back to forms</button>
		</div>
	</form>
</div>
