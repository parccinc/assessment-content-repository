<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

use oat\tao\helpers\Template;

?>

<script type="text/javascript">
	requirejs.config({
		config: {
			'taoTestBatteries/controller/batteries' : {

			}
		}
	});
</script>

<div class="xhtml_form form-content space" style="width: auto;">
	<h2>Create New Form</h2>
	<form action="<?= _url('editForm', 'Forms', 'taoTestBatteries') ?>" method="post" name="form">
		<div>
			<label class="form_desc" style="width: 120px;">Battery</label>
			<!--<input type="text" name="batteryLabel" id="batteryLabel" value="<?= get_data('battery_label') ?>" readonly>-->
			<span class="form-elt-info form-elt-container"><?= get_data('battery_label') ?></span>
			<input type="hidden" name="battery_uri" id="uriBattery" value="<?= get_data('battery_uri') ?>">
		</div>
		<br/><br/>
		<div>
			<label class="form_desc" style="width: 120px;">Form*</label>
			<span class="form-elt-info form-elt-container">
				<input type="text" name="formLabel" id="formLabel" maxlength="20"></br>
				<span style="float: left; margin-left: 127px;"><i>Form label must be unique within the battery</i></span>
			</span>
		</div>
		<br/><br/>
		<div>
			<label class="form_desc" style="width: 120px;">Test</label>
			<span class="form-elt-info form-elt-container">
				<select id="test" name="test">
					<option value="">(Pick a Test)</option>
					<?php foreach (get_data('tests_array') as $test): ?>
						<option value="<?= $test['uri'] ?>"<?= $test['selected'] ? ' selected="selected"' : '' ?>><?= $test['label'] ?></option>
					<?php endforeach ?>
				</select>
			</span>
		</div>
		<br/><br/>
		<div style="text-align: right">
            <button type="submit" class="form-submitter action btn-info small active" id="saveEditing">Save Form</button>
		</div>
	</form>
</div>
<?php
	Template::inc('footer.tpl', 'tao');
?>
