/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

define(['module', 'context'], function (module, context) {

	var batteriesController = {
		start: function () {
			var config = module.config();
			var rootUrl = context.root_url + "taoTestBatteries/Forms/";
			var form_revisions = config.form_revisions;
			var revision_actions = config.revision_actions;

			/**
			 * @author Oussama Saddane <oussama.saddane@breaktech.com>
			 * Disable Save Form Button when the form field is empty
			 */
			if ($("#formLabel").val() === '') {
				$('#saveEditing').attr("disabled", "disabled");
			}

			$("#formLabel").bind("change paste keyup", function () {
				if ($(this).val().trim() === '') {                  // REV: Added trim().
					$('#saveEditing').prop('disabled', true);
				} else {
					$('#saveEditing').prop('disabled', false);
				}
			});


			/**
			 * @author Oussama Saddane <oussama.saddane@breaktech.com>
			 * Handle enter key on Form input / call save event instead of delete
			 **/

			$('#formLabel').bind('keypress', function (event) {
				if (event.keyCode === 13) {
					$('#saveEditing').click();
					return false;
				}
			});

			$('#deleteForm').click(function () {
				if (confirm('Please confirm deletion')) {
					$('#form_edit').attr('action', rootUrl + "delete");
					return true;
				}
				return false;
			});


			$('#saveEditing').click(function () {
				$('#form_edit').attr('action', rootUrl + "editForm");
				return true;
			});

			/**
			 * Handles Form Revision Froperties form actions
			 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
			 * @returns {true | false} to the form
			 */
			// delete Form Revision
			$('#deleteRevisionBtn').click(function() {
				if (confirm('Please confirm deletion')) {
					$('#formRevisionProperties').attr('action', rootUrl + 'deleteRevision');
					return true;
				}

				return false;
			});

			// publish Form Revision
			$('#publishRevisionBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl + 'publishFormRevision');
				return true;
			});

			// republish Form Revision
			$('#republishRevisionBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl + 'publishFormRevision');
				return true;
			});

			// unpublish Form Revision
			$('#unpublishRevisionBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl + 'unpublishFormRevision');
				return true;
			});

			// cancel publishing
			$('#cancelPublishBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl);
				return true;
			});

			// cancel unpublishing
			$('#cancelUnpublishBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl);
				return true;
			});
			
			// battery updating
			$('#updateBatteryBtn').click(function() {
				$('#formRevisionProperties').attr('action', rootUrl + 'updateTestProperties');
				return true;
			});

			/**
			 * @author Vlad Karpenko <vlad.karpenko@breaktech.com>
			 * Change buttons names/visibily/availability depending on publishing status
			 * Page: forms.tpl
			 */
			function changeButtonsStatus() {
				var n = revision_actions.length;
				var id = '';

				for (var i = 0; i < n; i++) {
					switch (revision_actions[i]['nick']) {
						case 'delete':
							id = '#deleteRevisionBtn';
							break;

						case 'publish':
							id = '#publishRevisionBtn';
							break;

						case 'republish':
							id = '#republishRevisionBtn';
							break;

						case 'unpublish':
							id = '#unpublishRevisionBtn';
							break;

						case 'cancel_pub':
							id = '#cancelPublishBtn';
							break;

						case 'cancel_unpub':
							id = '#cancelUnpublishBtn';
							break;

						case 'activate':
							id = '#activateBtn';
							break;

						case 'deactivate':
							id = '#deactivateBtn';
							break;
							
						case 'republishBatteryProperties':
							id = '#updateBatteryBtn';
							break;

						default:
							id = '';
					}

					if (id != '') {
						$(id).prop('disabled', revision_actions[i]['disabled']);
						$(id).text(revision_actions[i]['button']);

						if (revision_actions[i]['visible']) {
							$(id).show();

							if (id == '#activateBtn') {
								$('#activate_revision').val('activate');
							} else {
								if(id == '#deactivateBtn') {
									$('#activate_revision').val('deactivate');
								}
							}
						}
						else {
							$(id).hide();
						}
					}
				}
			}

			changeButtonsStatus();

			/**
			 * @author Oussama Saddane <oussama.saddane@breaktech.com>
			 * Disable Compile Form Now Button when there is no selected test
			 */
			function compileFormNowBtnStatus() {
				if ($("#test option:selected").val() != '') {
					$('#compileFormBtn').prop('disabled', false);
				} else {
					$('#compileFormBtn').prop('disabled', true);
				}
			}

			compileFormNowBtnStatus();

			$('#test').on('change', function () {
				compileFormNowBtnStatus();
			});

			$('#form_selected').on('change', function () {
				$('#compile_form').attr('action', rootUrl + 'renderFormsUI');
				$('#compile_form').submit();
			});

			$('#revision_selected').on('change', function () {
				$('#form_revisions').attr('action', rootUrl + 'renderFormsUI');
				$('#form_revisions').submit();
			});
		} // start function
	} // batteriesController

	// expose the controller
	return batteriesController;
}); //function define
