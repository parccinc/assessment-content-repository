<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Built on PHPUnit version 5.1.4
 */
namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use core_kernel_classes_Resource as Resource;
use taoTestBatteries_helpers_LockManager as LockManager;
use oat\taoWorkspace\model\lockStrategy\LockSystem as LockSystem;
use oat\taoWorkspace\model\lockStrategy\Lock as Lock;
use oat\tao\model\lock\implementation\OntoLock as OntoLockSystem;
use oat\tao\model\lock\implementation\OntoLockData as OntoLockData;
use oat\tao\model\lock\implementation\NoLock as NoLockSystem;
use oat\oatbox\Configurable as Configurable;

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

/**
 * Class LockManagerTest
 *
 * @package oat\taoTestBatteries\test
 */
class LockManagerTest extends BaseTest {
	private $lm;

	public function __construct() {
		$this->lm = new \stdClass();
		$this->lm->default = new LockManager();
		$this->lm->lockSystem = new LockManager(new LockSystem());
		$this->lm->ontoLock = new LockManager(new OntoLockSystem());
		$this->lm->noLock = new LockManager(new NoLockSystem());

		parent::__construct();
	}

	protected function setUp() {
		// Set up all locks before running tests.
		try {
			// (LockSystem) Unlock if owned by wrong user.
			$res = new Resource($this->defaultLockedLockSystemBatteryUri);
			if($this->lm->lockSystem->isLocked($res)) {
				$owner =$this->lm->lockSystem->getOwner($res);
				if($owner->getUri() !== $this->virtualUserRole) {
					$this->lm->lockSystem->smartRelease($res, $owner->getUri(), true);
				}
			}

			// (LockSystem) Lock for appropriate user.
			if(!$this->lm->lockSystem->isLocked($res)) {
				$this->lm->lockSystem->smartLock($res);
			}

			// (OntoLock) Unlock if owned by wrong user.
			$res = new Resource($this->defaultLockedOntoLockBatteryUri);
			if($this->lm->ontoLock->isLocked($res)) {
				$owner =$this->lm->ontoLock->getOwner($res);
				if($owner->getUri() !== $this->virtualUserRole) {
					$this->lm->ontoLock->smartRelease($res, $owner->getUri(), true);
				}
			}

			// (OntoLock) Lock for appropriate user.
			if(!$this->lm->ontoLock->isLocked($res)) {
				$this->lm->ontoLock->smartLock($res);
			}

			// (LockSystem) Unlock if locked.
			$res = new Resource($this->defaultUnlockedBatteryUri);
			if ($this->lm->lockSystem->isLocked($res)) {
				$this->lm->lockSystem->smartRelease($res, null, true);
			}

			// (OntoLock) Unlock if locked.
			$res = new Resource($this->defaultUnlockedBatteryUri);
			if ($this->lm->ontoLock->isLocked($res)) {
				$this->lm->ontoLock->smartRelease($res, null, true);
			}
		} catch (Exception $ex) {
			throw new Exception('Failed setting up lock systems for current test: ' . $ex->getMessage());
		}

		TaoPhpUnitTestRunner::initTest();
	}

	protected function tearDown() {
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::resourceIsCommittable
	 */
	public function test_resourceIsCommittable_NotLocked_ReturnsFalse() {
		$resource = new Resource('x');

		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods([
				 'isLocked'
			])
			->getMock();

		$mock->method('isLocked')
			->will($this->returnValue(false));

		$this->assertFalse($this->invokeMethod($mock, 'resourceIsCommittable', [$resource]));
		return $resource;
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::resourceIsCommittable
	 * @depends test_resourceIsCommittable_NotLocked_ReturnsFalse
	 * @expectedException common_Exception
	 * @expectedExceptionMessage User is not of an expected type.
	 */
	public function test_resourceIsCommittable_BadUsers_ThrowsException($resource) {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods([
				'isLocked',
				'getOwner',
				'getCurrentUser_LM'
			])
			->getMock();

		$mock->method('isLocked')
			->will($this->returnValue(true));

		$mock->method('getOwner')
			->will($this->returnValue('something'));

		$mock->method('getCurrentUser_LM')
			->will($this->returnValue('something'));

		$this->invokeMethod($mock, 'resourceIsCommittable', [$resource]);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::resourceIsCommittable
	 * @depends test_resourceIsCommittable_NotLocked_ReturnsFalse
	 */
	public function test_resourceIsCommittable_IncorrectUser_ReturnsFalse($resource) {
		$owner = new Resource($this->badRole);
		$currentUser = new Resource($this->virtualUserRole);

		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods([
				'isLocked',
				'getOwner',
				'getCurrentUser_LM'
			])
			->getMock();

		$mock->method('isLocked')
			->will($this->returnValue(true));

		$mock->method('getOwner')
			->will($this->returnValue($owner));

		$mock->method('getCurrentUser_LM')
			->will($this->returnValue($currentUser));

		$this->assertFalse($this->invokeMethod($mock, 'resourceIsCommittable', [$resource]));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::resourceIsCommittable
	 * @depends test_resourceIsCommittable_NotLocked_ReturnsFalse
	 */
	public function test_resourceIsCommittable_NotLockSystem_ReturnsFalse($resource) {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods([
				'isLocked',
				'getOwner',
				'getCurrentUser_LM',
				'isLockSystem'
			])
			->getMock();

		$mock->method('isLocked')
			->will($this->returnValue(true));

		$mock->method('getOwner')
			->will($this->returnValue($resource));

		$mock->method('getCurrentUser_LM')
			->will($this->returnValue($resource));

		$mock->method('isLockSystem')
			->will($this->returnValue(false));

		$this->assertFalse($this->invokeMethod($mock, 'resourceIsCommittable', [$resource]));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::resourceIsCommittable
	 * @depends test_resourceIsCommittable_NotLocked_ReturnsFalse
	 */
	public function test_resourceIsCommittable_LockSystemCorrectUser_ReturnsTrue($resource) {
		$sameUser = new Resource($this->badRole);

		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods([
				'isLocked',
				'getOwner',
				'getCurrentUser_LM',
				'isLockSystem'
			])
			->getMock();

		$mock->method('isLocked')
			->will($this->returnValue(true));

		$mock->method('getOwner')
			->will($this->returnValue($sameUser));

		$mock->method('getCurrentUser_LM')
			->will($this->returnValue($sameUser));

		$mock->method('isLockSystem')
			->will($this->returnValue(true));

		$this->assertTrue($this->invokeMethod($mock, 'resourceIsCommittable', [$resource]));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::__construct
	 */
	public function test_construct_NoArgs_ReturnsLockManagerObject() {
		$this->assertInstanceOf('taoTestBatteries_helpers_LockManager', new LockManager());
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::__construct
	 */
	public function test_construct_LockSystemArg_ReturnsLockManagerObject() {
		$this->assertInstanceOf('taoTestBatteries_helpers_LockManager', new LockManager(new LockSystem()));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::__construct
	 */
	public function test_construct_OntoLockArg_ReturnsLockManagerObject() {
		$this->assertInstanceOf('taoTestBatteries_helpers_LockManager', new LockManager(new OntoLockSystem()));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::__construct
	 */
	public function test_construct_NoLockArg_ReturnsLockManagerObject() {
		$this->assertInstanceOf('taoTestBatteries_helpers_LockManager', new LockManager(new NoLockSystem()));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_UnlockedLockSystem_ReturnsNull() {
		$this->assertNull($this->lm->lockSystem->getLockData(new Resource($this->defaultUnlockedBatteryUri)));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_UnlockedOntoLock_ReturnsNull() {
		$this->assertNull($this->lm->ontoLock->getLockData(new Resource($this->defaultUnlockedBatteryUri)));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_UnlockedNoLock_ReturnsNull() {
		$this->assertNull($this->lm->noLock->getLockData(new Resource($this->defaultUnlockedBatteryUri)));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_LockedLockSystem_ReturnsLock() {
		$this->assertInstanceOf(
			'oat\taoWorkspace\model\lockStrategy\Lock',
			$this->lm->lockSystem->getLockData(new Resource($this->defaultLockedLockSystemBatteryUri))
		);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_LockedOntoLock_ReturnsOntoLockData() {
		$this->assertInstanceOf(
			'oat\tao\model\lock\implementation\OntoLockData',
			$this->lm->ontoLock->getLockData(new Resource($this->defaultLockedOntoLockBatteryUri))
		);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getLockData
	 */
	public function test_getLockData_LockedNoLock_ReturnsNull() {
		$this->assertNull($this->lm->noLock->getLockData(new Resource($this->defaultLockedOntoLockBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getOwner
	 */
	public function test_getOwner_LockedLockSystem_ReturnsResource() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => new LockSystem()])
			->setMethods(['getLockData'])
			->getMock();

		$battery = new Resource($this->defaultLockedLockSystemBatteryUri);

		$lock = new Lock(
			$battery,
			$this->anonymousRole,
			time(),
			new Resource($this->defaultLockedLockSystemBatteryWorkCopyUri)
		);

		$mock->method('getLockData')
			->willReturn($lock);

		$owner = $mock->getOwner($battery);

		$this->assertInstanceOf(
			'core_kernel_classes_Resource',
			$owner
		);

		$this->assertSame($owner->getUri(), $this->anonymousRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getOwner
	 */
	public function test_getOwner_LockedOntoLock_ReturnsResource() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => new OntoLockSystem()])
			->setMethods(['getLockData'])
			->getMock();

		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);

		$lock = new OntoLockData(
			$battery,
			$this->anonymousRole,
			time()
		);

		$mock->method('getLockData')
			->willReturn($lock);

		$owner = $mock->getOwner($battery);

		$this->assertInstanceOf(
			'core_kernel_classes_Resource',
			$owner
		);

		$this->assertSame($owner->getUri(), $this->anonymousRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getOwner
	 */
	public function test_getOwner_NoLock_ReturnsNull() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => new NoLockSystem()])
			->setMethods(['getLockData'])
			->getMock();

		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);

		$lock = new OntoLockData(
			$battery,
			$this->anonymousRole,
			time()
		);

		$mock->method('getLockData')
			->willReturn($lock);

		$this->assertNull($mock->getOwner($battery));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getOwner
	 */
	public function test_getOwner_UnlockedLockSystem_ReturnsNull() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => new LockSystem()])
			->setMethods(['getLockData'])
			->getMock();

		$mock->method('getLockData')
			->willReturn(null);

		$this->assertNull($mock->getOwner(new Resource($this->defaultUnlockedBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::getOwner
	 */
	public function test_getOwner_UnlockedOntoLock_ReturnsNull() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => new OntoLockSystem()])
			->setMethods(['getLockData'])
			->getMock();

		$mock->method('getLockData')
			->willReturn(null);

		$this->assertNull($mock->getOwner(new Resource($this->defaultUnlockedBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_LockSystemNoParam_ReturnsVirtualUserRole() {
		$this->assertSame($this->invokeMethod($this->lm->lockSystem, 'handleUserUri'), $this->virtualUserRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_OntoLockNoParam_ReturnsVirtualUserRole() {
		$this->assertSame($this->invokeMethod($this->lm->ontoLock, 'handleUserUri'), $this->virtualUserRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_NoLockNoParam_ReturnsVirtualUserRole() {
		$this->assertSame($this->invokeMethod($this->lm->noLock, 'handleUserUri'), $this->virtualUserRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_LockSystemNoParamAnonDefaultRole_ReturnsAnonymousRole() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
				->setConstructorArgs(['implementation' => new LockSystem()])
				->setMethods(['getCurrentUser_LM'])
				->getMock();

		// It doesn't matter what this function returns,
		// as long as it's not a core_kernel_classes_Resource.
		$mock->method('getCurrentUser_LM')
				->willReturn($this->badRole);

		$this->assertSame($this->invokeMethod($mock, 'handleUserUri'), $this->anonymousRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_OntoLockNoParamAnonDefaultRole_ReturnsAnonymousRole() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
				->setConstructorArgs(['implementation' => new OntoLockSystem()])
				->setMethods(['getCurrentUser_LM'])
				->getMock();

		// It doesn't matter what this function returns,
		// as long as it's not a core_kernel_classes_Resource.
		$mock->method('getCurrentUser_LM')
				->willReturn($this->badRole);

		$this->assertSame($this->invokeMethod($mock, 'handleUserUri'), $this->anonymousRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_NoLockNoParamAnonDefaultRole_ReturnsAnonymousRole() {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
				->setConstructorArgs(['implementation' => new NoLockSystem()])
				->setMethods(['getCurrentUser_LM'])
				->getMock();

		// It doesn't matter what this function returns,
		// as long as it's not a core_kernel_classes_Resource.
		$mock->method('getCurrentUser_LM')
				->willReturn($this->badRole);

		$this->assertSame($this->invokeMethod($mock, 'handleUserUri'), $this->anonymousRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_LockSystemBadRoleParam_ReturnsBadRole() {
		$this->assertSame($this->invokeMethod($this->lm->lockSystem, 'handleUserUri', [$this->badRole]), $this->badRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_OntoLockBadRoleParam_ReturnsBadRole() {
		$this->assertSame($this->invokeMethod($this->lm->ontoLock, 'handleUserUri', [$this->badRole]), $this->badRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::handleUserUri
	 */
	public function test_handleUserUri_NoLockBadRoleParam_ReturnsBadRole() {
		$this->assertSame($this->invokeMethod($this->lm->noLock, 'handleUserUri', [$this->badRole]), $this->badRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLocked
	 */
	public function test_isLocked_NotLockedLockSystem_ReturnsFalse() {
		$this->assertFalse($this->lm->lockSystem->isLocked(new Resource($this->defaultUnlockedBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLocked
	 */
	public function test_isLocked_NotLockedOntoLock_ReturnsFalse() {
		$this->assertFalse($this->lm->ontoLock->isLocked(new Resource($this->defaultUnlockedBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLocked
	 */
	public function test_isLocked_NoLock_ReturnsFalse() {
		$this->assertFalse($this->lm->noLock->isLocked(new Resource($this->defaultLockedLockSystemBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLocked
	 */
	public function test_isLocked_LockedLockSystem_ReturnsTrue() {
		$this->assertTrue($this->lm->lockSystem->isLocked(new Resource($this->defaultLockedLockSystemBatteryUri)));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLocked
	 */
	public function test_isLocked_LockedOntoLock_ReturnsTrue() {
		$this->assertTrue($this->lm->ontoLock->isLocked(new Resource($this->defaultLockedOntoLockBatteryUri)));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::setLock
	 */
	public function test_setLock_LockSystemUnlockedBatteryAnonUser_ReturnsTrue() {
		$battery = new Resource($this->defaultUnlockedBatteryUri);

		$this->invokeMethod(
			$this->lm->lockSystem,
			'setLock',
			[
				$battery,
				$this->anonymousRole
			]
		);

		$this->assertTrue($this->lm->lockSystem->isLocked($battery));
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::setLock
	 */
	public function test_setLock_OntoLockUnlockedBatteryAnonUser_ReturnsTrue() {
		$battery = new Resource($this->defaultUnlockedBatteryUri);

		// Try locking the battery.
		$this->invokeMethod(
			$this->lm->ontoLock,
			'setLock',
			[
				$battery,
				$this->anonymousRole
			]
		);

		// Determine if battery was successfully locked.
		$isLocked = $this->lm->ontoLock->isLocked($battery);

		$this->assertTrue($isLocked);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::releaseLock
	 */
	public function test_releaseLock_LockSystemLockedBattery_ReturnsTrue() {
		$battery = new Resource($this->defaultLockedLockSystemBatteryUri);

		// Try unlocking the battery.
		$this->invokeMethod(
			$this->lm->lockSystem,
			'releaseLock',
			[
				$battery,
				$this->virtualUserRole
			]
		);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->lockSystem->isLocked($battery);

		$this->assertFalse($isLocked);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::releaseLock
	 * @expectedException common_exception_Unauthorized
	 */
	public function test_releaseLock_LockSystemLockedBatteryWrongUser_ThrowsException() {
		$battery = new Resource($this->defaultLockedLockSystemBatteryUri);

		// Try unlocking the battery.
		$this->invokeMethod(
			$this->lm->lockSystem,
			'releaseLock',
			[
				$battery,
				$this->badRole
			]
		);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::releaseLock
	 * @expectedException common_exception_Unauthorized
	 */
	public function test_releaseLock_OntoLockLockedBatteryWrongUser_ThrowsException() {
		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);

		// Try unlocking the battery.
		$this->invokeMethod(
			$this->lm->ontoLock,
			'releaseLock',
			[
				$battery,
				$this->badRole
			]
		);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::releaseLock
	 */
	public function test_releaseLock_OntoLockLockedBattery_ReturnsTrue() {
		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);

		// Try locking the battery.
		$this->invokeMethod(
			$this->lm->ontoLock,
			'releaseLock',
			[
				$battery,
				$this->virtualUserRole
			]
		);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->ontoLock->isLocked($battery);

		$this->assertFalse($isLocked);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::forceReleaseLock
	 */
	public function test_forceReleaseLock_LockSystemLockedBatteryAnonUser_ReturnsTrue() {
		$battery = new Resource($this->defaultLockedLockSystemBatteryUri);

		// Try locking the battery.
		$this->invokeMethod(
			$this->lm->lockSystem,
			'forceReleaseLock',
			[
				$battery,
				$this->virtualUserRole
			]
		);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->lockSystem->isLocked($battery);

		$this->assertFalse($isLocked, 'Force release lock failed.');
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::forceReleaseLock
	 */
	public function test_forceReleaseLock_OntoLockLockedBatteryAnonUser_ReturnsTrue() {
		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);

		// Try locking the battery.
		$this->invokeMethod(
			$this->lm->ontoLock,
			'forceReleaseLock',
			[
				$battery,
				$this->virtualUserRole
			]
		);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->ontoLock->isLocked($battery);

		$this->assertFalse($isLocked, 'Force release lock failed.');
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartLock
	 */
	public function test_smartLock_LockSystem_LockSet() {
		$battery = new Resource($this->defaultUnlockedBatteryUri);
		$lm = new LockManager(new LockSystem());

		$lm->smartLock($battery, $this->anonymousRole);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->lockSystem->isLocked($battery);

		$this->assertTrue($isLocked, 'SmartLock failed.');
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartLock
	 */
	public function test_smartLock_OntoLock_LockSet() {
		$battery = new Resource($this->defaultUnlockedBatteryUri);
		$lm = new LockManager(new OntoLockSystem());

		$lm->smartLock($battery, $this->anonymousRole);

		// Determine if battery was successfully unlocked.
		$isLocked = $this->lm->ontoLock->isLocked($battery);

		$this->assertTrue($isLocked, 'SmartLock failed.');
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartLock
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Can't set lock.
	 */
	public function test_smartLock_LockSystemBadRole_ThrowsException() {
		$battery = new Resource($this->defaultLockedLockSystemBatteryUri);
		$lm = new LockManager(new LockSystem(), $this->anonymousRole . 'x');

		$lm->smartLock($battery, $this->badRole);
	}

	// Weak test: Contains dependency
	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartLock
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Can't set lock.
	 */
	public function test_smartLock_OntoLockBadRole_ThrowsException() {
		$battery = new Resource($this->defaultLockedOntoLockBatteryUri);
		$lm = new LockManager(new OntoLockSystem(), $this->anonymousRole . 'x');

		$lm->smartLock($battery, $this->badRole);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 */
	public function test_smartRelease_LockSystemGoodCommit_ReturnsTrue() {
		$battery = new Resource('x');

		$mock = $this->mock_smartRelease();

		$this->assertTrue($mock->smartRelease($battery));
		return $battery;
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Failed committing resource.
	 */
	public function test_smartRelease_LockSystemBadCommit_ThrowsException($battery) {
		$mock = $this->mock_smartRelease([
			'commitResource_LM' => 'exception'
		]);

		$mock->smartRelease($battery);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 */
	public function test_smartRelease_LockSystemAllowCommitFalse_CallsReleaseLock($battery) {
		$mock = $this->mock_smartRelease();

		$mock->expects($this->once())
			->method('releaseLock')
			->with($this->identicalTo($battery));

		$mock->smartRelease($battery, null, false, false);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 */
	public function test_smartRelease_OntoLockReleased_ReturnsTrue($battery) {
		$mock = $this->mock_smartRelease([
			'resourceIsCommittable' => false
		]);

		$this->assertTrue($mock->smartRelease($battery));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Failed releasing lock.
	 */
	public function test_smartRelease_OntoLockFailedUnlocking_ThrowsException($battery) {
		$mock = $this->mock_smartRelease([
			'resourceIsCommittable' => false,
			'releaseLock'           => false
		]);

		$mock->smartRelease($battery);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 */
	public function test_smartRelease_UnlockedBattery_ReturnsFalse($battery) {
		$mock = $this->mock_smartRelease([
			'isLocked'  => false
		]);

		$this->assertfalse($mock->smartRelease($battery));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 */
	public function test_smartRelease_GoodForceRelease_ReturnsTrue($battery) {
		$mock = $this->mock_smartRelease([
			'isLocked'  => 'exception'
		]);

		$this->assertTrue($mock->smartRelease($battery, null, true));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Can't force-release lock.
	 */
	public function test_smartRelease_FailedForceRelease_ThrowsException($battery) {
		$mock = $this->mock_smartRelease([
			'isLocked'          => 'exception',
			'forceReleaseLock'  => false
		]);

		$mock->smartRelease($battery, null, true);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::smartRelease
	 * @depends test_smartRelease_LockSystemGoodCommit_ReturnsTrue
	 * @expectedException common_Exception
	 * @expectedExceptionMessage Can't release lock.
	 */
	public function test_smartRelease_FailedNoForceRelease_ThrowsException($battery) {
		$mock = $this->mock_smartRelease([
			'isLocked'  => 'exception'
		]);

		$mock->smartRelease($battery, null, false);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 * @expectedException common_Exception
	 * @expectedExceptionMessage String expected. Received integer
	 */
	public function test_implementationIsA_IntegerParam_ThrowsException() {
		$this->invokeMethod($this->lm->lockSystem, 'implementationIsA', [2]);
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_LockSystem_ReturnsTrue() {
		$this->assertTrue($this->invokeMethod($this->lm->lockSystem,
			'implementationIsA',
			['oat\taoWorkspace\model\lockStrategy\LockSystem']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_UnknownLockSystem_ReturnsFalse() {
		$this->assertFalse($this->invokeMethod($this->lm->lockSystem,
			'implementationIsA',
			['Not/A/LockSystem']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_OntoLock_ReturnsTrue() {
		$this->assertTrue($this->invokeMethod($this->lm->ontoLock,
			'implementationIsA',
			['oat\tao\model\lock\implementation\OntoLock']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_UnknownOntoLock_ReturnsFalse() {
		$this->assertFalse($this->invokeMethod($this->lm->ontoLock,
			'implementationIsA',
			['Not/A/OntoLock']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_NoLock_ReturnsTrue() {
		$this->assertTrue($this->invokeMethod($this->lm->noLock,
			'implementationIsA',
			['oat\tao\model\lock\implementation\NoLock']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::implementationIsA
	 */
	public function test_implementationIsA_UnknownNoLock_ReturnsFalse() {
		$this->assertFalse($this->invokeMethod($this->lm->noLock,
			'implementationIsA',
			['Not/A/NoLock']
		));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLockSystem
	 */
	public function test_isLockSystem_LockSystem_ReturnsTrue() {
		$mock = $this->mock_implementationIsA(new LockSystem(), true);
		$this->assertTrue($this->invokeMethod($mock, 'isLockSystem'));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isLockSystem
	 */
	public function test_isLockSystem_NotLockSystem_ReturnsFalse() {
		$mock = $this->mock_implementationIsA(new LockSystem(), false);
		$this->assertFalse($this->invokeMethod($mock, 'isLockSystem'));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isOntoLock
	 */
	public function test_isOntoLock_OntoLock_ReturnsTrue() {
		$mock = $this->mock_implementationIsA(new OntoLockSystem(), true);
		$this->assertTrue($this->invokeMethod($mock, 'isOntoLock'));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isOntoLock
	 */
	public function test_isOntoLock_NotOntoLock_ReturnsFalse() {
		$mock = $this->mock_implementationIsA(new OntoLockSystem(), false);
		$this->assertFalse($this->invokeMethod($mock, 'isOntoLock'));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isNoLock
	 */
	public function test_isNoLock_NoLock_ReturnsTrue() {
		$mock = $this->mock_implementationIsA(new NoLockSystem(), true);
		$this->assertTrue($this->invokeMethod($mock, 'isNoLock'));
	}

	/**
	 * @covers \taoTestBatteries_helpers_LockManager::isNoLock
	 */
	public function test_isNoLock_NotNoLock_ReturnsFalse() {
		$mock = $this->mock_implementationIsA(new NoLockSystem(), false);
		$this->assertFalse($this->invokeMethod($mock, 'isNoLock'));
	}

	/********************************************/
	private function mock_LockManager($params = []) {
		$names = [
			'isLocked',
			'handleUserUri',
			'resourceIsCommittable',
			'commitResource_LM',
			'releaseLock',
			'forceReleaseLock'
		];

		foreach($names as $name) {
			$$name = array_key_exists($name, $params) ? $params[$name] :
				($name === 'handleUserUri' ? 'irrelevant' : true);
		}

		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods($names)
			->getMock();

		foreach($names as $name) {
			if (is_string($$name) && stristr($$name, 'exception')) {
				$mock->method($name)
					->will($this->throwException(new \Exception));
			} else {
				$mock->method($name)
					->will($this->returnValue($$name));
			}
		}

		return $mock;
	}

	private function mock_implementationIsA(Configurable $lockSystem, $returns) {
		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setConstructorArgs(['implementation' => $lockSystem])
			->setMethods(['implementationIsA'])
			->getMock();

		$mock->method('implementationIsA')
			->will($this->returnValue($returns));

		return $mock;
	}

	private function mock_smartRelease($params = []) {
		$names = [
			'getCurrentUser_LM',
			'commitResource_LM',
			'isLocked',
			'handleUserUri',
			'resourceIsCommittable',
			'releaseLock',
			'forceReleaseLock',
			'getOwner'
		];

		foreach($names as $name) {
			switch($name) {
				case 'handleUserUri':
					break;
			}

			// Default value is true, except for 'handleUserUri', which is 'irrelevant'.
			$$name = array_key_exists($name, $params) ? $params[$name] :
				($name === 'handleUserUri' ? 'irrelevant' : true);
		}

		$mock = $this->getMockBuilder('taoTestBatteries_helpers_LockManager')
			->setMethods($names)
			->getMock();

		foreach($names as $name) {
			if (is_string($$name) && stristr($$name, 'exception')) {
				$mock->method($name)
					->will($this->throwException(new \Exception));
			} else {
				$mock->method($name)
					->will($this->returnValue($$name));
			}
		}

		return $mock;
	}
}