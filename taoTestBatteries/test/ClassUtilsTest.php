<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Test TestBatteries extension (TestBatteries class)
 *
 * @author BT
 * @package taoTestBatteries
 */

namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use taoTestBatteries_helpers_Utils as utils;

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

/**
 * Class ClassUtilsTest
 * @package oat\taoTestBatteries\test
 */
class ClassUtilsTest extends BaseTest
{
	protected $imageUrlWithExtension = "http://fineprintnyc.com/images/blog/history-of-logos/google/google-logo.png";
	protected $imageUrlWithoutExtension = "https://sp.yimg.com/ib/th?id=JN.DrBkMR6w%2bnlucry23TZBqQ&pid=15.1&P=0";
	
	protected $arr = [
		"first" => "red apple",
		"second" => "green apple",
		"third" => "yellow apple"
	];

	/**
	 * Tests initialization
	 */
	protected function setUp()
	{
		TaoPhpUnitTestRunner::initTest();
	}
	
	public function test_ArraySearchSubstring_SearchApple_ReturnsFirst()
	{
		$this->assertEquals("first", utils::arraySearchSubstring($this->arr, "apple"));
	}

	public function test_ArraySearchSubstring_SearchGreen_ReturnsSecond()
	{
		$this->assertEquals("second", utils::arraySearchSubstring($this->arr, "green"));
	}
	
	public function test_ArraySearchSubstring_SearchOrange_ReturnsFalse()
	{
		$this->assertEquals(false, utils::arraySearchSubstring($this->arr, "orange"));
	}
	
	public function test_GetMimeType_UrlWithExtension_ReturnsPng()
	{
		$this->assertEquals('image/png', utils::getMimeType($this->imageUrlWithExtension));
	}
	
	public function test_GetMimeType_UrlWithoutExtension_ReturnsJpeg()
	{
		$this->assertContains(utils::getMimeType($this->imageUrlWithoutExtension), ['image/jpeg', 'image/jpg']);
	}
	
	/**
	 * @expectedException \common_Exception
	 * @expectedExceptionMessage Failed opening file:
	 */
	public function test_GetMimeType_InvalidUrl_ThrowsCommonException()
	{
		utils::getMimeType("http://sdkgfalfdjgabdfkjvabdfvbadlfdknasd");
	}
	
	public function test_GetMimeType_LocalFile_ReturnsMimeType()
	{
		$this->assertEquals("image/jpeg", utils::getMimeType($this->localImage));
	}
	
	public function test_GetMimeType_LocalFileUseExtension_ReturnsJpeg()
	{
		$this->assertEquals("image/jpeg", utils::getMimeType($this->localImage, true));
	}
	
	public function test_IsSupportedFiletype_UrlWithoutExtension_Succeeds()
	{
		$this->assertTrue(utils::isSupportedFiletype($this->imageUrlWithoutExtension));
	}
	
	public function test_IsSupportedFiletype_UrlWithoutExtension_StoresFiletypeJpeg()
	{
		$filetype = '';
		utils::isSupportedFiletype($this->imageUrlWithoutExtension, $filetype);
		$this->assertContains($filetype, ['jpg', 'jpeg']);
	}
	
	public function test_IsSupportedFiletype_UrlWithExtension_Succeeds()
	{
		$this->assertTrue(utils::isSupportedFiletype($this->imageUrlWithExtension));
	}
	
	public function test_IsSupportedFiletype_UrlWithExtension_StoresFiletypePng()
	{
		$filetype = '';
		utils::isSupportedFiletype($this->imageUrlWithExtension, $filetype);
		$this->assertEquals('png', $filetype);
	}
	
	public function test_IsDummyAsset_RealResources_Fails()
	{
		$assets = [
			"http://fineprintnyc.com/images/blog/history-of-logos/google/google-logo.png",
			"https://sp.yimg.com/ib/th?id=JN.DrBkMR6w%2bnlucry23TZBqQ&pid=15.1&P=0",
			"0_0_0map.jpg"
		];
		
		$count = count($assets);
				
		self::assessDummyArray($assets);
		$this->assertCount($count, $assets);
	}
	
	public function test_IsDummyAsset_CorrectDummies_Succeeds()
	{
		$assets = [
			"0_0_1_",
			"5_102_0_",
			"10000_5000000_9999_"
		];
				
		self::assessDummyArray($assets);
		$this->assertEmpty($assets);
	}
	
	public function test_IsDeletableBattery_EmptyTestBattery_ReturnsMessage()
	{
		$this->markTestSkipped();
	}
	
	public function test_FormatTestDisplayName_LongNameNoLimitDefined_Shortened()
	{
		$longName = "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456790123456789"; // 109 characters
		$shortName = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345679…";
		
		$this->assertEquals($shortName, utils::formatTestDisplayName($longName));
	}
	
	public function test_FormatTestDisplayName_LongName50LimitDefined_Shortened()
	{
		$longName = "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456790123456789"; // 109 characters
		$shortName = "0123456789012345678901234567890123456789012345678…"; // 50 characters
		
		$this->assertEquals($shortName, utils::formatTestDisplayName($longName, 50));
	}
	
	public function test_FormatTestDisplayName_ShortNameNoLimitDefined_NoChange()
	{
		$longName = "01234567890123456789012345678901234567890123456789"; // 50 characters
		$shortName = "01234567890123456789012345678901234567890123456789";
		
		$this->assertEquals($shortName, utils::formatTestDisplayName($longName));
	}
	
	public function test_FormatTestDisplayName_ShortName100LimitDefined_NoChange()
	{
		$longName = "01234567890123456789012345678901234567890123456789"; // 50 characters
		$shortName = "01234567890123456789012345678901234567890123456789";
		
		$this->assertEquals($shortName, utils::formatTestDisplayName($longName, 100));
	}
	
	public function test_GetTestItems_ValidTestUri_ReturnsTestItems()
	{
		$this->markTestSkipped();
		utils::getTestItems($this->testUri);
	}
	
	public function test_ValidateResponse_EmptyResponse_ReturnsErrorMessage()
	{
		$this->assertEquals("Invalid ADP response", utils::validateResponse([]));
	}

	/**
	 * @return array
	 */
	public function test_ValidateResponse_ValidResponse_ReturnEmptyString()
	{
		$validResponse = [
			'result' => [
				'adpPublisherRequestId' => null,
				'testBatteryFormRevisionPublishedDateTime' => null,
				'revisionId' => $this->revisionId,
				'status' => null
			],
			'meta' => [
				'status' => 'success'
			]
			
		];
		
		$this->assertEmpty(utils::validateResponse($validResponse));
		return $validResponse;
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @param $response
	 */
	public function test_ValidateResponse_MissingResultKey_ReturnsErrorMessage($response)
	{
		unset($response['result']);
		$this->assertEquals("Invalid ADP response", utils::validateResponse($response));
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @param $response
	 */
	public function test_ValidateResponse_MissingRevisionIdKey_ReturnsErrorMessage($response)
	{
		unset($response['result']['revisionId']);
		$this->assertEquals("Invalid ADP response", utils::validateResponse($response));
	}
	
	/**
	 * @expectedException \common_Exception
	 * @expectedExceptionMessage An error occurred making a request to publish this form revision. Please check the log file for error details.
	 */
	public function test_ParseResponse_EmptyResponse_ThrowsCommonException()
	{
		$response = utils::parseResponse([], $this->revisionId);
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @param $adp
	 * @throws \common_Exception
	 */
	public function test_ParseResponse_ValidResponse_ReturnSuccessfulResponse($adp)
	{
		$adpResponse = json_encode($adp);
		$acrResponse = utils::parseResponse($adpResponse, $this->revisionId);
		$this->assertArrayHasKey('success', $acrResponse);
		$this->assertArrayHasKey('testBatteryFormRevisionPublishedDateTime', $acrResponse);
		$this->assertArrayHasKey('errorDescription', $acrResponse);
		$this->assertTrue($acrResponse['success']);
		$this->assertEmpty($acrResponse['errorDescription']);
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @expectedException \common_Exception
	 * @expectedExceptionMessage Unit Test Error
	 * @param $response
	 * @throws \common_Exception
	 */
	public function test_ParseResponse_ContainsErrorCodeAndDescription_ThrowsCommonException($response)
	{
		$response['result']['errorCode'] = 400;
		$response['result']['errorDescription'] = "Unit Test Error";
		
		utils::parseResponse(json_encode($response), $this->revisionId);
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @expectedException \common_Exception
	 * @expectedExceptionMessage Unit Test Error
	 * @param $response
	 * @throws \common_Exception
	 */
	public function test_ParseResponse_ContainsErrorDescription_ThrowsCommonException($response)
	{
		$response['result']['errorDescription'] = "Unit Test Error";
		
		utils::parseResponse(json_encode($response), $this->revisionId);
	}

	/**
	 * @depends test_ValidateResponse_ValidResponse_ReturnEmptyString
	 * @expectedException \common_Exception
	 * @expectedExceptionMessage Incorrect revision number
	 * @param $response
	 * @throws \common_Exception
	 */
	public function test_ParseResponse_WrongRevisionId_ThrowsCommonException($response)
	{
		utils::parseResponse(json_encode($response), $this->revisionId . '0');
	}
	
	/**
	 * @expectedException \common_Exception
	 */
	public function test_ValidateFormTest_NoUri_ThrowsCommonException()
	{
		utils::validateFormTest('');
	}
	
	public function test_ValidateFormTest_ValidUri_ReturnsEmptyString()
	{
		$this->markTestSkipped();
		$this->assertEmpty(utils::validateFormTest($this->testUri));
	}

	/**
	 * @param $assets
	 */
	private static function assessDummyArray(&$assets)
	{
		foreach ($assets as $key => $asset) {
			$assets[$key] = (bool) utils::isDummyAsset($asset);
			
			if ($assets[$key] === true) {
				unset($assets[$key]);
			}
		}
	}
}
