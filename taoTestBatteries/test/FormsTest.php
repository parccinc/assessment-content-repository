<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Test TestBatteries extension (Forms class)
 *
 * @author BT
 * @package taoTestBatteries
 */
namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use \core_kernel_classes_Resource;
use \core_kernel_classes_Class;
use \taoTestBatteries_models_classes_FormsService;
use oat\oatbox\user\LoginService;

// Constants
define("VHOST_PATH", "https://localhost/Tao-Platform");
define("BATTERY_ID", "i1428961154970942");
define("FORM_ID", "i1428961177559243");
define("TEST_ID", "i1428961122137940");

// Don't touch
define("BATTERY_URI", VHOST_PATH . ".rdf#" . BATTERY_ID);
define("FORM_URI", VHOST_PATH . ".rdf#" . FORM_ID);
define("TEST_URI", VHOST_PATH . ".rdf#" . TEST_ID);

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

class FormsTest extends BaseTest
{

	protected $service = null;

	protected $propertyPrefix = 'PROPERTY_FORM_';

	protected $formProperties = array();

	protected $login = "";

	protected $password = "";

	protected $userUri = "";

	protected $hash = "";

	/**
	 * Tests initialization
	 */
	protected function setUp()
	{
		TaoPhpUnitTestRunner::initTest();
		$this->disableCache();
		
		$testUserData = [
						PROPERTY_USER_LOGIN => 'cltest',
						PROPERTY_USER_PASSWORD => 'cltest',
						// PROPERTY_USER_LASTNAME => 'Line11',
						// PROPERTY_USER_FIRSTNAME => 'Command',
						// PROPERTY_USER_MAIL => 'cltest@cltest.com',
						// PROPERTY_USER_DEFLG => \tao_models_classes_LanguageService::singleton()->getLanguageByCode(DEFAULT_LANG)->getUri(),
						// PROPERTY_USER_UILG => \tao_models_classes_LanguageService::singleton()->getLanguageByCode(DEFAULT_LANG)->getUri(),
						PROPERTY_USER_ROLES => [
										INSTANCE_ROLE_GLOBALMANAGER,
										INSTANCE_ROLE_SYSADMIN
						]
		];
		
		$this->userService = \core_kernel_users_Service::singleton();
		
		$this->password = $testUserData[PROPERTY_USER_PASSWORD];
		$this->hash = \core_kernel_users_Service::getPasswordHash()->encrypt($testUserData[PROPERTY_USER_PASSWORD]);
		$testUserData[PROPERTY_USER_PASSWORD] = $this->hash;
		
		$taoUserClass = new \core_kernel_classes_Class(CLASS_TAO_USER);
		// $user = $taoUserClass->createInstanceWithProperties($testUserData);
		// \common_Logger::i('Created user ' . $user->getUri());
		
		$this->login = $testUserData[PROPERTY_USER_LOGIN];
		// $this->userUri = $user->getUri();
		
		$this->service = taoTestBatteries_models_classes_TestBatteriesService::singleton();
		$this->formProperties = $this->getConstants($this->propertyPrefix);
		
		/*
		 * $_POST['uri'] = BATTERY_URI;
		 * $_POST['battery_label'] = "Unit Test Battery";
		 * $_POST['form_label'] = "Unit Test From";
		 * $_POST['test_label'] = "Test Label";
		 * $_POST['test_uri'] = TEST_URI;
		 * $_POST['form_uri'] = FORM_URI;
		 */
		
		$_SERVER['REQUEST_METHOD'] = 'POST';
		
		/*
		 * // Saving Revision Properties
		 * $revisionClass = new core_kernel_classes_Class(CLASS_REVISION);
		 * $revision = $revisionClass->createInstanceWithProperties(
		 * array(
		 * RDFS_LABEL => $revisionNumber,
		 * PROPERTY_REVISION_NUMBER => $revisionNumber,
		 * PROPERTY_REVISION_COMPILATION_TIME => date("Y-m-d H:i:s"),
		 * PROPERTY_REVISION_PUBLISHING_TIME => 'False', // To be added with actual compilation
		 * PROPERTY_REVISION_RUNTIME => 'False', // To be added with actual compilation
		 * PROPERTY_REVISION_PUBLISHING_STATUS => PROPERTY_REVISION_PUBLISHING_STATUS_UNPUBLISHED,
		 * PROPERTY_REVISION_TEST => $testUri,
		 * PROPERTY_REVISION_COMPILED_BY => common_session_SessionManager::getSession()->getUser()->getIdentifier(),
		 * PROPERTY_REVISION_FORM => $formUri
		 * )
		 * );
		 */
		
		/*
		 * $batteryLabel = $compile->getRequestParameter('battery_label');
		 * $formLabel = $compile->getRequestParameter('form_label');
		 * $testUri = $compile->getRequestParameter('test_uri');
		 * $formUri = $compile->getRequestParameter('form_uri');
		 * $testLabel = $compile->getRequestParameter('test_label');
		 * $revisionNumber = count(taoTestBatteries_helpers_Utils::getRevisionsByFormUri($formUri)) + 1;
		 */
		// $revisionNumber = $this->generateTestPackage($testUri);
	}

	/**
	 * Test service implementation.
	 *
	 * @return void
	 */
	public function testService()
	{
		$this->markTestSkipped();
		$this->assertInstanceOf('tao_models_classes_Service', $this->service);
		$this->assertInstanceOf('taoTestBatteries_models_classes_TestBatteriesService', $this->service);
	}

	/**
	 * Test class creation.
	 *
	 * @return \core_kernel_classes_Class
	 */
	public function testFormClassCreate()
	{
		$this->markTestSkipped();
		$this->assertTrue(defined('CLASS_FORM'));
		$formClass = new core_kernel_classes_Class(CLASS_FORM);
		$this->assertInstanceOf('core_kernel_classes_Class', $formClass);
		$this->assertEquals(CLASS_FORM, $formClass->getUri());
		
		return $formClass;
	}

	/**
	 * @depends testFormClassCreate
	 *
	 * @param
	 *        	$formClass
	 * @return \core_kernel_classes_Class
	 */
	public function testSubClassCreate($formClass)
	{
		$this->markTestSkipped();
		$subFormClassLabel = 'subForm class';
		$subFormClass = $this->service->createSubClass($formClass, $subFormClassLabel);
		$this->assertInstanceOf('core_kernel_classes_Class', $subFormClass);
		$this->assertEquals($subFormClassLabel, $subFormClass->getLabel());
		
		return $subFormClass;
	}

	/**
	 * @depends testSubClassCreate
	 *
	 * @param
	 *        	$class
	 * @return void
	 */
	public function testDeleteClass($class)
	{
		$this->markTestSkipped();
		// Get the parent class.
		$parents = $class->getParentClasses();
		$this->assertCount(1, $parents);
		$parent = array_values($parents)[0];
		
		// Delete the class.
		$this->assertTrue($class->isSubClassOf($parent));
		$this->assertTrue($class->delete());
		$this->assertTrue(! $class->isSubClassOf($parent));
	}

	/**
	 * Test form properties
	 * @depends testFormClassCreate
	 *
	 * @param
	 *        	$formClass
	 * @return void
	 */
	public function testProperties(core_kernel_classes_Class $formClass)
	{
		$this->markTestSkipped();
		$properties = $formClass->getProperties(false);
		$propertiesValues = array_values($this->formProperties);
		
		foreach ($properties as $key => $data) {
			$this->assertContains($key, $propertiesValues);
		}
	}

	/**
	 * @depends testFormClassCreate
	 *
	 * @param
	 *        	$formClass
	 * @return \core_kernel_classes_Resource
	 */
	public function testGetFormTest(core_kernel_classes_Class $formClass)
	{
		$this->markTestSkipped();
		$test = taoTestBatteries_helpers_Utils::getFormTest(FORM_URI);
		$this->assertInstanceOf('core_kernel_classes_Resource', $test);
		$this->assertTrue($test->exists());
		$this->assertTrue(taoTestBatteries_helpers_Utils::testExists($test->getUri()));
		
		return $test;
	}

	/**
	 * @depends testGetFormTest
	 *
	 * @param
	 *        	$testInst
	 */
	public function testGetTestItems(core_kernel_classes_Resource $testInst)
	{
		$this->markTestSkipped();
		try {
			taoTestBatteries_helpers_Utils::isValidItems($testInst->getUri());
		} catch (Exception $ex) {
			$this->assertTrue(false);
		}
		$items = taoTestBatteries_helpers_Utils::getTestItems($testInst->getUri());
		
		$this->assertGreaterThan(0, count($items));
		foreach ($items as $item) {
			$this->assertInstanceOf('core_kernel_classes_Resource', $item);
		}
		
		return $items;
	}

	/**
	 *
	 * @return core_kernel_classes_Resource
	 */
	public function testCreateUser()
	{
		$this->markTestSkipped();
		$newUser = new core_kernel_classes_Resource($this->userUri);
		$this->assertInstanceOf('core_kernel_classes_Resource', $newUser);
		$this->assertTrue($newUser->exists());
		return $newUser;
	}

	/**
	 * Test form compilation
	 *
	 * @return void
	 */
	public function testCompileForm()
	{
		$success = LoginService::login($this->login, $this->password);
		$this->assertTrue($success);
		
		$this->startSession();
		$this->assertNotEmpty($_SESSION['ClearFw']);
		
		include_once ("../actions/class.Forms.php");
		$compile = new taoTestBatteries_actions_Forms();
		
		// $_COOKIE[session_name()] = session_id();
		
		// Try creating a user first and then using that user to login with.
		$response = $this->curl("https://localhost/taoTestBatteries/Forms/compileFormNow", CURLOPT_POST, "data", [
						"login: " . $this->login,
						"password: " . $this->password
		], [
						"uri" => BATTERY_URI,
						"test_uri" => TEST_URI,
						"form_uri" => FORM_URI
		], session_name() . "=" . session_id());
		
		common_Logger::i("response = $response");
		
		// $txtResponse = json_decode($response, true);
		
		if (is_array($response)) {
			print_r($response);
			$this->assertEquals(200, $response);
		} else {
			echo $response;
			$this->assertNotEquals(200, $response);
		}
		/*
		 * common_Logger::i('New version created! Uri:' . $revision->getUri() . ' Label:' . $revision->getLabel() . ' Location:' . $compile->getRevisionDirectory()->getPath());
		 *
		 * $test = new core_kernel_classes_Resource($testUri);
		 * $testLabel = $test->getLabel();
		 *
		 * common_Logger::i('New version created! Uri:' . $revision->getUri() . ' Label:' . $revision->getLabel());
		 *
		 * //TODO: Clean up the data and filesystem in the tearDown function
		 *
		 * $this->assertInstanceOf('core_kernel_classes_Resource', $revision);
		 */
	}
	
	// TODO: Implement tearDown function
	
	/**
	 * Function that run after unit test has run, so you can clean up any added data here
	 */
	public function tearDown()
	{
		// $user = new core_kernel_classes_Resource($this->userUri);
		// $user->delete();
		$this->restoreCache();
	}

	/**
	 * Get Form properties as array(const => value)
	 *
	 * @param
	 *        	$prefix
	 * @return array
	 */
	public function getConstants($prefix)
	{
		$dump = array();
		
		foreach (get_defined_constants() as $key => $value) {
			if (substr($key, 0, strlen($prefix)) == $prefix) {
				$dump[$key] = $value;
			}
		}
		return $dump;
	}

	protected function curl($url, $method = CURLOPT_HTTPGET, $returnType = "data", $curlopt_httpheaders = array(), $postFields = [], $cookie = null)
	{
		$process = curl_init($url);
		if ($method != "DELETE") {
			curl_setopt($process, $method, 1);
		} else {
			curl_setopt($process, CURLOPT_CUSTOMREQUEST, "DELETE");
		}
		
		curl_setopt($process, CURLOPT_USERPWD, $this->login . ":" . $this->password);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($process, CURLOPT_SSL_VERIFYPEER, VERIFY_SSL_CERTIFICATE);
		curl_setopt($process, CURLOPT_SSL_VERIFYHOST, VERIFY_SSL_CERTIFICATE);
		curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
		
		if (! is_null($cookie)) {
			curl_setopt($process, CURLOPT_COOKIE, $cookie);
		}
		
		$headers = array_merge(array(
						"Accept: application/json"
		), $curlopt_httpheaders);
		
		curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
		
		if ($method == CURLOPT_POST) {
			curl_setopt($process, CURLOPT_POSTFIELDS, $postFields);
		}
		// curl_setopt($process,CURLOPT_HTTPHEADER,$curlopt_httpheaders);
		
		$data = curl_exec($process);
		$data = ($returnType === "data") ? $data : curl_getinfo($process, $returnType);
		
		curl_close($process);
		return $data;
	}

	protected function startSession()
	{
		define("PHP_SESSION_KEY", "common_session_Session");
		$user = LoginService::authenticate($this->login, $this->password);
		$session = new common_session_DefaultSession($user);
		
		// do not start session in cli mode (testcase script)
		if ($session instanceof common_session_StatefulSession) {
			// start session if not yet started
			if (session_id() === '') {
				session_name(GENERIS_SESSION_NAME);
				session_start();
			} else {
				// prevent session fixation.
				session_regenerate_id();
			}
			
			PHPSession::singleton()->setAttribute(PHP_SESSION_KEY, $session);
		}
		return true;
	}
}
