<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

return [
	'anonymousRole'								=> 'http://www.tao.lu/Ontologies/generis.rdf#AnonymousRole',
	'badRole'									=> 'http://www.tao.lu/Ontologies/generis.rdf#BadRole',
	'virtualUserRole'							=> 'http://parcc.edu/PARCC.rdfvirtualTestUser',
	'defaultUnlockedBatteryUri'					=> 'https://localhost/TAO.rdf#i14479580142660222',
	'defaultLockedLockSystemBatteryUri'			=> 'https://localhost/TAO.rdf#i14479603982151227',
	'defaultLockedLockSystemBatteryWorkCopyUri'	=> 'https://localhost/TAO.rdf#i14479604382825229',
	'defaultLockedOntoLockBatteryUri'			=> 'https://localhost/TAO.rdf#i14480375386931278'
];
