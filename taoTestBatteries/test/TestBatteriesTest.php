<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use \core_kernel_classes_Class;
use \taoTestBatteries_models_classes_TestBatteriesService;

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

/**
 * Test TestBatteries extension (TestBatteries class)
 *
 * @author BT
 * @package taoTestBatteries
 */
class TestBatteriesTest extends BaseTest
{

	protected $service = null;
	protected $propertyPrefix = 'PROPERTY_TEST_BATTERY_';
	protected $batteryProperties = [];

	/**
	 * Tests initialization
	 */
	protected function setUp()
	{
		TaoPhpUnitTestRunner::initTest();

		$this->service = taoTestBatteries_models_classes_TestBatteriesService::singleton();
		$this->batteryProperties = $this->getConstants($this->propertyPrefix);
	}

	/**
	 * Test class creation
	 * @return \core_kernel_classes_Class
	 */
	public function testClassCreate()
	{
		$this->assertTrue(defined('CLASS_TEST_BATTERY'));
		$batteryClass = $this->service->getRootclass();
		$this->assertInstanceOf('core_kernel_classes_Class', $batteryClass);
		$this->assertEquals(CLASS_TEST_BATTERY, $batteryClass->getUri());

		return $batteryClass;
	}

	/**
	 * @depends testClassCreate
	 * @param $batteryClass
	 * @return \core_kernel_classes_Class
	 */
	public function testSubClassCreate($batteryClass)
	{
		$subBatteryClassLabel = 'subBattery class';
		$subBatteryClass = $this->service->createSubClass($batteryClass, $subBatteryClassLabel);
		$this->assertInstanceOf('core_kernel_classes_Class', $subBatteryClass);
		$this->assertEquals($subBatteryClassLabel, $subBatteryClass->getLabel());

		return $subBatteryClass;
	}

	/**
	 * @depends testSubClassCreate
	 * @param $class
	 * @return void
	 */
	public function testDeleteClass($class)
	{
		$this->markTestSkipped();
		$this->assertTrue($this->service->deleteSubClass($class));
	}

	/**
	 * Get testBattery properties as array(const => value)
	 * @param $prefix
	 * @return array
	 */
	public function getConstants($prefix)
	{
		$dump = [];

		foreach (get_defined_constants() as $key => $value) {
			if (substr($key, 0, strlen($prefix)) == $prefix) {
				$dump[$key] = $value;
			}
		}

		return $dump;
	}

	/**
	 * Test service implementation
	 * @return void
	 */
	public function testService()
	{
		$this->assertInstanceOf('tao_models_classes_Service', $this->service);
		$this->assertInstanceOf('taoTestBatteries_models_classes_TestBatteriesService', $this->service);
	}

	/**
	 * Test battery properties
	 * @depends testClassCreate
	 * @param $batteryClass
	 * @return void
	 */
	public function testProperties(core_kernel_classes_Class $batteryClass)
	{
		$class = new core_kernel_classes_Class($batteryClass->getUri());
		$properties = $class->getProperties(false);
		$propertiesValues = array_values($this->batteryProperties);

		foreach ($properties as $key => $data) {
			$this->assertContains($key, $propertiesValues);
		}
	}
}
