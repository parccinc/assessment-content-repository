<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

namespace oat\taoTestBatteries\test;

require_once dirname(__FILE__) . '/../includes/raw_start.php';

use oat\tao\test\TaoPhpUnitTestRunner;

/**
 * Class BaseTest
 * @package oat\taoTestBatteries\test
 */
class BaseTest extends TaoPhpUnitTestRunner
{

	/**
	 *
	 */
	public function __construct()
	{
		// Get config options from config.php.
		$config = require("config.php");
		
		// Loop through config parameters.
		foreach ($config as $property => $value) {
			// Store config parameters into test class object.
			$this->$property = $value;
		}
	}

	/**
	 * @param $object
	 * @param $methodName
	 * @param array $parameters
	 * @return mixed
	 */
	protected function invokeMethod(&$object, $methodName, array $parameters = [])
	{
		$reflection = new \ReflectionClass(get_class($object));
		$method = $reflection->getMethod($methodName);
		$method->setAccessible(true);

		return $method->invokeArgs($object, $parameters);
	}
}
