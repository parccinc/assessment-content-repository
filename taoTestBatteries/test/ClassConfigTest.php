<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Test TestBatteries extension (TestBatteries class)
 *
 * @author BT
 * @package taoTestBatteries
 */

namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use taoTestBatteries_helpers_Config as Config;

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

/**
 * Class ClassConfigTest
 * @package oat\taoTestBatteries\test
 */
class ClassConfigTest extends BaseTest
{

	/**
	 * Tests initialization
	 */
	protected function setUp()
	{
		TaoPhpUnitTestRunner::initTest();
	}
	
	public function test_GetDefaultFileSource_Successful()
	{
		$this->markTestSkipped();
		$this->assertTrue(Config::getDefaultFileSource());
	}
}
