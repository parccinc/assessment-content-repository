<?php
/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradeable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright © 2015 (original work) Breakthrough Technologies, LLC;
 */

/**
 * Test TestBatteries extension (TestBatteries class)
 *
 * @author BT
 * @package taoTestBatteries
 */

namespace oat\taoTestBatteries\test;

use oat\tao\test\TaoPhpUnitTestRunner;
use taoTestBatteries_helpers_Compilation as Comp;

include_once dirname(__FILE__) . '/../includes/raw_start.php';
include_once 'BaseTest.php';

/**
 * Class ClassCompilationTest
 * @package oat\taoTestBatteries\test
 */
class ClassCompilationTest extends BaseTest
{
	protected $dataUrl = "data:image\/jpeg;base64,\/9j\/4AAQSkZJRgABAQAAAQABAAD\/2wCEAAkGBxQSEBUUEhQVFhUXGBYYFxUYGBcXGRUVFxgaGRgZFhUYHSggGBwnHBwXITEiJSkrLi4vFyA0ODYsOCgtLisBCgoKDg0OGxAQGywkHyQsLCwsLDQsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLP\/AABEIALYBFgMBIgACEQEDEQH\/xAAcAAABBQEBAQAAAAAAAAAAAAADAAEEBQYCBwj\/xAA9EAABAgUCAwYDBQcEAwEAAAABAhEAAxIhMQRBBVFhBhMicYGRMqHwFEKxwdEHIzNSYnLhFYKS8VNjokP\/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX\/xAAnEQACAgICAgMAAAcAAAAAAAAAAQIREiEDMRNBIlFhBBQyQnGh8P\/aAAwDAQACEQMRAD8A0lMKmC0w9Me0eYBph6YLTCphWAKmHogtMPTAAGmFTB6YVMFgBphUx1PLB3pulyz2cBunnBzLBhWOiNTCpgxRCph2IDTDUwemFTBYAKYVMHphUwWACmFTB6YamCwA0wqYPTCogsCPTCpg9ENRBYAKYVMGphqYLADTCpg1MNTBYUBphqYNTCpgsKA0w1MGphmgsANMKmDUw1MFgBphqYPTDUwrABTCg1MPBYE2mFTBaYemJsYGmFTBqYVMOwBUwqYNTCpgsANMKmD0QqYVgUvaLU93JyxUtAGedRxfCTFnLIUARggEeRvGZ7XSK5gII\/dhJI6kl38gQfWLbsvPVM0\/jABQpSAxJdKWpJ5FtoxjyXyNGrhUEyyphUwYJjoJjazKiPTCpiT3UN3UFhRHphqYkd3CogsKI9MKmJFENTBYUAphwiDNCaEAGiHMuCFMNTAMCURzTB6YamGIDTDUwemGogCgFMIpg9EMUwrCiOUw1MHphUwWACmGpg9MNTBYAaYamD0QqILCgFMKC0woLET6YVEHphUxORpiAphUwemFTDsVAaYVMGphENmDIKBUwqYMEwqYMgox2j08jUzNbNAkqBKUpnKluUKSgJVcsfCeTXBvFnwKT3ZUgzUTKmUKZapbc3ClKf7seaabgs9Ou1OnluqlSyJXeTEoUhRcKUpKkswKCbucc49F7PcBmSZtS0hKQlQS06bMdyAHTMURhz7R50JNcx2yinxF\/THEpQU7NYkHoRz5ZiTTFJ2a4mJ51AH3JywP7MJPqUmO9z3RxqJa0w7QWmFTDsVAaYVMGphUwWFAgmOxLeOqYamFY0cKlxyUwWmFRBYAaYamDUw1MFioDTCpgtMKmCwoDTDUwamFTBYUAphqYPRDUwZBQCmGpg5TDUwrCgFMKmDUw1MFhQFoamD0w1EFhQCmHg3dmFCyCiwaGaHhPGWRvQzQqYd4d4eYsTmmIPHFU6Wcb2lzGIyDSWIbrFhGQ\/aXxXutCUJUQuZMRLcZAH7xX\/yG\/wB0Jz0CiXPB+IJ7iV3syWmZQitNQFKmDhlMc8xEyZrpSUlRmIpAJJqBYC5xHlOt1XfITMLXSHYdGPzEQ0gEP88et9oyXO6NXwo1vC+ISft2omBVpq00qpV4khCWa3N41HCuLyTLlSwDKJSmmWoEEEuKOThr3948x+IA9GPmP8N7Rw7dOuGjFN5ZGrXxxPWuOawyNLOmhnly1qD3FSQWcb3aPPf2W6lSZ3dkjxoL8ypBdN\/IqeKbiPHJp0k2QFeELllSXH7xCh4SDlnAt\/RFrwnhuslSxqU6VEoI8QVX4kgkv4OTKYg7CLlzbTM48Wmj1UJjruzGB0HazUKC6ilwxDJGLhTj\/jBR2tn\/ANPqnp5xp\/MInwM3QlR0JEYrT9rpxJBCcFvCc\/8AKOUdr526UdfCbD\/lC84eFm0pHOGKYqezix3LXsS5LuSbkl96qvaLasRceS1ZDhTo5phUx1WITxWYsTgpjloITDQ8xUDaGH4RW9puOp0UjvVJKnNKUjdRBIqOwsYwfZPj+oTMKwidOQqykJQopCUggd3sCkIPnSX5xL5EilBs9OphUwpa6kgsQ4BYhiH2I2PSOoeZNAymGaCwoMx4gmhqYLChZhiBphUwWGMGYYg6YVEEaE0LMeIKiFBCIULMMRVw4VFDK7RSjkLT5pP5PFhpdaiZ8CgpssbjzGREtNGlon1QqojiZdo6qgAM8eE9peMq1M+Yanl94tSBsKglIP8AwQj3Me4hUfPWt1QXMWpgmpSiwDBLlgGbkBEvsPRb8O1Y7goUoBjZyBZXU72JiapVTFCkqD3U7hgL3vvb35RRcK1kuWVmYEq\/dqCaggistSWmEA3HnGj43xGVOUvxIRUmlqpZbwBP3VEHD25xlLujWHVjz1FQllJcH4ibsliQwvuW9YHPBBDMAC5PJsN6wQK00vTSAlKEzAgCZMZKe9UwvVV49z6xC1eolzJakGYkOCHqFuRd9jeJW0U9FRPnr\/1FJpBSpUohRFThP3utyoECPY9PMmKkzZSpilES2H7sBMwlawlI8LpXSAVPZyinCqvG53DgaaJyCoLt403SpnZjYuAet49J7PcNny0LkapDaOQlU4z3IVMUicVJBJLUKSlTp5MXu0E3oI9kJGnmS5VRYKKVVZASQSwL+QMRJc5SwopI+B72ALOSb2AufSJfabiumny5yETkpExJDlixOXD3yYruJHTDTGVKKdOVoIKqUIdTXIYirk8Qi2Fka11VoUlTFNTHwh\/i9NvWONTqO6UlC1XUujnYOSRucAW\/mfYwfU8SkqSAoy5ZpSAAUAKDFz4VG+IrdbMkzZspRnIFExCy5FKmaoFyBe+T7CGhM2HAOLATZaAQa5hBFwQKV02P9R+UbR48q1WokDXSpsmeggJDuuWEoKSSB4LC5ffePR5fEpagCFhjf36xtxO0Y8mmTnhPENWuli5WkeuIDouLypqAtCwQfP52tGtMztFi8J4gniCOfyMdo1qD94etvxh0xWij\/aLp69CpTt3akLHW9Df\/AF8oj9jk65UtJM1CQWYGp2ADuAggfD81f0tnO3XaOYZk\/SlSO6dFwDVYJX8Tsb9I13Dloksn7YElC5csoKDeZPQFS0HxCoUqcHAp6Rhys240aQqfMNEHRakLSVJmiYmpSHCChlIUUqFyXvvjcO8Sa40TtaIaphYZ4HXDVQxUEeGqgdUNVAAWqGqgRMcvCANXDVQJ4Z4BBa4UCeFDAxQng7t9bWjmWvJCh5sX\/wCoNL4Cpx8I9\/fOY7kdnbsVgHLB382eGlRGRFBTl7ncZb1hxMIFpix5Eti1otpnDJdLKWn+4m+3W8czdDJKW70X3qG\/TlBbXsHRUHVK2mLZruol\/wBI8+4ulMucsAAJckP7tHoGpVp0KKaZyms4CWPUPtHnPFlpVqJrJLFagKmduRLQSkqHFbD8IqXNlolrMszChBWgl01rSDcXj1rtNwKVp9NLmL0UvWakmiYoBYrUmUtXeFINnKAPNY5x5H2ZFOslABv38oeoWDH0Tx\/iydJIM1YUQCfhSVGyVKPhBGyTvHPydo6eNaZj+O8Ak6eVJCdIrUpWpI7tc2YpMgGlPgpwBXk4SgxO4r2N0coKVK0UtaqQySZhHwzVCwO5SkesaPjXGBpkyiUqJmrSgAJqurncN5\/KFxrXnThS0pqNIt\/aicvmM0N6xnbNGkYziPZ+TJ1NMrQIoPi75lHu1JklYIewFSQPNUalUpatNOBSlaFSglMsgkqV4gupsggoAA5GAcZ7QstWnKFupBdTDuwTKMy6nfZvaB8J10wT11WkBCaCUkCtwFHvDY77\/hAwRT9pOy8qWuUnT8PlzUrqExTTHlhrHwH09RAu0fZREsypcrRnUoUVCZMmLnKVKTTlKkqDCw9feNUnj6O\/my6kBEtIKVlaWWqlJYe5HpFLxntiE8N71NImL7xBRWgql2mBK1AZDpTy+IQJsHRU8b4fppEzToXKlTgru0rXOV4kJUplWQUiwvcebw2p0ug+2S5Wn0+kXLNFUwKLpJWQqkhTWTSel4gytPIXpNOhMiUqZORqK533pJTaWSxsS9nzQW5wXg3ApEvTTZk6UiZMSmYEmZMUUKWmctCSJTgCyQbFyOWYa6F7LTiPAdJI4jp6JSQFSlPLZSgaXCVUsSrk+zCK9fE0uQSU0s5sPiNvKJWmk6PvNIoSdOlSpRVMlhSSlKykGlQLuxcObhopOLa4rnOmSuakUJAly1TPFKr8SqXJusvZsdIfHOmyOWFpAeP6pS5RSkzWVlrCkXUHxh7HOIuNPwv7PJSUImIKrkTJiV1AMAWQAEm\/4R3wHiyEoWJ+mnIUWFU2WpCVIJAUE1AOpnJF3th4bXdpUSgAnxJQohAVlQIACVm\/w3L5LJB5xt5G5pmeCUGgf21bs34frBJk2aA7W8v1EcartMp0eBCPhUX8RUMkJOANtyIfXdqkrQpNBZns5Nr2Au8a+Yx8bMj2qSFTQoi5TfF2cA+34CPUOG8BlTdOhS0kqXK0yiph8aPEkgtlgEnp5x5Z2i1KFrlqlvSUOFEEVAmxyY9n4K\/2XTs38KU75buxj1b0eOfllezp4o1oyHaab3epWgKpTYsFUs4fl1isOrH\/AJFD\/cXvE7tYQNWssh\/D8QH8gwSDGdnkKOJY\/tIHyjSHKqSMZx+TLtPEWF5hw\/iPPzeGHFAcTVHyf8hGd+xlnKk\/iw5kiBzNMtzv6jF2PyMXmvsin9Go+2nZUzz8UD\/1f\/2EfXlGWSlQIFJcvZsgM7A5yPeJmmlA\/wAQLAGALflDckvYUaAcYOyn939oY8d\/qb3\/ACioOmQoChKhcO5Jd+kCXwtYOFeyvzEC5EwaND\/rJs0zPUwGbxyYn74PNqj+EVcjgk1XwpUzZIp9LwjwicGBlLL7+Ij5BhDUhUWKe0SzuR6E\/mIURJXBtTsFgf3EfiIUGQUSF6hZd1KL5uYB3Qd2Djdr+7QHTa8KdKVgqFwPEKhbcg7dYeatZUkpsHdQLH0fcHyDNHnNu9s6nxJe0SXbb5f4hhNDsyXZ9sRUDh807pwbGpT4w+D7C0Hk6aclJCmU5AcO6Uh3Z2viCl9kUWRms3gz1QPkRGD4wofaJpAZ1HcbDy\/CNr9mBQEqUp3uUml8ZDcnv5ecYDipaatiaallO5Zy1+cONeimkumXuh0iZnEZNN6lSCXSzlKAtQbq3rmPZuOytNIAXMTLCVHuwlUvvE1lyDSA7sCM7x5D2HUlXEtNS5uPizUmUqr0sY9k7QcTkSAn7QHSVgJsVMq7EADOYq9I1j7YPV6TT9wJ\/cy6UIM4pCEOoCWVU3TFeNfKEhU4aWW4KkN3aP8A8u\/USxIZxLIAfcRpdLqZZ06lAOjukLCW+4Q7Uno1op5PGJRkmcqQQkj+GlKSXSZxKhVTkJOQDe8IogcXmyjMOn+zpZclSyqhLJZJU1XmAMbxPlTDLlTAJI7qXJC5YSACZnjqSlOAwCCLfeMB4pxhInnTCUp1SFTO8pSyQyvDVU72Zma+YsuFqmVkFI7ru0FKnuVkqrSRyACC\/wDUYXofsF2f1ZnaczFIUCKiErYKYJBvSAPYYaMn+0DikxfBgooKe8nJStO6UJWqkq5eJKPcRqFccKeKy9JT4FSVrC7+JfidLN90J5\/fin\/aFqFr4MtS5ZQpXc1Ic+D96jNr7e8EXsT6ZhtdwjWLk6edp5y0oKUSimVMWhYZc1VawlgUh1M53jTfs6Gsl6efO1C5ylJFMuQtZIGFFbEkVEnPnzik7AcdnDVIlpdUubShiCyEpUpzbB8RO\/W2PQ5Ou1L6ppXwWkWLzPEQS+Dak++zQ2yY12Zrt1JTMRI1MyaJcwSiVSg5UrCgAQQUAGpyfKKr9k+ulrWtMsklPeEkgiy1IKM3JYEHy6xG4YE63R63UzpRmTSkKHdlSSFTVLCmS7KAABZT2THH7G5UvvdS3\/rCS5SSDWWYHo7dIdaYXtGz49oPt2llLTMUhJVKP8KslM9SZYqTWmlqgTc2Bjzjj\/DlSpqpaVmZ3aK2ooINSwoUVqfwoChf72I9f06EzZSV0ThUJZoTNmoUkTFJSqopU\/hCiSP6Y8s4lKUriGt\/iS1JUoJdalLpQFBHiWSTUkJNz94CCDYTRO4F2ZmaoIT9oCCZSJl5BUEhYBCQrvg5DgG0Fldkjc95OtqO4tIpxfvR+9Pg6xuuzXDgrTSCtc4lUtJUnvpwKSQ5cIWBvyH6S5uiSBeUtxMCPFOml5dQBmXVhnLdIHNgoo8\/7QdhlDu5chC5hCJh7wUpdapy1fCpQG\/4RfcV4+NGjSSlJNYly1LS7UpCKf8AdcH26xdT5MuQoT5d0oSQtCVlRYzHJShi6vCL1CwxHivGOLK1OoXOWokrNkvZKRYAMcDl1iJydCk8ejXdseKoGpIUhCkqEtSSXKmUgNbHOKFU6VUASANyEpDWPSOtPwuZrrpMt0Jp8SqTSMKblf6tFVq5CUpYrlm3NXu7Q+OWrsykr2dTNSgFKgK0v4kl0lhcMU\/XSD8C40mVqlTHFBdKkguw+7fn+ptFGiedvT\/Ec6icQA7B+txiE+VsadGy492lkTQUuXSpJlrDsq3iBcAixUPYxDka+WglaQot\/OxtvZuUZTSo\/eYB3PipwMAnBPlF4jWBMpyEqLEWCSxINy9\/UQ85ZCrRd8M4lNWFrTqESwylmWAip2ASlIbLA+ZHWFL1+oBNMycSNiFEC29WMxa8ClJ\/0ibSEgqSlyQ9TrSp+oct5xQocLSkJlEk3ZIBZjYn6xFrlexzilRI1PHNUlQBmkWsbNf+akbN9PEid2jnBVtQmliVK7vBYY8N8fMxwjSpURXKkvzv8meOpmgR4aRKBfxOgqBHIPt5iDzk4DL40VJAOsJ3HhMttrqSC\/lCgkyRpksSmUBhyGvyBIhRHmf7\/oMP8FJOVLlahAlbOD4i6ypgws7jPvGjKR\/MPl+sYedM\/eglrk3O7dWtuzxeSeIIpR4sliCzgYYsABtHNO9Mot50xg4ZRta3OKXjPFCkKCNlJALcnJb2ERJ3Fh3rpuA7dfP5xQzdYokqY3L4wf06wQi2Sa6TxxpJUtwoMCMu9nHSMXPUXCmGc\/jV74iTJmVIYFlB6uVPJV+d7xGYrfJ54uRt7fhG\/H8bGan9npP+oSClJUQVeENihQJckBgC9+W8e18X4eZvh7sEhRIJuxD\/AAvg4L4zHh3YMn7WjKbEZAYHw3fOdi8eycJ7UJXojNKqFAzGClAkkOp2ALBtmtFPZrDoqtfxNppSkJDywAkuEil0qCHv0DDAHOLrRdpJX2UanuVBKqUiWlKHBrmJfLZHPcRh+z\/G0r109c0JIKFUS1OACkS1BhcKICTbei20bzgep8KSsIRL7tSiGAAKFZsbBiT1d4h2hxOeJ9pUJWmQJSnmygsKZNICwWChuYtNBPW6h3YoTLSpK3DqWaqktswCL71HlDcW18pEshS0gqSoJBIBLchvFBx3tTJ00pCZn\/694jLYlk5NgSqlP+6CyzBTO0618XTqVpFKXADm3hNNyzvY7WtHXaDthqdRwpKJ9HeTFJqYFJYCu6dhdIwcXjCyZhUoqNg5IZk3c\/y2f9IbXa3vJl1lSsOX3zcm+WgvZhkaPsnxJcmfpaSgGsAuEWlrWSolRJpuM2Uw6R6JxvtjPTqtTJSZVHdzPs6gtPjV3SKavFnvSQMWJzmPD1EpWC7F3sfLPV94u9ZwqelSlLnldAQa65iiKgCEk5SfERdg4I3EUNSNJ2CkoPD9cqZJQtclCCygHFphWQSCxAc\/7Wig7Fyx9oALAVJPxUuWIZ6k7Vb7e5OBa06fSalKVKImIUFO\/icU3u++YidmJiQpJJUBWgkizgWbLOXPoIbl2F9HuWg4KhSEqVJ\/eESiuXMmKmpQ6kCYAVEgqCamIyR1jzbWcLJ4jrJbJQsKNAlmkJDKVLYuG8NL9XjbJ1swpHx4BpSsKJNgEm+He4JFt3jBz9UZfFdSpKlglKVG5BBKUkpJRtte7C7nLizSR6dwDgCFaaSZiQomWkLSs1gFvF8RLl35\/rLR2bQBbT6YHvP\/ABj+DV5fHT6PFXwLWrOmSVzCo0Em7jFQviw33jFT+Nrmqpmrql1EWFjfwknLtb0MS2NG77S6\/T6eUZC5JCFpWSJaUAMXSbEjn52jwOcqQJp7lSu7B8JILhJO4e\/LO0b\/AImsLlGUpyQ4SS5ZAA+95FgTmPJkKKTYEFruD4SefKBfK0Z8hcTOJ+IpSosxHnzcY2EQVz35Bxf62isqg0hTnqducX40jMsEB0ZZh8oM9eMbEwGWpku125v0JI3jvTqdy+wsx64aMmADvL2S53I\/Aj2i84Vq2AuxD\/EPwP6xH0elIUpQUliQ5KmYuSxcWJ\/KCT9EpCUJrdSwVKZQUhDrMsMUm7hveHKCktGkdbNPwzjVGhVKmCpFKQMhiCCHOC5YWMA4HJlTJZKviDlQAskOWcthgDmKcyyZCmuEHYZFvjvYhwCR\/NHIUwUMVJc3OfJ8OIwla9ik97NWrhiMso+kP\/p6f5SeQYCKLS60pSxD2NyBlrEbxwO1TJlhMvxJLqVV\/EA2IGLbvu+0EIyn0xXH6LqVppLkTJRQzMVJNKiRekgXhop5XaNSppVMEwS2LIQpmU4a5Zwzwo2wl9jTgZ4SiVB7BufQgEP9W6xZSJolhjd9yB7HpaK0TTTs4jsTXDHBt\/3Eu2QdamcKgpIAfZj8hHP2VTOhNTEPyA5k7DMCFwXFx7kRJkHwqYlmx5btuM+8PoEVUzXlCzRYbh7P0aJMteDz2O5w8QUS6pjOR+fT1icpLEAgirncYsR1tiNJJLQFnwRBTP8ACUuQaXNLOcfiPWLfVz6JKZblfNIJwQbhlNa4dsBPOM9p55lqcPgh3Um6rXIHr1i0lapS5QShvC6iSBUrYpdsM1niLZSI+l4qUzFUuLNcmxd3B2ZrRccN4lMRKKZa1AX3UHJaw88t5Rj9VKKFkM4G77ljt+cTtPqz3fh+6b7EHzZ+nrEckL2K6NNM1ilqYqLpZW922ij7UzJs+XKXMNkkpKf5Hb4Rkixv5RK0+oSZ3gKlAAVE\/wAxAKj5E7wLtL4UBsPdg1rFOSXtfoScwuNOMintFXJCQkpdwxNv+sRFlMq4wBcn7zYxjnBJaTSXz6D3I22bnA5Ad8qbcnwgcg9z7NGq9kkuekCm7gixPPOeUWXD+LzgogTCElqgyWABAJa3iFvb1itnIV3INhhgmw6EubbQOSAVB7sxJHr0hKqEi4lagS5C1AlSnKAP3llFNJBpADkVWUdnHKIPAF0zApQBBUkFJy5fFJHXcXaIc7UkzVBg1TszBR6tzvFnwVSakgsHIODnzfPTMOWojNh2z1aho1lC5ibIBmCxZUyWFFwRkWLeUYrswpRnqKys+HxFy9qQHJL8h5RoOP6wTEKloJNSFWJISpSSiYlRBABV4Wv1jPcENBWRZkKu5FiHL+0XGSqi5PZ6zwDXlWkQ+O7XYBmZByR5j3MYfiKUiepgkCX4SkjYqOGLK+jFv2T1KPshSpbLUhaa3+DwnbAFOOdPOMrptXQtQK8gy5mRWtylyhVvWJLbRZa7ihClSpgpcVNSMPgkXBLHrGL4zPRM1ClIQQnDE1Y3frFhOXTqCSkPSEuCWVYAXHQD2iAuZ4yzsTcPUQcZbrDjV2voxlKyuoOwxny5wbTkevPrsI4VKNRBBx5e5OIJImNhn53L9BGr6ETEp8JJFrv02+vOJXCpTgAFnDX5kOPOI82qiYW3GCMqLB2y7n2h9EopUkG4w9jjyxGDtoCdM0KlpDFnsbBtt9jEGTw5bKSstuA73Ta48ostVqUipJsbe4xj6vHKpq1eIh3ccvn\/AJjJTmlQMBImGWhctTkLU9gxctgg9N4hzpoqXRg2zscxbBQdJBv0a7G4\/wARSTpKgskgZJcH6Y9IuDyewLLS6wum7gexeAT0EKLYSbW2P0IfTSzZT26Hne3+Ymd\/djvkefrCUsHoCPOmuw9YUXOn4SmYEqUcBgBazvfrChr+IghYspJiEvghJ39946RKZnP4PHSJmx8\/Tdo7RLHPJeIsZ3QCGCRUbO8NJlLQs+Tje2Mjl+UK4wR5\/PESJeqdIq57ej5ibfoCv1ARU7kP7v8ApHcghRAyLX2AG8Q9cF1l6gCfCfr1g8ks1SiSPXfY8o0a12A81SkKOyLciT6bMecHRPLGlwWy27bbbGOu8BZwbiziwa3vETVVICXVUAp\/I3xa2flErevYwWoX4gpW4blgAXbeJEhdUqYyQrkDVcAg3tn1O8LUaNapQUlBN80qYE5AYZgOj7wlSCnAV0uAbE4zGvcQNLxZcozZapDBKpUsgJve9SVcjb894qeM68oYliTtZ08j+PvEfh8xQSkKJ8Is4FkuCGKizfF74huI\/vXfN2bmH3+sQsUp\/g29jaaWkmwGWsefJizdeY3iPMkL74pSHPIs2CbEbMPlHehlqSBVUnGSBbpuPWLrUaOySCp8knIDtY7ef6wOWMg7KjVSFoS0wAFSQQ5+LyA8450ygopP8ou4JPv7xc8ZnCeiWklxLSoIsl2qqyBdyTvtFObVVN8BYCoAgbvz\/wA9YE7X6J0BnqBUWBIdv7ds7tFnwk+LNwXxkWz\/ANxUS1ukG1y5u2+T+sSZSqSk0m+TYAFmcHYuYclqhFpq1qVMCiC5e17EG5AvbEQ9ECmu5uCPQg2+ZESNdqO7G2R5gHcerjl+cErVe7sRe\/NvyiI3Vg2aTg2kBSuYZgQkJKabMolJYdBt+kVnZychpgmOVqcpuQCpnuRkNAJE4lJlhTZPLOOX0Y74bOMmaagLAB8ZcAQXqmNSIur4gsKShYpoDbmo2ve5FvnFbMU6gdt\/MfOLKbLM1VTVKKttujm1or9ZJUlKCch9wWxb65RrCr0Iedp3Kg1wlLMH6xH0rB+e31zifw6TULG7sN9sHblA+HyaZoSsAEkhlCz4DuLDeKi7tAi5m8NCtOyEkFV6mBrKai2bXgMrgcyWUqWoFIIJAKjfkLZifrdQElLKApSkWNnY2bds7ZEMmeopCQp3ze\/O\/wAoxlOSVGjoreOTiuWk2aq9r7hrwLSq8IKgS4N3ZuvPDm3IxYrMumgmqo4OXfJ6B4jJCKh4HAsCp7Nvy5YicljSIlvYLUzVVDlzJcNgF\/T5R3TUCCb2Dj5OPreOuMS0lFeQC21nfGSzuMQPQqenNyACb3wbPa3RsRP9toQBZIYNu3t022gql0q2YjMPxOWUru4uzHc9NoHPFg9yCQ7X9SPIxfaQFxw\/iKkuBy3h4r9GggYHzvDxzySspMiTnsctt9dISFMXHw+9L2uI508wAUqN\/K4tCWmlJBxi\/LkPlHR+EE+SwDG\/l+nKIs2YEqASCXOXLMNgcekcyZrkC5Ye\/rElakliRcO3R\/rMQli9gBKAtJKgpxbBs\/JxaGRov5TkfCQFX5O9vWDqmPbfpgvHCE0k+H9fMsWeKtgHlpUlIBFLhx1uzhtrbx3qJTp6jcc9w7WgM+7KBD9WGebYh06e3iSKXGLi28S\/sCf2ZmGU4LpTZ7sSwYFtvbeIkycO8YNc5SnJfzgaZgS9NLCxBbfmTyfq8czNUkWNynny6e7wU27G5aoeez1EG9j6P57bdICogeln2b8hHcvVghmLXDW8UOqXUoBSSQeRb3+cV12IHOJKwGzi9j9fkYn6KbMBUXJCVBnu1hYHk\/LpEZMgoX4SFJyyspO+3ODT9YVLLuHu3Vy\/5+8DkmqQ06GloLEfefy\/xEXVAd2pL4Cgw23udh9eUzSzyVNtkls\/Qg0wJuFOxxfruIhSaexIy2nmgDYY3bflvFvI0qlppSoJNunom++0NqeHJd5YANs9d25xL0mlX3S7ABCXw1Rdhc7X6842lNS6KWzjjGhTLKZdSSpKT4mDEg7dP1itEwLJCjc5IBNV7uNiQ7xYcQnUqdQDlKat72a3Nm+UVCp4NkVBiL7t15mHFNoT7Dyqa2BfF6rkWZwPwixUjvAoCq6bKe1xi31mKGTqFd4TVk3xfkDZoupqEhAqIewADjlcn1PtE8ipoQ2lkTEsWBYO+WI3B57YjnXzWlHvGUS4Aa4Oc7fhDnVtUlw1ySHIPS+\/+cWhd54XpBZndmH4CEru2OyLw2fcFahc45+ZxFhrUjvCpQSVWI5lLXcD8op9OCMAvs\/u7wbulLUFEMLF855PFuHysCcQkpdmADv1w5G5Z\/eODqKUgpGWBLXb0O7xLn6eqWavhAJYWDeUCUCg\/wBzeEJdm\/LEReh0yPJl1uPhYbeI3L4G8WitMFBLlSSAKasOGHiB+rwjSQSlISWIPUtk56egio4rNWlQUoq+IMWZ07hJ5dfKBLJ0UqSCKlrZSScDzukuGaI0tZq5Nm\/V9rRN0BBUqk+F7B3YPa53aIUyV3c1iM3AS3Nsnyf1hr2iaLTiOpBUgb0gKBHPd2cbbRHXJYMm5Bxa9iWPP15Q2uWAQ7gMGcu3SOZiDS6dyPFUARts1ut8xCWkI70eovi9+WH5MzwoiVeF1C7tYHlud4UN8dsRI+wJLk8tsPl2jg6d0hycEC+4u8KFEqTAHLkFJLEYsfaHQvaFCjRb7A6qu5OC31eO9SGQFJsLk7k25kwoUHtADXrFlBZhYbZctmDyVlQAOTvnl9YhQoJJUBD4kspWC71AEggeVuUT5MkLlgl2IFnx8s4vDwoc9RQETXI7tVnNVmdmP6RLQTQT1BY3Afl6H5CFCiX\/AEpgC1CghbBw\/LF8+E+nzgi5jpFQduflkfOHhQq0hA1JNdLsKbNtdt4OjUkNliCf1\/CFCgqwGQo10HcOG+QPPMXGg0qkIDKupTLtlBwkHkwu43OYUKFHtGnH2R+1WnqmKBCfDp0nyZSsfM+rRk5CkoUXSFpbByCORh4UdEVqhz7Juk0IaoFQUxPTDkGDCSolnBKmsXbnnb0hQowcnZBBTNIWoA2J33i94bpgQxAJbGBjp6QoUHM6SoBSdGE1FZfPhAcM3X6tHOl1FSN2u3MXf84eFD5EXLQKWpkupnfYdOsVCeJFSvGHGw8i\/wDiFCjTjimmSyZI1gsKS1TEO7m5ibM1AWpKCLG3l5QoUS4qxpgdEsIJqSLKoJBvYE7jlEfikyiYpPltmoOH+eIUKFFLP\/vwPR1qkghJ3s17M1x9c4FrJBlhCkqZyGHJ3zzxDQoIPoQXTppJCrta1rkA88QoUKE1ZJ\/\/2Q==";
	protected $noExtUrl = "https://sp.yimg.com/ib/th?id=JN.DrBkMR6w%2bnlucry23TZBqQ&pid=15.1&P=0";
	protected $pngUrl = "http://fineprintnyc.com/images/blog/history-of-logos/google/google-logo.png";
	protected $batteryPropertiesValues;

	/**
	 *
	 */
	public function __construct()
	{
		$this->batteryPropertiesValues = (array) json_decode('{"http:\/\/www.w3.org\/2000\/01\/rdf-schema#label":[{"literal":"Battery 4\/30","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#TestingProgram":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#DiagnosticAssessments","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Subject":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#ELA","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Grade":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#MultiLevel","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Security":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Secure","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Multimedia":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Embeded","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#TestScoring":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Immediate","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Permission":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#RequiresCustomPermissions","label":null,"comment":"","debug":""}],"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#TestPrivacy":[{"uriResource":"http:\/\/www.tao.lu\/Ontologies\/taoTestBattery.rdf#Private","label":null,"comment":"","debug":""}]}');
	}

	/**
	 * Tests initialization
	 */
	protected function setUp()
	{
		TaoPhpUnitTestRunner::initTest();
	}

	/**
	 * @return string
	 */
	public function test_ConvertIllegalFilenameCharacters_HttpsNoExtension_QuestionMarksToUnderscores()
	{
		$check = "https://sp.yimg.com/ib/th_id=JN.DrBkMR6w%2bnlucry23TZBqQ&pid=15.1&P=0";
		$this->assertEquals($check, Comp::convertIllegalFilenameCharacters($this->noExtUrl));
		return $check;
	}

	public function test_ADPizeUrl_HttpPngPassedAsArray_ReturnAdpString()
	{
		$adpString = "[[ADP]]0_1_4_google-logo.png";
		$this->assertEquals($adpString, Comp::ADPizeURL($this->pngUrl, [0, 1, 4]));
	}

	public function test_ADPizeUrl_HttpsNoExtensionPassedAsString_ReturnAdpString()
	{
		$check = "[[ADP]]0_1_4_th_id=JN.DrBkMR6w%2bnlucry23TZBqQ&pid=15.1&P=0.jpg";
		$this->assertEquals($check, Comp::ADPizeURL($this->noExtUrl, "0_1_4_"));
	}

	/**
	 * @expectedException \common_Exception
	 */
	public function test_ADPizeUrl_NoItemNum_ThrowCommonException()
	{
		@Comp::ADPizeURL($this->noExtUrl, [0, 1]);
	}

	/**
	 * @expectedException \common_Exception
	 */
	public function test_ADPizeUrl_NoUrl_ThrowCommonException()
	{
		@Comp::ADPizeURL($url, [0, 1, 4]);
	}

	/**
	 * @expectedException \common_Exception
	 */
	public function test_ADPizeUrl_InvalidIDs_ThrowsCommonException()
	{
		@Comp::ADPizeURL($this->noExtUrl, null);
	}

	public function test_ADPizeUrl_DataUrlPassedAsString_ReturnDataUrl()
	{
		$this->assertEquals($this->dataUrl, Comp::ADPizeURL($this->dataUrl, "0_1_4"));
	}

	/**
	 * @return Comp
	 */
	public function test_Constructor_Creation_Successful()
	{
		$this->markTestSkipped();
		$comp = new Comp($this->dataPath);
		$this->assertInstanceOf('taoTestBatteries_helpers_Compilation', $comp);
		return $comp;
	}

    /**
     * @depends test_Constructor_Creation_Successful
     * @param $comp
     * @return
     */
	public function test_ValidateBattery_GoodInput_NoException($comp)
	{
		$comp->validateBattery($this->batteryUri, $this->batteryPropertiesValues);
		return $comp;
	}

    /**
     * @depends test_ValidateBattery_GoodInput_NoException
     * @expectedException \common_Exception
     * Needs some work. common_Logger doesn't work yet.
     * @param $comp
     */
	public function test_Validatebattery_EmptyProperty_ThrowCommonException($comp)
	{
		$this->markTestSkipped();
		$modValues = $this->batteryPropertiesValues;
		unset($modValues[array_keys($modValues)[0]][0]);
		\common_Logger::w("Somethhing");
		$comp->validateBattery($this->batteryUri, $modValues);
		$this->assertTrue(true);
	}

	public function test_TestOptimization_SmallArray_ReturnOptimized()
	{
		$revisionArray = [
			"identifier" => "Test-2",
			"pub_resourceManifest" => [
				0 => "[[ADP]]0_0_0_earth.png",
				1 => "[[ADP]]0_0_0_moon.png",
				2 => "[[ADP]]0_0_0_jupiter.png",
				3 => "[[ADP]]0_0_0_ganymede.png",
				4 => "[[ADP]]0_0_0_neptune.png"
			],
			"testPart" => [
				0 => [
					"identifier" => "testPart-1",
					"navigationMode" => "linear",
					"submissionMode" => "simultaneous",
					"assessmentSection" => [
						"identifier" => "Identifier",
						"title" => "Title",
						"assessmentItem" => [
							"assessmentItemContent" => [
								"serial" => "Serial",
								"attributes" => [
									"label" => "Label",
									"title" => "Title",
									"body" => [
										"body" => "Body",
										"serial" => "Serial"
									]
								],
								"responseProcessing" => [
									"serial" => "Serial",
									"attributes" => [
										"something" => "anything",
										"anotherSomething" => "anotherAnything"
									]
								]
							]
						]
					]
				]
			],
			"pub_schemaVersion" => 1,
			"title" => "NOT OPTIMIZED-Test 2"
		];

		$optimizedArray = [
			"pub_resourceManifest" => [
				0 => "[[ADP]]0_0_0_earth.png",
				1 => "[[ADP]]0_0_0_moon.png",
				2 => "[[ADP]]0_0_0_jupiter.png",
				3 => "[[ADP]]0_0_0_ganymede.png",
				4 => "[[ADP]]0_0_0_neptune.png"
			],
			"testPart" => [
				0 => [
					"navigationMode" => "linear",
					"assessmentSection" => [
						"title" => "Title",
						"assessmentItem" => [
							"assessmentItemContent" => [
								"serial" => "Serial",
								"attributes" => [
									"title" => "Title",
									"body" => [
										"body" => "Body",
										"serial" => "Serial"
									]
								],
								"responseProcessing" => [
									"serial" => "Serial"
								]
							]
						]
					]
				]
			],
			"title" => "NOT OPTIMIZED-Test 2"
		];

		// Test Rules.
		$testRules = unserialize(TEST_RULES);
		$this->assertEquals($optimizedArray, Comp::testOptimization($revisionArray, $testRules));

	}
}
