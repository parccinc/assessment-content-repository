/*global define*/
define(
    [
        'taoQtiItem/qtiCreator/widgets/static/Widget',
        'parccAnswerMasking/creator/widget/states/states'
    ],
    function (Widget, states) {
        'use strict';
        var parccAnswerMasking = Widget.clone();

        parccAnswerMasking.initCreator = function () {
            this.registerStates(states);
            this.element.data('scenario', 'authoring');
            Widget.initCreator.call(this);
        };

        parccAnswerMasking.buildContainer = function () {
            this.$container = this.$original;
            this.$container.addClass('widget-box');
        };
        return parccAnswerMasking;
    }
);