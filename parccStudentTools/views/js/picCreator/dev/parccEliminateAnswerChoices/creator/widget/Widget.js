/*global define*/
define(
    [
        'taoQtiItem/qtiCreator/widgets/static/Widget',
        'parccEliminateAnswerChoices/creator/widget/states/states'
    ],
    function (Widget, states) {
        'use strict';
        var parccEliminateAnswerChoices = Widget.clone();

        parccEliminateAnswerChoices.initCreator = function () {
            this.registerStates(states);
            this.element.data('scenario', 'authoring');
            Widget.initCreator.call(this);
        };

        parccEliminateAnswerChoices.buildContainer = function () {
            this.$container = this.$original;
            this.$container.addClass('widget-box');
        };
        return parccEliminateAnswerChoices;
    }
);