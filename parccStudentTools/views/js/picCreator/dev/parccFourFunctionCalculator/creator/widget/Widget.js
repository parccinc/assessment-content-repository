define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccFourFunctionCalculator/creator/widget/states/states'
], function(Widget, states){

    var FourFunctionCalculatorWidget = Widget.clone();

    FourFunctionCalculatorWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    FourFunctionCalculatorWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return FourFunctionCalculatorWidget;
});