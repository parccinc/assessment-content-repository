define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccDictionary/creator/widget/states/states'
], function(Widget, states){

    var DictionaryWidget = Widget.clone();

    DictionaryWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    DictionaryWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return DictionaryWidget;
});