define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccGraphPaper/creator/widget/states/states'
], function(Widget, states){

    var GraphPaperWidget = Widget.clone();

    GraphPaperWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    GraphPaperWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return GraphPaperWidget;
});