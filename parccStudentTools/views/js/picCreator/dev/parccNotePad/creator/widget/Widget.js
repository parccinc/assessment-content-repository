define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccNotePad/creator/widget/states/states'
], function(Widget, states){

    var NotePadWidget = Widget.clone();

    NotePadWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    NotePadWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return NotePadWidget;
});