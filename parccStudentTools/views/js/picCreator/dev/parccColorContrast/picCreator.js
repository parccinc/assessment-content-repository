define([
    'lodash',
    'taoQtiItem/qtiCreator/editor/infoControlRegistry',
    'parccColorContrast/creator/widget/Widget',
    'tpl!parccColorContrast/creator/tpl/color-contrast'
],
function (_, registry, Widget, markupTpl) {
    'use strict';

    /**
     * Retrieve data from manifest
     */
    var manifest = registry.get('parccColorContrast').manifest;

    /**
     * Configuration of the container
     */
    var is = {
        transparent: true,
        movable: false,
        rotatable: {
            tl: false,
            tr: false,
            br: false,
            bl: false
        },
        adjustable: {
            x:  false,
            y:  false,
            xy: false
        },
        transmutable: false
    };

    // the position in which the checkbox should appear
    var position = -1;

    return {
        /**
         * (required) Get the typeIdentifier of the custom interaction
         *
         * @returns {String}
         */
        getTypeIdentifier: function () {
            return manifest.typeIdentifier;
        },

        /**
         * (required) Get the widget prototype
         * Used in the renderer
         *
         * @returns {Object} Widget
         */
        getWidget: function () {
            return Widget;
        },

        /**
         * (optional) Get the default properties values of the PIC.
         * Used on new PIC instance creation
         *
         * @returns {Object}
         */
        getDefaultProperties: function () {
            return {
                is: is,
                position: position
            };
        },

        /**
         * (optional) Callback to execute on the
         * Used on new pic instance creation
         *
         * @returns {Object}
         */
        afterCreate: function (pic) {
            //do some stuff
        },

        /**
         * (required) Returns the QTI PIC XML template
         *
         * @returns {function} handlebar template
         */
        getMarkupTemplate: function () {
            return markupTpl;
        },

        /**
         * (optional) Allows passing additional data to xml template
         *
         * @returns {function} handlebar template
         */
        getMarkupData: function (pic, defaultData) {

            defaultData = _.defaults(defaultData, {
                typeIdentifier: manifest.typeIdentifier,
                title: manifest.label,
                is: is,
                position: position,
                //referenced as a required file in manifest.media[]
                icon: manifest.typeIdentifier + '/runtime/media/color-contrast-icon.svg',
                alt: manifest.short || manifest.label
            });

            return defaultData;
        }
    };
});
