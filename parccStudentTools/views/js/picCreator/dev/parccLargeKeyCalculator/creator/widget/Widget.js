define([
    'taoQtiItem/qtiCreator/widgets/static/Widget',
    'parccLargeKeyCalculator/creator/widget/states/states'
], function(Widget, states){

    var LargeKeyCalculatorWidget = Widget.clone();

    LargeKeyCalculatorWidget.initCreator = function(){
        
        this.registerStates(states);
        
        Widget.initCreator.call(this);
    };
    
    LargeKeyCalculatorWidget.buildContainer = function(){
        
        this.$container = this.$original;
        this.$container.addClass('widget-box');
    };
    
    return LargeKeyCalculatorWidget;
});