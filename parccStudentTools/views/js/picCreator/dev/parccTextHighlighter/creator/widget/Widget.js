/*global define*/
define(
    [
        'taoQtiItem/qtiCreator/widgets/static/Widget',
        'parccTextHighlighter/creator/widget/states/states'
    ],
    function (Widget, states) {
        'use strict';
        var toolWidget = Widget.clone();

        toolWidget.initCreator = function () {
            this.registerStates(states);
            Widget.initCreator.call(this);
        };

        toolWidget.buildContainer = function () {
            this.$container = this.$original;
            this.$container.addClass('widget-box');
        };
        return toolWidget;
    }
);