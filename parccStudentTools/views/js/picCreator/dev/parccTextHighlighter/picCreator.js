/*global define*/
define(
    [
        'lodash',
        'taoQtiItem/qtiCreator/editor/infoControlRegistry',
        'parccTextHighlighter/creator/widget/Widget',
        'tpl!parccTextHighlighter/creator/tpl/markup'
    ],
    function (_, registry, Widget, markupTpl) {
        'use strict';
        /**
         * Retrieve data from manifest
         */
        var manifest = registry.get('parccTextHighlighter').manifest,
            // the position in which the checkbox should appear
            position = 2;

        return {
            /**
             * (required) Get the typeIdentifier of the custom interaction
             * 
             * @returns {String}
             */
            getTypeIdentifier : function () {
                return manifest.typeIdentifier;
            },
            /**
             * (required) Get the widget prototype
             * Used in the renderer
             * 
             * @returns {Object} Widget
             */
            getWidget : function () {
                return Widget;
            },
            /**
             * (optional) Get the default properties values of the PIC.
             * Used on new PIC instance creation
             * 
             * @returns {Object}
             */
            getDefaultProperties : function (pic) {
                return {
                    position: position
                };
            },

            /**
             * (optional) Callback to execute on the 
             * Used on new pic instance creation
             * 
             * @returns {Object}
             */
            afterCreate : function (pic) {
                //do some stuff
            },

            /**
             * (required) Returns the QTI PIC XML template 
             * 
             * @returns {function} handlebar template
             */
            getMarkupTemplate : function () {
                return markupTpl;
            },

            /**
             * (optional) Allows passing additional data to xml template
             * 
             * @returns {function} handlebar template
             */
            getMarkupData : function (pic, defaultData) {

                defaultData = _.defaults(defaultData, {
                    typeIdentifier : manifest.typeIdentifier,
                    title : manifest.label,
                    position: position,
                    //referenced as a required file in manifest.media[]
                    icon : manifest.icon,
                    alt : manifest.short || manifest.label
                });

                return defaultData;
            }
        };
    }
);