<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *               
 * 
 */
return array(
    'name' => 'taoParcc',
    'label' => 'Community Edition',
    'description' => 'the PARCC Edition extension',
    'license' => 'PARCC',
    'version' => '1.4.0',
    'author' => 'Open Assessment Technologies SA',
    'requires' => array(
        'taoCe' => '*',
        'parccStudentTools' => '*',
        'parccTei' => '*',
        'parccMetadata' => '*',
        'taoMediaManager' => '*',
        'taoDacSimple' => '*',
        'taoRevision' => '*',
	    'taoCssDevKit' => '*',
        'taoWorkspace' => '*',
        'taoQtiItem' => '>=2.12.12'
    ),
    'managementRole' => 'http://www.tao.lu/Ontologies/generis.rdf#taoParccManager',
    'acl' => array(
    ),
    'install' => array(
        'php' => array(
            dirname(__FILE__) . '/scripts/install/setMultiColumn.php',
            dirname(__FILE__) . '/scripts/install/setThemeConfig.php',
            dirname(__FILE__) . '/scripts/install/setLocaleNumbersConfig.php',
            dirname(__FILE__) . '/scripts/install/registerQtiItemScripts.php'
        )
    ),
    'uninstall' => array(
    ),
    'update' => 'oat\\taoParcc\\scripts\\update\\Updater',
    'routes' => array(
        '/taoParcc' => 'oat\\taoParcc\\controller'
    ),
    'constants' => array(
        # views directory
        "DIR_VIEWS" => dirname(__FILE__).DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR,
	    
        #BASE URL (usually the domain root)
        'BASE_URL' => ROOT_URL.'taoParcc/',
        
        #BASE WWW the web resources path
        'BASE_WWW' => ROOT_URL.'taoParcc/views/'
    )
);
