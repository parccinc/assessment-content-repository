# Working with themes

- Open a command window in views/build

## Install the grunt task
- `npm install .` installs all required elements

## Run the task
Synopsis `grunt task [options]`

- `grunt csssdk` compiles all item themes
- `grunt csssdk:my-theme` compiles only the theme *my-theme*
- `grunt watch:csssdk` creates a watch for each theme
- `grunt watch:my-theme` watches only the theme *my-theme*

### Options for the task
- `--target` compile for areas other than items, e. g. platform|tests|delivery
- `--style` e. g. expanded, default is compressed ([SASS docs](http://sass-lang.com/documentation/file.SASS_REFERENCE.html#output_style))

* Note that for now `.scss` templates are only available for items *

## Grunt built-in options
[See options in the Grunt documentation](http://gruntjs.com/using-the-cli)