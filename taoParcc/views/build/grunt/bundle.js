module.exports = function(grunt) {
    'use strict';

    var requirejs   = grunt.config('requirejs') || {};
    var clean       = grunt.config('clean') || {};
    var copy        = grunt.config('copy') || {};

    var root        = grunt.option('root');
    var libs        = grunt.option('mainlibs');
    var ext         = require(root + '/tao/views/build/tasks/helpers/extensions')(grunt, root);
    var out         = 'output';

    /**
     * Remove bundled and bundling files
     */
    clean.taoparccscripts = [out];
    clean.taoparccqtiscorer = [out];

    /**
     * Compile the parcc user scripts
     */
    requirejs.taoparccscripts = {
        options: {
            baseUrl : '../js',
            mainConfigFile : './config/requirejs.build.js',
            paths : { 'taoParcc' : root + '/taoParcc/views/js'},
            out:  out + '/userScripts.min.js',
            include: ['taoParcc/userScripts/passagify/passagify', 'taoParcc/userScripts/pearsonTableGrid/pearsonTableGrid' ],
            exclude : libs
        }
    };

    /**
     * copy the bundles to the right place
     */
    copy.taoparccscripts = {
        option:  {
            force : true
        },
        files: [{
            cwd: out,
            expand : true,
            src: ['userScripts.min*'],
            dest: root + '/taoParcc/views/js/userScripts/'
        }]
    };


    var paths = {
        'taoQtiItem' : root + '/taoQtiItem/views/js',
        'taoQtiItemCss' :  root + '/taoQtiItem/views/css',
        'taoItems' : root + '/taoItems/views/js',
        'qtiCustomInteractionContext' : root + '/taoQtiItem/views/js/runtime/qtiCustomInteractionContext',
        'qtiInfoControlContext' : root + '/taoQtiItem/views/js/runtime/qtiInfoControlContext',

        'taoParcc/lib' : root + '/taoParcc/views/js/lib',
        'qti.customOperators.text.stringToNumber' : root + '/taoParcc/views/js/scoring/processor/expressions/operators/custom/stringToNumber',
        'qti.customOperators.math.graph.CountPointsThatSatisfyEquation' : root + '/taoParcc/views/js/scoring/processor/expressions/operators/custom/countPointsThatSatisfyEquation'
    };
    
    /**
     * Compile the new item runner as a standalone library
     */
    requirejs.taoparccqtiscorer = {
        options: {
            baseUrl : '../js',
            mainConfigFile : './config/requirejs.build.js',
            //optimize: "none",
            findNestedDependencies : true,
            uglify2: {
                mangle : false,
                output: {
                    'max_line_len': 400
                }
            },
            wrap : {
                start : '',
                end : "define(['taoQtiItem/scoring/qtiScorer'], function(scorer){ return scorer; });"
            },
            wrapShim: true,
            paths : paths,
            include: ['lodash', 'qti.customOperators.text.stringToNumber', 'qti.customOperators.math.graph.CountPointsThatSatisfyEquation'],
            name: "taoQtiItem/scoring/qtiScorer",
            out: out + "/taoParcc/qtiScorer.min.js"
        }
    };

    /**
     * copy the bundles to the right place
     */
    copy.taoparccqtiscorer = {
        option:  {
            force : true
        },
        files: [{
            cwd: out + "/taoParcc/",
            expand : true,
            src: ['qtiScorer.min*'],
            dest: root + '/taoParcc/views/js/'
        }]
    };
    
    grunt.config('clean', clean);
    grunt.config('requirejs', requirejs);
    grunt.config('copy', copy);

    // bundle task
    grunt.registerTask('taoparccscripts', ['clean:taoparccscripts', 'requirejs:taoparccscripts', 'copy:taoparccscripts']);
    grunt.registerTask('taoparccqtiscorer', ['clean:taoparccqtiscorer', 'requirejs:taoparccqtiscorer', 'copy:taoparccqtiscorer']);
};
