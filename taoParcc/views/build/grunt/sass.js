module.exports = function(grunt) {
    'use strict';

    var sass    = grunt.config('sass') || {};
    var watch   = grunt.config('watch') || {};
    var notify  = grunt.config('notify') || {};
    var root    = grunt.option('root') + '/taoParcc/views/';

    sass.taoparcc = {
        files : {}
    };
    sass.taoparcc.files[root + 'js/userScripts/passagify/css/passage.css'] = root + 'js/userScripts/passagify/scss/passage.scss';

    watch.taoparccsass = {
        files : [root + 'js/userScripts/*/scss/*.scss', root + 'scss/*.scss'],
        tasks : ['sass:taoparcc', 'notify:taoparccsass'],
        options : {
            debounceDelay : 1000
        }
    };

    notify.taoparccsass = {
        options: {
            title: 'Grunt SASS',
            message: 'SASS files compiled to CSS'
        }
    };

    grunt.config('sass', sass);
    grunt.config('watch', watch);
    grunt.config('notify', notify);

    //register an alias for main build
    grunt.registerTask('taoparccsass', ['sass:taoparcc']);
};
