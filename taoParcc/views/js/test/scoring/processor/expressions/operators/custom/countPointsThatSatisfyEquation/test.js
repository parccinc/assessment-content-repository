define([
    'lodash',
    'taoQtiItem/scoring/processor/expressions/preprocessor',
    'taoParcc/scoring/processor/expressions/operators/custom/countPointsThatSatisfyEquation'
], function (_, preProcessorFactory, insideProcessor) {
    "use strict";

    QUnit.module('API');

    QUnit.test('structure', function (assert) {
        assert.ok(_.isPlainObject(insideProcessor), 'the processor expose an object');
        assert.ok(_.isFunction(insideProcessor.process), 'the processor has a process function');
        assert.ok(_.isArray(insideProcessor.operands), 'the processor has a process function');
    });

    QUnit.module('Process');

    var dataProvider = [{
        title: 'points satisfy equation',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['0 0', '2 1', '-2 -1', '4 2', '-4 -2']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=1/2*x'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 5
        }
    }, {
        title: 'points not satisfy equation',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['23 34', '23 21']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=1/2*x'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 0
        }
    }, {
        title: 'same points not satisfy equation',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['2 1', '-2 -1', '4 2', '-4 -2', '23 34', '23 21']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=1/2*x'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 4
        }
    }, {
        title: 'same points in string not satisfy equation',

        operands: [{
            cardinality: 'multiple',
            baseType: 'string',
            value: ['2 1', '-2 -1', '4 2', '-4 -2', '23 34', '23 21', '3 1.5', '1.2 0.6', '2.1 1.1']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=1/2*x'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 6
        }
    }, {
        title: 'Check special vars in equation (#pi=pi)',

        operands: [{
            cardinality: 'multiple',
            baseType: 'string',
            value: ['1 pi', '1 \\pi', '1 #pi', '2 34']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=x*#pi'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 3
        }
    }, {
        title: 'Algebra formulas',

        operands: [{
            cardinality: 'multiple',
            baseType: 'string',
            value: ['1/2*#pi 1', '0*#pi 0', '#pi/4 sqrt(2)/2']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=sin(x)'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 3
        }
    }, {
        title: 'Camel case equation not supported',

        operands: [{
            cardinality: 'multiple',
            baseType: 'string',
            value: ['2 1', '-2 -1', '4 2', '-4 -2', '23 34', '23 21', '3 1.5', '1.2 0.6', '2.1 1.1']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'Y=1/2*x'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 0
        }
    }, {
        title: 'Check equation',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['2 1 0', '-2 -1 45', '4 2 11', '-4 -2 -7', '23 34 44', '23 21 55', '0 0 0']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y+x=x+y'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 7
        }
    }, {
        title: 'without operands',
        expectedResult: null
    }, {
        title: 'Extra parameter',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['2 1 0', '-2 -1 45', '4 2 11', '-4 -2 -7', '23 34 44', '23 21 55', '0 0 0']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y+x=x+y+z'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 0
        }
    }, {
        title: 'Without one preset parameters',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['2 1 0']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y+z=y+z'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 0
        }
    }, {
        title: 'Without preset parameters',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['2 1 0']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 's+z=s+z'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 0
        }
    }, {
        title: 'Test for KAS +-',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            value: ['0 0', '1 5', '2 -16', '2 2', '5 -25']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=4x^2+xy'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 3
        }
    }, {
        title: 'Test for Sin(pi)',

        operands: [{
            cardinality: 'multiple',
            baseType: 'point',
            // approximation is 1e-10, so with big numbers it doesn't works
            value: ['1 0', '1000 0', '9999999999999999 0']
        }, {
            cardinality: 'single',
            baseType: 'string',
            value: 'y=-1*x*sin(#pi)'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'integer',
            value: 2
        }
    }];

    QUnit
        .cases(dataProvider)
        .test('inside ', function (data, assert) {
            insideProcessor.preProcessor = preProcessorFactory({});

            insideProcessor.operands = data.operands;

            insideProcessor.expression = {attributes: {}};

            assert.deepEqual(insideProcessor.process(), data.expectedResult, 'The inside is correct');
        });
});
