/**
 * Copyright (c) 2016 Open Assessment Technologies, S.A.
 *
 * @author A.Zagovorichev, <zagovorichev@1pt.com>
 */

define([
    'lodash',
    'taoQtiItem/scoring/processor/expressions/preprocessor',
    'taoParcc/scoring/processor/expressions/operators/custom/stringToNumber'
], function (_, preProcessorFactory, stringToNumberProcessor) {
    "use strict";

    QUnit.module('API');

    QUnit.test('structure', function (assert) {
        assert.ok(_.isPlainObject(stringToNumberProcessor), 'the processor expose an object');
        assert.ok(_.isFunction(stringToNumberProcessor.process), 'the processor has a process function');
        assert.ok(_.isArray(stringToNumberProcessor.operands), 'the processor has a process function');
    });


    QUnit.module('Process');
    
    var dataProvider = [{
        title: 'integer',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '5'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: 5
        }
    }, {
        title: 'empty',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: ''
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: NaN
        }
    }, {
        title: 'float',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '12345.9009873'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: 12345.9009873
        }
    }, {
        title: 'negative float',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '-12345.9009873'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: -12345.9009873
        }
    }, {
        title: 'negative zero',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '-0'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: 0
        }
    }, {
        title: 'alphabetic',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: 'Not a number'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: NaN
        }
    }, {
        title: 'string with number',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '1.02 is a float'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: 1.02
        }
    }, {
        title: 'string with number',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: '1 is a int and .02 is a decimal'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: 1
        }
    }, {
        title: 'string with number in the end',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: 'is a decimal .02'
        }],
        expectedResult: {
            cardinality: 'single',
            baseType: 'float',
            value: NaN
        }
    }, {
        title: 'null',

        operands: [{
            cardinality: 'single',
            baseType: 'string',
            value: null
        }],
        expectedResult: null
    }, {
        title: 'without operands',
        expectedResult: null
    }];

    
    QUnit
        .cases(dataProvider)
        .test('stringToNumber ', function (data, assert) {
            stringToNumberProcessor.preProcessor = preProcessorFactory(data.state ? data.state : {});

            stringToNumberProcessor.operands = data.operands;

            assert.deepEqual(stringToNumberProcessor.process(), data.expectedResult, 'The stringToNumber is correct');
        });
});
