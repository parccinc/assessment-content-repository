define([
    'jquery',
    'lodash',
    'taoQtiItem/runner/qtiItemRunner',
    'taoParcc/userScripts/passagify/passagify',
    'json!taoParcc/test/passagify/samples/json/withPages.json'
], function($, _, qtiItemRunner, passagify, itemData){
    'use strict';

    var runner;
    var fixtureContainerId = 'item-container';
    var outsideContainerId = 'outside-container';

    module('Visual Test', {
        teardown : function(){
            if(runner){
//                runner.clear();
            }
        }
    });
    
    QUnit.asyncTest('Display and play', function(assert){

        var $container = $('#' + outsideContainerId);

        assert.equal($container.length, 1, 'the item container exists');
        assert.equal($container.children().length, 0, 'the container has no children');

        runner = qtiItemRunner('qti', itemData)
            .on('render', function(){
                
                passagify.exec();

                QUnit.start();
            })
            .init()
            .render($container);
    });
});

