define([
    'jquery'
], function ($){
    'use strict';
    
    function exec(){
        $('.qti-item .table-grid').each(function() {
            var $interaction = $(this);
            var $prompt = $interaction.find('.qti-prompt');
            
            if ($prompt.length > 0) {
                var $th = $interaction.find('.match-interaction-area .matrix thead tr th:first');
                
                if ($th.length > 0) {
                    $th.append($prompt.contents());
                }
            }
        });
    };
    
    return {
        exec : exec
    };
});
