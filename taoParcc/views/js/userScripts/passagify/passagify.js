/**  
 * Copyright (c) 2015 (original work) Open Assessment Technologies;
 */
define([
    'jquery',
    'lodash',
    'tpl!taoParcc/userScripts/passagify/tpl/passage',
    'tpl!taoParcc/userScripts/passagify/tpl/scrolling',
    'tpl!taoParcc/userScripts/passagify/tpl/simple',
    'tpl!taoParcc/userScripts/passagify/tpl/tab-navigation',
    'ui/waitForMedia',
    'css!taoParcc/userScripts/passagify/css/passage'
], function ($, _, passageTpl, scrollingTpl, simpleTpl, navTpl){
    'use strict';
    
    /**
     * Parse orginal passages DOM to extract all containing information
     * 
     * @param {JQuery} $nav
     * @returns {Array}
     */
    function parsePassagesData($nav){
        var passagesData = [];
        var $tabContent = $nav.next('.tab-content');
        
        $tabContent.children('.tab-pane').each(function (){
            var $pane = $(this);
            var paneData = {
                id : $pane.attr('id')
            };
            var panelClass = $pane.children('.tab-scrolling').attr('class');
            
            if(panelClass){
                panelClass.replace(/(?:^|\s)(passage[0-9]+)(?:\s|$)/, function($0, $1){
                    paneData.height = $1;
                });
            }

            if($pane.children('.tab-scrolling').length){
                paneData.type = 'scrolling';
                paneData.content = $pane.children('.tab-scrolling').html();
            }else{
                paneData.type = 'simple';
                paneData.content = $pane.html();
            }

            passagesData.push(paneData);
        });

        return passagesData;
    }
    
    /**
     * Prepare actual passages DOM from the initial one
     * 
     * @param {JQuery} $nav
     * @returns {JQuery} the newly created and prepared DOM
     */
    function createPassagesDom($nav){

        var passagesData = parsePassagesData($nav);
        var $passageContainer = $('<div class="tao-passage-container">', {});
        var $passages = $('<div class="passages">').appendTo($passageContainer);

        $nav.children('li').each(function (){

            var $li = $(this);
            var active = $li.hasClass('active');
            var id = $li.children('a').attr('href').substring(1);//the href targets ids, e.g. #firstStimulus
            var title = $li.children('a').text();
            var passageData = _.find(passagesData, {id : id});
            var $passage = $(passageTpl({
                title : title,
                id : id,
                type : passageData.type,
                height : passageData.height || '',
                active : active
            }));
            
            $passage.html(passageData.content);

            $passages.append($passage);
        });

        $nav.next('.tab-content').remove();
        $nav.replaceWith($passageContainer);
        return $passageContainer;
    }
    
    /**
     * Init the passages dom
     * 
     * @param {JQueru} $dom - initial dom elemeent
     */
    function initPassages($dom){

        $dom.find('.passage-scrolling').each(function (){
            var $passage = $(this);
            initScrollingPassageMarkup($passage);
        });
    }
    
    /**
     * Init the markup for a scrolling passage
     * 
     * @param {JQuery} $passage
     */
    function initScrollingPassageMarkup($passage){

        //prepare content
        var passageContent = simpleTpl({
            content : $passage.html()
        });
        //add scrollbar
        $passage.html(scrollingTpl({
            content : passageContent
        }));
    }
    
    /**
     * Init tabbing functionality
     * 
     * @param {JQuery} $dom
     * @returns {tabbingApi}
     */
    function initTabbing($dom){

        //create markup:
        initTabbingMarkup($dom);
        
        var $tabContainer = $dom.find('.passages-tabs');
        var $tabs = $tabContainer.children('.passage');
        var $nav = $tabContainer.children('.passages-tab-navigation');
        var i = 0;
        var $active = $nav.find('li:first-child a');
        $tabs.hide();
        $nav.children('li').each(function(){
            var $li = $(this),
                $a = $li.children('a');

            if(i < $tabs.length){
                var $tab = $($tabs[i]);
                $a.data('passage-tab', $tab);

                if($li.hasClass('passages-tab-active')){
                    $active = $a;
                }
            }else{
                $a.hide();
            }
            i++;
        });
        $nav.on('click', 'a', function(e){
            e.preventDefault();
            activateTab($(this));
        });
        //activate initial tab:
        activateTab($active);

        /**
         * Activate a tab identified by its trigger <a>
         *
         * @param {JQuery} $a
         */
        function activateTab($a){
            var $li = $a.parent();
            var $tab = $a.data('passage-tab');
            if($tab && $tab.length){
                //toggle visibility:
                $tab.show().siblings('.passage').hide();
                //change li class:
                $li.addClass('passages-tab-active').siblings('li').removeClass('passages-tab-active');
                //update active element:
                $active = $a;
                //event
                $tab.trigger('activate.tab', [$tab.data('passage-id')]);
            }
        }

        var tabbingApi = {
            activate : function(passageId){
                var $trigger = $nav.find('a[data-passage-id=' + passageId + ']');
                activateTab($trigger);
            },
            getActive : function(){
                var $passage = $active.data('passage-tab');
                if($passage.length){
                    return $passage;
                }
            }
        };

        $tabContainer.attr('data-stuff', true);
        $tabContainer.data('tabbing-api', tabbingApi);
        return tabbingApi;
    }
    
    /**
     * Prepare the markup for tabbing widget
     * 
     * @param {JQuery} $dom
     */
    function initTabbingMarkup($dom){

        var tplData = {
            passages : []
        };
        var $tabContainer = $dom.children('.passages').addClass('passages-tabs');
        var $passages = $tabContainer.children('.passage').addClass('passage-tab');
        
        $passages.each(function(){
            var $passage = $(this);
            tplData.passages.push({
                title : $passage.attr('title'),
                active : $passage.hasClass('active'),
                id : $passage.data('passage-id')
            });
            $passage.removeAttr('title');
        });

        //remove old markup:
        $tabContainer.children('.passages-tab-navigation').remove();
        $tabContainer.prepend(navTpl(tplData));
    }
    
    /**
     * Transform Parcc style passages into interactive widget
     */
    function exec(){
        $('.qti-item .stimulus_content, ul.tabbed-passages').each(function (){
            var $nav = $(this);
            var $passageContainer = createPassagesDom($nav);
            //init scrolling on all "scrollable" frame container
            initPassages($passageContainer);
            //init tabbing
            initTabbing($passageContainer);
        });
    };

    /**
     * @exports
     */
    return {
        exec : exec
    };
});