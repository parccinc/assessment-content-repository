/**
 * The stringToNumber operator processor.
 * 
 * @see taoQtiItem/scoring/processor/expressions/operators/customOperator
 * @see http://www.imsglobal.org/question/qtiv2p1/imsqti_infov2p1.html#element10569
 *
 * @author Bertrand Chevrier <bertrand@taotesting.com>
 */

define( ['lodash'], function(_){
    'use strict';

    /**
     * Process operand and returns the number.
     * @exports taoQtiItem/scoring/processor/expressions/operators/custom/stringToNumber
     * @return float or null
     */
    return {

        //equality algos based on different tolerance modes

        constraints : {
            minOperand : 1,
            maxOperand : 1,
            cardinality : ['single'],
            baseType : ['string']
        },

        operands   : [],

        /**
         * Process the string and returns a float
         * @returns {?ProcessingValue} the number or null
         */
        process : function process() {

            var value;
            var result = {
                cardinality : 'single',
                baseType : 'float'
            };
            
            if (!this.operands || !this.operands.length) {
                return null;
            }

            //if at least one operand is null, then break and return null
            if (_.some(this.operands, _.isNull) === true || _.some(this.operands[0], _.isNull) === true) {
                return null;
            }

            value = this.preProcessor.parseVariable(this.operands[0]).value;

            //remove commas for us way to separate thousands
            if(_.isString(value)){
                value = value.replace(/,/g, '');
            }

            result.value = parseFloat(value);

            return result;
        }
    };
});
