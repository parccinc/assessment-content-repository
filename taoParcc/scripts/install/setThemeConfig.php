<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *
 *
 */

use oat\tao\model\ThemeRegistry;
use oat\tao\model\websource\DirectWebSource;
use Jig\Utils\StringUtils;

$itemThemesDataPath = FILES_PATH.'taoParcc'.DIRECTORY_SEPARATOR.'themes'.DIRECTORY_SEPARATOR;

$itemThemesDataPathFs = \tao_models_classes_FileSourceService::singleton()->addLocalSource('Parcc Theme FileSource', $itemThemesDataPath);

// Use theme's display names
$themes = array(
   /* 'PARCC Default',*/
    'Black on Cream',
    // 1.0.3 -> 1.0.4 adds
    'White on Black',
    'Light Blue on Dark Blue',
    'Grey on Green',// from cream to green => 1.0.4 => 1.0.5
    'Black on White',
    'Black on Light Magenta',
    'Black on Light Blue'
);

$websource = DirectWebSource::spawnWebsource($itemThemesDataPathFs, ROOT_URL . 'taoParcc/themes/');
ThemeRegistry::getRegistry()->setWebSource($websource->getId());

foreach($themes as $name) {
    $pathFragment = StringUtils::removeSpecChars($name);
    $id = StringUtils::camelize($pathFragment);
    ThemeRegistry::getRegistry()->registerTheme($id, $name, ThemeRegistry::WEBSOURCE . $pathFragment . DIRECTORY_SEPARATOR . 'theme.css', array('items'));
}

ThemeRegistry::getRegistry()->setDefaultTheme('items', 'blackOnWhite');
