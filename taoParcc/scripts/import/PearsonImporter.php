<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\taoParcc\scripts\import;

use common_report_Report as Report;
use core_kernel_classes_Class as ClassesClass;
use taoItems_models_classes_ItemsService as ItemsService;
use oat\taoQtiItem\model\qti\exception\ExtractException;
use oat\taoQtiItem\model\qti\exception\ParsingException;
use oat\taoQtiItem\model\qti\ImportService;
use taoQtiTest_models_classes_QtiTestService as QtiTestService;

class PearsonImporter
{
    protected $package;
    protected $packageName;
    protected $report;
    protected $errorMsg;
    protected $packageParser;

    public function __construct($package)
    {
        $this->package = $package;
        $this->packageName = pathinfo($package, PATHINFO_FILENAME);
        $this->report = new Report(Report::TYPE_INFO);
        $this->errorMsg = __("The provided archive is invalid. Make sure it is not corrupted and that it contains an 'imsmanifest.xml' file.");
    }

    public function import($package)
    {
        if ($this->hasTests($package)) {
            $this->importAsTest($package);
        } else {
            $this->importAsItem($package);
        }

        return $this->report;
    }

    protected function importAsTest($package)
    {
        try {
            $qtiTestService = QtiTestService::singleton();
            $class = new ClassesClass(constant('TAO_TEST_CLASS'));

            $this->report->add($qtiTestService->importMultipleTests($class, $package));

        } catch (ExtractException $e) {
            $this->report = Report::createFailure(__('The ZIP archive containing the IMS QTI Item cannot be extracted.'));
        } catch (ParsingException $e) {
            $this->report = Report::createFailure(__('The ZIP archive does not contain an imsmanifest.xml file or is an invalid ZIP archive.'));
        } catch (\Exception $e) {
            $this->report = Report::createFailure(__("An unexpected error occured during the import of the IMS QTI Item Package."));
        }

        return $this->report;
    }

    protected function importAsItem($package)
    {
        $rootClass = ItemsService::singleton()->getRootClass();
        $class = $rootClass->createSubClass($this->packageName);

        $importService = ImportService::singleton();

        $this->report->add($importService->importQTIPACKFile($package, $class, true, null, true, true, true));
    }

    protected function hasTests($package) {
        $zip = new \ZipArchive();

        if ($zip->open($package) !== true) {

            return false;
        }

        $manifest = new \DOMDocument('1.0', 'UTF-8');
        $manifest->loadXML($zip->getFromName('imsmanifest.xml'));
        $zip->close();

        $xpath = new \DOMXPath($manifest);
        $xpath->registerNamespace('qti', 'http://www.imsglobal.org/xsd/imscp_v1p1');
        $tests = $xpath->query('//qti:resources//qti:resource[@type="imsqti_test_xmlv2p1"]');

        return ($tests->length > 0) ? true : false;
    }

}
