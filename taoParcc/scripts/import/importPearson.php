<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *
 *
 */

namespace oat\taoParcc\scripts\import;

use common_ext_ExtensionsManager;
use tao_helpers_report_Rendering as ReportRendering;

// start system
require_once __DIR__ . '/../../../tao/includes/raw_start.php';

// load extensions
common_ext_ExtensionsManager::singleton()->getExtensionById('taoQtiItem');
common_ext_ExtensionsManager::singleton()->getExtensionById('taoQtiTest');

// fetch parameters
$script = array_shift($argv);

if (count($argv) < 1) {
    echo 'Usage: ' . $script . ' FILE' . PHP_EOL;
    die(1);
}

$package = array_shift($argv);

if (!file_exists($package)) {
    echo "Pearson package not found at $package", PHP_EOL;
    die(1);
}

$importer = new PearsonImporter($package);

// at start lets convert malformed package to valid package
$converter = new PearsonConverter($package);
$convertedPackage = $converter->convert();

$report = $importer->import($convertedPackage);

echo ReportRendering::renderToCommandline($report);
