<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\taoParcc\scripts\import;

/**
 * Utility methods related to Person Conversion/Import.
 * 
 * @author Jérôme Bogaerts <jerome@taotesting.com>
 *
 */
class PearsonUtils
{
    /**
     * Change the node name of a given $node with $newName.
     * 
     * @param \DOMElement $node
     * @param string $newName
     * @return \DOMElement
     */
    static public function changeNodeName(\DOMElement $node, $newName)
    {
        $childnodes = array();
        foreach ($node->childNodes as $child) {
            $childnodes[] = $child;
        }
        
        $newnode = $node->ownerDocument->createElement($newName);
        foreach ($childnodes as $child) {
            $child2 = $node->ownerDocument->importNode($child, true);
            $newnode->appendChild($child2);
        }
        
        foreach ($node->attributes as $attrName => $attrNode) {
            $attrValue = $attrNode->nodeValue;
            $newnode->setAttribute($attrName, $attrValue);
        }
        
        $node->parentNode->replaceChild($newnode, $node);
        return $newnode;
    }
}