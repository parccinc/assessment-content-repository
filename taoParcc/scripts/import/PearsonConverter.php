<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2015 (original work) Open Assessment Technologies SA (under the project TAO-PRODUCT);
 *
 */

namespace oat\taoParcc\scripts\import;

use oat\taoQtiItem\model\qti\exception\QtiModelException;
use oat\taoQtiItem\model\qti\PackageParser;
use tao_helpers_File;

/**
 * Custom Package Pearson Converter
 *
 * @author Ivan Klimchuk <klimchuk@1pt.com>
 */
class PearsonConverter
{
    private $tmpPathToPackage;

    public function __construct($package)
    {
        $parser = new PackageParser($package);
        $this->tmpPathToPackage = $parser->extract();

        if (!file_exists($this->tmpPathToPackage . 'imsmanifest.xml')) {
            // try find data in inner folder (if archive was created with inner folder)
            foreach (glob($this->tmpPathToPackage . '*', GLOB_ONLYDIR) as $dir) {
                if (file_exists($dir . '/imsmanifest.xml')) {
                    $this->tmpPathToPackage = $dir . '/';
                }
            }
        }
    }

    public function convert()
    {
        $manifestFile = $this->tmpPathToPackage . 'imsmanifest.xml';

        $itemsDirectory = $this->tmpPathToPackage . 'items';
        $testsDirectory = $this->tmpPathToPackage . 'tests';

        mkdir($itemsDirectory);
        mkdir($testsDirectory);

        libxml_use_internal_errors(true);

        $manifest = new \DOMDocument('1.0', 'UTF-8');
        $manifest->formatOutput = true;
        $manifest->preserveWhiteSpace = false;
        $manifest->validateOnParse = false;

        if (!$manifest->loadXML(file_get_contents($manifestFile))) {
            throw new QtiModelException('Wrong QTI manifest output format');
        }

        $manifest->documentElement->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'http://www.imsglobal.org/xsd/imscp_v1p1');

        // fix typo in attributes of manifest element (root)
        if ($manifest->documentElement->hasAttribute('idenfitier')) {
            $identifier = $manifest->documentElement->getAttribute('idenfitier');
            $manifest->documentElement->removeAttribute('idenfitier');
            $manifest->documentElement->setAttribute('identifier', $identifier);
        }

        $xpath = new \DOMXPath($manifest);
        $xpath->registerNamespace('qti', 'http://www.imsglobal.org/xsd/imscp_v1p1');

        $dependencies = array();
        foreach ($xpath->query('//qti:resource') as $resource) {

            $identifier = $resource->getAttribute('identifier');

            if (in_array($resource->getAttribute('type'), array('imsqti_apipitem_xmlv2p2', 'imsqti_apipitem_xmlv2p1'))) {

                $qtiItemsDirectory = $itemsDirectory . '/' . $identifier;

                mkdir($qtiItemsDirectory);
                rename($this->tmpPathToPackage . $resource->getAttribute('href'), $qtiItemsDirectory . '/qti.xml');

                $resource->setAttribute('href',  "items/$identifier/qti.xml");
                $resource->setAttribute('type', 'imsqti_item_xmlv2p1');

                $this->processItemFile($qtiItemsDirectory . '/qti.xml');

                $itemDependencies = $resource->getElementsByTagName('dependency');
                if ($itemDependencies->length) {
                    foreach ($itemDependencies as $dependency) {
                        $ref = $dependency->getAttribute('identifierref');
                        foreach (glob($this->tmpPathToPackage . 'assets/' . $ref . '.*') as $file) {
                            $fileNode = $manifest->createElement('file');
                            $fileNode->setAttribute('href', 'assets/' . basename($file));
                            $resource->appendChild($fileNode);
                        }
                        $resource->removeChild($dependency);
                    }
                }

                // move assets files
                foreach ($resource->getElementsByTagName('file') as $fileNode) {
                    $href = $fileNode->getAttribute('href');

                    $filename = basename($href);
                    if (pathinfo($href, PATHINFO_EXTENSION) == 'xml'
                        && pathinfo($href, PATHINFO_FILENAME) == $identifier
                    ) {
                        $filename = 'qti.xml';
                    }

                    $itemFilePath = "items/$identifier/$filename";
                    $fileNode->setAttribute('href', $itemFilePath);

                    if (file_exists($this->tmpPathToPackage . $href)) {
                        tao_helpers_File::copy($this->tmpPathToPackage . $href, $this->tmpPathToPackage . $itemFilePath);
                    }

                    // stimulus converting
                    if (pathinfo($this->tmpPathToPackage . $itemFilePath, PATHINFO_EXTENSION) == 'xml'
                        && pathinfo($this->tmpPathToPackage . $itemFilePath, PATHINFO_FILENAME) != 'qti'
                    ) {
                        $this->processStimulusFile($this->tmpPathToPackage . $itemFilePath);
                    }
                }

                $dependency = $manifest->createElement('dependency');
                $dependency->setAttribute('identifierref', $identifier);
                $dependencies[] = $dependency;
            }

            if ($identifier == 'Assessment') {

                $testFile = $resource->getAttribute('href');
                $testIdentifier = $this->processTestFile($testFile);
                $resource->setAttribute('identifier', $testIdentifier);

                $qtiTestsDirectory = $testsDirectory . '/' . $testIdentifier;
                mkdir($qtiTestsDirectory);
                rename($this->tmpPathToPackage . $resource->getAttribute('href'), $qtiTestsDirectory . '/test.xml');

                $resource->setAttribute('href', "tests/$testIdentifier/test.xml");
                $resource->setAttribute('type', 'imsqti_test_xmlv2p1');

                foreach ($resource->getElementsByTagName('file') as $file) {
                    $file->setAttribute('href', "tests/$testIdentifier/test.xml");
                }

                foreach ($dependencies as $dependency) {
                    $resource->appendChild($dependency);
                }
            }
        }

        file_put_contents($manifestFile, $manifest->saveXML());

        // remove old assets folder
        array_map(function ($file) {
            unlink($file);
        }, glob($this->tmpPathToPackage . 'assets/*'));
        if (file_exists($this->tmpPathToPackage . 'assets')) {
            rmdir($this->tmpPathToPackage . 'assets');
        }

        $package = $this->tmpPathToPackage . 'package.zip';
        $this->zip($package);

        return $package;
    }

    protected function processTestFile($testFile)
    {
        $test = new \DOMDocument('1.0', 'UTF-8');
        $test->formatOutput = true;
        $test->preserveWhiteSpace = false;
        $test->validateOnParse = false;

        if (!$test->loadXML(file_get_contents($this->tmpPathToPackage . $testFile))) {
            throw new QtiModelException('Wrong TEST item output format');
        }

        $identifier = $test->documentElement->getAttribute('identifier');

        // remove not allowed attributes
        $attributes = array(
            'exitEnabled',
            'formId',
            'formName',
            'grade',
            'itemStyles',
            'saveResponses',
            'secure',
            'securityLevel',
            'subject',
            't3Account',
            't3Administration',
            't3Assessment',
            't3PTM',
            'timeLimit',
        );
        foreach ($attributes as $attribute) {
            $test->documentElement->removeAttribute($attribute);
        }
        foreach ($test->documentElement->getElementsByTagName('toolsets') as $node) {
            $test->documentElement->removeChild($node);
        }
        foreach ($test->documentElement->getElementsByTagName('accomtoolsets') as $node) {
            $test->documentElement->removeChild($node);
        }

        $xpath = new \DOMXPath($test);
        $xpath->registerNamespace('qti', 'http://www.imsglobal.org/xsd/imsqti_v2p1');

        foreach ($xpath->query('//qti:assessmentItemRef') as $ref) {
            $href = '../../items/' . $ref->getAttribute('identifier') . '/qti.xml';
            $ref->setAttribute('href', $href);

            $ref->removeAttribute('toolset');
            $ref->removeAttribute('webcontent');
            $ref->removeAttribute('webcontenttitle');
        }

        foreach($xpath->query('//qti:testPart') as $part) {
            $part->setAttribute('identifier', str_replace(' ', '-', $part->getAttribute('identifier')));
        }

        foreach($xpath->query('//qti:assessmentSection') as $section) {
            $section->removeAttribute('sealCode');
        }

        file_put_contents($this->tmpPathToPackage . $testFile, $test->saveXML());

        return $identifier;
    }

    protected function processItemFile($itemFile)
    {
        $item = new \DOMDocument('1.0', 'UTF-8');
        $item->formatOutput = true;
        $item->preserveWhiteSpace = false;
        $item->validateOnParse = false;

        if (!$item->loadXML(file_get_contents($itemFile))) {
            throw new QtiModelException('Wrong QTI Item output format');
        }

        // fix image path
        $xpath = new \DOMXPath($item);
        $xpath->registerNamespace('qti', 'http://www.imsglobal.org/xsd/apip/apipv1p0/qtiitem/imsqti_v2p1');

        foreach ($xpath->query('//qti:div[@class="abbi-image"]//qti:img') as $image) {
            $src = $image->getAttribute('src');
            $src = explode('/', $src);
            $image->setAttribute('src', '/' . array_pop($src));
        }

        // fix response labels
        $regexp = '/^[A-Z]{1}_/';
        $correctResponseValue = $xpath->query('//qti:correctResponse//qti:value')->item(0);
        $correctResponseValue->nodeValue = preg_replace($regexp, '', $correctResponseValue->nodeValue);

        $variants = $xpath->query('//qti:itemBody//qti:choiceInteraction//qti:simpleChoice');
        foreach ($variants as $variant) {
            $variant->setAttribute('identifier', preg_replace($regexp, '', $variant->getAttribute('identifier')));
        }
        
        // fix template declarations
        $templateDeclarations = $xpath->query('//qti:templateDeclaration');
        foreach ($templateDeclarations as $templateDeclaration) {
            PearsonUtils::changeNodeName($templateDeclaration, 'outcomeDeclaration');
        }

        file_put_contents($itemFile, $item->saveXML());
    }

    protected function processStimulusFile($file)
    {
        $xml = new \DOMDocument('1.01', 'UTF-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        $xml->validateOnParse = false;

        if (!$xml->loadXML(file_get_contents($file))) {
            throw new QtiModelException('Wrong Stimulus output format');
        }

        $xml->documentElement->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'http://www.imsglobal.org/xsd/imsqti_v2p1');

        file_put_contents($file, $xml->saveXML());
    }

    protected function zip($name)
    {
        $zip = new \ZipArchive();
        $zip->open($name, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(realpath($this->tmpPathToPackage)),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($this->tmpPathToPackage));

                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();
    }
}
