<?php
/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2014 (original work) Open Assessment Technologies SA;
 *
 *
 */

namespace oat\taoParcc\scripts\update;

use common_ext_ExtensionsManager;
use common_ext_ExtensionUpdater;
use oat\oatbox\event\EventManager;
use oat\tao\model\accessControl\func\AccessRule;
use oat\tao\model\accessControl\func\AclProxy;
use oat\tao\model\ClientLibConfigRegistry;
use oat\tao\model\ThemeRegistry;
use oat\tao\model\websource\DirectWebSource;

/**
 * 
 * @author Joel Bout <joel@taotesting.com>
 */
class Updater extends common_ext_ExtensionUpdater {

    /**
     * @param string $initialVersion
     * @return string string
     */
    public function update($initialVersion) {
        
        $currentVersion = $initialVersion;

        //migrate from 1.0.0 to 1.0.1
        if ($currentVersion == '1.0.0') {
            $currentVersion = '1.0.1';
        }
        
        if ($currentVersion == '1.0.1') {
            //turning the multi-column option to true
            $ext = \common_ext_ExtensionsManager::singleton()->getExtensionById('taoQtiItem');
            $ext->setConfig('qtiCreator', array('multi-column' => true));
            $currentVersion = '1.0.2';
        }

        if ($currentVersion == '1.0.2') {

            $itemThemesDataPath = FILES_PATH.'taoParcc'.DIRECTORY_SEPARATOR.'themes'.DIRECTORY_SEPARATOR;
            $itemThemesDataPathFs = \tao_models_classes_FileSourceService::singleton()->addLocalSource('Parcc Theme FileSource', $itemThemesDataPath);


            $websource = DirectWebSource::spawnWebsource($itemThemesDataPathFs, ROOT_URL . 'taoParcc/themes/');
            ThemeRegistry::getRegistry()->setWebSource($websource->getId());

            ThemeRegistry::getRegistry()->createTarget('items', 'taoQtiItem/views/css/qti-runner.css');
            ThemeRegistry::getRegistry()->registerTheme('tao', 'TAO', 'taoQtiItem/views/css/themes/default.css', array('items'));
            ThemeRegistry::getRegistry()->setDefaultTheme('items', 'tao');
            
            ThemeRegistry::getRegistry()->registerTheme('parccDefault', 'PARCC Default', ThemeRegistry::WEBSOURCE . 'parcc-default' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('blackOnCream', 'Black on cream', ThemeRegistry::WEBSOURCE. 'black-on-cream'. DIRECTORY_SEPARATOR . 'theme.css', array('items'));


            $currentVersion = '1.0.3';
        }

        if ($currentVersion == '1.0.3') {

            ThemeRegistry::getRegistry()->registerTheme('whiteOnBlack', 'White on Black', ThemeRegistry::WEBSOURCE . 'white-on-black' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('lightBlueOnDarkBlue', 'Light Blue on Dark Blue', ThemeRegistry::WEBSOURCE . 'light-blue-on-dark-blue' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('greyOnGreen', 'Grey on Cream', ThemeRegistry::WEBSOURCE . 'grey-on-green' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('blackOnWhite', 'Black on White', ThemeRegistry::WEBSOURCE . 'black-on-white' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('blackOnLightMagenta', 'Black on Light Magenta', ThemeRegistry::WEBSOURCE . 'black-on-light-magenta' . DIRECTORY_SEPARATOR .'theme.css', array('items'));
            ThemeRegistry::getRegistry()->registerTheme('blackOnLightBlue', 'Black on Light Blue', ThemeRegistry::WEBSOURCE . 'black-on-light-blue' . DIRECTORY_SEPARATOR .'theme.css', array('items'));

            $currentVersion = '1.0.4';
        }



        if ($currentVersion == '1.0.4') {

            ThemeRegistry::getRegistry()->unregisterTheme('greyOnGreen');
            ThemeRegistry::getRegistry()->registerTheme('greyOnGreen', 'Grey on Green', ThemeRegistry::WEBSOURCE . 'grey-on-green' . DIRECTORY_SEPARATOR .'theme.css', array('items'));

            $currentVersion = '1.0.5';
        }

        if ($currentVersion == '1.0.5') {

            ThemeRegistry::getRegistry()->unregisterTheme('parccDefault');
            ThemeRegistry::getRegistry()->setDefaultTheme('items', 'blackOnWhite');
            
            $currentVersion = '1.0.6';
        }

        if ($currentVersion === '1.0.6') {
            $eventManager = $this->getServiceManager()->get(EventManager::CONFIG_ID);
            $eventManager->attach(
                'oat\\generis\\model\\data\\event\\ResourceCreated',
                array(
                    'oat\\generis\\model\\data\\permission\\PermissionManager',
                    'catchEvent'
                )
            );
            $this->getServiceManager()->register(EventManager::CONFIG_ID, $eventManager);
            $currentVersion = '1.0.7';
        }

        if ($currentVersion === '1.0.7') {

            ClientLibConfigRegistry::getRegistry()->register(
                'util/locale', ['decimalSeparator' => '.', 'thousandsSeparator' => ',']
            );

            $currentVersion = '1.0.8';
        }
        
        if($currentVersion === '1.0.8'){

            ClientLibConfigRegistry::getRegistry()->register('taoQtiItem/runtime/qtiBootstrap', array(
                'userModules' => array('taoParcc/userScripts/passagify/passagify')
            ));

            $currentVersion = '1.1.0';
        }
		
		if($currentVersion === '1.1.0'){
            $currentVersion = '1.2.0';
        }

        if($currentVersion === '1.2.0'){
            $libConfig = ClientLibConfigRegistry::getRegistry()->get('taoQtiItem/runtime/qtiBootstrap');
            $libConfig['userModules'][] = 'taoParcc/userScripts/pearsonTableGrid/pearsonTableGrid';
            ClientLibConfigRegistry::getRegistry()->set('taoQtiItem/runtime/qtiBootstrap', $libConfig);
            $currentVersion = '1.3.0';
        }

        $this->skip('1.3.0', '1.4.0');
    }

}
